<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use pm\Firemon112\Models\Station as StationModel;
use Backend\Widgets\Lists as BackendList;

class Station extends Controller
{
    protected $adminUsersListWidget;

    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('pm.Firemon112', 'main-menu-item2', 'side-menu-item');

        $this->initAdminUsersListWidget();
    }

    protected function initAdminUsersListWidget()
    {
        // Create and bind the List Widget to the controller
        $this->adminUsersListWidget = $this->makeAdminUsersListWidget();

        // Explicitly register the widget with addDynamicMethod
        $this->addDynamicMethod(
            $this->adminUsersListWidget->alias,
            function () {
                return $this->adminUsersListWidget;
            }
        );
    }

    protected function makeAdminUsersListWidget()
    {
        // Configure the list widget instance
        $config = $this->makeConfig([
            'model'   => new \RainLab\User\Models\User, // A dummy User model
            'alias'   => 'adminUsersListWidget', // Ensure alias is unique
            'columns' => $this->getAdminUsersListColumns(), // Define columns
            'noRecordsMessage' => 'No Admin Users Found',
        ]);

        // Create the list widget
        $widget = $this->makeWidget(BackendList::class, $config);

        // Dynamically inject the data (admin users collection)
        $widget->bindEvent('list.extendRecords', function () {
            $station = $this->getStationModel(); // Get the current Station
            return $station->admin_users;       // Use the station's admin_users collection
        });

        return $widget;
    }

    protected function getAdminUsersListColumns()
    {
        return [
            'id' => [
                'label'    => 'ID',
                'type'     => 'number',
                'sortable' => false, // not yet working due to widget registration bug
            ],
            'name' => [
                'label'    => 'Name',
                'type'     => 'text',
                'sortable' => false, // not yet working due to widget registration bug
            ],
            'email' => [
                'label' => 'Email',
                'type'  => 'text',
            ],
            'station[name]' => [
                'label' => 'Heimatwehr',
                'type'  => 'text',
            ],
            //TODO: Add Link to profile
        ];
    }

    protected function getStationModel()
    {
        $stationId = $this->params[0] ?? 0;
        return StationModel::find($stationId);
    }
}
