<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use \Flash;
use pm\Firemon112\Models\Alert;

class Alerts extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('pm.Firemon112', 'main-menu-item', 'side-menu-item2');
    }

    function onQueryWeather($recordId)
    {
        try {
            $alert = Alert::find($recordId);
            if ($alert !== null) {
                $alert->dispatchQueryWeather();
                Flash::success('Weatherdata Query scheduled - refresh to see results');
            }
        } catch (\Exception $e) {
            Flash::error('Exception for ID ' . $recordId.': '.$e->getMessage());
        }
    }
}
