<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use pm\Firemon112\Models\Pushtoken as PushtokenModel;
use pm\Firemon112\Classes\Apn\ApnPushClient;
use pm\Firemon112\Classes\Expo\ExpoMessage;
use pm\Firemon112\Classes\ExpoPush;
use pm\Firemon112\Classes\Fcm\FcmPushClient;
use pm\Firemon112\Models\Settings;

class Pushtoken extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
    }

    public function onSendTestNotification()
    {
        $notificationJson = post('notification_json');
        $token = post('token');
        $token_type = post('token_type');

        // Validate JSON
        $payload = json_decode($notificationJson, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception("Invalid JSON provided");
        }

        // Call your business logic to send the notification
        $res = $this->sendPushNotification($token, $token_type, $payload);

        Flash::success($res);
    }

    protected function sendPushNotification($token, $token_type, $payload)
    {
        if ($token_type == PushtokenModel::$TOKEN_TYPE_EXPO) {
            $resp = $this->sendExpoPushDebug($token, $payload);
            return "Successfully sent Expo Notification\n".print_r($resp, true);
        } else if ($token_type == PushtokenModel::$TOKEN_TYPE_APN) {
            $resp = $this->sendApnPushDebug($token, $payload);
            return "Successfully sent APN Notification\n".print_r($resp, true);
        } else if ($token_type == PushtokenModel::$TOKEN_TYPE_FCM) {
            $resp = $this->sendFcmPushDebug($token, $payload);
            return "Successfully sent FCM Notification\n".print_r($resp, true);
        } else {
            throw new \Exception( "Not supported Push-Type " .$token_type . " - exit");
        }
    }

    private function sendExpoPushDebug($token, $payload) {
        $expo_access_token = Settings::get('expo_access_token');

        $expo = new ExpoPush();
        $expo->setAccessToken($expo_access_token);

        $message = new ExpoMessage([]);
        $message->setData($payload['data'])
            ->setTitle($payload['title'])
            ->setBody($payload['body'])
            ->setChannelId($payload['channel_id'])
            ->setCategoryId($payload['category_id'])
            ->setData($payload['data'])
            ->setPriority($payload['data'])
            ->playSound();

        return $expo->send($message)->to($token)->push()->getData();
    }

    private function sendApnPushDebug($token, $payload) {
        $apn = new ApnPushClient();
        $responses = $apn->sendNotifications(
            [$token],
            $payload['title'],
            $payload['body'],
            $payload['topic'],
            $payload['data'],
            $payload['topic'] == PushtokenModel::$TOKEN_TOPIC_ALERT
        );
        return $responses;
    }

    private function sendFcmPushDebug($token, $payload) {
        // we are not very strict and pass json directly - be aware!
        $notifier = new FcmPushClient();
        $payload['token'] = $token;
        return $notifier->sendNotification($payload);
    }

}
