<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Leitstellen extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('pm.Firemon112', 'main-menu-item2', 'side-menu-item5');
    }
}
