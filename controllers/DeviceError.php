<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class DeviceError extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('pm.Firemon112', 'main-menu-item', 'side-menu-item4');
    }
}
