<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use \Flash;
use \Artisan;
use pm\Firemon112\Models\InterestedStation;
use pm\Firemon112\Models\Helper;
use Mail;

class InterestedStations extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('pm.Firemon112', 'main-menu-item', 'side-menu-item7');
    }

    function onEnableTestaccount($recordId)
    {
        try {
            $ip = InterestedStation::find($recordId);
            if ($ip === null) {
                throw new \Exception("Could not find InterestedPerson by ID ". $recordId);
            }
            $name = Helper::SplitName($ip->contact_person);
            $exitCode = Artisan::call('firemon:fastadd:station', [
                "--sToken" => $ip->token,
                "--sName" => $ip->station_name,
                "--sEmail" => $ip->token.'@firemon112.de',
                "--sLong" => $ip->long,
                "--sLat" => $ip->lat,
                "--sLeitstelleId" => $ip->leitstelle_id,
                "--sStationType" => $ip->station_type,
                "--mqUser" => $ip->token,
                "--mqPass" => str_random(12),
                "--uCreate" => true,
                "--uEmail" => $ip->email,
                "--uSendmail" => true,
                "--uName" => $name[0],
                "--uSurname" => $name[1]
            ]);
            if ($exitCode != 0) {
                throw new \Exception(Artisan::output());
            }
            $ip->status = InterestedStation::$STATUS_TESTING;
            $ip->save();

            // TESTING-Mail
            // Send Mail with additional info
            Mail::sendTo($ip->email, 'pm.firemon112::mail.testaccount-created', ['obj' => $ip]);

            Flash::success('Added Testaccount for ID ' . $recordId);
        } catch (\Exception $e) {
            Flash::error('Exception for ID ' . $recordId.': '.$e->getMessage());
        }
    }
}
