<?php namespace pm\Firemon112\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Training extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('pm.Firemon112', 'main-menu-item3','side-menu-item3');
    }
}
