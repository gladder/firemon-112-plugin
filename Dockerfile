FROM aspendigital/octobercms:php7.3-apache

RUN apt-get update -y
RUN apt-get install -y libgmp-dev re2c libmhash-dev libmcrypt-dev file libicu-dev
RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/
RUN docker-php-ext-install sockets
RUN pecl install mailparse
RUN docker-php-ext-enable mailparse
RUN docker-php-ext-configure gmp
RUN docker-php-ext-install gmp
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl
ENV PHP_MEMORY_LIMIT=1024M
