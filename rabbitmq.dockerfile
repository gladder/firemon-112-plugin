# Use the official RabbitMQ image with the management plugin
FROM rabbitmq:management

# Enable the MQTT and Web MQTT plugins
RUN rabbitmq-plugins enable --offline rabbitmq_mqtt rabbitmq_web_mqtt
