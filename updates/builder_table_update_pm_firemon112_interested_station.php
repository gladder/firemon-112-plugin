<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112InterestedStation extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_interested_station', function($table)
        {
            $table->string('token', 128)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_interested_station', function($table)
        {
            $table->dropColumn('token');
        });
    }
}