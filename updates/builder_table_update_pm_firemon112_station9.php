<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station9 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->string('station_type', 16)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('station_type');
        });
    }
}