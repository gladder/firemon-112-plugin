<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Device4 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->boolean('watchdog')->default(0);
            $table->boolean('hdmi_cec')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->dropColumn('watchdog');
            $table->dropColumn('hdmi_cec');
        });
    }
}