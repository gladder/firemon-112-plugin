<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alert6 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->text('depesche')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->text('depesche')->nullable(false)->change();
        });
    }
}