<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Device11 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->integer('auto_reboot')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->dropColumn('auto_reboot');
        });
    }
}