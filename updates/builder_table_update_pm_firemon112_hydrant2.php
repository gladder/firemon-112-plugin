<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Hydrant2 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_hydrant', function($table)
        {
            $table->string('waterpipe', 16)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_hydrant', function($table)
        {
            $table->dropColumn('waterpipe');
        });
    }
}