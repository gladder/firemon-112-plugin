<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alertgroup4 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->boolean('trigger_alert_email')->default(1);
            $table->boolean('trigger_alert_sms')->default(1);
            $table->boolean('trigger_alert_call')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->dropColumn('trigger_alert_email');
            $table->dropColumn('trigger_alert_sms');
            $table->dropColumn('trigger_alert_call');
        });
    }
}