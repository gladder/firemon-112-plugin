<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Training extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_training', function($table)
        {
            $table->string('identifier', 32)->default('');
            $table->string('hash', 40)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_training', function($table)
        {
            $table->dropColumn('identifier');
            $table->dropColumn('hash');
        });
    }
}