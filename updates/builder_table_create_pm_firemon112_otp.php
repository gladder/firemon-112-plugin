<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Otp extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_otp', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('reason', 32);
            $table->string('otp', 64);
            $table->string('identifier', 256);
            $table->dateTime('used_at')->nullable();
            $table->dateTime('expired_at');
            $table->index(['reason','otp','identifier'], 'IDX_ident');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_otp');
    }
}