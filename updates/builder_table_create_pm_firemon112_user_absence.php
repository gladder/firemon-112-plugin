<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112UserAbsence extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_user_absence', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id')->unsigned();
            $table->dateTime('valid_from');
            $table->dateTime('valid_to');
            $table->string('absence_type', 32);
            $table->boolean('auto_cancel_events');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_user_absence');
    }
}
