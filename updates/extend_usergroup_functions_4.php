<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUsergroupFunctions4 extends Migration
{
    private $newCols = ['is_role_alert_readonly'];
    public function up()
    {
        if (Schema::hasColumns('user_groups', $this->newCols)) {
            return;
        }

        Schema::table('user_groups', function($table)
        {
            $table->boolean('is_role_alert_readonly')->default(0);
        });
    }

    public function down()
    {
        if (Schema::hasTable('user_groups') && Schema::hasColumns('user_groups', $this->newCols)) {
            Schema::table('user_groups', function ($table) {
                $table->dropColumn($this->newCols);
            });
        }
    }
}