<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Invite extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_invite', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id_sender')->unsigned()->default(0);
            $table->integer('user_id_recipient')->unsigned()->default(0);
            $table->string('type', 16);
            $table->string('status', 16);
            $table->text('payload');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_invite');
    }
}