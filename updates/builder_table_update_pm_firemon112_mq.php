<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Mq extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_mq', function($table)
        {
            $table->string('name', 128)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_mq', function($table)
        {
            $table->dropColumn('name');
        });
    }
}