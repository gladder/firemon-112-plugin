<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Device9 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->string('running_version', 32)->change();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->string('running_version', 16)->change();
        });
    }
}