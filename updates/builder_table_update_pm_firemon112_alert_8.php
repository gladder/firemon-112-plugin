<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alert8 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->string('identifier', 32)->nullable();
            $table->index('identifier', 'IDX_identifier');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dropIndex('IDX_identifier');
            $table->dropColumn('identifier');
        });
    }
}