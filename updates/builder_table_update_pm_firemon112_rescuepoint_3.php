<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Rescuepoint3 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->text('note');
            $table->boolean('is_contactpoint');
            $table->boolean('is_emergencyshelter');
        });
    }

    public function down()
    {
        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->dropColumn('note');
            $table->dropColumn('is_contactpoint');
            $table->dropColumn('is_emergencyshelter');
        });
    }
}