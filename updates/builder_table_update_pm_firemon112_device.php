<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Device extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->integer('mq_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->dropColumn('mq_id');
        });
    }
}