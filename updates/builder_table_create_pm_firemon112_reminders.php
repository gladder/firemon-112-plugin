<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Reminders extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_reminders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->integer('remindable_id');
            $table->string('remindable_type', 255);
            $table->string('notification_type', 16);
            $table->dateTime('reminder_trigger_at');
            $table->dateTime('reminder_handled_at')->nullable();
            $table->index(['remindable_id','remindable_type'], 'IDX_remindable_id_type');
            $table->index(['reminder_trigger_at','reminder_handled_at'], 'IDX_reminder_dates');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_reminders');
    }
}