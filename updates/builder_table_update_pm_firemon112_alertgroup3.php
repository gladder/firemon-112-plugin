<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alertgroup3 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->string('message_type', 32)->default('');
            $table->integer('prio')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->dropColumn('message_type');
            $table->dropColumn('prio');
        });
    }
}