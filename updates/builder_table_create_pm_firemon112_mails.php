<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Mails extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_mails', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->text('mail');
            $table->string('token', 64);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_mails');
    }
}
