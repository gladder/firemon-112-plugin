<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUsergroupFunctions2 extends Migration
{
    private $newCols = ['station_id','is_mission_operator','short_name','is_role_station','is_role_presence','is_role_alert','is_role_events'];
    public function up()
    {
        if (Schema::hasColumns('user_groups', $this->newCols)) {
            return;
        }

        Schema::table('user_groups', function($table)
        {
            $table->integer('station_id')->unsigned()->default(0);
            $table->boolean('is_mission_operator')->default(0);
            $table->boolean('is_role_station')->default(0);
            $table->boolean('is_role_presence')->default(0);
            $table->boolean('is_role_alert')->default(0);
            $table->boolean('is_role_events')->default(0);
            $table->string('short_name', 16)->default('');
        });
    }

    public function down()
    {
        if (Schema::hasTable('user_groups') && Schema::hasColumns('user_groups', $this->newCols)) {
            Schema::table('user_groups', function ($table) {
                $table->dropColumn($this->newCols);
            });
        }
    }
}