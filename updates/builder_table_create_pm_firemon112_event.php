<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Event extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_event', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('station_id')->unsigned();
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->string('category', 64);
            $table->string('label', 255);
            $table->text('description')->nullable();
            $table->boolean('subscription_required')->default(0);
            $table->integer('user_group_id')->unsigned()->default(0);
            $table->boolean('email_reminder')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_event');
    }
}