<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Assemblyarea extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_assemblyarea', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('label', 32);
            $table->text('coords');
            $table->decimal('long', 9, 6);
            $table->decimal('lat', 9, 6);
            $table->integer('station_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_assemblyarea');
    }
}