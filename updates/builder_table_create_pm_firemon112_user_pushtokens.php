<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112UserPushtokens extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_user_pushtoken', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('token', 255)->default('');
            $table->string('token_type', 32)->default('');
            $table->string('device_name', 255)->default('');
            $table->integer('user_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_user_pushtoken');
    }
}