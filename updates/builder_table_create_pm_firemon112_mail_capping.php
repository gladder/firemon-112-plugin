<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112MailCapping extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_mail_capping', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('email_address', 256);
            $table->string('email_type', 32);
            $table->string('email_reason', 32);
            $table->dateTime('last_sendout');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_mail_capping');
    }
}