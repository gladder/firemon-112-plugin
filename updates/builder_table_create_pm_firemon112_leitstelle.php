<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Leitstelle extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_leitstelle', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 64);
            $table->string('email', 256);
            $table->string('phone', 32);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_leitstelle');
    }
}
