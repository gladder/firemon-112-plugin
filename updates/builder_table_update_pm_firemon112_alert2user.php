<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alert2user extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert2user', function($table)
        {
            $table->integer('alertgroup_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alert2user', function($table)
        {
            $table->dropColumn('alertgroup_id');
        });
    }
}