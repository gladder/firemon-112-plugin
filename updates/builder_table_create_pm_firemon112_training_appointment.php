<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112TrainingAppointment extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_training_appointment', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('training_id')->unsigned();
            $table->dateTime('from_datetime');
            $table->dateTime('to_datetime');
            $table->string('note', 256);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_training_appointment');
    }
}