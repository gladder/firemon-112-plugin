<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Deviceerror extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_deviceerror', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('device_id')->unsigned();
            $table->text('msg')->nullable();
            $table->string('url', 256)->nullable();
            $table->string('line_no', 32)->nullable();
            $table->string('column_no', 32)->nullable();
            $table->text('error')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_deviceerror');
    }
}