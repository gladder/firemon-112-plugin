<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112CustomSettings extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_custom_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('ownerable_id')->unsigned()->default(0);
            $table->string('ownerable_type', 255)->default('');
            $table->string('settings_key', 64);
            $table->text('settings_payload');
            $table->index(['ownerable_id','ownerable_type'], 'IDX_ownerable_id_type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_custom_settings');
    }
}