<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Presence extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_presence', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->dateTime('visible_from');
            $table->dateTime('visible_to');
            $table->integer('station_id')->unsigned()->default(0);
            $table->text('payload');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_presence');
    }
}