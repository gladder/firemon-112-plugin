<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlertAddDatetimeIndex extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->index(['schedule_trigger_at', 'schedule_handled_at'], 'IDX_scheduler');
        });
    }

    public function down()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dropIndex('IDX_scheduler');
        });
    }
}