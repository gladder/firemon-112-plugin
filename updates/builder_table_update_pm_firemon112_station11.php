<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station11 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->string('sms77io_key', 64)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('sms77io_key');
        });
    }
}