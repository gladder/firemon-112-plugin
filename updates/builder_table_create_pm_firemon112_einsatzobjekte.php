<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Einsatzobjekte extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_einsatzobjekte', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('einsatzobjekt', 128)->nullable();
            $table->string('strasse', 128);
            $table->string('ort', 128);
            $table->string('ortsteil', 128)->nullable();
            $table->decimal('lat', 9, 6);
            $table->decimal('long', 9, 6);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_einsatzobjekte');
    }
}