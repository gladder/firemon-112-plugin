<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112GpioAlertTrigger extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_gpio_alert_trigger', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('station_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('label', 64);
            $table->smallInteger('gpio_number')->unsigned();
            $table->string('gpio_monitoring_mode', 32);
            $table->integer('alert_stichwort_id')->unsigned();
            $table->string('alert_information', 255);
            $table->string('alert_einsatzobjekt', 255);
            $table->string('alert_strasse', 255);
            $table->string('alert_ort', 255);
            $table->decimal('alert_long', 9, 6);
            $table->decimal('alert_lat', 9, 6);
            $table->text('alert_mak');
            $table->boolean('enabled');
            $table->timestamp('last_trigger_performed')->nullable();
            $table->integer('limit_trigger_minutes')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_gpio_alert_trigger');
    }
}