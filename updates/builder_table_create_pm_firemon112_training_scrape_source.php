<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112TrainingScrapeSource extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_training_scrape_source', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('training_source_id')->unsigned();
            $table->string('label', 64);
            $table->string('url', 512);
            $table->boolean('active')->default(0);
            $table->string('scrape_algo', 32);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_training_scrape_source');
    }
}