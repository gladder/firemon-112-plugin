<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alertgroup6 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->string('message_custom', 255)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->dropColumn('message_custom');
        });
    }
}