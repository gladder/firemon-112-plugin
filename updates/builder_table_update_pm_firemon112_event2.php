<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Event2 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_event', function($table)
        {
            $table->string('location', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_event', function($table)
        {
            $table->dropColumn('location');
        });
    }
}