<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUsergroupFunctions extends Migration
{
    public function up()
    {
        if (Schema::hasColumns('user_groups', ['is_mission_role','marker_color','is_mission_admin','prio'])) {
            return;
        }

        Schema::table('user_groups', function($table)
        {
            $table->boolean('is_mission_role')->default(0);
            $table->boolean('is_mission_admin')->default(0);
            $table->string('marker_color', 10)->nullable();
            $table->integer('prio')->unsigned()->default(0);
        });
    }

    public function down()
    {
        if (Schema::hasTable('user_groups') && Schema::hasColumns('user_groups', ['is_mission_role','marker_color','is_mission_admin','prio'])) {
            Schema::table('user_groups', function ($table) {
                $table->dropColumn(['is_mission_role','marker_color','is_mission_admin','prio']);
            });
        }
    }
}