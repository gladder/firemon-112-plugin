<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alert7 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dateTime('anonymized_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dropColumn('anonymized_at');
        });
    }
}