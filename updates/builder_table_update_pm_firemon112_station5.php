<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station5 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->boolean('diverafree_enabled')->default(0);
            $table->string('diverafree_accesskey', 64)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('diverafree_enabled');
            $table->dropColumn('diverafree_accesskey');
        });
    }
}