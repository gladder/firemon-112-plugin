<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112AlertUserFeedback extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_alert_user_feedback', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('alert_id')->unsigned()->default(0);
            $table->integer('user_id')->unsigned()->default(0);
            $table->smallInteger('feedback_status')->unsigned()->default(0);
            $table->dateTime('feedback_performed')->nullable()->default(null);
            $table->index('user_id', 'IDX_userId');
            $table->index('alert_id', 'IDX_alertId');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_alert_user_feedback');
    }
}