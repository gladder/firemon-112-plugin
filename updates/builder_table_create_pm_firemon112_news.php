<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112News extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('content', 255)->default('');
            $table->dateTime('visible_from');
            $table->dateTime('visible_to');
            $table->integer('prio')->unsigned()->default(0);
            $table->string('category', 32)->default('');
            $table->integer('station_id')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_news');
    }
}