<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Mails extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_mails', function($table)
        {
            $table->text('mail_text')->default('');
            $table->mediumtext('mail')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_mails', function($table)
        {
            $table->dropColumn('mail_text');
            $table->text('mail')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
