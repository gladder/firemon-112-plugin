<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Einsatzobjekt extends Migration
{
    public function up()
    {
        Schema::rename('pm_firemon112_einsatzobjekte', 'pm_firemon112_einsatzobjekt');
    }
    
    public function down()
    {
        Schema::rename('pm_firemon112_einsatzobjekt', 'pm_firemon112_einsatzobjekte');
    }
}