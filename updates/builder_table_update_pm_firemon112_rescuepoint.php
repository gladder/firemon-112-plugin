<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Rescuepoint extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->string('type', 32)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->dropColumn('type');
        });
    }
}