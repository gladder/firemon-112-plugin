<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Einsatzobjekt4 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_einsatzobjekt', function($table)
        {
            $table->boolean('documentation_public')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_einsatzobjekt', function($table)
        {
            $table->dropColumn('documentation_public');
        });
    }
}