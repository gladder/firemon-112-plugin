<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Contact extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_contact', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('label', 255)->default('');
            $table->string('phone', 255)->default('');
            $table->string('mobile', 255)->default('');
            $table->string('email', 255)->default('');
            $table->string('street_no', 255)->default('');
            $table->string('zip_city', 255)->default('');
            $table->string('type', 64)->default('');
            $table->integer('contactable_id')->unsigned()->default(0);
            $table->string('contactable_type', 255)->default('');
            $table->index(['contactable_id','contactable_type'], 'IDX_contact_id_type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_contact');
    }
}