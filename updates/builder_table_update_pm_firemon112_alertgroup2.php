<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alertgroup2 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->boolean('enabled')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->dropColumn('enabled');
        });
    }
}