<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station6 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->string('monitoring_emails', 512)->nullable();
            $table->string('forward_depesche_emails', 512)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('monitoring_emails');
            $table->dropColumn('forward_depesche_emails');
        });
    }
}
