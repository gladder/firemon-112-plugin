<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Event extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_event', function($table)
        {
            $table->boolean('duty')->default(0);
            $table->boolean('public')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_event', function($table)
        {
            $table->dropColumn('duty');
            $table->dropColumn('public');
        });
    }
}