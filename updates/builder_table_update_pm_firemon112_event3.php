<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Event3 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_event', function($table)
        {
            $table->boolean('email_reminder_done')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_event', function($table)
        {
            $table->dropColumn('email_reminder_done');
        });
    }
}