<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUserAlarmCallSecond extends Migration
{

    public function up()
    {
        if (Schema::hasColumns('users', ['alert_call_second'])) {
            return;
        }

        Schema::table('users', function($table)
        {
            $table->string('alert_call_second', 32)->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('users') && Schema::hasColumns('users', ['alert_call_second'])) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['alert_call_second']);
            });
        }
    }
}