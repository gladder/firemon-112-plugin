<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Wachendisplay extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_wachendisplay', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->text('enable_eventcategories')->nullable();
            $table->text('enable_schedule')->nullable();
            $table->text('module_layout')->nullable();
            $table->string('name', 64)->default('');
            $table->integer('station_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_wachendisplay');
    }
}