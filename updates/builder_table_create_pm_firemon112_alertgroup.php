<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Alertgroup extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_alertgroup', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('label', 64);
            $table->string('filter', 64);
            $table->string('criteria', 16);
            $table->integer('station_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_alertgroup');
    }
}