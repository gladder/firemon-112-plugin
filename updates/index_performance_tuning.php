<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class IndexPerformanceTuning extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->index('station_id', 'IDX_a_stationId');
        });

        Schema::table('pm_firemon112_alert_dispatch_job', function($table)
        {
            $table->index('alert_id', 'IDX_adj_alertId');
        });

        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->index('station_id', 'IDX_ag_stationId');
        });

        Schema::table('pm_firemon112_assemblyarea', function($table)
        {
            $table->index('station_id', 'IDX_aa_stationId');
        });

        Schema::table('pm_firemon112_device', function($table)
        {
            $table->index('station_id', 'IDX_d_stationId');
            $table->index('user_id', 'IDX_d_userId');
            $table->index('token', 'IDX_d_token');
        });

        Schema::table('pm_firemon112_deviceerror', function($table)
        {
            $table->index('device_id', 'IDX_de_deviceId');
        });

        Schema::table('pm_firemon112_einsatzobjekt', function($table)
        {
            $table->index('station_id', 'IDX_eo_stationId');
        });

        Schema::table('pm_firemon112_event', function($table)
        {
            $table->index('station_id', 'IDX_e_stationId');
        });

        Schema::table('pm_firemon112_event_subscription', function($table)
        {
            $table->index('event_id', 'IDX_s_eventId');
            $table->index('user_id', 'IDX_s_userId');
        });

        Schema::table('pm_firemon112_hydrant', function($table)
        {
            $table->index('station_id', 'IDX_h_stationId');
        });

        Schema::table('pm_firemon112_mail_capping', function($table)
        {
            $table->index('email_address', 'IDX_mc_email');
        });

        Schema::table('pm_firemon112_mails', function($table)
        {
            $table->index('token', 'IDX_m_token');
        });

        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->index('station_id', 'IDX_rp_stationId');
        });
    }

    public function down()
    {

        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dropIndex('IDX_a_stationId');
        });

        Schema::table('pm_firemon112_alert_dispatch_job', function($table)
        {
            $table->dropIndex('IDX_adj_alertId');
        });

        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->dropIndex('IDX_ag_stationId');
        });

        Schema::table('pm_firemon112_assemblyarea', function($table)
        {
            $table->dropIndex('IDX_aa_stationId');
        });

        Schema::table('pm_firemon112_device', function($table)
        {
            $table->dropIndex('IDX_d_stationId');
            $table->dropIndex('IDX_d_userId');
            $table->dropIndex('IDX_d_token');
        });

        Schema::table('pm_firemon112_deviceerror', function($table)
        {
            $table->dropIndex('IDX_de_deviceId');
        });

        Schema::table('pm_firemon112_einsatzobjekt', function($table)
        {
            $table->dropIndex('IDX_eo_stationId');
        });

        Schema::table('pm_firemon112_event', function($table)
        {
            $table->dropIndex('IDX_e_stationId');
        });

        Schema::table('pm_firemon112_event_subscription', function($table)
        {
            $table->dropIndex('IDX_s_eventId');
            $table->dropIndex('IDX_s_userId');
        });

        Schema::table('pm_firemon112_hydrant', function($table)
        {
            $table->dropIndex('IDX_stationId');
        });

        Schema::table('pm_firemon112_mail_capping', function($table)
        {
            $table->dropIndex('IDX_email');
        });

        Schema::table('pm_firemon112_mails', function($table)
        {
            $table->dropIndex('IDX_token');
        });

        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->dropIndex('IDX_stationId');
        });
    }
}