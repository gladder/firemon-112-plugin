<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station7 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->integer('leitstelle_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('leitstelle_id');
        });
    }
}
