<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Hydranttype extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_hydranttype', function($table)
        {
            $table->string('svg_tactical', 2048)->nullable();
            $table->string('svg_overview', 2048)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_hydranttype', function($table)
        {
            $table->dropColumn('svg_tactical');
            $table->dropColumn('svg_overview');
        });
    }
}