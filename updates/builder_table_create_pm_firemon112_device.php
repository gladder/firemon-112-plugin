<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Device extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_device', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('station_id')->unsigned();
            $table->string('token', 128);
            $table->string('name', 128);
            $table->string('state', 32);
            $table->string('type', 32);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_device');
    }
}