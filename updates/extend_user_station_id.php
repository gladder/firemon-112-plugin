<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUserStationId extends Migration
{
    public function up()
    {
        if (Schema::hasColumns('users', ['station_id'])) {
            return;
        }

        Schema::table('users', function($table)
        {
            $table->integer('station_id')->unsigned()->nullable()->index();
        });
    }

    public function down()
    {
        if (Schema::hasTable('users') && Schema::hasColumns('users', ['station_id'])) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['station_id']);
            });
        }
    }
}