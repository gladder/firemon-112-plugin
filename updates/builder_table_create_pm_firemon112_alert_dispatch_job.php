<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112AlertDispatchJob extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_alert_dispatch_job', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('alert_id')->unsigned();
            $table->string('dispatch_type', 32);
            $table->dateTime('dispatched_datetime')->nullable();
            $table->text('result')->nullable();
            $table->text('payload')->nullable();
            $table->boolean('dry_run')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pm_firemon112_alert_dispatch_job');
    }
}