<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Alertgroup2user extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_alertgroup2user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('alertgroup_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->primary(['alertgroup_id','user_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_alertgroup2user');
    }
}
