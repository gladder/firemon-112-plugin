<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Group2groupable extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_group2groupable', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_group_id')->unsigned();
            $table->integer('groupable_id')->unsigned();
            $table->string('groupable_type', 255);
            $table->primary(['user_group_id','groupable_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_group2groupable');
    }
}