<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUserAllowAppUsage extends Migration
{

    public function up()
    {
        if (Schema::hasColumns('users', ['allow_app_usage'])) {
            return;
        }

        Schema::table('users', function($table)
        {
            $table->boolean('allow_app_usage')->default(0);
        });
    }

    public function down()
    {
        if (Schema::hasTable('users') && Schema::hasColumns('users', ['allow_app_usage'])) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['allow_app_usage']);
            });
        }
    }
}