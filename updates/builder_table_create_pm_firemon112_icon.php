<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Icon extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_icon', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('label', 255)->default('');
            $table->string('group', 32)->default('');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_icon');
    }
}