<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Training extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_training', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('training_source_id')->unsigned()->default(0);
            $table->string('label', 256);
            $table->date('from_date');
            $table->date('to_date');
            $table->string('note', 256);
            $table->integer('slots_available')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_training');
    }
}