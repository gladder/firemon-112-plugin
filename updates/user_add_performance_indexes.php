<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UserAddPerformanceIndexes extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_user_pushtoken', function($table)
        {
            $table->index(['user_id'], 'IDX_user_id');
        });
        Schema::table('pm_firemon112_user_absence', function($table)
        {
            $table->index(['user_id'], 'IDX_user_id');
        });
    }

    public function down()
    {
        Schema::table('pm_firemon112_user_pushtoken', function($table)
        {
            $table->dropIndex('IDX_user_id');
        });
        Schema::table('pm_firemon112_user_absence', function($table)
        {
            $table->dropIndex('IDX_user_id');
        });
    }
}