<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112GpioAlertTrigger2device extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_gpio_alert_trigger2device', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('device_id')->unsigned();
            $table->integer('gpio_alert_trigger_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_gpio_alert_trigger2device');
    }
}