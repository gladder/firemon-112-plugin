<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alert5 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->text('weather')->nullable()->default(null);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dropColumn('weather');
        });
    }
}