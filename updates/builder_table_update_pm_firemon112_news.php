<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112News extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_news', function($table)
        {
            $table->text('content_long');
            $table->boolean('is_general')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_news', function($table)
        {
            $table->dropColumn('content_long');
            $table->dropColumn('is_general');
        });
    }
}