<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUserAlerting extends Migration
{
    private $newCols = [
        'alert_email',
        'trigger_alert_email',
        'alert_sms',
        'trigger_alert_sms',
        'alert_call',
        'trigger_alert_call'
    ];

    public function up()
    {
        if (Schema::hasColumns('users', $this->newCols)) {
            return;
        }

        Schema::table('users', function($table)
        {
            $table->string('alert_email', 256)->nullable();
            $table->string('alert_sms', 32)->nullable();
            $table->string('alert_call', 32)->nullable();
            $table->boolean('trigger_alert_email')->default(0);
            $table->boolean('trigger_alert_sms')->default(0);
            $table->boolean('trigger_alert_call')->default(0);
        });
    }

    public function down()
    {
        if (Schema::hasTable('users') && Schema::hasColumns('users', $this->newCols)) {
            Schema::table('users', function ($table) {
                foreach ($this->newCols as $col) {
                    $table->dropColumn($col);
                }
            });
        }
    }
}