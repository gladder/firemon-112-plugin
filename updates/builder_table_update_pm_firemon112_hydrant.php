<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Hydrant extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_hydrant', function($table)
        {
            $table->integer('station_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_hydrant', function($table)
        {
            $table->dropColumn('station_id');
        });
    }
}