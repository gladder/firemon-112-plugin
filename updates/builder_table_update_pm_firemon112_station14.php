<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station14 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->boolean('alert_details_protected')->default(0);
            $table->string('registration_pin', 16)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('alert_details_protected');
            $table->string('registration_pin', 16)->default(null)->change();
        });
    }
}