<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Leitstelle2 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_leitstelle', function($table)
        {
            $table->string('mail_algo', 64)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_leitstelle', function($table)
        {
            $table->dropColumn('mail_algo');
        });
    }
}