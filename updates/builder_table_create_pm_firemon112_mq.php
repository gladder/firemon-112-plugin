<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Mq extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_mq', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('station_id')->unsigned();
            $table->string('user', 128);
            $table->string('pass', 128);
            $table->string('host', 128);
            $table->string('vhost', 128);
            $table->integer('port')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_mq');
    }
}