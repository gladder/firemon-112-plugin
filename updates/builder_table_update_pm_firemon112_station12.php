<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Station12 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->smallInteger('alert_feedback_type')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_station', function($table)
        {
            $table->dropColumn('alert_feedback_type');
        });
    }
}