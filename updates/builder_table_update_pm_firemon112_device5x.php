<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Device5x extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->integer('depesche_delay')->unsigned()->default(0);
            $table->integer('depesche_gong_type')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->dropColumn('depesche_delay');
            $table->dropColumn('depesche_gong_type');
        });
    }
}