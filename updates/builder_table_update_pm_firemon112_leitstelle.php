<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Leitstelle extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_leitstelle', function($table)
        {
            $table->string('email_alarm', 256)->nullable();
            $table->renameColumn('email', 'email_contact');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_leitstelle', function($table)
        {
            $table->dropColumn('email_alarm');
            $table->renameColumn('email_contact', 'email');
        });
    }
}
