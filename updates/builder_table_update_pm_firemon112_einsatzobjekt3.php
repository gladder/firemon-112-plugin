<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Einsatzobjekt3 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_einsatzobjekt', function($table)
        {
            $table->text('coords');
            $table->string('key_no', 255)->default('');
            $table->string('map_no', 255)->default('');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_einsatzobjekt', function($table)
        {
            $table->dropColumn('coords');
            $table->dropColumn('key_no');
            $table->dropColumn('map_no');
        });
    }
}