<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Presence extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_presence', function($table)
        {
            $table->string('category', 32)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_presence', function($table)
        {
            $table->dropColumn('category');
        });
    }
}