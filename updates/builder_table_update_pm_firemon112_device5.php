<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Device5 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->boolean('depesche_speech_enabled')->default(0);
            $table->integer('depesche_speech_count')->unsigned()->default(1);
            $table->integer('depesche_speech_interval')->unsigned()->default(30);
            $table->dateTime('last_heartbeat')->nullable();
            $table->string('running_version', 16)->nullable()->default('unknown');
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_device', function($table)
        {
            $table->dropColumn('depesche_speech_enabled');
            $table->dropColumn('depesche_speech_count');
            $table->dropColumn('depesche_speech_interval');
            $table->dropColumn('last_heartbeat');
            $table->dropColumn('running_version');
        });
    }
}