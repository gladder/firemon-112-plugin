<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112InterestedStation extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_interested_station', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('station_name', 128);
            $table->integer('leitstelle_id')->unsigned()->default(0);
            $table->decimal('long', 9, 6);
            $table->decimal('lat', 9, 6);
            $table->string('email', 128);
            $table->string('station_type', 16);
            $table->text('comment')->nullable();
            $table->text('status');
            $table->string('contact_person', 128);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_interested_station');
    }
}