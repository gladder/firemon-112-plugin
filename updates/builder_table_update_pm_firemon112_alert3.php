<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alert3 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dateTime('schedule_trigger_at')->nullable();
            $table->dateTime('schedule_handled_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alert', function($table)
        {
            $table->dropColumn('schedule_trigger_at');
            $table->dropColumn('schedule_handled_at');
        });
    }
}