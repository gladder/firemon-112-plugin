<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUsergroupFunctions3 extends Migration
{
    public function up()
    {
        Schema::table('user_groups', function($table)
        {
            $table->index('station_id', 'IDX_stationId');
        });
    }

    public function down()
    {
        Schema::table('user_groups', function($table)
        {
            $table->dropIndex('IDX_stationId');
        });
    }
}