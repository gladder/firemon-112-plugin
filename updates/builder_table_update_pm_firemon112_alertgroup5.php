<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Alertgroup5 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->boolean('trigger_alert_app')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_alertgroup', function($table)
        {
            $table->dropColumn('trigger_alert_app');
        });
    }
}