<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePmFiremon112Iconmarker extends Migration
{
    public function up()
    {
        Schema::create('pm_firemon112_iconmarker', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('icon_id')->unsigned()->default(0);
            $table->decimal('long', 9, 6)->default(0.0);
            $table->decimal('lat', 9, 6)->default(0.0);
            $table->integer('iconmarkerable_id')->unsigned()->default(0);
            $table->string('iconmarkerable_type', 255)->default('');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->index(['iconmarkerable_id','iconmarkerable_type'], 'IDX_iconmarker_id_type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pm_firemon112_iconmarker');
    }
}