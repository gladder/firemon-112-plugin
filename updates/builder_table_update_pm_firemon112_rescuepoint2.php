<?php namespace pm\Firemon112\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePmFiremon112Rescuepoint2 extends Migration
{
    public function up()
    {
        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->integer('station_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pm_firemon112_rescuepoint', function($table)
        {
            $table->dropColumn('station_id');
        });
    }
}