<?php namespace pm\Firemon112;

use pm\Firemon112\Classes\JWT\UserPluginResolver;
use pm\Firemon112\Classes\JWT\UserPluginResolverContract;
use pm\Firemon112\Classes\JWT\UserProvider;
use pm\Firemon112\Classes\JWT\JWTGuard;
use pm\Firemon112\Classes\JWT\UserModelHandler;
use pm\Firemon112\Models\Settings;
use pm\Firemon112\Components;
use System\Classes\PluginManager;
use PHPOpenSourceSaver\JWTAuth\Providers\LaravelServiceProvider;

use System\Classes\PluginBase;
use Event, Config;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Models\UserGroup as UserGroup;
use RainLab\User\Controllers\Users as UsersController;
use RainLab\User\Controllers\UserGroups as UserGroupsController;

use Illuminate\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

use Yaml;
use File;

class Plugin extends PluginBase
{

    /* requires composer require tymon/jwt-auth */
    public $require = ['RainLab.User'];

    public function boot()
    {
        $this->extendUserModel();
        $this->extendUserGroupModel();
        $this->extendUsersController();
        $this->extendUserGroupsController();

        $this->registerJWTConfigs();
        $this->addEventListeners();
        if (Settings::get('recaptcha-enabled')) {
            $this->app['Illuminate\Contracts\Http\Kernel']->pushMiddleware('pm\Firemon112\Middleware\CaptchaMiddleware');
        }
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Basic Settings',
                'description' => 'Manage backend and frontend settings',
                'category'    => 'Firemon 112',
                'icon'        => 'icon-bell',
                'class'       => 'pm\Firemon112\Models\Settings',
                'order'       => 500,
                'keywords'    => 'firemon 112 settings',
                'permissions' => []
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            \pm\Firemon112\Components\FiremonSession::class => 'firemonSession',
            \pm\Firemon112\Components\AlarmTestComponent::class => 'alarmTestComponent',
            \pm\Firemon112\Components\GpioAlertTriggerComponent::class => 'gpioAlertTriggerComponent',
            \pm\Firemon112\Components\MonitorComponent::class => 'monitorComponent',
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('firemon:depesche:handle', 'pm\Firemon112\Console\HandleDepesche');
        $this->registerConsoleCommand('firemon:alert:schedule', 'pm\Firemon112\Console\AlertSchedule');
        $this->registerConsoleCommand('firemon:fastadd:station', 'pm\Firemon112\Console\FastaddStation');
        $this->registerConsoleCommand('firemon:mail:handle', 'pm\Firemon112\Console\HandleMail');
        $this->registerConsoleCommand('firemon:monitor:devices', 'pm\Firemon112\Console\MonitorDevices');
        $this->registerConsoleCommand('firemon:event:reminder', 'pm\Firemon112\Console\EventReminder');
        $this->registerConsoleCommand('firemon:training:scrape', 'pm\Firemon112\Console\TrainingScrape');
        $this->registerConsoleCommand('firemon:alert:trigger', 'pm\Firemon112\Console\AlertTrigger');
        $this->registerConsoleCommand('firemon:database:cleanup', 'pm\Firemon112\Console\CleanupDatabase');
        $this->registerConsoleCommand('firemon:query:weather', 'pm\Firemon112\Console\QueryWeather');
        $this->registerConsoleCommand('firemon:anonymize:data', 'pm\Firemon112\Console\AnonymizeData');
        $this->registerConsoleCommand('firemon:reboot:device', 'pm\Firemon112\Console\RebootDevice');
        $this->registerConsoleCommand('firemon:absence:worker', 'pm\Firemon112\Console\AbsenceWorker');

        $this->app->singleton(
            UserPluginResolverContract::class,
            function() { return UserPluginResolver::instance(); }
        );

        $this->registerGates();
        $this->registerJWT();
        $this->registerConsoleCommand('firemon:swiss:armyknife', 'pm\Firemon112\Console\SwissArmyKnife');
    }

    public function registerSchedule($schedule)
    {
        $schedule->command('firemon:alert:schedule')->everyMinute();
        $schedule->command('firemon:monitor:devices')->hourly();
        $schedule->command('firemon:event:reminder')->hourly();
        $schedule->command('firemon:training:scrape')->twiceDaily(9, 19);
        $schedule->command('firemon:database:cleanup')->weekly();
        $schedule->command('firemon:anonymize:data')->daily();
        $schedule->command('firemon:reboot:device')->dailyAt(3);
        $schedule->command('firemon:absence:worker')->hourly();
    }

    public function registerFormWidgets()
    {
        return [
            'Pm\Firemon112\FormWidgets\DepescheWidget' => 'pm_firemon112_depeschewidget',
            'Pm\Firemon112\FormWidgets\LongLatWidget' => 'pm_firemon112_longlatwidget',
            'Pm\Firemon112\FormWidgets\GeofenceWidget' => 'pm_firemon112_geofencewidget',
            'Pm\Firemon112\FormWidgets\JsonViewerWidget' => 'pm_firemon112_jsonviewerwidget'
        ];
    }

    protected function extendUsersController()
    {
        UsersController::extendFormFields(function($widget) {
            // Prevent extending of related form instead of the intended User form
            if (!$widget->model instanceof UserModel) {
                return;
            }

            $configFile = plugins_path('pm/firemon112/extensions/user/extend-user.yaml');
            $config = Yaml::parse(File::get($configFile));
            $widget->addTabFields($config);
        });
        UsersController::extendListColumns(function($listWidget) {
            // Only for the User model
            if (!$listWidget->model instanceof UserModel) {
                return;
            }

            $listWidget->addColumns([
                'station_name' => [
                    'label' => 'Feuerwehr',
                    'relation' => 'station',
                    'select' => 'name'
                ]
            ]);
        });
        UsersController::extend(function ($controller) {
            // If the controller doesn't alredy implement the relation config add this...
            $controller->implement[] = 'Backend.Behaviors.RelationController';
            // See above directory layout to understand this better
            $controller->relationConfig = plugins_path('pm/firemon112/extensions/user/extend-config-relation.yaml');
        });

    }

    protected function extendUserModel()
    {
        UserModel::extend(function($model) {
            $model->belongsTo = [ 'station' =>  'pm\Firemon112\Models\Station' ];
            $model->belongsToMany = [
                'groups' => [UserGroup::class, 'table' => 'users_groups'],
                'alertgroups' => [
                    'pm\Firemon112\Models\AlertGroup',
                    'table' => 'pm_firemon112_alertgroup2user',
                    'key' => 'user_id',
                    'otherKey' => 'alertgroup_id'
                ],
                'linked_stations' => [
                    'pm\Firemon112\Models\Station',
                    'table' => 'pm_firemon112_station2user',
                    'key' => 'user_id',
                    'otherKey' => 'station_id'
                ],
                'alerts' => [
                    'pm\Firemon112\Models\Alert',
                    'table' => 'pm_firemon112_alert2user',
                    'key' => 'user_id',
                    'otherKey' => 'alert_id',
                    'pivot' => ['alertgroup_id', 'station_id']
                ],
            ];
            $model->hasMany = [
                'subscriptions' =>  'pm\Firemon112\Models\EventSubscription',
                'alert_feedbacks' =>  'pm\Firemon112\Models\AlertUserFeedback',
                'devices' =>  'pm\Firemon112\Models\Device',
                'invites_sender' =>  [
                    'pm\Firemon112\Models\Invite',
                    'key' => 'user_id_sender',
                    'otherKey' => 'id',
                ],
                'invites_recipient' =>  [
                    'pm\Firemon112\Models\Invite',
                    'key' => 'user_id_recipient',
                    'otherKey' => 'id',
                ],
                'otp' =>  'pm\Firemon112\Models\OneTimePassword',
                'pushtokens' =>  'pm\Firemon112\Models\Pushtoken',
                'absences' =>  'pm\Firemon112\Models\Absence',
                'reminders' =>  'pm\Firemon112\Models\Reminder',
            ];
            $model->attachOne = [
                'avatar' => 'System\Models\File'
            ];
            $model->morphMany = [
                'settings' => [\pm\Firemon112\Models\CustomSettings::class, 'name' => 'ownerable', 'delete' => true],
            ];
            $model->rules['alert_email'] = 'email';
            $model->rules['alert_sms'] = ['regex:/^(\+491|491|00491|01){1}[0-9]*$/', 'between:10,16'];
            $model->rules['alert_call'] = ['regex:/^(\+49|49|0049|0){1}[0-9]*$/', 'between:6,22'];
            $model->rules['alert_call_second'] = ['regex:/^(\+49|49|0049|0){1}[0-9]*$/', 'between:6,22'];
            $model->addFillable(['alert_email', 'alert_sms', 'alert_call', 'alert_call_second']);
            $model->addHidden([
                'activated_at',
                'last_login',
                'created_at',
                'updated_at',
                'deleted_at',
                'last_seen',
                'is_guest',
                'is_superuser',
                'created_ip_address',
                'last_ip_address',
                'is_activated',
                'permissions'
            ]);
            $model->addDynamicMethod('getJwtPwhAttribute', function() use ($model) {
                return substr(hash('sha256', $model->password),0,16);
            });
            $model->addDynamicMethod('getRelatedEventsRelation', function($station_id) use ($model) {
                $station = $station_id == $model->station_id ? $model->station : $model->linked_stations->find($station_id) ?? $model->station;
                return $station->events()->whereDoesntHave('groups')->orWhereHas('groups', function ($query) use ($model, $station) {
                    $query->whereIn('user_group_id', $model->groups()->where('station_id', $station->id)->pluck('id')->toArray());
                });
            });
            $model->addDynamicMethod('getRelatedEvents', function($station_id) use ($model) {
                return $model->getRelatedEventsRelation($station_id)->get();
            });
            $model->addDynamicMethod('getRelatedAlertGroupsAttribute', function() use ($model) {
                $directAlertGroups = $model->alertgroups;
                $groupedAlertGroups = $model->groups->flatMap(
                    function ($group) {
                        return $group->alertgroups;
                    }
                )->unique('id');
                return $directAlertGroups->concat($groupedAlertGroups)->unique('id');
            });
        });
    }

    protected function extendUserGroupModel()
    {
        UserGroup::extend(function($model) {
            $model->belongsTo = [ 'station' =>  'pm\Firemon112\Models\Station' ];
            $model->morphedByMany = [
                'alertgroups'  => ['pm\Firemon112\Models\AlertGroup', 'table' => 'pm_firemon112_group2groupable', 'name' => 'groupable'],
                'events' => ['pm\Firemon112\Models\Event', 'table' => 'pm_firemon112_group2groupable', 'name' => 'groupable']
            ];
            $model->addFillable([
                'is_mission_role',
                'marker_color',
                'is_mission_admin',
                'prio',
                'station_id',
                'is_mission_operator',
                'short_name',
                'is_role_station',
                'is_role_presence',
                'is_role_alert',
                'is_role_events',
                'is_role_alert_readonly'
            ]);
            $model->addDynamicMethod('getIsFunktionAttribute', function() use ($model) {
                return $model->is_mission_role;
            });
            $model->addDynamicMethod('getIsAdminAttribute', function() use ($model) {
                return $model->is_role_station || $model->is_role_presence || $model->is_role_alert || $model->is_role_events;
            });
            $model->addDynamicMethod('getIsGruppeAttribute', function() use ($model) {
                return !$model->is_mission_role && !$model->is_role_station && !$model->is_role_presence && !$model->is_role_alert && !$model->is_role_events;
            });
            $model->addDynamicMethod('scopeByStation', function ($query, $station_id) {
                return $query->where('station_id', $station_id);
            });
        });
    }

    protected function extendUserGroupsController()
    {
        UserGroupsController::extendFormFields(function($widget) {
            // Prevent extending of related form instead of the intended User form
            if (!$widget->model instanceof UserGroup) {
                return;
            }

            $configFile = plugins_path('pm/firemon112/extensions/usergroup/extend-usergroup.yaml');
            $config = Yaml::parse(File::get($configFile));
            $widget->addTabFields($config);
        });

        UserGroupsController::extendListColumns(function($widget) {

            // Prevent extending of an unintended list
            if (!$widget->model instanceof UserGroup) {
                return;
            }


            $configFile = plugins_path('pm/firemon112/extensions/usergroup/extend-list-columns.yaml');
            $config = Yaml::parse(File::get($configFile));
            $widget->addColumns($config);

        });

    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                // A global function, i.e str_plural()
                'base64_encode' => 'base64_encode',
                'base64_decode' => 'base64_decode',
            ],
            'functions' => [
                'secureReCaptchaButton' => function($label, $class) {
                    return '<button class="g-recaptcha '.$class.'" data-sitekey="'.Settings::get('recaptcha-site-key').'" data-callback="onRecaptchaSubmit" data-attach-loading>'.$label.'</button>';
                },
                'secureReCaptchaScript' => function($formId, $isAjax) {
                    $markup = '
                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=de" async defer></script>
                    ';
                    if ($isAjax) {
                        $markup .= '
                            <script type="text/javascript">
                            function onRecaptchaSubmit(token) {
                                $("#'.$formId.'").request("",{
                                    complete: function() {
                                        grecaptcha.reset();
                                    }
                                });
                            }
                            </script>
                        ';
                    } else {
                        $markup .= '
                            <script type="text/javascript">
                            function onRecaptchaSubmit(token) {
                                $("#'.$formId.'").submit();
                            }
                            </script>
                        ';
                    }
                    return $markup;
                }
            ]
        ];
    }

    private function registerJWTConfigs()
    {
        $pluginNamespace = str_replace('\\', '.', strtolower(__NAMESPACE__));
        $packages = Config::get($pluginNamespace . '::packages');

        foreach ($packages as $name => $options) {
            if (!empty($options['config']) && !empty($options['config_namespace'])) {
                Config::set($options['config_namespace'], $options['config']);
            }
        }
    }

    private function addEventListeners()
    {
        Event::subscribe(UserModelHandler::class);
    }

    private function registerJWT(): void
    {
        (new LaravelServiceProvider($this->app))->register();

        $this->app->singleton('JWTGuard', static function ($app): Guard {
            $guard = new JWTGuard(
                $app['tymon.jwt'],
                new UserProvider(app(UserPluginResolverContract::class)->getModel()),
                $app['request'],
                new \October\Rain\Events\Dispatcher()
            );

            $app->refresh('request', $guard, 'setRequest');
            return $guard;
        });
    }

    private function registerGates(): void
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('Gate', \Illuminate\Support\Facades\Gate::class);

        $this->app->singleton(GateContract::class, function ($app): Gate {
            return new Gate($app, function () use ($app): ?User {
                return app(UserPluginResolverContract::class)->getProvider()->user();
            });
        });
    }
}
