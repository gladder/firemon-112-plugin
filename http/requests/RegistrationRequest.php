<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 */
class RegistrationRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'surname' => 'required|string',
            'alert_sms' => 'sometimes|string',
            'registration_pin' => 'required|integer',
        ];
    }
}