<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 */
class AppointmentsRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'year' => 'required|integer',
            'month' => 'integer',
            'appointment_id' => 'integer',
            'include_sub_details' => 'integer',
            'station_id' => 'integer'
        ];
    }
}