<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 */
class AbsenceRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'required|integer',
            'absence_type' => 'required|string',
            'user_id' => 'required|integer',
            'valid_from' => 'required|date',
            'valid_to' => 'required|date|after:valid_from',
            'auto_cancel_events' => 'boolean'
        ];
    }
}