<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 */
class AlertFeedbackRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'alert_id' => 'required|integer',
            'feedback' => 'required|integer',
        ];
    }
}