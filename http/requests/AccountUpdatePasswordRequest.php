<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 */
class AccountUpdatePasswordRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'password' => 'required|between:8,255|confirmed',
            'password_confirmation' => 'required_with:password|between:8,255',
        ];
    }
}