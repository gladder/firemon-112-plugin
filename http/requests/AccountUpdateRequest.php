<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 */
class AccountUpdateRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'surname' => 'required|string',
            'alert_sms' => ['sometimes','regex:/^(\+491|491|00491|01){1}[0-9]*$/', 'between:10,16'],
            'alert_email' => 'sometimes|email',
            'alert_call' => ['sometimes','regex:/^(\+49|49|0049|0){1}[0-9]*$/', 'between:6,22'],
            'alert_call_second' => ['sometimes','regex:/^(\+49|49|0049|0){1}[0-9]*$/', 'between:6,22'],
        ];
    }
}