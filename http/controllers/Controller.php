<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use pm\Firemon112\Classes\JWT\UserPluginResolverContract;
use pm\Firemon112\Classes\JWT\JWTGuard;

/**
 * Base JWTAuth Controller
 */
abstract class Controller
{
    /**
     * @var UserPluginResolverContract
     */
    protected $userPluginResolver;

    /**
     * @var JWTGuard
     */
    protected $JWTGuard;

    /**
     * @param $userPluginResolver
     */
    public function __construct(UserPluginResolverContract $userPluginResolver)
    {
        $this->userPluginResolver = $userPluginResolver;
        $this->JWTGuard = app('JWTGuard');
    }
}