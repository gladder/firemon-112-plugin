<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use pm\Firemon112\Classes\JWT\TokenDto;
use pm\Firemon112\Http\Requests\ActivationRequest;

/**
 *
 */
class ActivationController extends Controller
{
    /**
     * @param ActivationRequest $registrationRequest
     * @throws \ApplicationException
     * @return mixed
     */
    public function __invoke(ActivationRequest $registrationRequest)
    {
        $user = $this->userPluginResolver
            ->getResolver()
            ->activateByCode($registrationRequest->validated());

        if (empty($user)) {
            throw new \ApplicationException('Activation failed');
        }

        $tokenDto = new TokenDto([
            'token' => $this->JWTGuard->login($user),
            'expires' => Carbon::createFromTimestamp($this->JWTGuard->getPayload()->get('exp')),
            'user' => $user,
        ]);

        return ['data' => $tokenDto->toArray()];
    }
}