<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Auth;
use pm\Firemon112\Classes\JWT\TokenDto;
use pm\Firemon112\Http\Requests\LoginRequest;
use pm\Firemon112\Http\Requests\SmsLoginRequest;
use pm\Firemon112\Http\Resources\TokenResource;
use \Exception;
use RainLab\User\Models\User;
use pm\Firemon112\Models\OneTimePassword;
use pm\Firemon112\Models\Helper;

/**
 *
 */
class SmsAuthController extends Controller
{
    /**
     * @param SmsLoginRequest $smsLoginRequest
     * @return array
     * @throws \Exception
     */
    public function __invoke(SmsLoginRequest $smsLoginRequest): array
    {
        //die($smsLoginRequest->input('phone') . ':::' . $smsLoginRequest->input('otp'));
        try {
            // init otp
            $user = Helper::findUserByAlertSms($smsLoginRequest->input('phone'));
            if ($user == null) {
                throw new \Exception("User nicht gefunden");
            }

            // set mobile phone to that number which is assigned to user
            $mobile_phone = $user->alert_sms;

            if ($smsLoginRequest->input('otp') == null || strlen($smsLoginRequest->input('otp')) <= 0) {
                $existing = OneTimePassword::where('identifier', $mobile_phone)->where('reason',OneTimePassword::$REASON_AUTH)->pending();
                if ($existing->count() <= 0) {
                    $otp_readable = OneTimePassword::getSmsPassword();
                    $otp = OneTimePassword::create([
                        'identifier' => $mobile_phone,
                        'reason' => OneTimePassword::$REASON_AUTH,
                        'otp' => OneTimePassword::getPasswordHash($otp_readable),
                        'user_id' => $user->id,
                        'expired_at' => Carbon::now('Europe/Berlin')->addMinutes(15)
                    ]);
                    \Queue::push('\pm\Firemon112\Jobs\HandleSmsOtp', ['otp_id' => $otp->id, 'otp_readable' => $otp_readable]);
                } else {
                    throw new PendingOtpException("Es ist ein noch nicht verwendetes, aber weiterhin gültiges Einmalpasswort vorhanden. Für ein neues Einmalpasswort muss das bestehende zunächst ablaufen.");
                }
                return ['success' => true, 'data' => $mobile_phone, 'pendingOtp' => true];
            } else {
                // login otp
                $otp_item = OneTimePassword::where('identifier', $mobile_phone)->where('reason',OneTimePassword::$REASON_AUTH)->pending()->first();
                if ($otp_item !== null && $otp_item->isPasswordMatching($smsLoginRequest->input('otp')) && !$otp_item->getIsExpiredAttribute() && !$otp_item->getIsUsedAttribute()) {
                    $otp_item->used_at = Carbon::now('Europe/Berlin');
                    $otp_item->save();
                    $user = User::find($otp_item->user_id);
                    if ($user !== null) {
                        // activate if needed
                        if (!$user->is_activated) {
                            $user->is_activated = 1;
                            $user->save();
                        }
                        Auth::loginUsingId($user->id);
                        $tokenDto = new TokenDto([
                            'token' => $this->JWTGuard->login($user),
                            'expires' => Carbon::createFromTimestamp($this->JWTGuard->getPayload()->get('exp')),
                            'user' => $user,
                        ]);
                        return ['success' => true, 'data' => $tokenDto->toArray()];
                    } else {
                        throw new PendingOtpException("Der Login ist fehlgeschlagen.");
                    }
                } else {
                    throw new \Exception("Der Login ist fehlgeschlagen.");
                }
            }
        } catch (PendingOtpException $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage(), 'pendingOtp' => true];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    }
}

class PendingOtpException extends \Exception {

}