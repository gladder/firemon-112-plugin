<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use pm\Firemon112\Http\Requests\RegistrationRequest;
use RainLab\User\Models\User;
use pm\Firemon112\Models\Station;
use pm\Firemon112\Models\Helper;

/**
 *
 */
class RegistrationController extends Controller
{
    /**
     * @param RegistrationRequest $registrationRequest
     * @throws \ApplicationException
     * @return mixed
     */
    public function __invoke(RegistrationRequest $registrationRequest)
    {
        try {
            $data = $registrationRequest->validated();

            if (strlen($data['name'])<=0) {
                throw new \Exception("Vorname fehlt");
            }
            if (strlen($data['surname'])<=0) {
                throw new \Exception("Nachname fehlt");
            }
            if (strlen($data['email'])<=0) {
                throw new \Exception("E-Mail fehlt");
            }
            if ($data['registration_pin']<=0) {
                throw new \Exception("Wehr-PIN fehlt");
            }
            $user = User::where('email', $data['email'])->first();
            if ($user != null) {
                throw new \Exception("Benutzer existiert bereits - Passwort vergessen?");
            }
            $station = Station::where('registration_pin',$data['registration_pin'])->first();
            if ($station == null) {
                throw new \Exception("Wehr nicht gefunden");
            }
            $frontenduser = Helper::CreateFrontendUser($data['name'], $data['surname'], $data['email'], $station->id, true);
            if ($frontenduser == null) {
                throw new \Exception("Benutzer konnte nicht erstellt werden.");
            }
            $frontenduser->allow_app_usage = 1;
            if (strlen($data['alert_sms']) > 0) {
                $frontenduser->alert_sms = $data['alert_sms'];
                $frontenduser->trigger_alert_sms = 1;
            }
            $frontenduser->save();
            Helper::SendUserRegisteredMailToAdmins($frontenduser);
            return ['success' => true];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }
}