<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use pm\Firemon112\Classes\JWT\TokenDto;
use pm\Firemon112\Http\Requests\LoginRequest;
use pm\Firemon112\Http\Requests\SmsLoginRequest;
use pm\Firemon112\Http\Resources\TokenResource;
use \Exception;

/**
 *
 */
class AuthController extends Controller
{
    /**
     * @param LoginRequest $loginRequest
     * @return array
     * @throws \Exception
     */
    public function __invoke(LoginRequest $loginRequest): array
    {
        try {
            $user = $this->userPluginResolver
                ->getProvider()
                ->authenticate($loginRequest->validated());

            if (empty($user)) {
                throw new \Exception('Login failed');
            }

            if ($user->allow_app_usage != 1) {
                throw new \Exception('App Nutzung nicht erlaubt!');
            }

            $tokenDto = new TokenDto([
                'token' => $this->JWTGuard->login($user),
                'expires' => Carbon::createFromTimestamp($this->JWTGuard->getPayload()->get('exp')),
                'user' => $user
            ]);

            return ['data' => $tokenDto->toArray()];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage() . '|' . $loginRequest->email . '|' . $loginRequest->password);
            return ['data' => [], 'error' => $e->getMessage()];
        }

    }
}