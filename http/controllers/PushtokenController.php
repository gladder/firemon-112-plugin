<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\PushtokenRequest;
use pm\Firemon112\Models\Pushtoken;

/**
 *
 */
class PushtokenController extends Controller
{
    /**
     * @return array
     */
    public function update(PushtokenRequest $request): array
    {
        $data = $request->validated();
        $guard = app('JWTGuard');
        $user = $guard->user();

        if ($user != null) {
            $token = Pushtoken::firstOrNew(['token' => $data['token']]);
            $token->fill($data);
            $token->user_id = $user->id;
            $token->save();
            if ($token->isFcmToken() || $token->isApnToken()) {
                // delete all existing expo tokens fpr this device name if new token is fcm or apn token
                foreach ($user->pushtokens as $existingToken) {
                    if ($existingToken->isExpoToken() && $existingToken->device_name == $token->device_name) {
                        $existingToken->delete();
                    }
                }
            }
        }

        return [];
    }

    public function delete(PushtokenRequest $request): array
    {
        $data = $request->validated();
        $guard = app('JWTGuard');
        $user = $guard->user();

        if ($user != null && array_key_exists('token', $data)) {
            foreach ($user->pushtokens as $existingToken) {
                if ($existingToken->token == $data['token']) {
                    $existingToken->delete();
                }
            }
        }

        return [];
    }
}