<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use pm\Firemon112\Classes\JWT\TokenDto;
use Illuminate\Http\Request;

/**
 *
 */
class RefreshController extends Controller
{
    /**
     * @return array
     */
    public function __invoke(Request $request): array
    {
        try {

            $tokenRefreshed = $this->JWTGuard->refresh(true); // we allow refresh for an unlimited time (we use changed pw to invalidate)
            $this->JWTGuard->setToken($tokenRefreshed);

            $user = $this->JWTGuard->user();

            if (empty($user)) {
                throw new \Exception('Login failed');
            }

            // If the token does not have the pwh claim that was introduced in april 2024 - add it
            if (!$this->JWTGuard->getPayload()->hasKey('pwh')) {
                $this->JWTGuard->claims([
                    'pwh'=>$user->jwt_pwh
                ]);
                $tokenRefreshed = $this->JWTGuard->refresh(true);
            }

            if ($user->allow_app_usage != 1) {
                throw new \Exception('App Nutzung nicht erlaubt!');
            }

            $tokenDto = new TokenDto([
                'token' => $tokenRefreshed,
                'expires' => Carbon::createFromTimestamp($this->JWTGuard->getPayload()->get('exp')),
                'user' => $user
            ]);

            return ['data' => $tokenDto->toArray()];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    }
}