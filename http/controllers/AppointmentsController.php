<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\AppointmentsRequest;
use pm\Firemon112\Models\Event;
use pm\Firemon112\Models\Helper;
use RainLab\User\Models\User;

/**
 *
 */
class AppointmentsController extends Controller
{
    private static $usersubTemplate = [
        "name" => "unknown user",
        "performed" => "",
        "note" => "",
        "state" => "no_feedback"
    ];

    /**
     * @return array
     */
    public function __invoke(AppointmentsRequest $request): array
    {
       try {
            $data = $request->validated();
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $appointments = [];

                $year = 0;
                if (array_key_exists('year', $data)) {
                    $year = intval($data['year']);
                }

                $month = 0;
                if (array_key_exists('month', $data)) {
                    $month = intval($data['month']);
                }

                $appointment_id = 0;
                if (array_key_exists('appointment_id', $data)) {
                    $appointment_id = intval($data['appointment_id']);
                }

                $include_sub_details = false;
                if (array_key_exists('include_sub_details', $data)) {
                    $include_sub_details = boolval($data['include_sub_details']);
                }

                $station_id = 0;
                if (array_key_exists('station_id', $data)) {
                    $linked_station = $user->linked_stations()->find(intval($data['station_id']));
                    if ($linked_station != null) {
                        $station_id = $linked_station->id;
                    }
                }
                if ($station_id <= 0) {
                    $station_id = $user->station->id;
                }

                if ($appointment_id > 0) {
                    $relatedEvents = $user->getRelatedEventsRelation($station_id)->byId($appointment_id)->get();
                } else {
                    if ($month > 0) {
                        $relatedEvents = $user->getRelatedEventsRelation($station_id)->byYear($year)->byMonth($month)->orderBy('date_from', 'ASC')->get();
                    } else {
                        $relatedEvents = $user->getRelatedEventsRelation($station_id)->byYear($year)->orderBy('date_from', 'ASC')->get();
                    }
                }

                $userCanSeeSubscriptions = Helper::userHasMissionOperator($user, $station_id) || Helper::userHasPermission($user, $station_id, Helper::$PERMISSION_IS_ROLE_EVENTS);

                foreach ($relatedEvents as $event) {
                    $item = $event->asInformationReducedArray();
                    $item['sub'] = [];
                    $subs = [
                        "count_available" => 0,
                        "count_not_available" => 0,
                    ];
                    if ($include_sub_details) {
                        $subs["available"] = [];
                        $subs["not_available"] = [];
                        $subs["no_feedback"] = [];
                    }
                    $subs_userids = [];
                    foreach ($event->subscriptions as $sub) {
                        if ($sub->user_id == $user->id) {
                            $item['sub'] = $sub;
                        }
                        if ($userCanSeeSubscriptions) {
                            if ($sub->available) {
                                $subs["count_available"]++;
                                if ($include_sub_details) {
                                    $subs["available"][] = $this->buildUserSubArray($sub);
                                }
                            } else {
                                $subs["count_not_available"]++;
                                if ($include_sub_details) {
                                    $subs["not_available"][] = $this->buildUserSubArray($sub);
                                }
                            }
                            $subs_userids[] = $sub['user_id'];
                        }
                    }

                    if ($userCanSeeSubscriptions) {
                        // fill in users with no feedback from group
                        if ($include_sub_details) {
                            foreach ($event->related_user as $event_user) {
                                if (!in_array($event_user->id, $subs_userids)) {
                                    $userSubNoFeedback = self::$usersubTemplate;
                                    $userSubNoFeedback["name"] = $event_user->name . " " . $event_user->surname;
                                    $subs["no_feedback"][] = $userSubNoFeedback;
                                }
                            }
                            usort($subs["available"], function($a, $b) {return strcmp($a["name"], $b["name"]);});
                            usort($subs["not_available"], function($a, $b) {return strcmp($a["name"], $b["name"]);});
                            usort($subs["no_feedback"], function($a, $b) {return strcmp($a["name"], $b["name"]);});
                        }

                        $item['subs'] = $subs;
                    }
                    $appointments[] = $item;
                }
                return ['success' => true, 'data' => $appointments];
            }
            return ['success' => false, 'data' => [], 'error' => 'User not found'];
       } catch (\Exception $e) {
           \Log::alert($e->getMessage());
           return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
       }
    }

    function buildUserSubArray($sub) {
        $res = self::$usersubTemplate;
        if ($sub != null) {
            $user = User::find($sub->user_id);
            if ($user != null) {
                $res["name"] = $user->name." ".$user->surname;
                $res["performed"] = $sub->performed;
                if ($sub->reason != null) {
                    $res["note"] = $sub->reason;
                }
                $res["state"] = $sub->available ? "available" : "not_available";
            }
        }
        return $res;
    }
}