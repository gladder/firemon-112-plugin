<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\TestPushtokenRequest;
use pm\Firemon112\Models\AlertDispatchJob;
use pm\Firemon112\Models\Pushtoken;

/**
 *
 */
class TestPushtokenController extends Controller
{
    /**
     * @return array
     */
    public function __invoke(TestPushtokenRequest $request): array
    {
        try {
            $data = $request->validated();
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $token = $user->pushtokens()->find(intval($data['token_id']));
                $delay_minutes = 0;
                if (array_key_exists('delay_minutes', $data)) {
                    $delay_minutes = min(intval($data['delay_minutes']),60);
                }
                if ($token != null) {
                    $d = AlertDispatchJob::create([
                        'alert_id' => 0,
                        'dispatch_type' => AlertDispatchJob::$type_PUSH,
                        'payload' => [
                            'origin' => $user->station->token, // TODO: make dynamic once we have different sounds for different stations
                            'reason' => "TEST-ALARM",
                            'message' => "Dies ist eine Test-Alarmierung...",
                            'data' => ["screen" => 'Alerts'], //["screen" => "AlertDetails", "alert" => $alert->reducedArray]
                            'recipients' => [$token->token],
                            'token_type' => $token->token_type,
                            'topic' => Pushtoken::$TOKEN_TOPIC_ALERT
                        ],
                        'dry_run' => false
                    ]);
                    if ($delay_minutes > 0) {
                        $when = Carbon::now('Europe/Berlin')->addMinutes($delay_minutes);
                        \Queue::later($when, '\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'low');
                    } else {
                        \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'low');
                    }
                    return ['success' => true, 'data' => []];
                }
            }
            return ['success' => false, 'data' => [], 'error' => 'User not found'];
        } catch (\Exception $e) {
            \Log::alert("Exception:" . $e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    }
}