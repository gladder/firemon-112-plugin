<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\AlertFeedbackRequest;
use pm\Firemon112\Models\AlertUserFeedback;

/**
 *
 */
class AlertFeedbackController extends Controller
{
    /**
     * @return array
     */
    public function __invoke(AlertFeedbackRequest $request): array
    {
       try {
            $data = $request->validated();
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $alert = $user->alerts()->find($data['alert_id']);
                if ($alert != null) {
                    $alert_feedback = $user->alert_feedbacks()->where('alert_id', $data['alert_id'])->first();

                    if ($data['feedback'] > 0) {
                        if (!in_array($data['feedback'], array_keys(AlertUserFeedback::getFeedbackStatusOptions()))) {
                            throw new Exception("Es ist ein Fehler aufgetreten (0xE000)");
                        }
                        if ($alert_feedback == null) {
                            $alert_feedback = new AlertUserFeedback();
                            $alert_feedback->user_id = $user->id;
                            $alert_feedback->alert_id = $alert->id;
                        }
                        $alert_feedback->feedback_status = $data['feedback'];
                        $alert_feedback->feedback_performed = Carbon::now('Europe/Berlin');
                        $alert_feedback->save();
                    }
                    return ['success' => true, 'data' => []];
                }
                return ['success' => false, 'data' => [], 'error' => 'Alert not found'];
            }
            return ['success' => false, 'data' => [], 'error' => 'User not found'];
       } catch (\Exception $e) {
           \Log::alert($e->getMessage());
           return ['success' => false, 'error' => $e->getMessage()];
       }
    }
}