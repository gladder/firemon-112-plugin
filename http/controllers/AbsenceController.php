<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\AbsenceRequest;
use pm\Firemon112\Models\Absence;

/**
 *
 */
class AbsenceController extends Controller
{
    /**
     * @return array
     */
    public function upsert(AbsenceRequest $absenceRequest): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }
            if ($absenceRequest->user_id != $user->id) {
                throw new \Exception("User-ID missmatch");
            }
            $absence = $user->absences->find($absenceRequest->id);
            if ($absence === null) {
                $absence = new Absence();
            }

            $absence->fill([
                'absence_type' => $absenceRequest->absence_type,
                'user_id' => $absenceRequest->user_id,
                'valid_from' => $absenceRequest->valid_from,
                'valid_to' => $absenceRequest->valid_to,
                'auto_cancel_events' => $absenceRequest->auto_cancel_events,
            ]);
            $absence->save();
            return ['success' => true, 'data' => $absence];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    }

    public function list(): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }
            return ['success' => true, 'data' => $user->absences];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    }

    public function delete(AbsenceRequest $absenceRequest): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }
            if ($absenceRequest->id <= 0) {
                throw new \Exception("Absence Request does not contain valid id");
            }
            if ($absenceRequest->user_id != $user->id) {
                throw new \Exception("User-ID missmatch");
            }
            $absence = $user->absences->find($absenceRequest->id);
            if ($absence === null) {
                throw new \Exception("Absence Entity not found");
            }
            $absence->delete();
            return ['success' => true];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }

    public function options(): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }
            return ['success' => true, 'data' => ['absence_type_options' => Absence::getAbsenceTypeOptions()]];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    }
}