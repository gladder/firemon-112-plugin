<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\AccountUpdateRequest;
use pm\Firemon112\Http\Requests\AccountUpdatePasswordRequest;

/**
 *
 */
class AccountController extends Controller
{
    /**
     * @return array
     */
    public function update(AccountUpdateRequest $accountUpdateRequest): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }

            $data = $accountUpdateRequest->validated();

            if (strlen($data['name'])<=0) {
                throw new \Exception("Vorname fehlt");
            } else {
                $user->name = trim($data['name']);
            }

            if (strlen($data['surname'])<=0) {
                throw new \Exception("Nachname fehlt");
            } else {
                $user->surname = trim($data['surname']);
            }

            if (strlen($data['email'])<=0) {
                throw new \Exception("E-Mail fehlt");
            } else {
                $user->email = trim($data['email']);
            }

            if (array_key_exists('alert_sms', $data)) {
                $user->alert_sms = trim($data['alert_sms']);
            }

            if (array_key_exists('alert_email', $data)) {
                $user->alert_email = trim($data['alert_email']);
            }

            if (array_key_exists('alert_call', $data)) {
                $user->alert_call = trim($data['alert_call']);
            }

            if (array_key_exists('alert_call_second', $data)) {
                $user->alert_call_second = trim($data['alert_call_second']);
            }

            $user->save();
            return ['success' => true];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }

    public function updatePassword(AccountUpdatePasswordRequest $accountUpdatePasswordRequest): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }

            $data = $accountUpdatePasswordRequest->validated();

            if (strlen($data['password'])<=0) {
                throw new \Exception("Passwort ist leer");
            } else {
                $user->password = trim($data['password']);
            }

            if (strlen($data['password_confirmation'])<=0) {
                throw new \Exception("Passwörter stimmen nicht überein");
            } else {
                $user->password_confirmation = trim($data['password_confirmation']);
            }

            $user->save();
            return ['success' => true];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }

    public function select(): array
    {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            if ($user === null) {
                throw new \Exception("User not authenticated");
            }
            return ['user' => $user];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false,  'user' => null, 'error' => $e->getMessage()];
        }
    }

}