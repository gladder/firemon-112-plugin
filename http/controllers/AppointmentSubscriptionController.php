<?php
declare(strict_types=1);

namespace pm\Firemon112\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use pm\Firemon112\Http\Requests\AppointmentSubscriptionRequest;
use pm\Firemon112\Models\EventSubscription;
use pm\Firemon112\Models\Event;

/**
 *
 */
class AppointmentSubscriptionController extends Controller
{
    /**
     * @return array
     */
    public function __invoke(AppointmentSubscriptionRequest $request): array
    {
       try {
            $data = $request->validated();
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $event = Event::find($data['event_id']); // this should be more secure - check if event is connected with station or linked station
                $sub = null;
                foreach ($event->subscriptions as $thissub) {
                    if ($thissub->user_id == $user->id) {
                        $sub = $thissub;
                        break;
                    }
                }
                if ($sub == null) {
                    $sub = new EventSubscription([
                        'event_id' => $data['event_id'],
                        'user_id' => $user->id,
                        'available' => intval($data['available']),
                        'performed' => Carbon::now('Europe/Berlin'),
                        'reason' => "" // not yet supported
                    ]);
                } else {
                    $sub->available = intval($data['available']);
                    $sub->performed = Carbon::now('Europe/Berlin');
                    $sub->reason = ""; // not yet supported
                }
                $sub->save();
                return ['success' => true];
            }
            return ['success' => false, 'error' => 'User not found'];
       } catch (\Exception $e) {
           \Log::alert($e->getMessage());
           return ['success' => false, 'error' => $e->getMessage()];
       }
    }
}