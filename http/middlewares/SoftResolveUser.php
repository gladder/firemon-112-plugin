<?php
declare(strict_types=1);
namespace pm\Firemon112\Http\Middlewares;

use pm\Firemon112\Classes\JWT\JWTGuard;

/**
 * Class SoftResolveUser
 * @package pm\Firemon112\Http\Middlewares
 */
class SoftResolveUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($obRequest, \Closure $next)
    {
        try {
            /** @var JWTGuard $obJWTGuard */
            $obJWTGuard = app('JWTGuard');
            $obJWTGuard->user();
        } catch (\Exception $ex) {}

        return $next($obRequest);
    }
}