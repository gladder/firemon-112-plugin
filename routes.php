<?php
use Illuminate\Http\Request;
use pm\Firemon112\Models\Helper;
use pm\Firemon112\Models\News;
use pm\Firemon112\Models\AlertUserFeedback;

Route::group([
    'prefix' => 'jwt',
], static function () {
    Route::post('absence', [pm\Firemon112\Http\Controllers\AbsenceController::class, 'upsert']);
    Route::get('absence', [pm\Firemon112\Http\Controllers\AbsenceController::class, 'list']);
    Route::delete('absence', [pm\Firemon112\Http\Controllers\AbsenceController::class, 'delete']);
    Route::options('absence', [pm\Firemon112\Http\Controllers\AbsenceController::class, 'options']);
    Route::post('account', [pm\Firemon112\Http\Controllers\AccountController::class, 'update']);
    Route::post('password', [pm\Firemon112\Http\Controllers\AccountController::class, 'updatePassword']);
    Route::get('account', [pm\Firemon112\Http\Controllers\AccountController::class, 'select']);
    Route::post('login', pm\Firemon112\Http\Controllers\AuthController::class)->middleware('throttle:5,1');
    Route::post('smslogin', pm\Firemon112\Http\Controllers\SmsAuthController::class);
    Route::post('refresh', pm\Firemon112\Http\Controllers\RefreshController::class);
    Route::post('register', pm\Firemon112\Http\Controllers\RegistrationController::class);
    Route::post('activate', pm\Firemon112\Http\Controllers\ActivationController::class);
    Route::post('pushtoken', [pm\Firemon112\Http\Controllers\PushtokenController::class, 'update']);
    Route::delete('pushtoken', [pm\Firemon112\Http\Controllers\PushtokenController::class, 'delete']);
    Route::post('test-pushtoken', pm\Firemon112\Http\Controllers\TestPushtokenController::class);
    Route::post('appointments', pm\Firemon112\Http\Controllers\AppointmentsController::class);
    Route::post('appointment-subscribe', pm\Firemon112\Http\Controllers\AppointmentSubscriptionController::class);
    Route::post('alert-feedback', pm\Firemon112\Http\Controllers\AlertFeedbackController::class);
    Route::post('register', pm\Firemon112\Http\Controllers\RegistrationController::class);
    Route::get('pushtoken', function() {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $tokens = [];
                foreach ($user->pushtokens as $token) {
                    $tokens[] = $token;
                }
                return ['success' => true, 'data' => $tokens];
            }
            return ['success' => true, 'data' => []];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    });
    Route::get('recent-alerts', function() {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $alerts = [];
                foreach ($user->alerts()->lastMonths(6)->limit(12)->get() as $alert) {
                    $alertArr = $alert->asInformationReducedArray();
                    $alertArr['feedback'] = [];
                    $alertArr['feedback_summary'] = $alert->getAlertFeedback(false, false);
                    $alert_feedback = $user->alert_feedbacks()->where('alert_id', $alert->id)->first();
                    if ($alert_feedback != null) {
                        $alertArr['feedback'] = $alert_feedback->toArray();
                    }
                    $alerted_station = $alert->getAlertedStation($user);
                    $alertArr['mission_admin'] = Helper::userHasMissionAdmin($user, $alerted_station->id);
                    $alertArr['mission_operator'] = Helper::userHasMissionOperator($user, $alerted_station->id);

                    $alerts[] = $alertArr;
                }
                return ['success' => true, 'data' => $alerts];
            }
            return ['success' => true, 'data' => []];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    });

    Route::get('alert-groups', function() {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                $alertgroups = [];
                foreach ($user->related_alert_groups->sortByDesc('prio') as $alertgroup) {
                    $alertgroups[] = $alertgroup;
                }
                return ['success' => true, 'data' => $alertgroups];
            }
            return ['success' => true, 'data' => []];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'data' => [], 'error' => $e->getMessage()];
        }
    });

    Route::post('delete-account', function() {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null) {
                return [
                    'success' => Helper::DeleteUser($user)
                ];
            }
            return ['success' => false];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'error' => $e->getMessage()];
        }
    });

    Route::get('news', function(Request $request) {
        try {
            $guard = app('JWTGuard');
            $user = $guard->user();
            $station_id = intval($request->query('station_id'));

            if ($station_id <= 0) {
                $station_id = $user->station_id;
            }

            if ($user != null) {
                return ['success' => true, 'data' => News::byStation($station_id)->with('image')->active()->get()];
            }
            return ['success' => true, 'data' => []];
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return ['success' => false, 'error' => $e->getMessage()];
        }
    });
});