<?php namespace pm\Firemon112\Traits;

use pm\Firemon112\Models\Helper;
use Carbon\Carbon;

trait DateTimeExtension
{
    public function getFormattedDateTime($dt) {
        if ($dt !== null) {
            $d = new Carbon($dt);
            return $d->format(Helper::GetDefaultDateTimeFormat());
        } else {
            return "";
        }
    }

    public function isNowBetween($from, $to) {
        $now = Carbon::now('Europe/Berlin');
        if ($from === null && $to === null) {
            return true;
        }
        if ($from !== null && $to === null) {
            return $now->isAfter($from);
        }
        if ($from === null && $to !== null) {
            return $now->isBefore($to);
        }
        if ($from !== null && $to !== null) {
            return $now->isAfter($from) && $now->isBefore($to);
        }
        return false;
    }
}