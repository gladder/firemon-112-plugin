<?php namespace pm\Firemon112\Traits;

use pm\Firemon112\Models\Helper;
use Input;
use System\Models\File;
trait SortOrderExtension
{
    function updateSortOrder($sortOrderParameterName)
    {
        if (strlen($sortOrderParameterName) <= 0) {
            throw new Exception('sortOrderParameterName nicht gefunden!');
        }
        $sortOrders = post($sortOrderParameterName);
        foreach ($sortOrders as $orderId => $sortOrder) {
            $file = $this->files()->where('id', $sortOrder)->first();
            $file->sort_order = $orderId;
            $file->save();
        }

    }
}