<?php namespace pm\Firemon112\Traits;

use pm\Firemon112\Models\Helper;
use Input;
use System\Models\File;
trait FileUploadExtension
{
    function uploadFile($uploadFileParameterName) {
        if (strlen($uploadFileParameterName) <= 0) {
            throw new Exception('uploadFileParameterName nicht gefunden!');
        }
        $files = Input::file($uploadFileParameterName);
        if (is_array($files)) {
            foreach ($files as $file) {
                $f = new File();
                $f->data = $file;
                $f->is_public = true;
                $f->save();
                $this->files()->add($f);
                $this->save();
            }
        }
    }

    function deleteFile($fileId) {
        if ($fileId <= 0) {
            throw new Exception('File nicht gefunden!');
        }
        $delete_file = null;
        foreach ($this->files as $file) {
            if ($file->id == $fileId) {
                $delete_file = $file;
            }
        }
        if ($delete_file != null) {
            $this->files()->remove($delete_file);
            $delete_file->forceDelete();
        }
    }

    function updateFileTitle($fileId, $uploadTitleParameterName) {
        if ($fileId <= 0) {
            throw new Exception('File nicht gefunden!');
        }
        if (strlen($uploadTitleParameterName) <= 0) {
            throw new Exception('uploadTitleParameterName nicht gefunden!');
        }
        $title = post($uploadTitleParameterName);
        $relatedFile = null;
        foreach ($this->files as $file) {
            if ($file->id == $fileId) {
                $file->title = $title;
                $file->save();
                $relatedFile = $file;
            }
        }
        return $relatedFile;
    }
}