<?php namespace pm\Firemon112\Traits;

use pm\Firemon112\Models\Contact;
use pm\Firemon112\Models\Helper;
trait ContactableExtension
{
    function saveContact($contactId, $fill) {
        $contact = $this->contacts->find($contactId);
        if ($contact == null) {
            $contact = Contact::create($fill);
            $this->contacts()->add($contact);
            $this->save();
        } else {
            $contact->fill($fill);
            $contact->save();
        }
        return $contact;
    }

    function createContact() {
        $contact = Contact::create(['label'=>'Neuer Kontakt']);
        $this->contacts()->add($contact);
        $this->save();
    }

    function deleteContact($contactId) {
        $delete_contact = $this->contacts->find($contactId);
        if ($delete_contact != null) {
            $this->contacts()->remove($delete_contact);
            $delete_contact->forceDelete();
        }
    }
}