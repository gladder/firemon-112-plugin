<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Depesche;
use Pm\Firemon112\Models\Leitstelle;
use pm\Firemon112\Models\Station;
use pm\Firemon112\Models\Stichwort;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use pm\Firemon112\Models\Alert;
use pm\Firemon112\Models\Helper;

class HandleDepesche extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:depesche:handle';

    /**
     * @var string The console command description.
     */
    protected $description = 'Parse Depesche String to object';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $stationRaw = $this->option('station');
            $depescheRaw = $this->option('depesche');

            $station = Station::where('token', '=', $stationRaw)->first();
            if ($station === null) {
                throw new \Exception("Station " . $stationRaw . " is not found!!! Alert stopped.");
            }
            $depesche = Depesche::fromArray($this->parseDepesche($depescheRaw, $station->leitstelle->mail_algo));
            // only debug $this->info(print_r($depesche, true));
            if ($depesche->isPlausible()) {
                $this->handleAlert($depesche, $station);
                // check if there is stationswehren where alert needs to be proxied to
                foreach ($station->child_stations as $child_station) {
                    if ($child_station->GetIsStandortAttribute()) {
                        $this->handleAlert($depesche, $child_station);
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while handling Alert for: ' . $stationRaw . ' with depesche ' . $depescheRaw);
            $this->error('Exception while handling Alert for: ' . $stationRaw . ' with depesche ' . $depescheRaw);
            trace_log($e);
        }
    }

    private function handleAlert($depesche, $station) {
        $alert = Alert::HandleCreateAlertForStation($depesche, $station);
        if ($alert !== null) {
            $this->info('Depesche saved: ' . $alert->id);
        } else {
            $this->info('Depesche coult not be saved');
        }
    }

    private function parseDepesche($depesche, $algo) {
        $arr = array();
        switch ($algo) {
            case Leitstelle::$algo_NK_KRLS_NORD_1:
                $arr = $this->algo_NK_KRLS_NORD_1($depesche);
                break;
            case Leitstelle::$algo_WK_KRLS_WEST_1:
                $arr = $this->algo_WK_KRLS_WEST_1($depesche);
                break;
            default:
                $arr = $this->algo_NK_KRLS_NORD_1($depesche);
        }
        return $arr;
    }

    private function algo_NK_KRLS_NORD_1($depesche) {
        $arr = array();
        $arr['Einsatznummer'] = Helper::extractString('/^allgemeine Einsatznummer:(.*)$/m', $depesche, 1);
        $arr['Einsatzstichwort'] = Helper::extractString('/^Einsatzstichwort:(.*)$/m', $depesche, 1);
        $arr['Einsatzbeginn'] = Helper::extractString('/^Einsatzbeginn:(.*)$/m', $depesche, 1);
        $arr['Klartext'] = Helper::extractString('/^\(Klartext\)(.*)$/m', $depesche, 1);
        $arr['Information'] = Helper::extractString('/^Information:(.*)$/m', $depesche, 1);
        $arr['Einsatzobjekt'] = Helper::extractString('/^Objekt:(.*)$/m', $depesche, 1);
        $arr['Strasse'] = Helper::extractString('/^Strasse\/Nr.:(.*)$/m', $depesche, 1);
        $arr['Plan'] = Helper::extractString('/^Plan-Nr.:(.*)$/m', $depesche, 1);
        $arr['Patient'] = Helper::extractString('/^\s*Patient:(.*)$/m', $depesche, 1);
        $arr['Prio'] = intval(Helper::extractString('/^\s*Prio:(.*)$/m', $depesche, 1));
        $ort_otrsteil = Helper::extractString('/^Ort\/Ortsteil:(.*)$/m', $depesche, 1);
        if (strpos($ort_otrsteil, "/") !== false ) {
            $parts = explode("/", $ort_otrsteil);
            if (count($parts) >= 1) {
                $arr['Ort'] = $parts[0];
            }
            if (count($parts) >= 2) {
                $arr['Ortsteil'] = $parts[1];
            }
        } else {
            $arr['Ort'] = $ort_otrsteil;
            $arr['Ortsteil'] = "";
        }
        $xy = Helper::extractString('/^X-\/Y-Koordinate:(.*)$/m', $depesche, 1);
        if (strpos($xy, "/") !== false ) {
            $parts = explode("/", $xy);
            if (count($parts) >= 1) {
                $arr['X'] = floatval(trim($parts[0]));
            }
            if (count($parts) >= 2) {
                $arr['Y'] = floatval(trim($parts[1]));
            }
            $LL = $this->utm2geo($arr['X'], $arr['Y'], 32);
            $arr['Lat'] = $LL[0];
            $arr['Long'] = $LL[1];
        } else {
            $arr['X'] = 0.0;
            $arr['Y'] = 0.0;
            $arr['Lat'] = 0.0;
            $arr['Long'] = 0.0;
        }

        $arr['MitalarmierteKraefte'] = array();

        $maKraefteRaw = Helper::extractString('/Alarm(.*)---------------------------------------------------------------------------(.*)---------------------------------------------------------------------------/s', $depesche, 2);
        $maKraefteRawItems = explode("\n", $maKraefteRaw);
        if (is_array($maKraefteRawItems)) {
            foreach ($maKraefteRawItems as $itemRaw) {
                $item = trim($itemRaw);
                if (strlen($item)>0) {
                    $arr['MitalarmierteKraefte'][] = $item;
                }
            }
        }

        return $arr;
    }

    private function algo_WK_KRLS_WEST_1($depesche) {
        $arr = array();
        $arr['Einsatznummer'] = Helper::extractString('/^Einsatznr:(.*)$/m', $depesche, 1);
        $arr['Einsatzstichwort'] = Helper::extractString('/^EinsatzSzenario:(.*)$/m', $depesche, 1);
        $arr['Einsatzbeginn'] = Helper::extractString('/^Alarmzeit:(.*)$/m', $depesche, 1);
        $sw = Stichwort::where('stichwort', $arr['Einsatzstichwort'])->first();
        if ($sw !== null) {
            $arr['Klartext'] = $sw->klartext;
        } else {
            $arr['Klartext'] = "Stichwort unklar!";
        }

        $arr['Information'] = Helper::extractString('/^EinsatzInfo:(.*)$/m', $depesche, 1);

        $arr['Einsatzobjekt'] = Helper::extractString('/^EO-Objekt:(.*)$/m', $depesche, 1);
        $tmpObjAbt = Helper::extractString('/^EO-ObjektAbt:(.*)$/m', $depesche, 1);
        if (strlen($tmpObjAbt)>0) {
            $arr['Einsatzobjekt'] .= " (".$tmpObjAbt.")";
        }

        $tmpStr = Helper::extractString('/^EO-Str:(.*)$/m', $depesche, 1);
        $tmpStrNr = Helper::extractString('/^EO-HausNr:(.*)$/m', $depesche, 1);
        $tmpStrAb = Helper::extractString('/^EO-StrAbschnitt:(.*)$/m', $depesche, 1);
        $arr['Strasse'] = $tmpStr;
        if (strlen($tmpStrNr)>0) {
            $arr['Strasse'] .= " ".$tmpStrNr;
        }
        if (strlen($tmpStrAb)>0) {
            $arr['Strasse'] .= " (".$tmpStrAb.")";
        }

        $arr['Plan'] = Helper::extractString('/^EO-ObjektPlanNr:(.*)$/m', $depesche, 1);

        $arr['Ort'] = Helper::extractString('/^EO-Ort:(.*)$/m', $depesche, 1);
        $arr['Ortsteil'] = Helper::extractString('/^EO-Ortsteil:(.*)$/m', $depesche, 1);

        $tmpEoZusatz = Helper::extractString('/^EO-Zusatzinfo:(.*)$/m', $depesche, 1);
        if (strlen($tmpEoZusatz)>0) {
            if (strlen($arr['Ortsteil'])>0) {
                $arr['Ortsteil'] .= " ";
            }
            $arr['Ortsteil'] .= $tmpEoZusatz;
        }

        $tmpLong = Helper::extractString('/^EO-KoordX:(.*)$/m', $depesche, 1);
        $tmpLat = Helper::extractString('/^EO-KoordY:(.*)$/m', $depesche, 1);

        $arr['Lat'] = floatval(trim($tmpLat));
        $arr['Long'] = floatval(trim($tmpLong));

        $arr['MitalarmierteKraefte'] = array();

        $maKraefteRaw = Helper::extractString('/AlarmierteEinheitenStart(.*)AlarmierteEinheitenEnde/s', $depesche, 1);
        $maKraefteRawItems = explode("\n", $maKraefteRaw);
        if (is_array($maKraefteRawItems)) {
            foreach ($maKraefteRawItems as $itemRaw) {
                $item = trim($itemRaw);
                if (strlen($item)>0) {
                    $arr['MitalarmierteKraefte'][] = $item;
                }
            }
        }

        return $arr;
    }

    private function utm2geo($ew, $nw, $zone) {

       // Längenzone zone, Ostwert ew und Nordwert nw im WGS84 Datum

       $band = substr($zone,2,1);
       $z1 = intval($zone);
       $ew1 = intval($ew);
       $nw1 = intval($nw);

       //WGS 84 Datum
       //Große Halbachse a und Abplattung f
       $a = 6378137.000;
       $f = 3.35281068e-3;

       //Polkrümmungshalbmesser c
       $c = $a/(1-$f);

       //Quadrat der zweiten numerischen Exzentrizität
       $ex2 = (2*$f-$f*$f)/((1-$f)*(1-$f));
       $ex4 = $ex2*$ex2;
       $ex6 = $ex4*$ex2;
       $ex8 = $ex4*$ex4;

       //Koeffizienten zur Berechnung der geographischen Breite aus gegebener
       //Meridianbogenlänge
       $e0 = $c*(pi()/180)*(1 - 3*$ex2/4 + 45*$ex4/64 - 175*$ex6/256 + 11025*$ex8/16384);
       $f2 =    (180/pi())*(    3*$ex2/8 - 3*$ex4/16  + 213*$ex6/2048 -  255*$ex8/4096);
       $f4 =               (180/pi())*(   21*$ex4/256 -  21*$ex6/256  +  533*$ex8/8192);
       $f6 =                             (180/pi())*(   151*$ex6/6144 -  453*$ex8/12288);

       //Entscheidung Nord-/Süd Halbkugel
       if ($band >= "N"|| $band == "")
          $m_nw = $nw1;
       else
          $m_nw = $nw1 - 10e6;

       //Geographische Breite bf zur Meridianbogenlänge gf = m_nw
       $sigma = ($m_nw/0.9996)/$e0;
       $sigmr = $sigma*pi()/180;
       $bf = $sigma + $f2*sin(2*$sigmr) + $f4*sin(4*$sigmr) + $f6*sin(6*$sigmr);

       //Breite bf in Radianten
       $br = $bf * pi()/180;
       $tan1 = tan($br);
       $tan2 = $tan1*$tan1;
       $tan4 = $tan2*$tan2;

       $cos1 = cos($br);
       $cos2 = $cos1*$cos1;

       $etasq = $ex2*$cos2;

       //Querkrümmungshalbmesser nd
       $nd = $c/sqrt(1 + $etasq);
       $nd2 = $nd*$nd;
       $nd4 = $nd2*$nd2;
       $nd6 = $nd4*$nd2;
       $nd3 = $nd2*$nd;
       $nd5 = $nd4*$nd;

       //Längendifferenz dl zum Bezugsmeridian lh
       $lh = ($z1 - 30)*6 - 3;
       $dy = ($ew1-500000)/0.9996;
       $dy2 = $dy*$dy;
       $dy4 = $dy2*$dy2;
       $dy3 = $dy2*$dy;
       $dy5 = $dy2*$dy3;
       $dy6 = $dy3*$dy3;

       $b2 = - $tan1*(1+$etasq)/(2*$nd2);
       $b4 =   $tan1*(5+3*$tan2+6*$etasq*(1-$tan2))/(24*$nd4);
       $b6 = - $tan1*(61+90*$tan2+45*$tan4)/(720*$nd6);

       $l1 =   1/($nd*$cos1);
       $l3 = - (1+2*$tan2+$etasq)/(6*$nd3*$cos1);
       $l5 =   (5+28*$tan2+24*$tan4)/(120*$nd5*$cos1);

       //Geographische Breite bw und Länge lw als Funktion von Ostwert ew
       // und Nordwert nw
       $bw = $bf + (180/pi()) * ($b2*$dy2 + $b4*$dy4 + $b6*$dy6);
       $lw = $lh + (180/pi()) * ($l1*$dy  + $l3*$dy3 + $l5*$dy5);
       return array($bw, $lw);
    }



    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['station', null, InputOption::VALUE_REQUIRED, 'Station Token', 'norderbrarup'],
            ['depesche', null, InputOption::VALUE_REQUIRED, 'Depesche String', 'json stuff'],
        ];
    }

}