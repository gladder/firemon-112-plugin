<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Carbon\Carbon;
use pm\Firemon112\Models\AlertDispatchJob;
use pm\Firemon112\Models\Device;
use pm\Firemon112\Models\DeviceError;
use pm\Firemon112\Models\Pushtoken;

class CleanupDatabase extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:database:cleanup';

    /**
     * @var string The console command description.
     */
    protected $description = 'Cleanup the waste!';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->info("Delete 30d old pending monitors");
        foreach (Device::state(Device::$STATE_PENDING)->get() as $device) {
            if ($device->last_update_minutes > 60*24*30) {
                // delete pending with 30d no update (remove completely! no soft-delete!)
                $this->info("delete id ".$device->id);
                if ($device->is_state_pending) {
                    $device->forceDelete();
                }
            }
        }

        $this->info("Delete 30d old device logs");
        foreach (DeviceError::outdated(Device::$STATE_PENDING)->get() as $log) {
            $this->info("delete id ".$log->id);
            $log->forceDelete();
        }

        $this->info("Delete expired push tokens");
        $delPushTokens = [];
        foreach (AlertDispatchJob::where('dispatched_datetime', '>', Carbon::now('Europe/Berlin')->subDays(8))->get() as $job) {
            $this->info("checking ".$job->id);
            if (array_key_exists('token_type', $job->payload)) {
                if ($job->payload['token_type'] == Pushtoken::$TOKEN_TYPE_EXPO) {
                    // handle expo
                    $this->info('handle Expo Response');
                    if (is_array($job->result) && count($job->result) > 0 && is_array($job->result[0]) && count($job->result[0]) > 0) {
                        foreach ($job->result[0] as $response) {
                            if (array_key_exists('status', $response) && $response['status'] == "error") {
                                if (array_key_exists('details', $response) && array_key_exists('error', $response['details'])) {
                                    if ($response['details']['error'] == "DeviceNotRegistered" && array_key_exists('expoPushToken', $response['details'])) {
                                        $delPushTokens[] = $response['details']['expoPushToken'];
                                        $this->warn('added expo token for deletion: ' . $response['details']['expoPushToken']);
                                    } else {
                                        $this->error("Unexpected Error in Push Job ".$job->id.": " . $response['details']['error']);
                                        \Log::error("Unexpected Error in Push Job ".$job->id.": " . $response['details']['error']);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($job->payload['token_type'] == Pushtoken::$TOKEN_TYPE_FCM) {
                    // handle fcm
                    $this->info('handle FCM Response');
                    if (is_array($job->result) && count($job->result) > 0) {
                        foreach ($job->result as $response) {
                            // TODO: rework log collection from fcm worker to make a more strict job here and not stringsearch and regex around
                            if (!is_array($response) and strpos($response, "Exception Client error") === 0) {
                                if (strpos($response, '"status": "NOT_FOUND"') !== false) {
                                    $pattern = '/\[token\] => ([\w\-\:\_]+)/';

                                    // Extract the token using preg_match
                                    preg_match($pattern, $response, $matches);

                                    // Check if a token was found
                                    if (!empty($matches[1])) {
                                        $token = $matches[1];
                                        $delPushTokens[] = $token;
                                        $this->warn('added fcm token for deletion: ' . $token);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($job->payload['token_type'] == Pushtoken::$TOKEN_TYPE_APN) {
                    // handle apn
                    $this->info('handle APN Response');
                    if (is_array($job->result) && count($job->result) > 0 && is_array($job->result[0]) and count($job->result[0]) > 0) {
                        foreach ($job->result[0] as $response) {
                            if (array_key_exists('statusCode', $response)) {
                                if ($response['statusCode'] != 200) {
                                    if ($response['statusCode'] == 400 || $response['statusCode'] == 410) {
                                        if (array_key_exists('token', $response)) {
                                            $delPushTokens[] = $response['token'];
                                            $this->warn('added apn token for deletion: ' . $response['token']);
                                        }
                                    } else {
                                        $this->error("Unexpected Error in Push Job ".$job->id.": " . $response['statusCode'] . "::" . $response['result']);
                                        \Log::error("Unexpected Error in Push Job ".$job->id.": " . $response['statusCode'] . "::" . $response['result']);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach (array_unique($delPushTokens) as $delToken) {
            $this->warn("now deleting " . $delToken);
            if (strlen($delToken)>0) {
                Pushtoken::where('token', $delToken)->delete();
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}