<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use pm\Firemon112\Models\Training;
use pm\Firemon112\Models\Helper;
use pm\Firemon112\Models\TrainingAppointment;
use Pm\Firemon112\Models\TrainingScrapeSource;
use Validator;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;
use Carbon\Carbon;
class TrainingScrape extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:training:scrape';

    /**
     * @var string The console command description.
     */
    protected $description = 'Scrape all sources for new / updated training data';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $scrapers = TrainingScrapeSource::active()->get();
            if (count($scrapers) > 0) {
                foreach ($scrapers as $scraper) {
                    switch ($scraper->scrape_algo) {
                        case TrainingScrapeSource::$algo_FOX112:
                            $this->scrape_FOX112($scraper);
                            break;
                        case TrainingScrapeSource::$algo_LFSSH:
                            $this->scrape_LFSSH($scraper);
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while Scraping Trainings');
            $this->error('Exception while Scraping Trainings' . $e);
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

    private $browser = null;
    private function scrape_FOX112($scraper) {
        $this->info('Scrape ' . $scraper->url);
        if ($this->browser === null) {
            $this->browser = new HttpBrowser(HttpClient::create(['verify_peer' => false, 'verify_host' => false]));
        }
        $crawler = $this->browser->request('GET', $scraper->url);
        usleep(500000); // sleep 500ms to prevent too many connections on webserver
        $rows = $crawler->filterXPath('//tr[@bgcolor="#EEEEEE"]');
        foreach ($rows as $row) {
            try {
                $data = [
                    'training_source_id' => $scraper->training_source_id,
                    'label' => "",
                    'from_date' => null,
                    'to_date' => null,
                    'note' => "",
                    'slots_available' => 0,
                    'identifier' => ""
                ];

                $values = $row->getElementsByTagName('td');

                if ($values->length >= 14) {

                    if (strpos($values[1]->textContent, '(') !== false) {
                        $data['label'] = Helper::extractString('/^(.*)\((.*)\)$/m', trim($values[1]->textContent), 1);
                        $data['note'] = Helper::extractString('/^(.*)\((.*)\)$/m', trim($values[1]->textContent), 2);
                    } else {
                        $data['label'] = trim($values[1]->textContent);
                    }

                    $data['identifier'] = trim($values[3]->textContent);
                    $data['from_date'] = Carbon::createFromFormat(Helper::GetDefaultDateFormat(), trim($values[5]->textContent), 'Europe/Berlin');
                    if (strlen($values[7]->textContent) > 0) {
                        $data['to_date'] = Carbon::createFromFormat(Helper::GetDefaultDateFormat(), trim($values[7]->textContent), 'Europe/Berlin');
                    } else {
                        $data['to_date'] = Carbon::createFromFormat(Helper::GetDefaultDateFormat(), trim($values[5]->textContent), 'Europe/Berlin');
                    }
                    $data['slots_available'] = intval($values[11]->textContent);

                    if (strlen($data['label']) > 0 && $data['training_source_id'] > 0) {
                        $data['hash'] = Training::hash($data);
                        $links = $values[13]->getElementsByTagName('a');
                        if (count($links) > 0 && strlen($links[0]->getAttribute('href'))> 0) {
                            parse_str($links[0]->getAttribute('href'), $output);
                            $id = 0;
                            if (array_key_exists('?lehrgangx', $output)) {
                                $id = intval($output['?lehrgangx']);
                            }
                            if ($id > 0) {
                                $data['hash'] = Training::hash($data['training_source_id'].'-'.$id);
                            }
                        }

                        $training = Training::firstOrNew(['hash' => $data['hash']]);

                        if ($training->slots_available != $data['slots_available'] || $training->id <= 0 || $training->appointments->count() <= 0) {
                            $training->fill($data);
                            $training->save();
                            $this->info('LG ' . $training->label . ' saved! ('.$training->hash.')');
                            if (count($links) > 0) {
                                try {
                                    $details_url = $links[0]->getAttribute('href');
                                    $details_crawler = $this->browser->request('GET', $scraper->url . $details_url);
                                    usleep(500000); // sleep 500ms to prevent too many connections on webserver
                                    $details_values = $details_crawler->filterXPath('//td[@colspan="13"]');
                                    TrainingAppointment::where('training_id', $training->id)->delete();
                                    foreach ($details_values as $details_value) {
                                        $d_date = Helper::extractString('/^(.*)Tag (.*) von(.*)$/m', $details_value->textContent, 2);
                                        $d_from = Helper::extractString('/^(.*)von (.*) bis(.*)$/m', $details_value->textContent, 2);
                                        if (strlen($d_from) <= 0) {
                                            $d_from = "00:00";
                                        }
                                        $d_to = Helper::extractString('/^(.*)bis (.*) Uhr(.*)$/m', $details_value->textContent, 2);
                                        if (strlen($d_to) <= 0) {
                                            $d_to = "23:59";
                                        }
                                        $d_note = Helper::extractString('/^(.*)Uhr,(.*)$/m', $details_value->textContent, 2);
                                        TrainingAppointment::create([
                                            'training_id' => $training->id,
                                            'from_datetime' => Carbon::createFromFormat(Helper::GetDefaultDateTimeFormat(), $d_date . ' ' . $d_from, 'Europe/Berlin'),
                                            'to_datetime' => Carbon::createFromFormat(Helper::GetDefaultDateTimeFormat(), $d_date . ' ' . $d_to, 'Europe/Berlin'),
                                            'note' => $d_note
                                        ]);
                                    }
                                } catch (\Exception $e) {
                                    \Log::alert('Exception while store Appointments');
                                    $this->error('Exception while store Appointments' . $e);
                                    trace_log($e);
                                }
                            }
                        } else {
                            $this->info('LG ' . $training->label . ' skipped - no changes');
                        }
                    }
                } else {
                    $this->error('skipped a row since less than 14 TD Elements available...');
                }
            } catch (\Exception $e) {
                \Log::alert('Exception while store Training');
                $this->error('Exception while store Training' . $e);
                trace_log($e);
            }
        }
    }

    private function scrape_LFSSH($scraper) {
        $this->info('Scrape ' . $scraper->url);
        if ($this->browser === null) {
            $this->browser = new HttpBrowser(HttpClient::create(['verify_peer' => false, 'verify_host' => false]));
        }
        $crawler = $this->browser->request('GET', $scraper->url);
        usleep(500000); // sleep 500ms to prevent too many connections on webserver
        $rows = $crawler->filterXPath('//table[1]/tr');
        $this->info('Scrape ' . count($rows));
        Training::where('training_source_id', $scraper->training_source_id)->delete(); // delete all
        foreach ($rows as $row) {
            try {
                $data = [
                    'training_source_id' => $scraper->training_source_id,
                    'label' => "",
                    'from_date' => null,
                    'to_date' => null,
                    'note' => "",
                    'slots_available' => 0,
                    'identifier' => ""
                ];

                $values = $row->getElementsByTagName('td');

                if ($values->length >= 3) {
                    $platz = trim($values[2]->textContent);
                    $data['label'] = trim($values[1]->textContent) . ' Platz ' . $platz;

                    $data['identifier'] = trim($values[0]->textContent);

                    $year = intval(substr($data['identifier'], -2)) + 2000;
                    $kw = intval(substr($data['identifier'],0,2));
                    $data['from_date'] = Carbon::now('Europe/Berlin')->year($year)->week($kw)->startOfWeek();
                    $data['to_date'] = Carbon::now('Europe/Berlin')->year($year)->week($kw)->endOfWeek()->previousWeekday();

                    $this->info('LG: ' . $data['identifier'] . ' ' . $data['from_date']. ' - ' . $data['to_date']);

                    $data['slots_available'] = 1;
                    $data['hash'] = Training::hash($data['identifier'].'-'.$platz);


                    $training = Training::create($data);
                    $this->info('LG ' . $training->label . ' saved!');

                    $diffDays = $data['from_date']->diffInDays($data['to_date']);
                    if ($diffDays > 0) {
                        // create appointments
                        try {
                            TrainingAppointment::where('training_id', $training->id)->delete();
                            for ($i = 0; $i <= $diffDays; $i++) {
                                TrainingAppointment::create([
                                    'training_id' => $training->id,
                                    'from_datetime' => Carbon::createFromFormat(Helper::GetDefaultDateTimeFormat(), $data['from_date']->format(Helper::GetDefaultDateFormat()) . ' ' . "00:00", 'Europe/Berlin'),
                                    'to_datetime' => Carbon::createFromFormat(Helper::GetDefaultDateTimeFormat(), $data['from_date']->format(Helper::GetDefaultDateFormat()) . ' ' . "23:59", 'Europe/Berlin'),
                                    'note' => 'Termine und Uhrzeiten zu '.$training->identifier.' <a href="https://www.lfs-sh.de/Inhalte/Lehrgaenge/Lehrgaenge2023.php" target="_blank">direkt bei der LFS-SH!</a>'
                                ]);
                                $data['from_date']->addDays(1);
                            }
                        } catch (\Exception $e) {
                            \Log::alert('Exception while store Appointments');
                            $this->error('Exception while store Appointments' . $e);
                            trace_log($e);
                        }
                    }
                } else {
                    $this->error('skipped a row since less than 3 TD Elements available...');
                }
            } catch (\Exception $e) {
                \Log::alert('Exception while store Training');
                $this->error('Exception while store Training' . $e);
                trace_log($e);
            }
        }
    }

}