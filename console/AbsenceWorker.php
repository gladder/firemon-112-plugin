<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Event;
use pm\Firemon112\Models\EventSubscription;
use Carbon\Carbon;
class AbsenceWorker extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:absence:worker';

    /**
     * @var string The console command description.
     */
    protected $description = 'Do Absence related stuff like unsubscribe from Events';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $events = Event::upcoming()->get();
            foreach ($events as $event) {
                $this->info("Handle {$event->label}");
                if (!$event->subscription_required) {
                    $this->info("Skip {$event->label} because no subscription required");
                    continue;
                }

                foreach ($event->related_user as $user) {
                    $user_absence = $user->absences()->overlapsWith($event->date_from, $event->date_to)->first();
                    if ($user_absence !== null) {
                        if ($event->subscriptions()->where('user_id', $user->id)->exists()) {
                            $this->info("skip because sub existing already " . $user->email);
                        } else {
                            EventSubscription::create([
                                'event_id' => $event->id,
                                'user_id' => $user->id,
                                'available' => 0,
                                'reason' => $user_absence->getAbsenceTypeReadableAttribute(),
                                'performed' => new Carbon('Europe/Berlin'),
                            ]);
                            $this->info("created absence-sub for " . $user->email);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while Event Reminder');
            $this->error('Exception while Event Reminder ' . $e);
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}