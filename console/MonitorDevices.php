<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Device;
use pm\Firemon112\Models\MailCapping;
use Validator;
use Mail;
class MonitorDevices extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:monitor:devices';

    /**
     * @var string The console command description.
     */
    protected $description = 'Check for Devices and alert if a device is not reachable';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $devices = Device::type(Device::$TYPE_MONITOR)->state(Device::$STATE_ACTIVE)->get();

            foreach ($devices as $device) {
                // Device needs watchdog, must be offline or wrong res and we only track devices for the first 24 hours.
                if ($device->watchdog && ($device->getIsOnlineStateError() || !$device->getIsDisplayResolutionOk()) && $device->getLastHeartbeatMinutesAttribute() < 60*24) {
                    $this->error($device->name . ' is unvailable! Sending Mail to Station '.$device->station->name.' if monitoring E-Mail available...');
                    if (strlen($device->station->monitoring_emails)>0) {
                        $recipients = explode(',', $device->station->monitoring_emails);
                        if (count($recipients) > 0) {
                            foreach ($recipients as $recipient) {
                                $validator = Validator::make(
                                    ['email' => $recipient],
                                    ['email' => 'email']
                                );
                                if ($validator->fails()) {
                                    \Log::error($recipient.' is not a valid monitoring email');
                                } else {
                                    if ($device->getIsOnlineStateError()) {
                                        if (MailCapping::ShouldSendMail($recipient, MailCapping::$MAILTYPE_MONITOR_OFFLINE, $device->token)) {
                                            Mail::send('pm.firemon112::mail.monitor-offline', array(
                                                'name' => $device->station->name,
                                                'monitor' => $device->name,
                                                'last_ping' => $device->getLastHeartbeatMinutesReadableAttribute()
                                            ), function ($message) use ($recipient) {
                                                $message->to($recipient);
                                            });
                                            MailCapping::DidSendMail($recipient, MailCapping::$MAILTYPE_MONITOR_OFFLINE, $device->token);
                                            \Log::info('sent Monitor Offline mail to ' . $recipient);
                                        } else {
                                            \Log::info('MAIL-Capped Monitor Offline mail to ' . $recipient);
                                        }
                                    }
                                    if (!$device->getIsDisplayResolutionOk()) {
                                        if (MailCapping::ShouldSendMail($recipient, MailCapping::$MAILTYPE_MONITOR_RESOLUTION, $device->token)) {
                                            Mail::send('pm.firemon112::mail.monitor-resolution', array(
                                                'name' => $device->station->name,
                                                'monitor' => $device->name,
                                                'res' => $device->running_version,
                                                'allowed_res' => Device::$WD_ALLOWED_RESOLUTIONS
                                            ), function ($message) use ($recipient) {
                                                $message->to($recipient);
                                            });
                                            MailCapping::DidSendMail($recipient, MailCapping::$MAILTYPE_MONITOR_RESOLUTION, $device->token);
                                            \Log::info('sent Monitor Resolution mail to ' . $recipient);
                                        } else {
                                            \Log::info('MAIL-Capped Monitor Resolution mail to ' . $recipient);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while Monitoring Devices');
            $this->error('Exception while Monitoring Devices ' . $e);
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}