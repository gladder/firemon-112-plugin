<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use pm\Firemon112\Models\Settings;
use Pm\Firemon112\Models\Station;
use Pm\Firemon112\Models\Mq;
use Auth;
use RainLab\User\Models\User;
use RainLab\User\Models\UserGroup;
use Symfony\Component\Console\Input\InputOption;
use pm\Firemon112\Models\Helper;

class FastaddStation extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:fastadd:station';

    /**
     * @var string The console command description.
     */
    protected $description = 'Add Station and MQ [and user] fast!';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $options = $this->option();
            if ($options['sToken'] === null) {
                throw new \Exception("Station Token is not set");
            }

            if ($options['sName'] === null) {
                throw new \Exception("Station Name is not set");
            }

            if ($options['sEmail'] === null) {
                throw new \Exception("Station E-Mail is not set");
            }

            if ($options['mqUser'] === null) {
                throw new \Exception("MQ Username is not set");
            }

            if ($options['mqPass'] === null) {
                throw new \Exception("MQ Password is not set");
            }

            if ($options['uCreate']) {
                if ($options['uEmail'] === null) {
                    throw new \Exception("User E-Mail is not set");
                }
            }

            $station = new Station();
            $station->name = $options['sName'];
            $station->token = $options['sToken'];
            $station->email = $options['sEmail'];
            $station->long = $options['sLong'];
            $station->lat = $options['sLat'];
            $station->station_type = $options['sStationType'];
            $station->leitstelle_id = $options['sLeitstelleId'];

            // Fill Defaults
            $station->diverafree_enabled = 0;
            $station->diverafree_accesskey = "";
            $station->monitoring_emails = "";
            $station->forward_depesche_emails = "";
            $station->parent_station_id = 0;
            $station->sms77io_key = "";
            $station->registration_pin = "";
            $station->alert_details_protected = 0;

            $station->save();
            $this->info("Created Station ".$station->name);

            $mq = new Mq();
            $mq->station_id = $station->id;
            $mq->user = $options['mqUser'];
            $mq->pass = $options['mqPass'];
            $mq->host = $options['mqHost'];
            $mq->vhost = $options['sToken'];
            $mq->port = $options['mqPort'];
            $mq->name = "MQ Config ".$station->name;
            $mq->save();
            $this->info("Created MQ ".$mq->name);

            if ($options['uCreate']) {
                if (strlen($options['uName']) > 0) {
                    $name = $options['uName'];
                } else {
                    $name = $options['sName'];
                }

                if (strlen($options['uSurname']) > 0) {
                    $surname = $options['uSurname'];
                } else {
                    $surname = "Admin";
                }

                $user = Helper::CreateFrontendUser($name, $surname, $options['uEmail'], $station->id, $options['uSendmail']);
                if ($user === null || $user->id <= 0) {
                    throw new \Exception("User coult not be created");
                } else {
                    // Add Admin Group
                    UserGroup::create([
                        'name' => 'Administrator',
                        'code' => md5(microtime(true)),
                        'is_mission_role' => 0,
                        'marker_color' => '',
                        'is_mission_admin' => 0,
                        'prio' => 0,
                        'station_id' => $station->id,
                        'is_mission_operator' => 0,
                        'short_name' => '',
                        'is_role_station' => 1,
                        'is_role_presence' => 1,
                        'is_role_alert' => 1,
                        'is_role_events' => 1
                    ]);

                    // Add WF Group
                    UserGroup::create([
                        'name' => 'Wehrführer',
                        'code' => md5(microtime(true)),
                        'is_mission_role' => 1,
                        'marker_color' => '#ff0000',
                        'is_mission_admin' => 1,
                        'prio' => 99,
                        'station_id' => $station->id,
                        'is_mission_operator' => 1,
                        'short_name' => 'WF',
                        'is_role_station' => 0,
                        'is_role_presence' => 0,
                        'is_role_alert' => 0,
                        'is_role_events' => 0
                    ]);

                    // Add GF Group
                    UserGroup::create([
                        'name' => 'Gruppenführer',
                        'code' => md5(microtime(true)),
                        'is_mission_role' => 1,
                        'marker_color' => '#0500d9',
                        'is_mission_admin' => 1,
                        'prio' => 98,
                        'station_id' => $station->id,
                        'is_mission_operator' => 1,
                        'short_name' => 'GF',
                        'is_role_station' => 0,
                        'is_role_presence' => 0,
                        'is_role_alert' => 0,
                        'is_role_events' => 0
                    ]);

                    // Add AGT Group
                    UserGroup::create([
                        'name' => 'Atemschutz',
                        'code' => md5(microtime(true)),
                        'is_mission_role' => 1,
                        'marker_color' => '#ffa900',
                        'is_mission_admin' => 0,
                        'prio' => 97,
                        'station_id' => $station->id,
                        'is_mission_operator' => 0,
                        'short_name' => 'AGT',
                        'is_role_station' => 0,
                        'is_role_presence' => 0,
                        'is_role_alert' => 0,
                        'is_role_events' => 0
                    ]);

                    // Add Einsatzabteilung Group
                    UserGroup::create([
                        'name' => 'Einsatzabteilung',
                        'code' => md5(microtime(true)),
                        'is_mission_role' => 0,
                        'marker_color' => '',
                        'is_mission_admin' => 0,
                        'prio' => 0,
                        'station_id' => $station->id,
                        'is_mission_operator' => 0,
                        'short_name' => 'AGT',
                        'is_role_station' => 0,
                        'is_role_presence' => 0,
                        'is_role_alert' => 0,
                        'is_role_events' => 0
                    ]);


                    // Link user to all Groups
                    $group_ids = $station->groups()->get()->lists('id');
                    $user->groups()->sync($group_ids);
                    $user->save();
                    $this->info("Created User ".$user->name);
                }
            }
            return 0;
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return 1;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['sToken', null, InputOption::VALUE_REQUIRED, 'Station Token', null],
            ['sName', null, InputOption::VALUE_REQUIRED, 'Station Name', null],
            ['sEmail', null, InputOption::VALUE_REQUIRED, 'Station E-Mail', null],
            ['sLong', null, InputOption::VALUE_OPTIONAL, 'Station Longitude', 9.772458],
            ['sLat', null, InputOption::VALUE_OPTIONAL, 'Station Latitude', 54.635002],
            ['sLeitstelleId', null, InputOption::VALUE_REQUIRED, 'Station Leitstelle ID', null],
            ['sStationType', null, InputOption::VALUE_REQUIRED, 'Station Type', null],
            ['mqUser', null, InputOption::VALUE_REQUIRED, 'MQ User Name', null],
            ['mqPass', null, InputOption::VALUE_REQUIRED, 'MQ Password', null],
            ['mqHost', null, InputOption::VALUE_OPTIONAL, 'MQ Hostname', Settings::get('mq-sys-host')],
            ['mqPort', null, InputOption::VALUE_OPTIONAL, 'MQ Web MQTT Port', Settings::get('mq-web-mqtt-port')],
            ['uCreate', null, InputOption::VALUE_OPTIONAL, 'User create?', false],
            ['uEmail', null, InputOption::VALUE_OPTIONAL, 'User E-Mail', ''],
            ['uEmail', null, InputOption::VALUE_OPTIONAL, 'User E-Mail', ''],
            ['uSendmail', null, InputOption::VALUE_OPTIONAL, 'Send activation mail?', false],
            ['uName', null, InputOption::VALUE_OPTIONAL, 'User Name', ''],
            ['uSurname', null, InputOption::VALUE_OPTIONAL, 'User Surame', ''],
        ];
    }

}