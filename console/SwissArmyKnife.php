<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use pm\Firemon112\Classes\Expo\ExpoMessage;
use pm\Firemon112\Classes\ExpoPush;
use Pm\Firemon112\Models\Alert;
use pm\Firemon112\Models\AlertUserFeedback;
use pm\Firemon112\Models\Helper;
use pm\Firemon112\Models\Settings;
use pm\Firemon112\Models\Station;
use pm\Firemon112\Models\Event;
use pm\Firemon112\Models\News;
use RainLab\User\Models\UserGroup as UserGroup;
use Symfony\Component\Console\Input\InputOption;
use pm\Firemon112\Models\Icon;
use Crypt;
use GuzzleHttp;

class SwissArmyKnife extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:swiss:armyknife';

    /**
     * @var string The console command description.
     */
    protected $description = 'This is some magic console command that acts as a helper to do anything what might be helpful';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $task = $this->option('task');
            if ($task=="dguv-211-041-icons") {
                $this->importDguv211041IconsFromTheme($this->option('theme'));
            }
            if ($task=="feedback_hash") {
                $hash = Helper::intToChars(1).":".Helper::intToChars(104);
                $this->info($hash);
            }
            if ($task=="test_push") {
                $this->info("Send test Push to ExponentPushToken[bYIli9Ie8SRBqTFoQwyrLp]");
                $expo = new ExpoPush();
                $expo->setAccessToken(Settings::get('expo_access_token'));
                $message = (new ExpoMessage([
                    'title' => "test title",
                    'body' => "test body",
                ])) ->setData(['_displayInForeground' => true])
                    ->setChannelId('default')
                    ->setBadge(0)
                    ->playSound();
                $resp = $expo->send($message)->to(["ExponentPushToken[bYIli9Ie8SRBqTFoQwyrLp]"])->push();
                $this->info(print_r($resp, true));
            }
            if ($task=="feedback_simulation") {
                $alertId = intval($this->option('payload'));
                $alert = Alert::find($alertId);
                if ($alert != null) {
                    $cases = [];
                    switch ($alert->station->alert_feedback_type) {
                        // Note: the cases array will contain more desired feedbacks more often since
                        // a random number will be used to determine which feedback type to simulate (which is normal distributed)
                        case Station::$TYPE_FEEDBACK_IN: {
                            $cases = [
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_N
                            ];
                            break;
                        }
                        case Station::$TYPE_FEEDBACK_IDN: {
                            $cases = [
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_D,
                                AlertUserFeedback::$TYPE_FEEDBACK_N
                            ];
                            break;
                        }
                        case Station::$TYPE_FEEDBACK_ILN: {
                            $cases = [
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_L,
                                AlertUserFeedback::$TYPE_FEEDBACK_L,
                                AlertUserFeedback::$TYPE_FEEDBACK_N
                            ];
                            break;
                        }
                        case Station::$TYPE_FEEDBACK_IDLN: {
                            $cases = [
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_I,
                                AlertUserFeedback::$TYPE_FEEDBACK_D,
                                AlertUserFeedback::$TYPE_FEEDBACK_L,
                                AlertUserFeedback::$TYPE_FEEDBACK_L,
                                AlertUserFeedback::$TYPE_FEEDBACK_N
                            ];
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    if (count($cases)>0) {
                        $client = new GuzzleHttp\Client();
                        foreach ($alert->station->all_users as $user) {
                            $url = url('f', [Helper::calcFeedbackToken($user->id, $alertId), $cases[rand(1, count($cases))-1]]);
                            $this->info("initiating feedback " . $url);
                            $client->request('GET',$url);
                            sleep(rand(2, 6));
                        }
                        $this->info("Finished simulating feedbacks");
                    } else {
                        $this->info("Feedback not enabled for station of alert");
                    }
                } else {
                    $this->info("Alert not found.");
                }

            }
            if ($task=="feedback_simulation_reset") {
                $alertId = intval($this->option('payload'));
                $alert = Alert::find($alertId);
                if ($alert != null) {
                    foreach ($alert->alert_user_feedbacks as $feedback) {
                        $feedback->forceDelete();
                    }
                }
                $this->info("Deleted Feedbacks for alert " . $alertId);
            }
            if ($task=="fix-alert-ident") {
                $this->fixAlertIdent();
            }

            if ($task=="recalc_reminders") {
                foreach (Event::all() as $event) {
                    $event->afterSave();
                }
                foreach (News::all() as $news) {
                    $news->afterSave();
                }
            }

            if ($task=="migrate_events_to_groups") {
                foreach (Event::all() as $event) {
                    if ($event->user_group_id > 0) {
                        $event->groups()->sync([$event->user_group_id]);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->error('Exception while handling SwittArmyKnife for task: ' . $task . ' - ' . $e->getMessage());
            \Log::alert('Exception while handling SwittArmyKnife for task: ' . $task . ' - ' . $e->getMessage());
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['task', null, InputOption::VALUE_REQUIRED, 'What should be done?', 'string'],
            ['theme', null, InputOption::VALUE_OPTIONAL, 'Identifies the theme that shuold be used', 'string'],
            ['payload', null, InputOption::VALUE_OPTIONAL, 'Identifies the Payload for a task (task needs to know hoe to deal with string)', 'string'],
        ];
    }

    public function importDguv211041IconsFromTheme($theme) {
        /*
         * this method expects firemon 112 theme that include dguv 211-041 icons
         */
        $dguvFolder = './themes/'.$theme.'/assets/dguv-211-041/';
        $this->info("Import Icons from folder " . $dguvFolder);
        $row = 1;
        if (($handle = fopen($dguvFolder . "dguv-211-041-index.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                $row++;

                $icon = new Icon();
                $icon->label = $data[1];
                $icon->group = $data[2];
                $icon->icon_small = $dguvFolder . '24x24/'.$data[0].'.png';
                $icon->icon_big = $dguvFolder . '256x256/'.$data[0].'.png';
                $icon->save();
                $this->info($icon->label . ' saved');
            }
            fclose($handle);
        }

    }

    function fixAlertIdent() {
        $all = Alert::all();
        foreach ($all as $alert) {
            $alert->identifier = $alert->getDepescheObject()->Einsatznummer . ($alert->test ? ' '.$alert->created_at->timestamp : '');
            $this->info("Updated alert " . $alert->id . " - ident is now " . $alert->identifier);
            $alert->save();
        }
    }

    function sendPush($token, $type, $message) {

    }


}