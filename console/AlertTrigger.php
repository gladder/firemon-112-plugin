<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Alert;
use Symfony\Component\Console\Input\InputOption;

class AlertTrigger extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:alert:trigger';

    /**
     * @var string The console command description.
     */
    protected $description = 'Triggers alerts using WorkerQueue';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $alertid = intval($this->option('alertid'));
            $alert = Alert::find($alertid);
            if ($alert != null) {
                $depesche = $alert->getDepescheObject();
                if ($depesche === null) {
                    throw new \Exception("Depesche null");
                }
                $this->info('Trigger Alert Dispatch for alert: ' . $depesche->Einsatznummer);
                $alert->dispatchAlerts($this);
            } else {
                $this->error('Alert Trigger: cannot find alert-id: ' . $alertid);
            }
        } catch (\Exception $e) {
            $this->error('Exception while handling AlertTrigger for alert-id: ' . $alertid . ' - ' . $e->getMessage());
            \Log::alert('Exception while handling AlertTrigger for alert-id: ' . $alertid . ' - ' . $e->getMessage());
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['alertid', null, InputOption::VALUE_REQUIRED, 'Database ID of alert', 'int'],
        ];
    }

}