<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Alert;
use pm\Firemon112\Models\Helper;
use Symfony\Component\Console\Input\InputOption;

class QueryWeather extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:query:weather';

    /**
     * @var string The console command description.
     */
    protected $description = 'Triggers alerts using WorkerQueue';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $alertid = intval($this->option('alertid'));
            $alert = Alert::find($alertid);
            if ($alert != null) {
                $depesche = $alert->getDepescheObject();
                if ($depesche === null) {
                    throw new \Exception("Depesche null");
                }
                $locationKeyResp = Helper::AccuWeatherGeopositionSearch($depesche->Lat,$depesche->Long);
                $locationKey = 0;
                if (array_key_exists("Key", $locationKeyResp)) {
                    $locationKey = $locationKeyResp["Key"];
                }
                if ($locationKey <= 0) {
                    throw new \Exception("no location key found!");
                }
                $this->info('Received Location: ' . $locationKey);

                $currentConditionsResp = Helper::AccuWeatherCurrentConditions($locationKey);

                $weatherPayload = [
                    'DateTime' => $currentConditionsResp[0]->LocalObservationDateTime,
                    'WeatherIcon' => $currentConditionsResp[0]->WeatherIcon,
                    'WeatherText' => $currentConditionsResp[0]->WeatherText,
                    'TemperatureValue' => $currentConditionsResp[0]->Temperature->Metric->Value,
                    'TemperatureUnit' => $currentConditionsResp[0]->Temperature->Metric->Unit,
                    'WindSpeedValue' => $currentConditionsResp[0]->Wind->Speed->Metric->Value,
                    'WindSpeedUnit' => $currentConditionsResp[0]->Wind->Speed->Metric->Unit,
                    'WindGustSpeedValue' => $currentConditionsResp[0]->WindGust->Speed->Metric->Value,
                    'WindGustSpeedUnit' => $currentConditionsResp[0]->WindGust->Speed->Metric->Unit,
                    'LocationKey' => $locationKey,
                    'WindDirectionDegrees' => $currentConditionsResp[0]->Wind->Direction->Degrees
                ];

                $this->info('Received Conditions: ' . print_r($weatherPayload, true));

                if (!empty($weatherPayload)) {
                    $alert->weather = $weatherPayload;
                    $alert->save();
                }
            } else {
                $this->error('Query Weather: cannot find alert-id: ' . $alertid);
            }
        } catch (\Exception $e) {
            $this->error('Exception while handling QueryWeather for alert-id: ' . $alertid . ' - ' . $e->getMessage());
            \Log::alert('Exception while handling QueryWeather for alert-id: ' . $alertid . ' - ' . $e->getMessage());
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['alertid', null, InputOption::VALUE_REQUIRED, 'Database ID of alert', 'int'],
        ];
    }

}