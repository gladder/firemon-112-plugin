<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Alert;
use Carbon\Carbon;
class AnonymizeData extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:anonymize:data';

    /**
     * @var string The console command description.
     */
    protected $description = 'Removed personal data from entities after a defined period of time';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $alerts = Alert::needsAnonymization()->get();
        foreach ($alerts as $alert) {
            $this->output->writeln('Anonymizing Alert: '.$alert->id.' NOW');
            \Log::info('Anonymizing Alert: '.$alert->id.' NOW');
            $alert->anonymize();
            $alert->anonymized_at = Carbon::now('Europe/Berlin');
            $alert->save();
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}