<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Alert;
use Carbon\Carbon;
class AlertSchedule extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:alert:schedule';

    /**
     * @var string The console command description.
     */
    protected $description = 'Trigger scheduled alerts';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $alerts = Alert::scheduled()->get();
        foreach ($alerts as $alert) {
            $this->output->writeln('Scheduled Alert: '.$alert->id.' gets handled NOW');
            \Log::info('Scheduled Alert: '.$alert->id.' gets handled NOW');

            $handled = Carbon::now('Europe/Berlin');
            $alert->schedule_handled_at = $handled;
            $alert->created_at = $handled; // fake creation date so that alert will be fresh to monitors
            $alert->save();
            $alert->dispatchQueryWeather(); // query weather again to get fresh data

            if ($alert->getTestAttribute()) {
                $alert->triggerTestAlert();
            } else {
                $alert->publishAlert();
                $alert->dispatchAlerts();
                if ($alert->station->diverafree_enabled) {
                    $alert->publishDiverafreeAlert();
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}