<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use pm\Firemon112\Models\Mail;
use pm\Firemon112\Models\Station;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Mail as EMail;
use Validator;

class HandleMail extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:mail:handle';

    /**
     * @var string The console command description.
     */
    protected $description = 'Parse Mail, extract Plaintext and forward if needed';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $mailBody = "";
        try {
            $token = $this->option('token');
            $parser = new \PhpMimeMailParser\Parser();

            if ($stream = fopen("php://stdin", "r")) {
                $mailBody = stream_get_contents($stream);
                fclose($stream);
            }

            $parser->setText($mailBody);
            $mailText = $parser->getMessageBody('text');
            $subject = $parser->getHeader('subject');
            $this->info('Received Mail '.$mailText);


            $mail = Mail::create(['token' => $token, 'mail' => $mailBody, 'mail_text' => $mailText]);
            $this->info('Received Mail '.$mail->id);

            $arrayHeaderFrom = $parser->getAddresses('from');

            $station = Station::where('token', '=', $token)->first();
            if ($station !== null) {
                if ($station->leitstelle !== null && strlen($station->leitstelle->email_alarm) > 0 && count($arrayHeaderFrom) > 0) {
                    // Handle Depesche if mail comes from leitstelle
                    if (strtolower(trim($station->leitstelle->email_alarm)) == strtolower(trim($arrayHeaderFrom[0]['address']))) {
                        $this->call('firemon:depesche:handle', ['--station' => $token, '--depesche' => $mailText]);
                        $this->info('handle alert for '.$token);
                    } else {
                        \Log::error($arrayHeaderFrom[0]['address'].' is not a valid leitstellen mail ('.$station->leitstelle->email_alarm.')!');
                        $this->error($arrayHeaderFrom[0]['address'].' is not a valid leitstellen mail ('.$station->leitstelle->email_alarm.')!');
                    }
                } else {
                    // if no email from leitstelle provided expect that alert is not a fake and handle it
                    $this->call('firemon:depesche:handle', ['--station' => $token, '--depesche' => $mailText]);
                    $this->info('handle alert for '.$token.' (optimistic that this is not fake)');
                }

                // Forward Mail if needed
                if (strlen($station->forward_depesche_emails)>0) {
                    $recipients = explode(',', $station->forward_depesche_emails);
                    if (count($recipients) > 0) {
                        foreach ($recipients as $untrimmed_recipient) {
                            $recipient = trim($untrimmed_recipient);
                            $validator = Validator::make(
                                ['email' => $recipient],
                                ['email' => 'email']
                            );
                            if ($validator->fails()) {
                                \Log::error($recipient.' is not a valid email');
                            } else {
                                EMail::send('pm.firemon112::mail.forward-mail', array(
                                    'name' => $station->name,
                                    'original_email' => $station->email,
                                    'original_subject' => $subject,
                                    'mail_text' => $mailText
                                ), function ($message) use ($recipient) {
                                    $message->to($recipient);
                                    // diabled due to SPAM $message->attachData($mailBody, 'OriginalMail.eml', ['mime' => 'message/rfc822']);
                                });
                                \Log::info('forwarded alert-mail to '.$recipient);
                            }
                        }
                    } else {
                        \Log::info('No forward email provided.');
                    }
                } else {
                    \Log::info('No forward email provided.');
                }
            } else {
                \Log::info('Station not found ('.$token.')');
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while handling Mail' . $mailBody);
            $this->error('Exception while handling Mail: ' . $e);
            trace_log($e);
        }
    }


    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['token', null, InputOption::VALUE_REQUIRED, 'Station Token (user part of E-Mail)', 'norderbrarup'],
        ];
    }

}