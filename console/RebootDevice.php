<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use Pm\Firemon112\Models\Device;
use Validator;
class RebootDevice extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:reboot:device';

    /**
     * @var string The console command description.
     */
    protected $description = 'Restart Monitors that have the restart_flag enabled';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $devices = Device::type(Device::$TYPE_MONITOR)->state(Device::$STATE_ACTIVE)->get();
            foreach ($devices as $device) {
                /* @var $device Device */
                if ($device->getShouldAutoRestartAttribute()) {
                    $device->reboot();
                    $this->info('Rebooting Device ' . $device->name);
                }
            }
            \Log::info('Rebooted all devices');
        } catch (\Exception $e) {
            \Log::alert('Exception while Rebooting Devices');
            $this->error('Exception while Rebooting Devices ' . $e);
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}