<?php namespace pm\Firemon112\Console;

use Illuminate\Console\Command;
use pm\Firemon112\Models\AlertDispatchJob;
use Pm\Firemon112\Models\Event;
use pm\Firemon112\Models\Pushtoken;
use Validator;
use Mail;
use Crypt;
use pm\Firemon112\Models\Helper;
class EventReminder extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'firemon:event:reminder';

    /**
     * @var string The console command description.
     */
    protected $description = 'Check for related Events and Remind Users';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        try {
            $events = Event::remindable()->get();
            foreach ($events as $event) {
                $this->info("Handle " . $event->label);
                $recipients = [];
                $push = [];

                //foreach ($event->related_user as $user) {
                //    $this->info("Handle " . $user->id);
                //    // TODO: Impl reminder - see code below - or more generic - deal with reminders here!
                //}

                foreach ($event->related_user as $user) {
                    // E-Mail
                    if (Helper::IsUserNotificationSetting($user, Helper::$USER_NOTIFICATION_ALLOW_EVENTS_EMAIL)) {
                        $recipients[] = [
                            'email' => $user->email,
                            'name' => $user->name,
                            'id' => $user->id
                        ];
                        $this->info("added recipient " . $user->email);
                    } else {
                        $this->info("skipped recipient " . $user->email . ' due to user settings');
                    }

                    // Push
                    if (Helper::IsUserNotificationSetting($user, Helper::$USER_NOTIFICATION_ALLOW_EVENTS_PUSH)) {
                        foreach ($user->pushtokens as $token) {
                            if (strlen($token->token_type)>0) {
                                if (!array_key_exists($token->token_type, $push)) {
                                    $push[$token->token_type] = [];
                                }
                                $push[$token->token_type][] = [
                                    "token" => $token->token
                                ];
                            }
                        }
                        $this->info("added tokens for " . $user->email);
                    } else {
                        $this->info("skipped tokens for " . $user->email . ' due to user settings');
                    }
                }

                if (count($push) > 0) {
                    // TODO : This will not work because we dont have a alert which is needed for AlertdispatchJob. Maybe make a more generic Job like NotificationJob??
                }

                if (count($recipients)>0) {
                    foreach ($recipients as $recipient) {
                        $validator = Validator::make(
                            ['email' => $recipient['email']],
                            ['email' => 'email']
                        );
                        if ($validator->fails()) {
                            \Log::error($recipient.' is not a valid email - event id {{ event.id }} reminder skipped');
                        } else {
                            $pos_crypto = Crypt::encrypt([
                                'user_id' => $recipient['id'],
                                'event_id' => $event->id,
                                'available' => '1'
                            ]);
                            $neg_crypto = Crypt::encrypt([
                                'user_id' => $recipient['id'],
                                'event_id' => $event->id,
                                'available' => '0'
                            ]);
                            $subscription = null;
                            foreach ($event->subscriptions as $thissub) {
                                if ($thissub->user_id == $recipient['id']) {
                                    $subscription = $thissub;
                                }
                            }

                            Mail::send('pm.firemon112::mail.event-reminder', array(
                                'name' => $recipient['name'],
                                'event' => $event,
                                'subscribe_positive_url' => url('event-response', [$pos_crypto]),
                                'subscribe_negative_url' => url('event-response', [$neg_crypto]),
                                'subscription' => $subscription
                            ), function ($message) use ($recipient) {
                                $message->to($recipient['email']);
                            });
                        }
                    }
                }
                $event->email_reminder_done = 1;
                $event->save();
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while Event Reminder');
            $this->error('Exception while Event Reminder ' . $e);
            trace_log($e);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}