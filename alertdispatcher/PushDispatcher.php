<?php namespace pm\Firemon112\AlertDispatcher;


use pm\Firemon112\Classes\Apn\ApnPushClient;
use pm\Firemon112\Classes\Expo\ExpoMessage;
use pm\Firemon112\Classes\ExpoPush;
use pm\Firemon112\Classes\Fcm\FcmPushClient;
use pm\Firemon112\Models\Pushtoken;
use pm\Firemon112\Models\Settings;



class PushDispatcher implements IAlertDispatcher {

    private $result = [];

    public function perform($alert_dispatch_job)
    {
        try {
            if ($alert_dispatch_job->dry_run) {
                $this->result[] = "Push DRY RUN for " . $alert_dispatch_job->payload['token_type'] . "-Push successful.";
            } else {
                if ($alert_dispatch_job->payload['token_type'] == Pushtoken::$TOKEN_TYPE_EXPO) {
                    $this->sendExpoPush($alert_dispatch_job);
                } else if ($alert_dispatch_job->payload['token_type'] == Pushtoken::$TOKEN_TYPE_APN) {
                    $this->sendApnPush($alert_dispatch_job);
                } else if ($alert_dispatch_job->payload['token_type'] == Pushtoken::$TOKEN_TYPE_FCM) {
                    $this->sendFcmPush($alert_dispatch_job);
                } else {
                    $this->result[] = "Not supported Push-Type " . $alert_dispatch_job->payload['token_type'] . " - exit";
                }
            }
        } catch (Exception $e) {
            $this->result[] = "Exception in PushDispatcher: " . $e->getMessage();
        }
    }

    public function result()
    {
        return $this->result;
    }

    public function sendExpoPush($alert_dispatch_job) {
        $expo_access_token = Settings::get('expo_access_token');

        $expo = new ExpoPush();
        $expo->setAccessToken($expo_access_token);

        $chunks = array_chunk($alert_dispatch_job->payload['recipients'], 50);

        $message = new ExpoMessage([]);
        $message->setData($alert_dispatch_job->payload['data'])
            ->setTitle($alert_dispatch_job->payload['reason'])
            ->setBody($alert_dispatch_job->payload['message'])
            ->setChannelId($alert_dispatch_job->payload['topic'])
            ->setCategoryId($alert_dispatch_job->payload['topic'])
            ->playSound();

        if ($alert_dispatch_job->payload['topic'] == Pushtoken::$TOKEN_TOPIC_ALERT) {
            $message->setTtl(12*60)
                ->setSound('default')
                ->setPriority('high')
                ->setBadge(1);
        }

        foreach ($chunks as $chunk) {
            $resp = $expo->send($message)->to($chunk)->push();
            $this->result[] = $resp->getData();
        }

    }

    public function sendFcmPush($alert_dispatch_job) {

        $title = $alert_dispatch_job->payload['reason'];
        $body = $alert_dispatch_job->payload['message'];
        $data = $alert_dispatch_job->payload['data'];

        $notifier = new FcmPushClient();

        $message = [
            'token' => '',
            'data' => [
                'title' => $title,
                'message'  => $body,
                'body' => json_encode($data), // Usage in Notification Handler
                'channelId' => $alert_dispatch_job->payload['topic'], // Android Channel Id
                'categoryId' => $alert_dispatch_job->payload['topic'], // needed for iOS
                'sound' => 'true', // valid for Android pre 8.0
            ],
            'android' => [
                'priority' => 'high',
                'ttl' => '3600s'
            ],
        ];

        if ($alert_dispatch_job->payload['topic'] == Pushtoken::$TOKEN_TOPIC_ALERT) {
            $message['data']['vibrate'] = '[0, 250, 250, 250]'; // valid for Android pre 8.0
            $message['data']['badge'] = '1'; // valid for Android pre 8.0
        }

        if (is_array($alert_dispatch_job->payload['recipients']) && count($alert_dispatch_job->payload['recipients'])>0) {
            foreach ($alert_dispatch_job->payload['recipients'] as $recipient) {
                $message['token'] = $recipient;
                $resp = $notifier->sendNotification($message);
                $this->result[] = $resp;
            }
        }
    }

    public function sendApnPush($alert_dispatch_job) {
        if (is_array($alert_dispatch_job->payload['recipients']) && count($alert_dispatch_job->payload['recipients'])>0) {
            $apn = new ApnPushClient();
            $responses = $apn->sendNotifications(
                $alert_dispatch_job->payload['recipients'],
                $alert_dispatch_job->payload['reason'],
                $alert_dispatch_job->payload['message'],
                $alert_dispatch_job->payload['topic'],
                $alert_dispatch_job->payload['data'],
                $alert_dispatch_job->payload['topic'] == Pushtoken::$TOKEN_TOPIC_ALERT
            );
            $this->result[] = $responses;
        }
    }
}