<?php namespace pm\Firemon112\AlertDispatcher;


use pm\Firemon112\Models\Alert;
use pm\Firemon112\Models\Helper;

class AppDispatcher implements IAlertDispatcher {

    private $result = ['success' => true];

    public function perform($alert_dispatch_job)
    {
        try {
            if (!$alert_dispatch_job->dry_run) {
                foreach ($alert_dispatch_job->payload['recipients'] as $recipient) {
                    Helper::MqSendMessageToStationToken('user-sub-'.$recipient, Alert::$MQ_ALERT);
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while performing AppDispatcher AlertDispatchJob: ' . $alert_dispatch_job->id . ' - ' . $e->getMessage());
            $this->result['success'] = false;
        }
    }

    public function result()
    {
        return $this->result;
    }
}