<?php namespace pm\Firemon112\AlertDispatcher;

use Mail;

class EmailDispatcher implements IAlertDispatcher {

    private $result = ['success' => true];

    public function perform($alert_dispatch_job)
    {
        try {
            if (!$alert_dispatch_job->dry_run) {
                foreach ($alert_dispatch_job->payload['recipients'] as $recipient) {
                    Mail::send('pm.firemon112::mail.alert-mail', array(
                        'mailmessage' => $alert_dispatch_job->payload['message'],
                    ), function ($message) use ($recipient) {
                        $message->to($recipient);
                    });
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while performing EmailDispatcher AlertDispatchJob: ' . $alert_dispatch_job->id . ' - ' . $e->getMessage());
            $this->result['success'] = false;
        }
    }

    public function result()
    {
        return $this->result;
    }
}