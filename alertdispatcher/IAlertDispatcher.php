<?php namespace pm\Firemon112\AlertDispatcher;

interface IAlertDispatcher
{
    public function perform($alert_dispatch_job);
    public function result();
}