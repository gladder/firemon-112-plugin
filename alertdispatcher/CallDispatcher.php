<?php namespace pm\Firemon112\AlertDispatcher;

use Sms77\Api\Client;
use pm\Firemon112\Models\Settings;


class CallDispatcher implements IAlertDispatcher {

    private $result = [];

    public function perform($alert_dispatch_job)
    {
        try {
            if (!$alert_dispatch_job->dry_run) {
                $key = Settings::get('sms77_io_key');
                if (strlen($alert_dispatch_job->payload['sms77io_key']) == 64) {
                    $key = $alert_dispatch_job->payload['sms77io_key'];
                }
                $client = new Client($key);
                $extra = [
                    'from' => Settings::get('sms77_io_sender')
                ];

                foreach ($alert_dispatch_job->payload['recipients'] as $recipient) {
                    $this->result[] = $client->voice($recipient, $alert_dispatch_job->payload['message'], $extra);
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while performing CallDispatcher AlertDispatchJob: ' . $alert_dispatch_job->id . ' - ' . $e->getMessage());
            $this->result['success'] = false;
        }

    }

    public function result()
    {
        return $this->result;
    }
}