<?php namespace pm\Firemon112\AlertDispatcher;

use Sms77\Api\Client;
use pm\Firemon112\Models\Settings;


class SmsDispatcher implements IAlertDispatcher {

    private $result = [];

    public function perform($alert_dispatch_job)
    {
        try {
            if (!$alert_dispatch_job->dry_run) {
                $key = Settings::get('sms77_io_key');
                if (strlen($alert_dispatch_job->payload['sms77io_key']) == 64) {
                    $key = $alert_dispatch_job->payload['sms77io_key'];
                }
                $client = new Client($key);
                $extra = [
                    'label' => $alert_dispatch_job->payload['origin'],
                    'from' => Settings::get('sms77_io_sender')
                ];

                $chunks = array_chunk($alert_dispatch_job->payload['recipients'], 25);

                foreach ($chunks as $chunk) {
                    $this->result[] = $client->sms(implode(",", $chunk), $alert_dispatch_job->payload['message'], $extra);
                }
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while performing SmsDispatcher AlertDispatchJob: ' . $alert_dispatch_job->id . ' - ' . $e->getMessage());
            $this->result['success'] = false;
        }
    }

    public function result()
    {
        return $this->result;
    }
}