<?php namespace pm\Firemon112\Middleware;

use Flash;
use Closure;
use pm\Firemon112\Models\Settings;
use ReCaptcha\ReCaptcha;
use October\Rain\Exception\AjaxException;

class CaptchaMiddleware {

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->exists('g-recaptcha-response'))
        {
            $recaptcha = new ReCaptcha( Settings::get('recaptcha-secret-key') );

            /**
             * Verify the reponse, pass user's IP address
             */
            $response = $recaptcha->verify(
                $request->input('g-recaptcha-response'),
                $request->ip()
            );

            /**
             * Fail, if the response isn't OK
             */

            if (! $response->isSuccess()) {
                if ($request->ajax()) {
                    throw new \Exception('Bitte lösen Sie das Captcha um zu bestätigen, dass Sie ein Mensch sind! (' . implode(', ', $response->getErrorCodes()).')');
                } else {
                    foreach ($response->getErrorCodes() as $code) {
                        Flash::error( $code );
                    }

                    return redirect()->back()->withInput();
                }
            }
        }

        /**
         * Handle request
         */
        return $next($request);
    }

}