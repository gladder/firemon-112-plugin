<?php namespace pm\Firemon112\Tests\Classes;

use pm\Firemon112\Tests\Firemon112TestCase;
use pm\Firemon112\Classes\Dpi;

class DpiTest extends Firemon112TestCase
{
    public function testNoDpiReplacement() {
        $str = "hello world";
        $str = Dpi::translateDpi($str);
        $this->assertSame("hello world", $str);
    }

    public function testSingleDpiReplacement()
    {
        $str = "hello +540 world";
        $str = Dpi::translateDpi($str);
        $this->assertSame("hello Schwangerschaft/Geburtshilfe - Notfall in der Schwangerschaft: akute fetale Gefährdung, Erstversorgung ggf. in ungeeigneter Klinik world", $str);
    }

    public function testMultiDpiReplacement()
    {
        $str = "hello +906 +749 world";
        $str = Dpi::translateDpi($str);
        $this->assertSame("hello MANV: R 75 Erkrankungen - Mund/Kiefer/Gesicht: Mund-Kiefer-Gesicht-Notfall, sonstiger world", $str);
    }
}