<?php namespace Pm\Firemon112\FormWidgets;

use Backend\Classes\FormWidgetBase;
use pm\Firemon112\Models\Settings;

/**
 * TestWidget Form Widget
 */
class GeofenceWidget extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'pm_firemon112_geofencewidget';

    public $zoom = 12;
    public $map_type = 'roadmap';
    public $inject_gmap_init = true;
    public $show_polygon_marker = true;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'zoom',
            'map_type',
            'inject_gmap_init',
            'show_polygon_marker',
        ]);

        if ($this->inject_gmap_init) {
            $this->addJs('https://maps.googleapis.com/maps/api/js?key=' . Settings::get('google-maps-api-key') . '&libraries=drawing');
        }
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('geofencewidget');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['center_long'] = 9.77251053;
        $this->vars['center_lat'] = 54.6354986;
        $this->vars['zoom'] = $this->zoom;
        $this->vars['map_type'] = $this->map_type;
        $this->vars['marker_icon'] = $this->getAssetPath('bereitstellungsraum.png');
        $this->vars['inject_gmap_init'] = $this->inject_gmap_init;
        $this->vars['show_polygon_marker'] = $this->show_polygon_marker;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/geofencewidget.css');
        $this->addJs('js/geofencewidget.js');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
