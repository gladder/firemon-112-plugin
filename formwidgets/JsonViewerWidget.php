<?php namespace Pm\Firemon112\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Pm\Firemon112\Models\Depesche;
use Input;
use pm\Firemon112\Models\Einsatzobjekt;
use pm\Firemon112\Models\Stichwort;

class JsonViewerWidget extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'pm_firemon112_jsonviewerwidget';

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('jsonviewerwidget');

    }

    public function prepareVars()
    {
        $this->vars['id'] = $this->getId();
        $this->vars['name'] = $this->getFieldName();
        $this->vars['json'] = json_encode($this->getLoadValue() == null ? array() : $this->getLoadValue(), JSON_PRETTY_PRINT);
    }

    public function init()
    {

    }

    public function loadAssets()
    {

    }

    public function getSaveValue($value)
    {
        if ($value == null || strlen($value) <= 0) {
            $value = "[]";
        }
        return json_decode($value);
    }
}