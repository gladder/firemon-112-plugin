let longlat_center_long = 0.0;
let longlat_center_lat = 0.0;
let longlat_lat_input;
let longlat_long_input;
let longlat_formScope;
jQuery(document).ready(function($){
    if ($('#longlat_map').length > 0) {
        bootstrapLongLatMap();
    } else {
        $(".tab-pane").on("click",function() {
            console.log("click");
            setTimeout(bootstrapMap, 500);
        });
    }
});

function bootstrapLongLatMap() {
    if ($('#longlat_map').length > 0) {
        longlat_formScope = $('#longlat_map').closest("form");

        if (longlat_formScope.find('input[name*="[lat]"]').length > 0) {
            longlat_lat_input = longlat_formScope.find('input[name*="[lat]"]');
        }
        if (longlat_formScope.find('input[name*="[long]"]').length > 0) {
            longlat_long_input = longlat_formScope.find('input[name*="[long]"]');
        }

        if (longlat_lat_input !== undefined && longlat_lat_input.length > 0 && longlat_lat_input.val().length > 0) {
            longlat_center_lat = longlat_lat_input.val();
        } else {
            longlat_center_lat = $('#longlat_map').attr('data-center-lat');
        }

        if (longlat_long_input !== undefined && longlat_long_input.length > 0 && longlat_long_input.val().length > 0) {
            longlat_center_long = longlat_long_input.val();
        } else {
            longlat_center_long = $('#longlat_map').attr('data-center-long');
        }

        initLongLatMap();
    }
}

function initLongLatMap() {
    console.log($('#longlat_map').attr('data-zoom'));
    var longlat_stationLonLat = new google.maps.LatLng(longlat_center_lat,longlat_center_long);
    var longlat_map = new google.maps.Map(document.getElementById("longlat_map"), {
        zoom: parseFloat($('#longlat_map').attr('data-zoom')),
        center: longlat_stationLonLat,
        mapTypeId: $('#longlat_map').attr('data-map-type')
    });
    console.log('gmap',longlat_map);
    var position_marker = new google.maps.Marker({
        position: longlat_stationLonLat,
        map: longlat_map
    });

    longlat_map.addListener('click', function(event) {
        position_marker.setPosition(event.latLng);
        console.log(event.latLng);
        if (longlat_lat_input !== undefined && longlat_lat_input.length > 0) {
            longlat_lat_input.val(event.latLng.lat);
        }
        if (longlat_long_input !== undefined && longlat_long_input.length > 0) {
            longlat_long_input.val(event.latLng.lng);
        }
    });


}