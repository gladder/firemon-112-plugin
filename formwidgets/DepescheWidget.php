<?php namespace Pm\Firemon112\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Pm\Firemon112\Models\Depesche;
use Input;
use pm\Firemon112\Models\Einsatzobjekt;
use pm\Firemon112\Models\Stichwort;

class DepescheWidget extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'pm_firemon112_depeschewidget';

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('depeschewidget');

    }

    public function prepareVars()
    {
        /*$arr = $this->getLoadValue();
        $depesche = new Depesche();
        if (strlen($json)>0) {
            $depesche = Depesche::fromArray(json_decode($json,true));
        }*/
        $this->vars['depesche'] = array();
        if (is_array($this->getLoadValue())) {
            $this->vars['depesche'] = $this->getLoadValue();
        }
        $this->vars['stichworte'] = Stichwort::orderBy('stichwort')->get();
        $this->vars['objekte'] = Einsatzobjekt::orderBy('einsatzobjekt')->get();
    }

    public function init()
    {

    }

    public function loadAssets()
    {
        $this->addJs('js/depeschewidget.js', 'pm.firemon112');
    }

    public function getSaveValue($value)
    {
        $fields = Input::all();
        if (strlen($fields['MitalarmierteKraefte']) > 0) {
            $mak = $fields['MitalarmierteKraefte'];
            $fields['MitalarmierteKraefte'] = array();
            if (strpos($mak, "\n") !== FALSE) {
                $fields['MitalarmierteKraefte'] = explode("\n",$mak);
            } else {
                $fields['MitalarmierteKraefte'][] = $mak;
            }
        }
        return Depesche::fromArray($fields);
    }
}