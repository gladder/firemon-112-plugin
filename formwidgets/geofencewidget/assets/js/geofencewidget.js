let geofence_center_long = 0.0;
let geofence_center_lat = 0.0;
let geofence_lat_input;
let geofence_long_input;
let show_polygon_marker = false;
let coord_input;
let geofence_formScope;
let polygon = null;
let marker_icon;
let br_marker;
let geofence_map;

let polyOpts = {
    fillColor: "#0000ff",
    fillOpacity: 0.25,
    strokeColor: "#ff0000",
    strokeOpacity: 0.5,
    strokeWeight: 2,
    clickable: true,
    editable: true,
    draggable: true,
    zIndex: 1,
}

jQuery(document).ready(function($){
    if ($('#geofence_map').length > 0) {
        bootstrapGeofenceMap();
    } else {
        $(".tab-pane").on("click",function() {
            console.log("click");
            setTimeout(bootstrapMap, 500);
        });
    }
});

function bootstrapGeofenceMap() {
    let jqMap = $('#geofence_map');
    if (jqMap.length > 0) {
        geofence_formScope = jqMap.closest("form");

        marker_icon = jqMap.attr('data-marker-icon');

        if (geofence_formScope.find('input[name*="[lat]"]').length > 0) {
            geofence_lat_input = geofence_formScope.find('input[name*="[lat]"]');
        }
        if (geofence_formScope.find('input[name*="[long]"]').length > 0) {
            geofence_long_input = geofence_formScope.find('input[name*="[long]"]');
        }
        if (geofence_formScope.find('input[name*="[coords]"]').length > 0) {
            coord_input = geofence_formScope.find('input[name*="[coords]"]');
        }

        if (geofence_lat_input !== undefined && geofence_lat_input.length > 0 && geofence_lat_input.val().length > 0) {
            geofence_center_lat = geofence_lat_input.val();
        } else {
            geofence_center_lat = jqMap.attr('data-center-lat');
        }

        if (geofence_long_input !== undefined && geofence_long_input.length > 0 && geofence_long_input.val().length > 0) {
            geofence_center_long = geofence_long_input.val();
        } else {
            geofence_center_long = jqMap.attr('data-center-long');
        }

        if (coord_input !== undefined && coord_input.length > 0 && coord_input.val().length > 0) {
            polygon = json2polygon(coord_input.val());
        }
        show_polygon_marker = (jqMap.attr('data-show-polygon-marker') == "1");
        initGeofenceMap();
    }
}

function initGeofenceMap() {
    const geofence_centerLonLat = new google.maps.LatLng(geofence_center_lat,geofence_center_long);
    let jqMap = $('#geofence_map');
    geofence_map = new google.maps.Map(document.getElementById("geofence_map"), {
        zoom: parseFloat(jqMap.attr('data-zoom')),
        center: geofence_centerLonLat,
        mapTypeId: jqMap.attr('data-map-type')
    });

    const drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
                google.maps.drawing.OverlayType.POLYGON,
            ],
        },
        polygonOptions: polyOpts

    });

    drawingManager.setMap(geofence_map);
    
    if (polygon !== null) {
        polygon.setMap(geofence_map);
        if (show_polygon_marker) {
            setBrMarker(polygonCenter(polygon));
        }
    }

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {

        let m = event.overlay;

        if (polygon) {
            polygon.setMap(null);
            polygon = null;
        }

        polygon = m;
        console.log(m);
        polygonAddEvents(polygon);
        persistPolygon();

    });

}

function persistPolygon() {
    let polyJson = polygon2Json(polygon);
    let polyCenter = polygonCenter(polygon);
    if (geofence_long_input !== undefined && geofence_long_input.length > 0) {
        geofence_long_input.val(polyCenter.lng);
    }
    if (geofence_lat_input !== undefined && geofence_lat_input.length > 0) {
        geofence_lat_input.val(polyCenter.lat);
    }
    if (coord_input !== undefined && coord_input.length > 0) {
        coord_input.val(polyJson);
    }
    if (show_polygon_marker) {
        setBrMarker(polygonCenter(polygon));
    }
}

function polygon2Json(polygon) {
    let coords = [];
    try {
        polygon.getPath().getArray().forEach(item => {
            coords.push({lng: item.lng(), lat: item.lat()});
        });
    } catch (e) { console.error(e); }
    return JSON.stringify(coords);
}

function polygonCenter(polygon) {
    let coord_min = { lng: 180.0, lat: 180.0 };
    let coord_max = { lng: -180.0, lat: -180.0 };
    try {
        polygon.getPath().getArray().forEach(item => {
            if (item.lng() < coord_min.lng) {
                coord_min.lng = item.lng();
            }
            if (item.lng() > coord_max.lng) {
                coord_max.lng = item.lng();
            }

            if (item.lat() < coord_min.lat) {
                coord_min.lat = item.lat();
            }
            if (item.lat() > coord_max.lat) {
                coord_max.lat = item.lat();
            }
        });
    } catch (e) { console.error(e); }
    return {
        lng: coord_min.lng + ( (coord_max.lng - coord_min.lng) / 2.0 ),
        lat: coord_min.lat + ( (coord_max.lat - coord_min.lat) / 2.0 )
    };
}

function json2polygon(json) {
    let coords = JSON.parse(json);
    let coordArray = [];
    coords.forEach( item => {
        coordArray.push(
            new google.maps.LatLng(item.lat,item.lng)
        );
    });
    console.log(coordArray);
    let poly = new google.maps.Polygon(polyOpts);
    poly.setPath(coordArray);
    polygonAddEvents(poly);
    return poly;
}

function polygonAddEvents(poly) {
    google.maps.event.addListener(poly, 'dragend', function() {
        console.log('dragged changed');
        persistPolygon();
    });
    google.maps.event.addListener(poly.getPath(), 'set_at', function() {
        console.log('set');
        persistPolygon();
    });
    google.maps.event.addListener(poly.getPath(), 'remove_at', function() {
        console.log('remove');
        persistPolygon();
    });
    google.maps.event.addListener(poly.getPath(), 'insert_at', function() {
        console.log('insert');
        persistPolygon();
    });
}

function setBrMarker(location) {
    let lbl = geofence_formScope.find('input[name*="[label]"]').val();
    if (br_marker != null) {
        br_marker.setMap(null);
    }
    br_marker = new google.maps.Marker({
        position: new google.maps.LatLng(location.lat,location.lng),
        map: geofence_map,
        icon: {
            url: marker_icon,
            labelOrigin: new google.maps.Point(30, 75),
        },
        label: {
            text: lbl,
            color: "#FF0000",
            fontWeight: "bold",
            fontSize: "20px",
        }
    });
}