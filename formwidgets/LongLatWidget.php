<?php namespace Pm\Firemon112\FormWidgets;

use Backend\Classes\FormWidgetBase;
use pm\Firemon112\Models\Settings;
/**
 * TestWidget Form Widget
 */
class LongLatWidget extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'pm_firemon112_longlatwidget';

    public $zoom = 12;

    public $map_type = 'roadmap';
    public $inject_gmap_init = true;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'zoom',
            'map_type',
            'inject_gmap_init',
        ]);

        if ($this->inject_gmap_init) {
            $this->addJs('https://maps.googleapis.com/maps/api/js?key='.Settings::get('google-maps-api-key'));
        }
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('longlatwidget');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['center_long'] = 9.77251053;
        $this->vars['center_lat'] = 54.6354986;
        $this->vars['zoom'] = $this->zoom;
        $this->vars['map_type'] = $this->map_type;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/longlatwidget.css');
        $this->addJs('js/longlatwidget.js');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
