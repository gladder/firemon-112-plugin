function stichwortChange() {
    var control = $('select[name="stichwort"]');
    if (control.length > 0) {
        if (control.val().length > 0) {
            var data = JSON.parse(control.val());
            $('input[name="Einsatzstichwort"]').val(data.stichwort);
            $('input[name="Klartext"]').val(data.stichwort + "   " + data.klartext);
        }
    }
}

function objektChange() {
    var control = $('select[name="objekt"]');
    if (control.length > 0) {
        if (control.val().length > 0) {
            var data = JSON.parse(control.val());
            $('input[name="Einsatzobjekt"]').val(data.einsatzobjekt);
            $('input[name="Strasse"]').val(data.strasse);
            $('input[name="Ort"]').val(data.ort);
            $('input[name="Ortsteil"]').val(data.ortsteil);
            $('input[name="Lat"]').val(data.lat);
            $('input[name="Long"]').val(data.long);
        }
    }
}