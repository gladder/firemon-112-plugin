<?php

return [
    [
        'name' => 'Rainlab.User',
        'model' => 'RainLab\User\Models\User',
        'resolver' => pm\Firemon112\Classes\JWT\RainlabPlugin::class,
        'provider' => 'user.auth',
    ],
//    [
//        'name' => 'Lovata.Buddies',
//        'model' => 'Lovata\Buddies\Models\User',
//        'resolver' => \ReaZzon\JWTAuth\Classes\Resolvers\BuddiesPlugin::class,
//        'provider' => 'auth.helper',
//    ]
];