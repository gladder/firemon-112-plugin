<?php namespace pm\Firemon112\Components;


use Cms\Classes\ComponentBase;
use pm\Firemon112\Models\Alert;
use pm\Firemon112\Models\Depesche;
use pm\Firemon112\Models\GpioAlertTrigger;
use pm\Firemon112\Models\Station;
use pm\Firemon112\Models\Stichwort;
use RainLab\User\Models\User;
use Carbon\Carbon;
use pm\Firemon112\Models\Helper;
use pm\Firemon112\Classes\Dpi;
use Exception;
use Flash;
use Redirect;
use Validator;
use Illuminate\Validation\Rule;
use ValidationException;

class GpioAlertTriggerComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'GPIO Alert Trigger Component'
        ];
    }

    function onRun() {
        $gpio_alert_triggers = array();
        $gpio_alert_trigger = null;
        $stichworte = Stichwort::getDropdownList();
        $objects[0] = "Einsatzobjekt wählen oder in Landkarte markieren";
        $objects_details = null;

        if ($this->page->station) {
            $gpio_alert_triggers = $this->page->station->gpio_alert_triggers;
            $trigger_edit_id = intval($this->param('id'));
            if ($trigger_edit_id > 0) {
                $gpio_alert_trigger = $gpio_alert_triggers->firstWhere('id', $trigger_edit_id);
            }
            foreach ($this->page->station->einsatzobjekte as $v) {
                $objects[$v->id] = $v->einsatzobjekt . " - " . $v->strasse . ", " . $v->ort;
            }
            $objects_details = $this->page->station->einsatzobjekte->toJson();
        }
        $this->page['gpio_numbers'] = GpioAlertTrigger::getGpioNumberOptions();;
        $this->page['gpio_monitoring_modes'] = GpioAlertTrigger::getGpioMonitoringModeOptions();
        $this->page['limit_trigger_minutes_options'] = GpioAlertTrigger::getLimitTriggerMinutesOptions();
        $this->page['gpio_alert_triggers'] = $gpio_alert_triggers;
        $this->page['gpio_alert_trigger'] = $gpio_alert_trigger;
        $this->page['stichworte'] = $stichworte;
        $this->page['einsatzobjekte'] = $objects;
        $this->page['einsatzobjekte_details'] = $objects_details;

    }
    function onSaveGpioAlertTrigger() {
        try {
            list($user, $station) = $this->checkPermission();

            $trigger_edit_id = intval($this->param('id'));

            $data = [
                'label' => post('label'),
                'gpio_number' => intval(post('gpio_number')),
                'gpio_monitoring_mode' => post('gpio_monitoring_mode'),
                'station_id' => $station->id,
                'enabled' => boolval(post('enabled')),
                'alert_stichwort_id' => intval(post('alert_stichwort_id')),
                'alert_information' => post('alert_information'),
                'alert_einsatzobjekt' => post('alert_einsatzobjekt'),
                'alert_strasse' => post('alert_strasse'),
                'alert_ort' => post('alert_ort'),
                'alert_long' => floatval(post('alert_long')),
                'alert_lat' => floatval(post('alert_lat')),
                'alert_mak' => post('alert_mak'),
                'limit_trigger_minutes' => intval(post('limit_trigger_minutes')),
            ];

            $dbValidator = Validator::make($data, [
                'station_id' => [
                    'required',
                    Rule::exists((new Station())->getTable(), 'id'),
                ],
                'alert_stichwort_id' => [
                    'required',
                    Rule::exists((new Stichwort())->getTable(), 'id'),
                ],
            ], [
                'station_id.exists' => 'Station existiert nicht.',
                'alert_stichwort_id.exists' => 'Stichwort existiert nicht.',
            ]);

            if ($dbValidator->fails()) {
                throw new ValidationException($dbValidator);
            }

            $trigger = new GpioAlertTrigger();
            if ($trigger_edit_id > 0) {
                $foundTrigger = $station->gpio_alert_triggers->find($trigger_edit_id);
                if ($foundTrigger != null) {
                    $trigger = $foundTrigger;
                }
            }

            $trigger->fill($data);
            $trigger->save();

            Flash::success("GPIO Alert Trigger gespeichert");
            return Redirect::to('gpio-alert-trigger-list');
        } catch (Exception $e) {
            // since its ajax request purge message directly to wipe from session and not wait for another server roundtrip
            Flash::error($e->getMessage())->purge();
        }
    }

    function onDeleteGpioAlertTrigger() {
        try {
            list($user, $station) = $this->checkPermission();

            $trigger_id = intval($this->param('id'));

            if ($trigger_id <= 0) {
                throw new Exception("Keine ID übergeben");
            }

            $trigger = $station->gpio_alert_triggers->find($trigger_id);

            $trigger->devices()->detach();
            $trigger->delete();
            Flash::success("GPIO Alarm Trigger " . $trigger->name . " gelöscht");
            return Redirect::to('gpio-alert-trigger-list');
        } catch (Exception $e) {
            // since its ajax request purge message directly to wipe from session and not wait for another server roundtrip
            Flash::error($e->getMessage())->purge();
        }
    }

    function onResetLastTriggerPerformed() {
        try {
            list($user, $station) = $this->checkPermission();

            $trigger_id = intval($this->param('id'));

            if ($trigger_id <= 0) {
                throw new Exception("Keine ID übergeben");
            }

            $trigger = $station->gpio_alert_triggers->find($trigger_id);

            $trigger->resetLastTriggerPerformed();
            Flash::success("Rate Limit zurückgesetzt");
            return Redirect::refresh();
        } catch (Exception $e) {
            // since its ajax request purge message directly to wipe from session and not wait for another server roundtrip
            Flash::error($e->getMessage())->purge();
        }
    }

    /**
     * @throws Exception
     */
    private function checkPermission(): array
    {
        $user = Helper::GetFeUser();
        if (!$user) {
            throw new Exception("Benutzer nicht gefunden");
        }

        $station = Helper::GetFeStation();
        if (!$station) {
            throw new Exception("Station nicht gefunden");
        }

        if (!Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_ALERT)) {
            throw new Exception("Keine Berechtigung für diese Aktion");
        }

        return [$user, $station];
    }
}