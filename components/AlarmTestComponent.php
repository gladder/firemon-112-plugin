<?php namespace pm\Firemon112\Components;


use Cms\Classes\ComponentBase;
use pm\Firemon112\Models\Alert;
use pm\Firemon112\Models\Depesche;
use pm\Firemon112\Models\Stichwort;
use RainLab\User\Models\User;
use Carbon\Carbon;
use pm\Firemon112\Models\Helper;
use pm\Firemon112\Classes\Dpi;
use Exception;
use Flash;
use Redirect;
use Validator;
use Illuminate\Validation\Rule;
use ValidationException;

class AlarmTestComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Alarm Test Component'
        ];
    }

    function onRun() {
        $objects = array();
        $words = Stichwort::getDropdownList();
        $dpis = array("" => "Dispositionsindikation hinzufügen");
        $objects[0] = "Einsatzobjekt wählen oder in Landkarte markieren";

        if ($this->page->station) {
            foreach ($this->page->station->einsatzobjekte as $v) {
                $objects[$v->id] = $v->einsatzobjekt . " - " . $v->strasse . ", " . $v->ort;
            }

            $this->page['einsatzobjekte_details'] = $this->page->station->einsatzobjekte->toJson();
            $lastKey = "";
            foreach (Dpi::$DpiKeys as $k => $v) {
                if ($lastKey != $k) {
                    $class = substr($k, 0, 2) . "X";
                    if (array_key_exists($class, Dpi::$DpiClasses)) {
                        $dpis[$class] = "==> " . $class . ": " . Dpi::$DpiClasses[$class];
                    }
                }
                $dpis[$k] = "DPI " . $k . ": " . $v;
                $lastKey = $k;
            }
        }
        $this->page['dpis'] = $dpis;
        $this->page['einsatzobjekte'] = $objects;
        $this->page['stichworte'] = $words;

    }
    function onStoreAlert() {
        try {
            $user = Helper::GetFeUser();
            if (!$user) {
                throw new Exception("Benutzer nicht gefunden");
            }

            $station = Helper::GetFeStation();
            if (!$station) {
                throw new Exception("Station nicht gefunden");
            }

            if (!Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_ALERT)) {
                throw new Exception("Keine Berechtigung für diese Aktion");
            }

            $data = [
                'lage' => post('lage'),
                'bezeichnung' => post('bezeichnung'),
                'strasse' => post('strasse'),
                'ort' => post('ort'),
                'patient' => post('patient'),
                'long' => floatval(post('long')),
                'lat' => floatval(post('lat')),
                'schedule_formatted' => post('schedule_formatted'),
                'stichwort_id' => intval(post('stichwort')),
                'trigger_alert_divera' => boolval(post('trigger_alert_divera')),
                'trigger_alert_schleifen' => boolval(post('trigger_alert_schleifen')),
                'trigger_alert_monitor' => boolval(post('trigger_alert_monitor')),
                'trigger_real_alert' => boolval(post('trigger_real_alert')),
                'mak' => post('mak'),
            ];

            $validator = Validator::make($data, [
                'lage' => 'required|string',
                'strasse' => 'required|string',
                'ort' => 'required|string',
                'long' => 'required|numeric|min:-180|max:180',
                'lat' => 'required|numeric|min:-90|max:90',
                'schedule_formatted' => 'nullable|date_format:' . Helper::GetDefaultDateTimeFormat(),
                'stichwort_id' => [
                    'required',
                    Rule::exists((new Stichwort())->getTable(), 'id'),
                ]
            ], [
                'lage.required' => 'Es fehlen Angaben zur Lage.',
                'strasse.required' => 'Es fehlen Angaben zur Straße.',
                'ort.required' => 'Es fehlen Angaben zum Einsatzort.',
                'long.required' => 'Es wurden keine Einsatz-Koordinaten übergeben!',
                'long.min' => 'Die Längengrad-Koordinate muss größer -180° sein.',
                'long.max' => 'Die Längengrad-Koordinate muss kleiner 180° sein.',
                'lat.required' => 'Es wurden keine Einsatz-Koordinaten übergeben!',
                'lat.min' => 'Die Breitengrad-Koordinate muss größer -90° sein.',
                'lat.max' => 'Die Breitengrad-Koordinate muss kleiner 90° sein.',
                'schedule_formatted.date_format' => 'Datum/Uhrzeit für Alarmierung ist fehlerhaft.',
                'stichwort_id.exists' => 'Das gewählte Stichwort ist ungültig.',
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $lage = $data['lage'];
            $bezeichnung = $data['bezeichnung'];
            $strasse = $data['strasse'];
            $ort = $data['ort'];
            $patient = $data['patient'];
            $long = $data['long'];
            $lat = $data['lat'];

            $schedule = null;
            $schedule_tmp = $data['schedule_formatted'];
            if (strlen($schedule_tmp)>0) {
                try {
                    $schedule = Carbon::createFromFormat(Helper::GetDefaultDateTimeFormat(), $schedule_tmp, 'Europe/Berlin');
                } catch (Exception $e) {
                    $schedule = null;
                    throw new Exception("Datum / Uhrzeit für Alarmplanung fehlerhaft!");
                }
            }

            if ($schedule !== null) {
                if ($schedule->lessThan(Carbon::now('Europe/Berlin'))) {
                    throw new Exception("Datum / Uhrzeit liegt in der Vergangenheit!");
                }
            }

            $stichwort = Stichwort::find($data['stichwort_id']);
            $trigger_alert_divera = $data['trigger_alert_divera'];
            $trigger_alert_schleifen = $data['trigger_alert_schleifen'];
            $trigger_alert_monitor = $data['trigger_alert_monitor'];
            $trigger_real_alert = $data['trigger_real_alert'];

            $depesche = new Depesche();

            if ($trigger_alert_divera) {
                if ($trigger_alert_schleifen) {
                    if ($trigger_alert_monitor) {
                        $depesche->Einsatznummer = Alert::$TEST_DIVERA_SCHLEIFEN_MONITOR_KEY;
                    } else {
                        $depesche->Einsatznummer = Alert::$TEST_DIVERA_SCHLEIFEN_KEY;
                    }
                } else {
                    if ($trigger_alert_monitor) {
                        $depesche->Einsatznummer = Alert::$TEST_DIVERA_MONITOR_KEY;
                    } else {
                        $depesche->Einsatznummer = Alert::$TEST_DIVERA_KEY;
                    }
                }
            } else {
                if ($trigger_alert_schleifen) {
                    if ($trigger_alert_monitor) {
                        $depesche->Einsatznummer = Alert::$TEST_SCHLEIFEN_MONITOR_KEY;
                    } else {
                        $depesche->Einsatznummer = Alert::$TEST_SCHLEIFEN_KEY;
                    }
                } else {
                    if ($trigger_alert_monitor) {
                        $depesche->Einsatznummer = Alert::$TEST_MONITOR_KEY;
                    } else {
                        $depesche->Einsatznummer = Alert::$TEST_DRY_KEY;
                    }
                }
            }

            if ($trigger_real_alert) {
                $depesche->Einsatznummer = Alert::generateFiremonEinsatznummer();
            } else {
                $depesche->Einsatznummer .= ' ' . time(); // append timestamp to make unique
            }

            $depesche->Einsatzstichwort = $stichwort->stichwort;

            $now = Carbon::now('Europe/Berlin');
            $depesche->Einsatzbeginn = $now->format("d.m.Y H:i:s");

            $depesche->Klartext = $stichwort->klartext;
            $depesche->Information = $lage;
            $depesche->Patient = $patient;
            $depesche->Einsatzobjekt = $bezeichnung;
            $depesche->Strasse = $strasse;
            $depesche->Ort = $ort;
            $depesche->Ortsteil = "";
            $depesche->X = 0.0;
            $depesche->Y = 0.0;
            $depesche->Lat = $lat;
            $depesche->Long = $long;
            $depesche->MitalarmierteKraefte = explode("\n", $data['mak']);

            date_default_timezone_set('Europe/Berlin');

            Alert::HandleCreateAlertForStation($depesche, $station, $schedule, $trigger_real_alert);
            // check if there is stationswehren where alert needs to be proxied to
            foreach ($station->child_stations as $child_station) {
                if ($child_station->GetIsStandortAttribute()) {
                    Alert::HandleCreateAlertForStation($depesche, $child_station, $schedule, $trigger_real_alert);
                }
            }

            Flash::success("Alarmierung angelegt");
            return Redirect::to('alarme');
        } catch (Exception $e) {
            // since its ajax request purge message directly to wipe from session and not wait for another server roundtrip
            Flash::error($e->getMessage())->purge();
        }
    }
}