<?php namespace pm\Firemon112\Components;


use Cms\Classes\ComponentBase;
use \pm\Firemon112\Models\Helper;
use \pm\Firemon112\Models\Alert;
use \pm\Firemon112\Models\Device;
use \pm\Firemon112\Models\Hydrant;
use \pm\Firemon112\Models\Hydranttype;
use \pm\Firemon112\Models\RescuePoint;
use \pm\Firemon112\Models\AssemblyArea;
use \pm\Firemon112\Models\Einsatzobjekt;
use \pm\Firemon112\Models\DeviceError;
use \pm\Firemon112\Models\Contact;
use \pm\Firemon112\Models\Wachendisplay;
use Carbon\Carbon;
use Input;

class MonitorComponent extends ComponentBase
{
    public $gui_version = "2.37";
    public function componentDetails()
    {
        return [
            'name' => 'Monitor Component'
        ];
    }

    function onRun() {
        $this->page['version'] = $this->gui_version;
        $this->page['injectToken'] = Input::get("injectToken");
        $this->page['injectClientType'] = Input::get("injectClientType");
        $this->page['injectAlert'] = intval(Input::get("injectAlert"));
        $this->page['setupExternalToken'] = Input::get("setupExternalToken");
    }

    function onGetConfig()
    {
        $token = input('token');

        if ($token === null || strlen($token) <= 0) {
            return null;
        }

        $type = input('type');
        $gui_version = input('gui_version');
        $wd_version = input('wd_version');
        $resolution = input('resolution');
        $ip = input('last_known_ip');

        if (Helper::isJwt($token)) {
            $guard = app('JWTGuard');
            $user = $guard->user();

            // we only check for app usage because permission to get any alert depends on alert groups
            if ($user != null && $user->allow_app_usage) {
                $device = new Device();
                $device->id = 999999999; // Faked id to get device running in web-ui
                $device->token = $token;
                $device->type = Device::$TYPE_MOBILE;
                $device->name = "App ".$user->name;
                $device->state = Device::$STATE_ACTIVE;
                $device->station_id = $user->station_id;
                $device->mq_id = 0;
                $device->watchdog = false;
                $device->user_id = $user->id;
                $device->alert_timeout = 60;
                // Inject Wallpaper from Station if existing
                if ($device->station->default_wallpaper !== null) {
                    $device->background = $device->station->default_wallpaper->path;
                }
                return $device;
            } else {
                return null;
            }
        }

        $device = Device::where('token', '=', $token)->with(['gpio_alert_triggers' => function($query) {
            $query->where('enabled', true);
        }])->first();
        if ($device === null) {
            $device = new Device();
            $device->token = $token;
            $device->type = $type;
            $device->name = strtoupper(substr($token, 0,4)) . "-" . strtoupper(substr($token,-4));
            $device->state = Device::$STATE_PENDING;
            $device->station_id = 0;
            $device->mq_id = 0;
            if (strlen($ip)>0) {
                $device->last_known_ip = $ip;
            }
            $device->save();
        } else {
            $device->running_version = $gui_version;
            if (strlen($wd_version)>0) {
                $device->running_version .= " wd-" . $wd_version;
                if (strlen($resolution)>0) {
                    $device->running_version .= " @".$resolution;
                }
            }
            $device->last_heartbeat = Carbon::now('Europe/Berlin');
            if (strlen($ip)>0) {
                $device->last_known_ip = $ip;
            }
            $device->save();
        }
        return $device;
    }
    function onGetStationConfig()
    {
        $token = input('token');

        if (Helper::isJwt($token)) {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null && $user->allow_app_usage) {
                return $user->station;
            } else {
                return null;
            }
        }

        $device = Device::where('token', '=', $token)->first();
        if ($device !== null) {
            return $device->station;
        }
        return [];
    }

    function onGetMqConfig()
    {
        $token = input('token');

        $device = Device::where('token', '=', $token)->first();
        if ($device !== null && $device->state == Device::$STATE_ACTIVE) {
            return $device->mq;
        }
        return [];
    }

    function onGetWachendisplayConfig()
    {
        $token = input('token');
        $arr = [];
        $device = Device::where('token', '=', $token)->first();
        if ($device !== null && $device->state == Device::$STATE_ACTIVE && $device->wachendisplay != null) {
            $arr['modules'] = $device->wachendisplay->getSelectedModules();
            $arr['todays_slots_time_series'] = $device->wachendisplay->getTodaysSlotsTimeSeries();
            $arr['layout'] = $device->wachendisplay->getLayoutVariant();
        }
        return $arr;
    }

    function onGetRenderedModule()
    {
        $token = input('token');
        $module_id = input('module_id');
        $cfg = input('cfg');
        $resp = "";
        $device = Device::where('token', '=', $token)->first();
        if ($device !== null && $device->state == Device::$STATE_ACTIVE && $device->wachendisplay != null) {
            $resp = $device->wachendisplay->getRenderedModule($module_id, $cfg);
        }
        return $resp;
    }

    function onGetRecentAlert()
    {
        $token = input('token');

        $device = Device::where('token', '=', $token)->first();
        if ($device !== null && $device->state == Device::$STATE_ACTIVE) {
            if ($device->user !== null) {
                if (Helper::userCanViewAlertDetails($device->user)) {
                    return $device->recentAlertTimeoutMinutes;
                } else {
                    return [];
                }
            } else {
                return $device->recentAlertTimeoutMinutes;
            }
        }
        return [];
    }

    function onGetRecentAlerts()
    {
        $token = input('token');

        $device = Device::where('token', '=', $token)->first();

        if ($device !== null && $device->state == Device::$STATE_ACTIVE) {
            if ($device->user !== null) {
                if (Helper::userCanViewAlertDetails($device->user)) {
                    return $device->recentAlertTimeoutMinutesAll;
                } else {
                    return [];
                }
            } else {
                return $device->recentAlertTimeoutMinutesAll;
            }
        }
        return [];
    }

    function onGetAlert()
    {
        $token = input('token');
        $alert_id = intval(input('alert_id'));

        if (Helper::isJwt($token)) {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user !== null) {
                $alert = $user->alerts()->find($alert_id);
                if ($alert !== null && $alert->userCanViewAlert($user)) {
                    return $alert;
                }
            }
            return [];
        }

        $device = Device::where('token', '=', $token)->first();
        if ($device !== null && $device->state == Device::$STATE_ACTIVE) {
            $alert = Alert::find($alert_id);
            if ($alert !== null) {
                if ($device->user !== null) {
                    if ($alert->userCanViewAlert($device->user)) {
                        return $alert;
                    } else {
                        return [];
                    }
                }
                if ($alert->station_id == $device->station_id) {
                    return $alert;
                }
            }
        }
        return [];
    }

    function onGetAlertFeedback()
    {
        $token = input('token');
        $alert_id = intval(input('alert_id'));

        $alert = Alert::find($alert_id);
        if ($alert == null) {
            $alert = new Alert(); // if not existing create an empty one to get valid response
        }
        return $alert->getAlertFeedback();
    }

    function onLoginUserToken()
    {
        $email = input('email');
        $password = input('password');
        $token = input('token');
        return Helper::loginUserToken($email, $password, $token);
    }

    function onGetHydrantsNearby()
    {
        $long = input('long');
        $lat = input('lat');
        $coord_delta = input('coord_delta');

        $resp = array();
        $h = Hydrant::whereBetween('long', [$long-$coord_delta, $long+$coord_delta])->whereBetween('lat', [$lat-$coord_delta, $lat+$coord_delta])->get();
        if ($h !== null && count($h)>0) {
            foreach ($h as $hydrant) {
                $resp[] = array(
                    'id' => $hydrant->id,
                    'name' => $hydrant->name,
                    'flowrate' => (int)$hydrant->flowrate,
                    'long' => (float)$hydrant->long,
                    'lat' => (float)$hydrant->lat,
                    'type' => $hydrant->hydranttype->name,
                    'icon' => ($hydrant->hydranttype->image ? $hydrant->hydranttype->image->path : '')
                );
            }
        }
        return $resp;
    }

    function onGetHydrantsNearbyV2()
    {
        $long = input('long');
        $lat = input('lat');
        $max_hydrants = input('max_hydrants');
        $coord_delta = input('coord_delta');

        $resp = array();
        $resp['hydrant_types'] = array();
        $resp['hydrants'] = array();
        $h = Hydrant::whereBetween('long', [$long-$coord_delta, $long+$coord_delta])->whereBetween('lat', [$lat-$coord_delta, $lat+$coord_delta])->get();
        $candidates = array();
        if ($h !== null && count($h)>0) {
            foreach ($h as $hydrant) {
                $candidates[] = array(
                    'id' => $hydrant->id,
                    'name' => $hydrant->name,
                    'flowrate' => (int)$hydrant->flowrate,
                    'long' => (float)$hydrant->long,
                    'lat' => (float)$hydrant->lat,
                    'distance' => (float)$hydrant->haversineGreatCircleDistance($lat, $long),
                    'hydranttype_id' => (int)$hydrant->hydranttype_id
                );
            }
        }
        usort($candidates, function($a, $b) {
            if ($a['distance'] == $b['distance']) {
                return 0;
            }
            return ($a['distance'] < $b['distance']) ? -1 : 1;
        });

        $filteredHydrants = [];
        foreach ($candidates as $hydrant) {
            $isDuplicate = false;
            foreach ($filteredHydrants as $filteredHydrant) {
                if ($hydrant['hydranttype_id'] == $filteredHydrant['hydranttype_id']) {
                    if (Hydrant::CalcHaversineGreatCircleDistance($hydrant['lat'], $hydrant['long'], $filteredHydrant['lat'], $filteredHydrant['long']) < 5.0) { // 5 meter
                        $isDuplicate = true;
                        break;
                    }
                }
            }
            if (!$isDuplicate) {
                $filteredHydrants[] = $hydrant;
            }
        }

        $resp['hydrants'] = array_slice($filteredHydrants, 0, $max_hydrants);

        $ht = Hydranttype::all();
        if ($ht !== null && count($ht)>0) {
            foreach ($ht as $hydranttype) {
                $resp['hydrant_types'][] = array(
                    'id' => $hydranttype->id,
                    'name' => $hydranttype->name,
                    'icon' => ($hydranttype->image ? $hydranttype->image->path : ''),
                    'svg_tactical' => $hydranttype->svg_tactical,
                    'svg_overview' => $hydranttype->svg_overview,
                );
            }
        }
        return $resp;
    }

    function onGetAssemblyAreasNearby()
    {
        $long = input('long');
        $lat = input('lat');
        $coord_delta = input('coord_delta');

        $resp = array();
        $a = AssemblyArea::whereBetween('long', [$long-$coord_delta, $long+$coord_delta])->whereBetween('lat', [$lat-$coord_delta, $lat+$coord_delta])->get();
        if ($a !== null && count($a)>0) {
            foreach ($a as $area) {
                $resp[] = $area;
            }
        }
        return $resp;
    }

    function onGetEinsatzobjekteNearby()
    {
        $long = input('long');
        $lat = input('lat');
        $coord_delta = input('coord_delta');
        $token = input('token');
        $alert_id = intval(input('alert_id')); // TODO: maybe limit object docu to 6 hours after alert only to prevent spoofing
        $trustedUser = false;

        if (Helper::isJwt($token)) {
            $guard = app('JWTGuard');
            $user = $guard->user();

            if ($user != null && $user->allow_app_usage) {
                $alert = $user->alerts()->find($alert_id);
                if ( ($alert->station !== null && $alert->station->alert_details_protected == 0) || ($alert->station !== null && $alert->station->alert_details_protected == 1 && Helper::userCanViewAlertDetails($user, $alert->station->id)) ) {
                    $trustedUser = true;
                }
            }
        }

        $device = Device::where('token', '=', $token)->first();

        $resp = array();
        $a = Einsatzobjekt::whereBetween('long', [$long-$coord_delta, $long+$coord_delta])->whereBetween('lat', [$lat-$coord_delta, $lat+$coord_delta])->get();
        if ($a !== null && count($a)>0) {
            foreach ($a as $obj) {
                $trustedDevice = ($device != null && $device->station_id == $obj->station_id);
                $resp[] = $obj->asInformationReducedArray($trustedUser || $trustedDevice);
            }
        }
        return $resp;
    }

    function onGetRescuePointsNearby()
    {
        $long = input('long');
        $lat = input('lat');
        $coord_delta = input('coord_delta');

        $resp = array();
        $rps = RescuePoint::whereBetween('long', [$long-$coord_delta, $long+$coord_delta])->whereBetween('lat', [$lat-$coord_delta, $lat+$coord_delta])->get();
        if ($rps !== null && count($rps)>0) {
            foreach ($rps as $rp) {
                $resp[] = $rp;
            }
        }
        return $resp;
    }

    public function scopeNearby($query, $lon, $lat)
    {
        $coord_delta = 0.1; // 0.1 degree
        return $query->whereBetween('long', [$lon-$coord_delta, $lon+$coord_delta])->whereBetween('lat', [$lat-$coord_delta, $lat+$coord_delta]);
    }

    function onGetMonitorVersion()
    {
        return $this->gui_version;
    }
    function onPostError()
    {
        $token = input('token');
        $msg = input('msg');
        $url = input('url');
        $lineNo = input('lineNo');
        $columnNo = input('columnNo');
        $error = input('error');

        $dError = new DeviceError();
        $dError->msg = $msg;
        $dError->url = $url;
        $dError->line_no = $lineNo;
        $dError->column_no = $columnNo;
        $dError->error = $error;
        $device = Device::where('token', '=', $token)->first();
        if ($device !== null) {
            $dError->device_id = $device->id;
        } else {
            $dError->device_id = 0;
        }
        $dError->save();
    }

    function onTriggerGpioAlert()
    {
        $token = input('token');
        $gpio_alert_trigger_id = intval(input('gpio_alert_trigger_id'));

        if ($token === null || strlen($token) <= 0 || $gpio_alert_trigger_id <= 0) {
            return response()->json([
                'error' => 'Invalid token or gpio_alert_trigger_id'
            ], 400); // HTTP 400 Bad Request
        }

        $device = Device::where('token', '=', $token)
            ->with(['gpio_alert_triggers' => function ($query) {
                $query->where('enabled', true);
            }])->first();

        if ($device === null) {
            return response()->json([
                'error' => 'Device not found'
            ], 404); // HTTP 404 Not Found
        }

        if ($device->state == Device::$STATE_ACTIVE) {
            // Suche den Trigger
            $trigger = $device->gpio_alert_triggers->find($gpio_alert_trigger_id);

            if ($trigger !== null && $trigger->enabled && $trigger->station_id == $device->station_id) {
                // Trigger auslösen
                $trigger->triggerAlert();

                return response()->json([
                    'success' => 'GPIO alert trigger executed successfully'
                ], 200); // HTTP 200 OK
            } else {
                return response()->json([
                    'error' => 'Trigger not found or not enabled or mismatch in station_id'
                ], 400); // HTTP 400 Bad Request
            }
        }

        // Gerät ist nicht aktiv
        return response()->json([
            'error' => 'Device is not active'
        ], 400); // HTTP 400 Bad Request
    }
}