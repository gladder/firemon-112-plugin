<?php namespace pm\Firemon112\Components;


use RainLab\User\Components\Session as BaseSession;

use Cms\Classes\ComponentBase;
use Auth;
use Session;
use Redirect;
use pm\Firemon112\Models\Helper;
use pm\Firemon112\Models\Settings;

/**
 * This Component makes use of \RainLab\User\Components\Session
 * to handle secured access on pages while bein able to
 * define a group property that needs to be true instead
 * of defining the group itself which is not flexible enough for us
 */
class FiremonSession extends BaseSession
{
    public function componentDetails()
    {
        return [
            'name'        => 'Firemon Session Component',
            'description' => 'Handles security access based on group flag extending the Rainlab User Plugin'
        ];
    }

    public function defineProperties()
    {
        $existing = parent::defineProperties();
        $existing['groupFlags'] = [
            'title'       => 'Group Flags',
            'description' => 'Group Flags that are allowed to get access',
            'type'        => 'set',
            'default'     => '',
            'items'       => [
                Helper::$PERMISSION_IS_ROLE_STATION => "Feuerwehr-Admin",
                Helper::$PERMISSION_IS_ROLE_PRESENCE => "Dienstplan-Admin",
                Helper::$PERMISSION_IS_ROLE_ALERT => "Alarm-Admin",
                Helper::$PERMISSION_IS_ROLE_ALERT_READONLY => "Alarm-Readonly",
                Helper::$PERMISSION_IS_ROLE_EVENTS => "Termine-Admin"
            ]
        ];
        return $existing;
    }

    protected function checkUserSecurity():bool
    {
        $group_flags = $this->property('groupFlags', []);
        if (is_array($group_flags) && count($group_flags) > 0) {
            return $this->checkUserSecurityByGroupFlags();
        }
        return parent::checkUserSecurity();
    }

    function onRun() {
        $this->page['user'] = null;
        $this->page['station'] = null;
        $this->page['alarmierung_admin'] = false;
        $this->page['alarmierung_readonly'] = false;
        $this->page['dienstplan_admin'] = false;
        $this->page['feuerwehr_admin'] = false;
        $this->page['termine_admin'] = false;
        $this->page['google_maps_api_key'] = Settings::get('google-maps-api-key');

        $user = Helper::GetFeUser();
        $station = Helper::GetFeStation();
        if ($user !== null && $station !== null) {
            $this->page['alarmierung_admin'] = Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_ALERT);
            $this->page['alarmierung_readonly'] = Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_ALERT_READONLY);
            $this->page['dienstplan_admin'] = Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_PRESENCE);
            $this->page['feuerwehr_admin'] = Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_STATION);
            $this->page['termine_admin'] = Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_EVENTS);
            $this->page['user'] = $user;
            $this->page['station'] = $station;
        }
    }

    private function checkUserSecurityByGroupFlags() {
        $allowedGroup = $this->property('security', parent::ALLOW_ALL);
        $groupFlags = $this->property('groupFlags', []);

        if (!is_array($groupFlags) || count($groupFlags) <= 0) {
            return false;
        }
        if (Auth::check()) {
            if ($allowedGroup == parent::ALLOW_GUEST) {
                return false;
            }

            $user = parent::user();
            if ($user === null) {
                return false;
            }

            $stationId = intval(Session::get('currentStationId', 0));
            if ($stationId <= 0) {
                $stationId = $user->station_id;
            }
            foreach ($groupFlags as $groupFlag) {
                if (Helper::userHasPermission($user, $stationId, $groupFlag)) {
                    return true;
                }
            }
            return false;
        } else {
            if ($allowedGroup == parent::ALLOW_USER) {
                return false;
            }
            return true;
        }
    }


    function onChangeStation() {
        Session::put('currentStationId', intval(post('station_id')));
        return Redirect::refresh();
    }
}