<?php namespace pm\Firemon112\Models;

use Model;
use \Cms\Classes\Theme;
use pm\Firemon112\Traits\ContactableExtension;
use pm\Firemon112\Traits\FileUploadExtension;
use pm\Firemon112\Traits\SortOrderExtension;

/**
 * Model
 */
class AssemblyArea extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use FileUploadExtension, SortOrderExtension, ContactableExtension;

    protected $dates = ['deleted_at'];

    protected $appends = [
        'img_small',
        'img_big'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $attachMany = [
        'files' => ['System\Models\File', 'delete' => true]
    ];

    public $morphMany = [
        'contacts' => [\pm\Firemon112\Models\Contact::class, 'name' => 'contactable', 'delete' => true],
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_assemblyarea';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label' => 'required|max:32'
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function getImgSmallAttribute() {
        return asset('themes/'.Theme::getActiveThemeCode()."/assets/icons/marker/br-small.png");
    }

    public function getImgBigAttribute() {
        return asset('themes/'.Theme::getActiveThemeCode()."/assets/icons/marker/br-big.png");
    }
}
