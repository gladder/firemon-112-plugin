<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Mail extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $fillable = ['mail','token','mail_text'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_mails';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function getMailDelaySecondsAttribute() {
        $pattern = "/\tfor.*;(.*)\n/";

        // Use preg_match_all to find all matches
        preg_match_all($pattern, $this->mail, $matches);

        // Check if we have at least two timestamps
        if (count($matches[1]) < 2) {
            return -1;
        }

        // Convert the matched timestamps from the end of the array (first received) and the start of the array (last received) to Unix timestamps
        $firstReceivedTimestamp = strtotime(trim($matches[1][count($matches[1]) - 1]));
        $lastReceivedTimestamp = strtotime(trim($matches[1][0]));

        // Calculate the delay in seconds
        $delayInSeconds = $lastReceivedTimestamp - $firstReceivedTimestamp;

        // Return the delay in seconds
        return $delayInSeconds;
    }
}
