<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;
use pm\Firemon112\Traits\FileUploadExtension;
use pm\Firemon112\Traits\SortOrderExtension;
use pm\Firemon112\Traits\DateTimeExtension;

/**
 * Model
 */
class News extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use FileUploadExtension, SortOrderExtension, DateTimeExtension;

    protected $dates = ['deleted_at','visible_from','visible_to'];

    public static $PRIO_HIGH_VALUE = 100;
    public static $PRIO_NORMAL_VALUE = 50;
    public static $PRIO_LOW_VALUE = 10;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_news';

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'content' => 'required',
        'category' => 'required',
        'station_id' => 'required',
        'visible_from' => 'required|date',
        'visible_to' => 'required|date|after:visible_from',
        'prio' => 'required',
        'image' => ' nullable|image'
    ];

    public $attributes = [
        'station_id' => 0,
    ];

    public $fillable = [
        'category',
        'station_id',
        'visible_from',
        'visible_to',
        'content',
        'content_long',
        'is_general',
        'prio',
    ];

    protected $appends = [
        'category_readable',
        'category_svg'
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public $attachMany = [
        'files' => ['System\Models\File', 'delete' => true]
    ];

    public $morphMany = [
        'reminders' => [\pm\Firemon112\Models\Reminder::class, 'name' => 'remindable'],
    ];

    public static function getCategorySvgs() {
        return array(
            'geraetehaus' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M12,3L20,9V21H15V14H9V21H4V9L12,3Z" /></svg>',
            'ausruestung' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M10.5,7H11.75L12,5H10.25L6,7.5V9H4V6.5L10,3H12V2H14V3H16L17,2.5V5.5L16,5H14L14.25,7H15.5A1.5,1.5 0 0,1 17,8.5V22H9V8.5A1.5,1.5 0 0,1 10.5,7Z" /></svg>',
            'fahrzeug' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M17.04,2C16.85,2 16.66,2.04 16.5,2.14L5.59,8.5H9.55L17.5,3.86C18,3.58 18.13,2.97 17.85,2.5C17.68,2.2 17.38,2 17.04,2M16,8V10H3A2,2 0 0,0 1,12H2V15H1V19H3A3,3 0 0,0 6,22A3,3 0 0,0 9,19H15A3,3 0 0,0 18,22A3,3 0 0,0 21,19H23V12.5L19.5,8H16M18,9.5H19L21.5,12.5V13.5H18V9.5M4,12H7V15H4V12M9,12H12V15H9V12M14,12H16V15H14V12M6,17.5A1.5,1.5 0 0,1 7.5,19A1.5,1.5 0 0,1 6,20.5A1.5,1.5 0 0,1 4.5,19A1.5,1.5 0 0,1 6,17.5M18,17.5A1.5,1.5 0 0,1 19.5,19A1.5,1.5 0 0,1 18,20.5A1.5,1.5 0 0,1 16.5,19A1.5,1.5 0 0,1 18,17.5Z" /></svg>',
            'organisatorisches' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M12 1A2.5 2.5 0 0 0 9.5 3.5A2.5 2.5 0 0 0 11 5.79V7H7A2 2 0 0 0 5 9V9.71A2.5 2.5 0 0 0 3.5 12A2.5 2.5 0 0 0 5 14.29V15H4A2 2 0 0 0 2 17V18.21A2.5 2.5 0 0 0 .5 20.5A2.5 2.5 0 0 0 3 23A2.5 2.5 0 0 0 5.5 20.5A2.5 2.5 0 0 0 4 18.21V17H8V18.21A2.5 2.5 0 0 0 6.5 20.5A2.5 2.5 0 0 0 9 23A2.5 2.5 0 0 0 11.5 20.5A2.5 2.5 0 0 0 10 18.21V17A2 2 0 0 0 8 15H7V14.29A2.5 2.5 0 0 0 8.5 12A2.5 2.5 0 0 0 7 9.71V9H17V9.71A2.5 2.5 0 0 0 15.5 12A2.5 2.5 0 0 0 17 14.29V15H16A2 2 0 0 0 14 17V18.21A2.5 2.5 0 0 0 12.5 20.5A2.5 2.5 0 0 0 15 23A2.5 2.5 0 0 0 17.5 20.5A2.5 2.5 0 0 0 16 18.21V17H20V18.21A2.5 2.5 0 0 0 18.5 20.5A2.5 2.5 0 0 0 21 23A2.5 2.5 0 0 0 23.5 20.5A2.5 2.5 0 0 0 22 18.21V17A2 2 0 0 0 20 15H19V14.29A2.5 2.5 0 0 0 20.5 12A2.5 2.5 0 0 0 19 9.71V9A2 2 0 0 0 17 7H13V5.79A2.5 2.5 0 0 0 14.5 3.5A2.5 2.5 0 0 0 12 1M12 2.5A1 1 0 0 1 13 3.5A1 1 0 0 1 12 4.5A1 1 0 0 1 11 3.5A1 1 0 0 1 12 2.5M6 11A1 1 0 0 1 7 12A1 1 0 0 1 6 13A1 1 0 0 1 5 12A1 1 0 0 1 6 11M18 11A1 1 0 0 1 19 12A1 1 0 0 1 18 13A1 1 0 0 1 17 12A1 1 0 0 1 18 11M3 19.5A1 1 0 0 1 4 20.5A1 1 0 0 1 3 21.5A1 1 0 0 1 2 20.5A1 1 0 0 1 3 19.5M9 19.5A1 1 0 0 1 10 20.5A1 1 0 0 1 9 21.5A1 1 0 0 1 8 20.5A1 1 0 0 1 9 19.5M15 19.5A1 1 0 0 1 16 20.5A1 1 0 0 1 15 21.5A1 1 0 0 1 14 20.5A1 1 0 0 1 15 19.5M21 19.5A1 1 0 0 1 22 20.5A1 1 0 0 1 21 21.5A1 1 0 0 1 20 20.5A1 1 0 0 1 21 19.5Z" /></svg>',
            'sonstiges' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M11,9H13V7H11M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M11,17H13V11H11V17Z" /></svg>'
        );
    }

    public static function getCategoryOptions() {
        return array(
            'geraetehaus' => 'Gerätehaus',
            'ausruestung' => 'Ausrüstung',
            'fahrzeug' => 'Fahrzeug',
            'organisatorisches' => 'Organisatorisches',
            'sonstiges' => 'Sonstiges'
        );
    }

    public function getCategoryReadableAttribute() {
        $all = self::getCategoryOptions();
        if (array_key_exists($this->category, $all)) {
            return $all[$this->category];
        }
        return "";
    }

    public function getCategorySvgAttribute() {
        $all = self::getCategorySvgs();
        if (array_key_exists($this->category, $all)) {
            return $all[$this->category];
        }
        return "";
    }

    public static function getPrioOptions() {
        return array(
            self::$PRIO_NORMAL_VALUE => 'Normal',
            self::$PRIO_HIGH_VALUE => 'Hoch',
            self::$PRIO_LOW_VALUE => 'Niedrig',
        );
    }

    public function getPrioReadableAttribute() {
        $all = self::getPrioOptions();
        if (array_key_exists($this->prio, $all)) {
            return $all[$this->prio];
        }
        return "";
    }

    public function getVisibleFromFormattedAttribute() {
        return $this->getFormattedDateTime($this->visible_from);
    }

    public function getVisibleToFormattedAttribute() {
        return $this->getFormattedDateTime($this->visible_to);
    }

    public function getIsImageAttribute() {
        return $this->image != null;
    }

    public function beforeDelete() {
        $this->reminders()->delete();
    }

    public function scopeActive($query)
    {
        // TODO: Make DB Mig to allow visible_from and visible_to to be null
        $query->where(function ($query) {
            // Check if news is either already visible or has no start date
            $query->whereNull('visible_from')
                  ->orWhere('visible_from', '<=', \DB::raw('CURRENT_TIMESTAMP()'));
        })->where(function ($query) {
            // Check if news is still visible or has no end date
            $query->whereNull('visible_to')
                  ->orWhere('visible_to', '>=', \DB::raw('CURRENT_TIMESTAMP()'));
        })
        ->orderBy('prio', 'DESC')
        ->orderBy('created_at', 'DESC');
    }

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function scopeGeneral($query)
    {
        return $query->where('station_id', 0)->where('is_general', 1);
    }

    public function scopeByYear($query, $year)
    {
        $query->whereRaw('YEAR(visible_from) = ' . intval($year))->orderBy('visible_from','DESC');
    }

    public function getIsPrioHighAttribute() {
        return $this->prio == self::$PRIO_HIGH_VALUE;
    }

    public function getIsVisibleAttribute() {
        return $this->isNowBetween($this->visible_from, $this->visible_to);
    }

    public function afterSave() {
        if ($this->station_id > 0 && $this->station) {
            foreach ($this->station->all_users as $user) {
                $this->updateReminders($user);
            }
        }
        // TODO: General News Reminders!
    }

    public function updateReminders($user) {
        $this->reminders()->user($user)->unhandled()->delete();
        $userSettings = Helper::GetUserNotificationSettings($user);
        if ($userSettings[Helper::$USER_NOTIFICATION_ALLOW_NEWS_PUSH] === true) {
            $this->createReminderIfNeeded($user->id, Reminder::$NOTIFICATION_TYPE_PUSH);
        }
    }

    private function createReminderIfNeeded($user_id, $reminder_type) {
        if ($this->visible_from->isFuture()) {
            $this->reminders()->create([
                'user_id' => $user_id,
                'reminder_trigger_at' => $this->visible_from,
                'notification_type' => $reminder_type,
            ]);
        }
    }
}
