<?php

namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;
use pm\Firemon112\Models\Event;


/**
 * This is the Wachendisplay model class.
 *
 * @package pm\Firemon112\Models
 *
 * @property int $id
 * @property int $station_id
 * @property string $name
 * @property array $enable_eventcategories
 * @property array $enable_schedule
 * @property array $module_layout
 * @property Carbon\Carbon $deleted_at
 * @property Carbon\Carbon $created_at
 * @property Carbon\Carbon $updated_at
 * @property pm\Firemon112\Models\Station $station
 * @property pm\Firemon112\Models\Device[] $devices
 */
class Wachendisplay extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $jsonable = ['enable_eventcategories','enable_schedule','module_layout'];

    public static $MODULE_KEY_NEWS_TEXT = "news_text";
    public static $MODULE_KEY_NEWS_IMAGE = "news_image";
    public static $MODULE_KEY_UPCOMING_EVENTS = "events_upcoming";
    public static $MODULE_KEY_CURRENT_EVENT = "current_event";
    public static $MODULE_KEY_DWD_WARNING = "dwd_warning";
    public static $MODULE_KEY_ALERT_STATS = "alert_stats";
    public static $MODULE_KEY_USERS_UNAVAILABLE = "users_unavailable";
    public static $MODULE_KEY_PRESENCE = "presence";
    public static $MODULE_CONFIG_PRESENCE_DAYSHIFT = "dayshift";
    public static $MODULE_CONFIG_PRESENCE_CATEGORY = "category";

    public static $MODULE_CONFIG_PRESENCE = "cfg_presence";

    /**-
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_wachendisplay';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'enable_eventcategories' => 'array',
        'enable_schedule' => 'array',
        'module_layout' => 'array',
    ];

    public static function getWeekdayOptions() {
        return array(
            '1' => 'Montags',
            '2' => 'Dienstags',
            '3' => 'Mittwochs',
            '4' => 'Donnerstags',
            '5' => 'Freitags',
            '6' => 'Samstags',
            '7' => 'Sonntags'
        );
    }

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];
    public $hasMany = [
        'devices' => 'pm\Firemon112\Models\Device'
    ];

    public function getScheduleStatusByFlags($dow, $slot): bool
    {
        foreach ($this->enable_schedule as $dowItem) {
            if ($dowItem['dow'] == $dow && is_array($dowItem['on'])) {
                return in_array($slot, $dowItem['on']);
            }
        }
        return false;
    }

    public function getTodaysStatusFlag(): array
    {
        foreach ($this->enable_schedule as $dowItem) {
            if ($dowItem['dow'] == Carbon::now('Europe/Berlin')->isoWeekday() && is_array($dowItem['on'])) {
                return $dowItem['on'];
            }
        }
        return [];
    }

    /**
     * @param $slots
     * @return array
     */
    public static function getTimeSeriesFromSlots($slots): array
    {
        $seconds = 0;
        $on = false;
        $series = [];
        // this is 15 Minutes based - each slot = 15 Mins
        for ($i = 0; $i < 96; $i++) {
            if (in_array($i, $slots)) {
                if (!$on) {
                    $on = true;
                    $series[] = array(
                        'from' => gmdate('H:i:s', $seconds),
                        'to' => 0
                    );
                }
            } else {
                if ($on) {
                    $series[count($series)-1]['to'] = gmdate('H:i:s', $seconds);
                }
                $on = false;
            }
            $seconds += 15*60;
        }
        // set to if last item was on
        if ($on) {
            $series[count($series)-1]['to'] = gmdate('H:i:s', $seconds);
        }
        return $series;
    }

    public static function getTodaysStatusFlagsForEvent($event) {
        $res = [];
        if ($event != null && $event->getIsTodayAttribute()) {
            $today = Carbon::now('Europe/Berlin')->startOfDay();
            $today->setTime(0,0,0);
            $from = Carbon::parse($event->date_from)->subMinutes(15); // enable 15 mins before
            $to = Carbon::parse($event->date_to);
            for ($i = 0; $i < 96; $i++) {
                if ($today->between($from, $to)) {
                    $res[] = $i;
                }
                $today->addMinutes(15);
            }
        }
        return $res;
    }

    function getTodaySlots() {
        $slots = [];
        $slots = array_merge($slots, $this->getTodaysStatusFlag());
        if ($this->enable_eventcategories != null) {
            foreach ($this->enable_eventcategories as $cat) {
                $events = Event::byStation($this->station_id)->byCategory($cat)->today()->get();
                foreach ($events as $event) {
                    $slots = array_merge($slots, self::getTodaysStatusFlagsForEvent($event));
                }
            }
        }
        return array_unique($slots, SORT_NUMERIC);
    }

    public static function getModules() {
        return [
            [
                'name' => 'Neuigkeiten (Text)',
                'key' => self::$MODULE_KEY_NEWS_TEXT,
                'reloadIntervalSeconds' => 5*60
            ],
            [
                'name' => 'Neuigkeiten (Bild)',
                'key' => self::$MODULE_KEY_NEWS_IMAGE,
                'reloadIntervalSeconds' =>  5*60
            ],
            [
                'name' => 'Aktuelles Event',
                'key' => self::$MODULE_KEY_CURRENT_EVENT,
                'reloadIntervalSeconds' => 5*60
            ],
            [
                'name' => 'Anstehende Termine',
                'key' => self::$MODULE_KEY_UPCOMING_EVENTS,
                'reloadIntervalSeconds' => 15*60
            ],
            [
                'name' => 'DWD Wetterwarnungen',
                'key' => self::$MODULE_KEY_DWD_WARNING,
                'reloadIntervalSeconds' => 30*60
            ],
            [
                'name' => 'Abwesenheiten (TODO!)',
                'key' => self::$MODULE_KEY_USERS_UNAVAILABLE,
                'reloadIntervalSeconds' => 30*60
            ],
            [
                'name' => 'Einsatz Statistik (TODO!)',
                'key' => self::$MODULE_KEY_ALERT_STATS,
                'reloadIntervalSeconds' => 30*60
            ],
            [
                'name' => 'Dienstplan',
                'key' => self::$MODULE_KEY_PRESENCE,
                'reloadIntervalSeconds' => 2*60,
                'cfg' => self::getModuleConfig(self::$MODULE_CONFIG_PRESENCE)
            ],
        ];
    }

    public static function getModuleConfig($key) {
        switch ($key) {
            case self::$MODULE_CONFIG_PRESENCE: {
                return [
                    'dropdown' =>
                        [
                            'label' => 'Kategorie',
                            'name' => self::$MODULE_CONFIG_PRESENCE_CATEGORY,
                            'values' => Presence::getCategoryOptions()
                        ],
                    'checkbox' =>
                        [
                            'label' => 'Plan von Morgen',
                            'name' => self::$MODULE_CONFIG_PRESENCE_DAYSHIFT
                        ]
                ];
                break;
            }
            default: {
                return [];
            }
        }
    }

    public function getRenderedModule($key, $cfg) {
        switch ($key) {
            case self::$MODULE_KEY_DWD_WARNING: {
                return $this->getModuleDwdWarning();
                break;
            }
            case self::$MODULE_KEY_UPCOMING_EVENTS: {
                return $this->getModuleUpcomingEvents();
                break;
            }
            case self::$MODULE_KEY_NEWS_TEXT: {
                return $this->getModuleNewsText();
                break;
            }
            case self::$MODULE_KEY_NEWS_IMAGE: {
                return $this->getModuleNewsImage();
                break;
            }
            case self::$MODULE_KEY_CURRENT_EVENT: {
                return $this->getModuleCurrentEvent();
                break;
            }
            case self::$MODULE_KEY_PRESENCE: {
                return $this->getModulePresence($cfg);
                break;
            }
            default: {
                return '<p>no output</p>';
            }
        }
    }

    private function getModuleUpcomingEvents(): string
    {
        $events = Event::byStation($this->station_id)->upcoming()->take(6)->get();
        $html = "";

        foreach ($events as $event) {
            $from = new Carbon($event->date_from);
            $time = $from->format('H:i');
            $html .= <<<EOL
            <div class="appt_item">
                <div class="appt_date">
                    <p class="appt_month">$from->shortMonthName</p>
                    <p class="appt_day">$from->day</p>
                </div>
                <div class="appt_appt">
                    <div class="appt_row">
                        <div class="appt_time">
                            <p class="appt_pill">$time</p>
                        </div>
                        <div class="appt_label">
                            <p class="appt_label_head">$event->label</p>
                            <p class="appt_label_sub">$event->location&nbsp;<span class="appt_tag appt_tag_$event->category">$event->categoryReadable</span></p>
                        </div>
                    </div>
                </div>
            </div>
EOL;
        }
        if (strlen($html) <= 0) {
            $html = "<p>Aktuell keine Termine</p>";
        }

        if (strlen($html)>0) {
            return '<div class="module-canvas-white">'.$html.'</div>';
        }
        return "";
    }

    private function getModuleCurrentEvent() {
        $event = Event::byStation($this->station_id)->today()->upcoming()->first();
        $html = "";
        if ($event != null) {
            $from = new Carbon($event->date_from);
            $time = $from->format('H:i');
            $sub_cnt = "";
            if ($event->subscription_required) {
                $cnt_true_items = 0;
                $cnt_false_items = 0;
                if (count($event->subscriptions)>0) {
                    $cnt_true_items = $event->subscriptions->filter(function ($v) {
                        return $v->available;
                    })->count();
                    $cnt_false_items = $event->subscriptions->filter(function ($v) {
                        return !$v->available;
                    })->count();
                }
                $sub_cnt = <<<EOL
                <div class="ce_count">
                    <p class="count available">
                        <span>$cnt_true_items</span>
                        Zugesagt
                    </p>
                    <p class="count not_available">
                        <span>$cnt_false_items</span>
                        Abgesagt
                    </p>
                </div>
EOL;
            }
            $info = "";
            if ($event->duty) {
                $info .= <<<EOL
                    <p class="hint" style="color:red;">
                         <i class="icon icon-info-sign"></i> Pflichttermin!
                    </p>
EOL;
            }
            if ($event->public) {
                $info .= <<<EOL
                    <p class="hint">
                         <i class="icon icon-info-sign"></i> Öffentlicher Termin
                    </p>
EOL;
            }
            $html .= <<<EOL
            <div class="ce_row">
                 <div class="ce_left">
                    <i class="icon icon-2x icon-calendar" style="color:#666666"></i>
                    <p class="time">HEUTE<br />$time</p>
                 </div>
                 <div class="ce_right">
                    <p class="caption">$event->label</p>
                    <p class="note">$event->description</p>
                    <p class="hint">
                        <i class="icon icon-map-marker"></i> $event->location&nbsp;<span class="appt_tag appt_tag_$event->category">$event->categoryReadable</span></p>
                    </p>
                    $info
                    $sub_cnt
                </div>
            </div>
EOL;

        }
        if (strlen($html) <= 0) {
            $html = "<p>Heute keine Termine</p>";
        }

        if (strlen($html) > 0) {
            return '<div class="module-canvas-white">' . $html . '</div>';
        }
    }

    private function getModuleNewsText(): string
    {
        $news = News::byStation($this->station_id)->active()->get();
        $html = "";

        foreach ($news as $n) {
            if (!$n->isImage) {
                $additionalCss = "";
                if ($n->getIsPrioHighAttribute()) {
                    $additionalCss .= " news_prio_high";
                }
                $html .= <<<EOL
            <div class="news_item$additionalCss">
                $n->categorySvg
                <p>$n->content</p>
            </div>
EOL;
            }
        }
        if (strlen($html) <= 0) {
            $html = "<p>Aktuell keine News</p>";
        }

        if (strlen($html)>0) {
            return '<div class="module-canvas-clear">'.$html.'</div>';
        }
        return "";
    }

    private function getModuleNewsImage(): string
    {
        $news = News::byStation($this->station_id)->active()->get();
        $html = "";
        $cnt = 0;
        foreach ($news as $n) {
            if ($n->isImage) {
                $additionalCss = "";
                if ($n->getIsPrioHighAttribute()) {
                    $additionalCss .= " news_prio_high";
                }
                $imgPath = $n->image->path;
                $html .= <<<EOL
                <div class="news_image_item">
                    <img src="$imgPath" alt="" class="img-responsive"/>
                    <div class="news_item$additionalCss">
                        <p>$n->content</p>
                    </div>
                </div>
EOL;
                $cnt++;
            }
        }
        if (strlen($html) <= 0) {
            $html = "<p>Aktuell keine News</p>";
        }

        if (strlen($html)>0) {
            $result = '<div class="module-canvas-clear news_image_items">'.$html.'</div>';
            $slider = <<<EOL
            <script>
                let slideIndex = 0;
                let newsImageTimeoutId = 0;
                showSlides();
                function showSlides() {
                  let i;
                  let slides = $('.news_image_item');
                  for (i = 0; i < slides.length; i++) {
                    $(slides[i]).hide();  
                  }
                  slideIndex++;
                  if (slideIndex > slides.length) {slideIndex = 1}
                  $(slides[slideIndex-1]).fadeIn(500);  
                  if (newsImageTimeoutId > 0) {
                    clearTimeout(newsImageTimeoutId);
                  }
                  newsImageTimeoutId = setTimeout(showSlides, 30000); // Change image every 2 seconds
                } 
            </script>
EOL;
            return $result . ($cnt > 1 ? $slider : '');
        }
        return "";
    }

    private function getModuleDwdWarning(): string
    {
        return '<div class="module-canvas-white"><img src="https://www.dwd.de/DWD/warnungen/warnapp_gemeinden/json/warnungen_gemeinde_map_shh.png" alt="DWD Warnwetter" class="img-responsive"/></div>';
    }

    private function getModulePresence($cfg): string
    {
        $dayshift = false;
        $category = "";
        if (is_array($cfg) && array_key_exists(self::$MODULE_KEY_PRESENCE.'_'.self::$MODULE_CONFIG_PRESENCE_DAYSHIFT, $cfg)) {
            $dayshift = $cfg[self::$MODULE_KEY_PRESENCE.'_'.self::$MODULE_CONFIG_PRESENCE_DAYSHIFT] == "true";
        }
        if (is_array($cfg) && array_key_exists(self::$MODULE_KEY_PRESENCE.'_'.self::$MODULE_CONFIG_PRESENCE_CATEGORY, $cfg)) {
            $category = $cfg[self::$MODULE_KEY_PRESENCE.'_'.self::$MODULE_CONFIG_PRESENCE_CATEGORY];
        }
        $display_item = null;
        if ($dayshift) {
            if (strlen($category)>0) {
                $display_item = Presence::byStation($this->station_id)->byCategory($category)->activeDayshift()->first();
            } else {
                $display_item = Presence::byStation($this->station_id)->activeDayshift()->first();
            }
        } else {
            if (strlen($category)>0) {
                $display_item = Presence::byStation($this->station_id)->byCategory($category)->active()->first();
            } else {
                $display_item = Presence::byStation($this->station_id)->active()->first();
            }
        }

        if ($display_item === null) {
            return '<div class="module-canvas-white">kein Dienstplan aktiv</div>';
        } else {
            return '<div class="module-canvas">'.$display_item->asHtml().'</div>';
        }
    }

    public function getModuleLayoutJsonAttribute() {
        return json_encode($this->module_layout);
    }

    public function getEnableScheduleJsonAttribute() {
        return json_encode($this->enable_schedule);
    }

    public function getTodaysSlotsTimeSeries() {
        return self::getTimeSeriesFromSlots($this->getTodaySlots());
    }

    public function getLayoutVariant() {
        $layouts = self::getLayoutVariants();
        $module_layout = 1;
        if ($this->module_layout != null && array_key_exists('variant', $this->module_layout)) {
            $module_layout = $this->module_layout['variant'];
        }
        foreach ($layouts as $layout) {
            if ($layout['id'] == $module_layout) {
                return $layout;
            }
        }
        return $layouts[0];
    }

    public function getSelectedModules() {
        if ($this->module_layout != null && array_key_exists('modules', $this->module_layout)) {
            $selectedModules =  $this->module_layout['modules'];
            $allModules = self::getModules();
            // add module definition
            for ($i = 0; $i < count($selectedModules); $i++) {
                for ($j = 0; $j < count($allModules); $j++) {
                    if ($selectedModules[$i]['id'] == $allModules[$j]['key']) {
                        $selectedModules[$i]['reloadIntervalSeconds'] = $allModules[$j]['reloadIntervalSeconds'];
                        break;
                    }
                }
            }
            return $selectedModules;
        }
        return [];
    }

    public static function getLayoutVariants(): array
    {
        return [
            [
                'id' => 1,
                'cols' => [100]
            ],
            [
                'id' => 2,
                'cols' => [50,50]
            ],
            [
                'id' => 3,
                'cols' => [30,70]
            ],
            [
                'id' => 4,
                'cols' => [70,30]
            ],
            [
                'id' => 5,
                'cols' => [33,33,33]
            ],
            [
                'id' => 6,
                'cols' => [25,50,25]
            ],
            [
                'id' => 7,
                'cols' => [20,30,30,20]
            ],
            [
                'id' => 8,
                'cols' => [40,40,20]
            ],
        ];
    }

    public function getUsedModuleCountAttribute(): int
    {
        if ($this->module_layout != null && array_key_exists('modules', $this->module_layout)) {
            return count($this->module_layout['modules']);
        }
        return 0;
    }
}
