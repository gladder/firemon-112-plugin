<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Pushtoken extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    public static $TOKEN_TYPE_EXPO = "expo";
    public static $TOKEN_TYPE_FCM = "android";
    public static $TOKEN_TYPE_APN = "ios";

    public static $TOKEN_TOPIC_ALERT = "alert";
    public static $TOKEN_TOPIC_EVENT = "event";
    public static $TOKEN_TOPIC_NEWS = "news";
    public static $TOKEN_TOPIC_TRAINING = "training";
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_user_pushtoken';


    /**
     * @var array Validation rules
     */
    public $rules = [
        'token' => 'required',
        'token_type' => 'required',
        'device_name' => 'required',
        'user_id' => 'required|integer',
    ];

    public $fillable = [
        'token',
        'token_type',
        'device_name',
        'user_id'
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User',
    ];

    public static function getTokenTypeOptions() {
        return [
            self::$TOKEN_TYPE_EXPO => 'EXPO',
            self::$TOKEN_TYPE_FCM => 'Google FCM',
            self::$TOKEN_TYPE_APN => 'Apple APN',
        ];
    }

    public function isExpoToken() {
        return $this->token_type == self::$TOKEN_TYPE_EXPO;
    }

    public function isFcmToken() {
        return $this->token_type == self::$TOKEN_TYPE_FCM;
    }

    public function isApnToken() {
        return $this->token_type == self::$TOKEN_TYPE_APN;
    }
}
