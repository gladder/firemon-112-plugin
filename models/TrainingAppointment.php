<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class TrainingAppointment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_training_appointment';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'training' =>  'pm\Firemon112\Models\Training'
    ];

    protected $fillable = [
        'training_id',
        'from_datetime',
        'to_datetime',
        'note'
    ];
}
