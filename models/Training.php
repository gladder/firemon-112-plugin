<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;

/**
 * Model
 */
class Training extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_training';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label'                  => 'required|between:1,128'
    ];

    public $belongsTo = [
        'training_source' =>  'pm\Firemon112\Models\TrainingSource'
    ];

    public $hasMany = [
        'appointments' =>  'pm\Firemon112\Models\TrainingAppointment',
    ];

    protected $fillable = [
        'training_source_id',
        'label',
        'from_date',
        'to_date',
        'note',
        'slots_available',
        'identifier',
        'hash'
    ];

    public function beforeSave()
    {
        if (strlen($this->hash) <= 0) {
            $this->hash = self::hash($this->toArray());
        }
    }

    public static function hash($obj) {
        if (is_array($obj)) {
            return sha1(
                $obj['training_source_id'] .
                $obj['label'] .
                $obj['identifier'] .
                $obj['from_date']->toDateString()
            );
        }
        return sha1($obj); // may be string ;)
    }

    public function getIsRelatedAttribute() {
        if ($this->slots_available <= 0) {
            return false;
        }
        $now = Carbon::now('Europe/Berlin');
        return $now->lessThan($this->from_date);
    }

}
