<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class CustomSettings extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $SETTINGS_KEY = "settings_key";
    public static $KEY_PRESENCE_CATEGORIES = "presence-categories";
    public static $USER_NOTIFICATIONS_SETTINGS = "user-notifications-settings";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_custom_settings';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'settings_key' => 'required',
        'settings_payload' => 'required',
    ];

    public $fillable = [
        'settings_key',
        'settings_payload'
    ];

    public $morphTo = [
        'ownerable' => [],
    ];

    protected $jsonable = ['settings_payload'];

    public function getPayload() {
        if (is_array($this->settings_payload)) {
            return $this->settings_payload;
        }
        return [];
    }

    public function afterSave() {
        if ($this->ownerable_type == \RainLab\User\Models\User::class) {
            if ($this->settings_key == self::$USER_NOTIFICATIONS_SETTINGS) {
                self::UpdateUserRemindersForStation($this->ownerable, $this->ownerable->station);
                foreach ($this->ownerable->linked_stations as $station) {
                    self::UpdateUserRemindersForStation($this->ownerable, $station);
                }
            }
        }
    }

    public static function UpdateUserRemindersForStation($user, $station) {
        foreach ($user->getRelatedEvents($station->id) as $event) {
            $event->updateReminders($user);
        }
        foreach ($station->news as $news) {
            $news->updateReminders($user);
        }
    }
}