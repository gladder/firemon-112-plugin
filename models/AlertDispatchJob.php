<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\AlertDispatcher\AppDispatcher;
use pm\Firemon112\AlertDispatcher\CallDispatcher;
use pm\Firemon112\AlertDispatcher\EmailDispatcher;
use pm\Firemon112\AlertDispatcher\IAlertDispatcher;
use pm\Firemon112\AlertDispatcher\PushDispatcher;
use pm\Firemon112\AlertDispatcher\SmsDispatcher;
use Carbon\Carbon;
use pm\Firemon112\Classes\ExpoPush;

/**
 * Model
 */
class AlertDispatchJob extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $type_SMS = "sms";
    public static $type_EMAIL = "email";
    public static $type_CALL = "call";
    public static $type_APP = "app";
    public static $type_PUSH = "push";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_alert_dispatch_job';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = [
        'alert_id',
        'dispatch_type',
        'payload',
        'dry_run'
    ];

    public $jsonable = [
        'payload',
        'result'
    ];

    public $belongsTo = [
        'alert' => 'pm\Firemon112\Models\Alert',
        'user' => 'RainLab\User\Models\User',
    ];

    public static function getDispatchTypeOptions() {
        return [
            self::$type_SMS => 'SMS',
            self::$type_EMAIL => 'E-Mail',
            self::$type_APP => 'Web-App',
            self::$type_CALL => 'Anruf',
            self::$type_PUSH => 'PUSH Notification'
        ];
    }

    public function handle() {
        try {
            $dispatcher = null;

            switch ($this->dispatch_type) {
                case self::$type_SMS:
                    $dispatcher = new SmsDispatcher();
                    break;
                case self::$type_EMAIL:
                    $dispatcher = new EmailDispatcher();
                    break;
                case self::$type_CALL:
                    $dispatcher = new CallDispatcher();
                    break;
                case self::$type_APP:
                    $dispatcher = new AppDispatcher();
                    break;
                case self::$type_PUSH:
                    $dispatcher = new PushDispatcher();
                    break;

            }

            if ($dispatcher instanceof IAlertDispatcher) {
                $dispatcher->perform($this);
                $this->result = $dispatcher->result();
                $this->dispatched_datetime = Carbon::now('Europe/Berlin');
                $this->save();
            } else {
                \Log::alert('Exception while handling AlertDispatchJob for id: ' . $this->id . ' - dispatcher was null');
            }
        } catch (\Exception $e) {
            \Log::alert('Exception while handling AlertDispatchJob for id: ' . $this->id . ' - ' . $e->getMessage());
        }
    }
}
