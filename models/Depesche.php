<?php namespace Pm\Firemon112\Models;

class Depesche {
    public $Einsatznummer = "";
    public $Einsatzstichwort = "";
    public $Einsatzbeginn = "";
    public $Klartext = "";
    public $Information = "";
    public $Einsatzobjekt = "";
    public $Strasse = "";
    public $Ort = "";
    public $Ortsteil = "";
    public $Plan = "";
    public $Patient = "";
    public $Prio = 0;
    public $X = 0.0;
    public $Y = 0.0;
    public $Lat = 0.0;
    public $Long = 0.0;
    public $MitalarmierteKraefte = array();


    function __construct() {

    }

    public static function fromArray($data)
    {
        $depesche = new self();
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                if (property_exists($depesche, $key) && !empty($val)) {
                    $depesche->$key = $val;
                }
            }
        }
        // take care that $MitalarmierteKraefte is still an array (even an empty one)
        if (!is_array($depesche->MitalarmierteKraefte)) {
            $depesche->MitalarmierteKraefte = array();
        }
        return $depesche;
    }

    public function anonymize() {
        $this->Einsatzobjekt = "";
        $this->Information = "";
        $this->Klartext = "";
        $this->Ort = "";
        $this->Ortsteil = "";
        $this->Plan = "";
        $this->Strasse ="";
        $this->Patient ="";
    }

    public function toArray() {
        return (array) $this;
    }

    public function isPlausible() {
        return strlen($this->Einsatznummer) > 0;
    }

    public static function getFields()
    {
        $rtn = array();
        foreach (array_keys(get_class_vars(__CLASS__)) as $var) {
            $rtn[] = $var;
        }
        return $rtn;
    }

    public function isTest() {
        return
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_SCHLEIFEN_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_SCHLEIFEN_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_SCHLEIFEN_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_SCHLEIFEN_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DRY_KEY) === 0;
    }

    public function isMonitorTest() {
        return
            strpos($this->Einsatznummer, Alert::$TEST_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_SCHLEIFEN_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_SCHLEIFEN_MONITOR_KEY) === 0;
    }

    public function isDiveraTest()
    {
        return
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_SCHLEIFEN_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_SCHLEIFEN_MONITOR_KEY) === 0;
    }

    public function isDryTest() {
        return
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DIVERA_MONITOR_KEY) === 0 ||
            strpos($this->Einsatznummer, Alert::$TEST_DRY_KEY) === 0;
    }

    public function getMitalarmierteKraefteWashed() {
        // remove date & time
        // remove 5-8 digits
        $washed = array();
        if (count($this->MitalarmierteKraefte) > 0) {
            foreach ($this->MitalarmierteKraefte as $K) {
                $K = preg_replace('/\d{5,}/', '', $K);
                $K = preg_replace('/\d{2}\.\d{2}\.\d{4}\s\d{2}\:\d{2}\:\d{2}/', '', $K);
                $K = trim($K);
                if (strlen($K) > 0) {
                    $washed[] = $K;
                }
            }
        }
        if (count($washed) <= 0) {
            $washed[] = ""; // add one single empty element to allow catch all to trigger if enabled if maks was empty
        }
        return $washed;
    }
}