<?php namespace pm\Firemon112\Models;

use Model;
use \Carbon\Carbon;

/**
 * Model
 */
class DeviceError extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_deviceerror';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'device' =>  'pm\Firemon112\Models\Device',
    ];

    public function scopeOutdated($query, $type) {
        return $query->where('created_at', '<=', Carbon::now('Europe/Berlin')->subDays(30));
    }
}
