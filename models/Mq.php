<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Mq extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $appends = [
        'use_ssl'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_mq';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public $hasMany = [
        'devices' =>  'pm\Firemon112\Models\Device',
    ];

    public function afterSave() {
        if (!Helper::SyncMqUser($this->user, $this->pass, "/")) {
            \Log::error('MQ User '.$this->user.' could not get updated on RabbitMQ Srv!');
        }
    }

    public function afterDelete() {
        if (!Helper::DeleteMqUser($this->user)) {
            \Log::error('MQ User '.$this->user.' could not get deleted on RabbitMQ Srv!');
        }
    }

    public function getUseSslAttribute() {
        return boolval(Settings::get('mq-use-ssl'));
    }
}
