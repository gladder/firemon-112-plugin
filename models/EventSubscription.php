<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class EventSubscription extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_event_subscription';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'event_id' => 'required',
        'user_id' => 'required',
        'available' => 'required'
    ];

    protected $fillable = [
        'event_id',
        'user_id',
        'available',
        'reason',
        'performed'
    ];

    public $belongsTo = [
        'event' =>  'pm\Firemon112\Models\Event',
        'user' =>  'RainLab\User\Models\User',
    ];

    public function scopeAvailable($query) {
        $query->where('available', true);
    }

    public function scopeNotAvailable($query) {
        $query->where('available', false);
    }
}
