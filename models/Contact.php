<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Contact extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $TYPE_TECHNICAL = "Technischer Ansprechpartner";
    public static $TYPE_TECHNICAL_FLAG = "technical";
    public static $TYPE_SUPERVISOR = "Hausmeister";
    public static $TYPE_SUPERVISOR_FLAG = "supervisor";
    public static $TYPE_EMERGENCY = "Notfallkontakt";
    public static $TYPE_EMERGENCY_FLAG = "emergency";
    public static $TYPE_OWNER = "Inhaber / Besitzer";
    public static $TYPE_OWNER_FLAG = "owner";
    public static $TYPE_SECURITY = "Sicherheitsdienst";
    public static $TYPE_SECURITY_FLAG = "security";

    public static $TYPE_OPERATOR = "Betreiber";
    public static $TYPE_OPERATOR_FLAG = "operator";

    public static $TYPE_AUTHORITY = "Behörde";
    public static $TYPE_AUTHORITY_FLAG = "authority";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_contact';

    public $fillable = [
        'label',
        'street_no',
        'zip_city',
        'email',
        'phone',
        'mobile',
        'type'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label' => 'required',
        'email' => 'email',
    ];

    static function getTypeOptions() {
        return [
            self::$TYPE_OWNER_FLAG => self::$TYPE_OWNER,
            self::$TYPE_EMERGENCY_FLAG => self::$TYPE_EMERGENCY,
            self::$TYPE_SUPERVISOR_FLAG => self::$TYPE_SUPERVISOR,
            self::$TYPE_TECHNICAL_FLAG => self::$TYPE_TECHNICAL,
            self::$TYPE_SECURITY_FLAG => self::$TYPE_SECURITY,
            self::$TYPE_AUTHORITY_FLAG => self::$TYPE_AUTHORITY,
            self::$TYPE_OPERATOR_FLAG => self::$TYPE_OPERATOR,
        ];
    }

    public $morphTo = [
        'contactable' => []
    ];
}
