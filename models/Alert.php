<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;
use pm\Firemon112\Classes\Dpi;
use pm\Firemon112\Jobs\QueryWeatherForAlert;
use pm\Firemon112\Models\AlertUserFeedback;
use pm\Firemon112\Models\Settings;
use pm\Firemon112\Models\Pushtoken;

/**
 * Model
 */
class Alert extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $jsonable = ['weather','depesche'];

    protected $with = ['station'];

    public static $MQ_ALERT = "ALERT";
    public static $MQ_FEEDBACK_PERFORMED = "FEEDBACK-PERFORMED";
    public static $TEST_DIVERA_KEY = "TEST-D";
    public static $TEST_SCHLEIFEN_KEY = "TEST-S";
    public static $TEST_MONITOR_KEY = "TEST-M";
    public static $TEST_DIVERA_SCHLEIFEN_KEY = "TEST-D-S";
    public static $TEST_DIVERA_MONITOR_KEY = "TEST-D-M";
    public static $TEST_SCHLEIFEN_MONITOR_KEY = "TEST-S-M";
    public static $TEST_DIVERA_SCHLEIFEN_MONITOR_KEY = "TEST-D-S-M";
    public static $TEST_DRY_KEY = "TEST-IS-DRY";

    public static $CHUNK_SIZE_EMAIL = 50;
    public static $CHUNK_SIZE_CALL = 30;
    public static $CHUNK_SIZE_SMS = 50;
    public static $CHUNK_SIZE_APP = 50;
    public static $CHUNK_SIZE_PUSH = 40;


    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public $hasMany = [
        'alert_dispatch_jobs' => 'pm\Firemon112\Models\AlertDispatchJob',
        'alert_user_feedbacks' => 'pm\Firemon112\Models\AlertUserFeedback',
    ];

    public $belongsToMany = [
        'users' => [
            'RainLab\User\Models\User',
            'table' => 'pm_firemon112_alert2user',
            'key' => 'alert_id',
            'otherKey' => 'user_id',
            'pivot' => ['alertgroup_id', 'station_id']
        ]
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_alert';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = [
        'identifier',
        'station_id'
    ];

    public function scopeActive($query)
    {
        $query->whereRaw('( DATE_ADD(created_at, INTERVAL 65 MINUTE) >= CURRENT_TIMESTAMP() AND ( (schedule_trigger_at IS NULL AND schedule_handled_at IS NULL) OR (schedule_handled_at >= schedule_trigger_at) ) )')->orderBy('id','DESC');
    }

    public function scopeActiveMinutes($query, $minutes)
    {
        $query->whereRaw('( DATE_ADD(created_at, INTERVAL '.intval($minutes).' MINUTE) >= CURRENT_TIMESTAMP() AND ( (schedule_trigger_at IS NULL AND schedule_handled_at IS NULL) OR (schedule_handled_at >= schedule_trigger_at) ) )')->orderBy('id','DESC');
    }

    public function scopeLastMonths($query, $last_month_count)
    {
        $query->whereRaw('( DATE_ADD(created_at, INTERVAL '.intval($last_month_count).' MONTH) >= CURRENT_TIMESTAMP() AND ( (schedule_trigger_at IS NULL AND schedule_handled_at IS NULL) OR (schedule_handled_at >= schedule_trigger_at) ) )')->orderBy('created_at','DESC');
    }

    public function scopeScheduled($query)
    {
        $query->whereRaw('( schedule_trigger_at IS NOT NULL AND schedule_trigger_at <= CURRENT_TIMESTAMP() AND schedule_handled_at IS NULL )')->orderBy('id','DESC');
    }

    public function scopeNeedsAnonymization($query)
    {

        $query->whereRaw('( anonymized_at IS NULL AND DATEDIFF(NOW(), created_at) > 180 )')->orderBy('id','DESC');
    }

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function getIsScheduledAttribute() {
        return $this->schedule_trigger_at !== null;
    }

    public function getIsScheduleHandledAttribute() {
        return $this->schedule_handled_at !== null;
    }

    public function afterFetch() {
        $depesche = $this->getDepescheObject();
        $depesche->Information = Dpi::translateDpi($depesche->Information);
        $this->depesche = $depesche->toArray();
    }

    public static function HandleCreateAlertForStation($depesche, $station, $schedule = null, $trigger_real_alert = true) {
        try {
            $alert = Alert::firstOrNew([
                'identifier' => $depesche->Einsatznummer,
                'station_id' => $station->id
            ]);

            if ($schedule !== null) {
                $depesche->Einsatzbeginn = $schedule->format("d.m.Y H:i:s");
                $alert->schedule_trigger_at = $schedule;
            }

            $alert->depesche = $depesche->toArray();
            $alert->save();

            if ($trigger_real_alert && (!$alert->getIsScheduledAttribute() || $alert->getIsScheduleHandledAttribute())) {
                $alert->publishAlert();
                $alert->dispatchAlerts();
                if ($alert->station->diverafree_enabled) {
                    $alert->publishDiverafreeAlert();
                }
            }
            return $alert;
        } catch (\Exception $e) {
            \Log::alert('Exception while handling create Alert for: ' . $station->token . ' with depesche ' . $depesche->Einsatznummer);
        }
    }

    public function afterCreate()
    {
        $depesche = $this->getDepescheObject();
        \Log::info($depesche->Einsatznummer . ' for '.$this->station->token);

        if ($this->getTestAttribute() && (!$this->getIsScheduledAttribute() || $this->getIsScheduleHandledAttribute())) {
            $this->triggerTestAlert();
        }

        $this->dispatchQueryWeather();

    }

    public function anonymize() {
        $depesche = $this->getDepescheObject();
        \Log::info('Anonymizing' . $depesche->Einsatznummer);
        $depesche = $this->getDepescheObject();
        $depesche->anonymize();
        $this->depesche = $depesche->toArray();
    }

    public function dispatchQueryWeather() {
        \Queue::push('\pm\Firemon112\Jobs\QueryWeatherForAlert', ['alert_id' => $this->id]);
    }

    public function beforeDelete() {
        $this->users()->sync([]); // cleanup user-mappings
    }

    public function triggerTestAlert() {
        if ($this->depesche != null && is_array($this->depesche) && !empty($this->depesche)) {
            $depesche = $this->getDepescheObject();

            if ($depesche->isMonitorTest()) {
                $this->publishAlert();
            }

            $this->dispatchAlerts(); // we always dispatch but will run it as dry test if not explicitly enabled. see isDry in Depesche.php

            if ($depesche->isDiveraTest() && $this->station->diverafree_enabled) {
                \Log::info('trigger with divera free for '.$this->station->token);
                $this->publishDiverafreeAlert();
            }
        }
    }

    public function getDepescheObject() {
        return Depesche::fromArray($this->depesche);
    }

    public function publishDiverafreeAlert() {
        try {
            $depesche = $this->getDepescheObject();
            $divera_url = "https://www.divera247.com/api/alarm?accesskey={accesskey}&title={stichwort}";
            $divera_url = str_replace("{accesskey}",$this->station->diverafree_accesskey, $divera_url);
            $divera_url = str_replace("{stichwort}",urlencode($depesche->Einsatzstichwort), $divera_url);

            $res = \Http::get($divera_url);
            \Log::info('Divera 24/7 Free-Response for '.$this->station->token.' alert '.$depesche->Einsatznummer.':'.$res);
        } catch (\Exception $e) {
            \Log::alert('Exception while publishing Divera 24/7 Free for: ' . $this->station->token);
            trace_log($e);
        }
    }

    public function publishAlert() {
        Helper::MqSendMessageToStationToken($this->station->token, self::$MQ_ALERT);
    }

    public function getTestAttribute() {
        $d = $this->getDepescheObject();
        return strpos($d->Einsatznummer, "TEST-") === 0;
    }

    public function getAnonymizedAttribute() {
        return $this->anonymized_at != null;
    }

    public function getScheduleFormattedAttribute() {
        if ($this->schedule !== null) {
            $d = new Carbon($this->schedule);
            return $d->format(Helper::GetDefaultDateTimeFormat());
        } else {
            return "";
        }
    }

    private function feedbackUserAbsence($user_absence) {
        try {
            $existing_alert_feedback = $user_absence->user->alert_feedbacks()->where('alert_id', $this->id)->first();
            // Only auto feedback absence if no existing feedback existing
            if ($existing_alert_feedback == null) {
                $alert_feedback = new AlertUserFeedback();
                $alert_feedback->user_id = $user_absence->user->id;
                $alert_feedback->alert_id = $this->id;
                $alert_feedback->feedback_status = AlertUserFeedback::$TYPE_FEEDBACK_N;
                $alert_feedback->feedback_performed = Carbon::now('Europe/Berlin');
                $alert_feedback->save();
            }
        } catch (\Exception $e) {
            \Log::alert("Exception ".$e->getMessage());
        }
    }

    public function dispatchAlerts($console = null) {
        try {
            $dispatch_groups = [];
            foreach ($this->station->alertgroups->where('enabled', '1')->sortByDesc('prio') as $group) {
                $dispatch_groups[] = $group;
            }

            if (!$this->getTestAttribute() || boolval(Settings::get('allow_testalert_trigger_other_stations'))) {
                if ($this->station->GetIsStandortAttribute() || $this->station->GetIsOrtswehrAttribute() || $this->station->GetIsGemeindewehrAttribute() || $this->station->GetIsAmtswehrAttribute()) {
                    if ($this->station->parent_station !== null) {
                        $dispatch_groups = array_merge($this->getParentDispatchGroup($this->station->parent_station), $dispatch_groups);
                    }
                }

                if ($this->station->GetIsStandortAttribute() || $this->station->GetIsOrtswehrAttribute() || $this->station->GetIsGemeindewehrAttribute()) {
                    if ($this->station->parent_station !== null && $this->station->parent_station->parent_station !== null) {
                        $dispatch_groups = array_merge($this->getParentDispatchGroup($this->station->parent_station->parent_station), $dispatch_groups);
                    }
                }

                if ($this->station->GetIsStandortAttribute() || $this->station->GetIsOrtswehrAttribute()) {
                    if ($this->station->parent_station !== null && $this->station->parent_station->parent_station !== null && $this->station->parent_station->parent_station->parent_station !== null) {
                        $dispatch_groups = array_merge($this->getParentDispatchGroup($this->station->parent_station->parent_station->parent_station), $dispatch_groups);
                    }
                }

                if ($this->station->GetIsStandortAttribute()) {
                    if ($this->station->parent_station !== null && $this->station->parent_station->parent_station !== null && $this->station->parent_station->parent_station->parent_station !== null && $this->station->parent_station->parent_station->parent_station->parent_station !== null) {
                        $dispatch_groups = array_merge($this->getParentDispatchGroup($this->station->parent_station->parent_station->parent_station->parent_station), $dispatch_groups);
                    }
                }
            }
            $depesche = $this->getDepescheObject();
            $maks = $depesche->getMitalarmierteKraefteWashed();
            $users_handled = [];
            $users_excluded = $this->alert_user_feedbacks()->whereIn('feedback_status',[
                AlertUserFeedback::$TYPE_FEEDBACK_I,
                AlertUserFeedback::$TYPE_FEEDBACK_D
            ])->pluck('user_id')->toArray(); // consider users that made feedback instand or direct as handled. all others will receive alert

            $dry_run = $depesche->isDryTest() || $console !== null;

            if ($console !== null) {
                if ($dry_run) {
                    $console->warn("DRY RUN!");
                }
            }

            foreach ($dispatch_groups as $group) {
                try {
                    if ($console !== null) {
                        $console->info("Candidate Group " . $group->label . ' (prio ' . $group->prio . ', id '.$group->id.')');
                    }
                    $match = false;
                    foreach ($maks as $mak) {
                        if ($group->getMatchesAttribute($mak)) {
                            $match = true;
                            break;
                        }
                    }
                    if ($match) {
                        \Log::alert("Dispatching for ".$group->label.' (ID: '.$group->id.')');
                        $emails = [];
                        $sms = [];
                        $calls = [];
                        $app = [];
                        $push = [];
                        $emails_feedback = [];
                        $sms_feedback = [];
                        if ($console !== null) {
                            $console->info("Dispatching " . $group->label. " with message " . $group->getMessage($depesche));
                        }
                        foreach ($group->related_users as $user) {
                            if (in_array($user->id, $users_excluded)) {
                                // user is excluded from possible additional alert because already performed positive feedback
                                continue;
                            }
                            if (!array_key_exists($user->id, $users_handled)) {
                                if ($console !== null) {
                                    $console->info("Dispatch to " . $user->name);
                                }
                                $user_absence = $user->absences()->active()->first();
                                $absent = false;
                                $absent_reason = "";
                                if ($user_absence !== null) {
                                    $absent = true;
                                    $absent_reason = $user_absence->getAbsenceTypeReadableAttribute();
                                    $this->feedbackUserAbsence($user_absence);
                                }
                                if ($group->trigger_alert_email && $user->trigger_alert_email && strlen($user->alert_email) > 0) {
                                    if ($this->station->alert_feedback_type > 0) {
                                        if ($absent) {
                                            $emails_feedback[] = [
                                                "absent" => $this->shortenString($user->alert_email, 50) . " ({$absent_reason})",
                                            ];
                                        } else {
                                            $emails_feedback[] = [
                                                "email" => $user->alert_email,
                                                "feedback_url" => url('f', [Helper::calcFeedbackToken($user->id, $this->id)])
                                            ];
                                        }
                                    } else {
                                        if ($absent) {
                                            $emails[] = [
                                                "absent" => $this->shortenString($user->alert_email, 50) . " ({$absent_reason})",
                                            ];
                                        } else {
                                            $emails[] = [
                                                "email" => $user->alert_email
                                            ];
                                        }
                                    }
                                }
                                if ($group->trigger_alert_sms && $user->trigger_alert_sms && strlen($user->alert_sms) > 0) {
                                    if ($this->station->alert_feedback_type > 0) {
                                        if ($absent) {
                                            $sms_feedback[] = [
                                                "absent" => $this->shortenString($user->alert_sms, 20) . " ({$absent_reason})"
                                            ];
                                        } else {
                                            $sms_feedback[] = [
                                                "sms" => $user->alert_sms,
                                                "feedback_url" => url('f', [Helper::calcFeedbackToken($user->id, $this->id)])
                                            ];
                                        }
                                    } else {
                                        if ($absent) {
                                            $sms[] = [
                                                "absent" => $this->shortenString($user->alert_sms, 20) . " ({$absent_reason})"
                                            ];
                                        } else {
                                            $sms[] = [
                                                "sms" => $user->alert_sms
                                            ];
                                        }
                                    }
                                }
                                if ($group->trigger_alert_call && $user->trigger_alert_call && (strlen($user->alert_call) > 0 || strlen($user->alert_call_second) > 0) ) {
                                    if ($absent) {
                                        if (strlen($user->alert_call) > 0) {
                                            $calls[] = [
                                                "absent" => $this->shortenString($user->alert_call, 20) . " ({$absent_reason})"
                                            ];
                                        }
                                        if (strlen($user->alert_call_second) > 0 && $user->alert_call_second != $user->alert_call) {
                                            $calls[] = [
                                                "absent" => $this->shortenString($user->alert_call_second, 20) . " ({$absent_reason})"
                                            ];
                                        }
                                    } else {
                                        if (strlen($user->alert_call) > 0) {
                                            $calls[] = [
                                                "call" => $user->alert_call,
                                            ];
                                        }
                                        if (strlen($user->alert_call_second) > 0 && $user->alert_call_second != $user->alert_call) {
                                            $calls[] = [
                                                "call" => $user->alert_call_second,
                                            ];
                                        }
                                    }
                                }
                                if ($group->trigger_alert_app && $user->allow_app_usage && Helper::IsUserNotificationSetting($user, Helper::$USER_NOTIFICATION_ALLOW_ALERT_PUSH)) {
                                    if ($absent) {
                                        $app[] = [
                                            "absent" => "User {$user->id} ({$absent_reason})"
                                        ];
                                    } else {
                                        $app[] = [
                                            "app" => $user->id
                                        ];
                                    }
                                    foreach ($user->pushtokens as $token) {
                                        if (strlen($token->token_type)>0) {
                                            if (!array_key_exists($token->token_type, $push)) {
                                                $push[$token->token_type] = [];
                                            }
                                            if ($absent) {
                                                $push[$token->token_type][] = [
                                                    "absent" => $this->shortenString($token->token, 16) . " ({$absent_reason})"
                                                ];
                                            } else {
                                                $push[$token->token_type][] = [
                                                    "token" => $token->token
                                                ];
                                            }
                                        }
                                    }
                                }
                                $users_handled[$user->id] = [
                                    'alertgroup_id' => intval($group->id),
                                    'station_id' => intval($group->station_id)
                                ];
                            }
                        }

                        $sms77io_key = "";
                        if (strlen($group->station->sms77io_key) == 64) {
                            $sms77io_key = $group->station->sms77io_key;
                        }

                        // create jobs
                        if (count($emails) > 0) {
                            $chunks = array_chunk($emails, self::$CHUNK_SIZE_EMAIL);
                            foreach ($chunks as $chunk) {
                                $d = AlertDispatchJob::create([
                                    'alert_id' => $this->id,
                                    'dispatch_type' => AlertDispatchJob::$type_EMAIL,
                                    'payload' => [
                                        'origin' => $this->station->token,
                                        'reason' => $group->label,
                                        'message' => $group->getMessage($depesche),
                                        'recipients' => $this->extractItemProperty($chunk, "email"),
                                        'absent' => $this->extractItemProperty($chunk, "absent"),
                                    ],
                                    'dry_run' => $dry_run
                                ]);
                                \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                            }
                        }
                        if (count($emails_feedback) > 0) {
                            foreach ($emails_feedback as $ef) {
                                $feedback_url = "";
                                if (array_key_exists("feedback_url", $ef)) {
                                    $feedback_url = $ef["feedback_url"];
                                }
                                $d = AlertDispatchJob::create([
                                    'alert_id' => $this->id,
                                    'dispatch_type' => AlertDispatchJob::$type_EMAIL,
                                    'payload' => [
                                        'origin' => $this->station->token,
                                        'reason' => $group->label,
                                        'message' => $group->getMessage($depesche)."\n\nEinsatz-Feedback melden: ".$feedback_url,
                                        'recipients' => array_key_exists('email', $ef) ? [$ef["email"]] : [],
                                        'absent' => array_key_exists('absent', $ef) ? [$ef["absent"]] : [],
                                    ],
                                    'dry_run' => $dry_run
                                ]);
                                \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                            }
                        }
                        if (count($sms) > 0) {
                            $chunks = array_chunk($sms, self::$CHUNK_SIZE_SMS);
                            foreach ($chunks as $chunk) {
                                $d = AlertDispatchJob::create([
                                    'alert_id' => $this->id,
                                    'dispatch_type' => AlertDispatchJob::$type_SMS,
                                    'payload' => [
                                        'origin' => $this->station->token,
                                        'reason' => $group->label,
                                        'message' => $group->getMessage($depesche),
                                        'recipients' => $this->extractItemProperty($chunk, "sms"),
                                        'absent' => $this->extractItemProperty($chunk, "absent"),
                                        'sms77io_key' => $sms77io_key
                                    ],
                                    'dry_run' => $dry_run
                                ]);
                                \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                            }
                        }
                        if (count($sms_feedback) > 0) {
                            foreach ($sms_feedback as $sf) {
                                $feedback_url = "";
                                if (array_key_exists("feedback_url", $sf)) {
                                    $feedback_url = $sf["feedback_url"];
                                }
                                $d = AlertDispatchJob::create([
                                    'alert_id' => $this->id,
                                    'dispatch_type' => AlertDispatchJob::$type_SMS,
                                    'payload' => [
                                        'origin' => $this->station->token,
                                        'reason' => $group->label,
                                        'message' => $group->getMessage($depesche)." ".$feedback_url,
                                        'recipients' => array_key_exists('sms', $sf) ? [$sf["sms"]] : [],
                                        'absent' => array_key_exists('absent', $sf) ? [$sf["absent"]] : [],
                                        'sms77io_key' => $sms77io_key
                                    ],
                                    'dry_run' => $dry_run
                                ]);
                                \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                            }
                        }
                        if (count($calls) > 0) {
                            $chunks = array_chunk($calls, self::$CHUNK_SIZE_CALL);
                            foreach ($chunks as $chunk) {
                                $d = AlertDispatchJob::create([
                                    'alert_id' => $this->id,
                                    'dispatch_type' => AlertDispatchJob::$type_CALL,
                                    'payload' => [
                                        'origin' => $this->station->token,
                                        'reason' => $group->label,
                                        'message' => $group->getMessage($depesche),
                                        'recipients' => $this->extractItemProperty($chunk, "call"),
                                        'absent' => $this->extractItemProperty($chunk, "absent"),
                                        'sms77io_key' => $sms77io_key
                                    ],
                                    'dry_run' => $dry_run
                                ]);
                                \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                            }
                        }
                        if (count($app) > 0) {
                            $chunks = array_chunk($app, self::$CHUNK_SIZE_APP);
                            foreach ($chunks as $chunk) {
                                $d = AlertDispatchJob::create([
                                    'alert_id' => $this->id,
                                    'dispatch_type' => AlertDispatchJob::$type_APP,
                                    'payload' => [
                                        'origin' => $this->station->token,
                                        'reason' => $group->label,
                                        'message' => $group->getMessage($depesche),
                                        'recipients' => $this->extractItemProperty($chunk, "app"),
                                        'absent' => $this->extractItemProperty($chunk, "absent")
                                    ],
                                    'dry_run' => $dry_run
                                ]);
                                \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                            }
                        }
                        if (count($push) > 0) {
                            foreach ($push as $token_type => $tokens) {
                                $chunks = array_chunk($tokens, self::$CHUNK_SIZE_PUSH);
                                foreach ($chunks as $chunk) {
                                    $d = AlertDispatchJob::create([
                                        'alert_id' => $this->id,
                                        'dispatch_type' => AlertDispatchJob::$type_PUSH,
                                        'payload' => [
                                            'origin' => $this->station->token,
                                            'reason' => $group->label,
                                            'message' => $group->getMessage($depesche),
                                            'data' => ["screen" => "AlertDetails", "alert" => $this->asInformationReducedArray()],
                                            'recipients' => $this->extractItemProperty($chunk, "token"),
                                            'absent' => $this->extractItemProperty($chunk, "absent"),
                                            'token_type' => $token_type,
                                            'topic' => Pushtoken::$TOKEN_TOPIC_ALERT
                                        ],
                                        'dry_run' => $dry_run
                                    ]);
                                    \Queue::push('\pm\Firemon112\Jobs\DispatchAlert', ['alert_dispatch_job_id' => $d->id],'high');
                                }
                            }
                        }
                    }

                } catch (\Exception $e) {
                    \Log::alert("Error while dispatching group ".$group->label." - ". $e->getMessage());
                }
            }
            $this->users()->syncWithoutDetaching($users_handled);
        } catch (\Exception $e) {
            if ($console !== null) {
                $this->error('Exception while handling dispatchAlerts for alert-id: ' . $this->id . ' - ' . $e->getMessage());
            }
            \Log::alert('Exception while handling dispatchAlerts for alert-id: ' . $this->id . ' - ' . $e->getMessage());
        }
    }

    private function getParentDispatchGroup($parent) {
        $dispatch_groups = [];
        foreach ($parent->alertgroups->where('enabled','1')->where('trigger_child_stations','1')->sortByDesc('prio') as $group) {
            $dispatch_groups[] = $group;
        }
        return $dispatch_groups;
    }

    public function getAlertFeedback($returnPeople = true, $returnGroups = true) {
        $af_i_cnt = 0;
        $af_i_people = [];
        $af_d_cnt = 0;
        $af_d_people = [];
        $af_l_cnt = 0;
        $af_l_people = [];
        $af_n_cnt = 0;
        $af_n_people = [];
        $nogroup_code = "nogroup";
        $station_user_ids = $this->station->getAllUserIds();
        $alerted_user_cnt = count(array_intersect($this->users->pluck('id')->toArray(), $station_user_ids));
        $groups = [];
        $groups[$nogroup_code] = [
            "prio" => -1,
            "label" => "Schlauchtrupp",
            "count" => 0,
            "marker_color" => "#000000",
            "short_name" => ""
        ];

        if ($this->alert_user_feedbacks->count() > 0) {
            foreach ($this->alert_user_feedbacks as $feedback) {
                $performed = new Carbon($feedback->feedback_performed,'Europe/Berlin');
                if ($feedback->user === null) {
                    continue; // not sure why this may be null but this leads to the fact that we dont delete relations correctly
                }
                $count_person_detail = in_array($feedback->user->id, $station_user_ids);
                $count_person_summary = true; // currently we always count all in summary
                $person = null;
                $mission_role_groups = [];
                if ($returnPeople || $returnGroups) {
                    $station_id = $this->station_id;
                    $userAlert = $this->users()->find($feedback->user->id);
                    if ($userAlert !== null) {
                        $new_station_id = intval($userAlert->pivot->station_id);
                        if ($new_station_id > 0) {
                            $station_id = $new_station_id;
                        }
                    }
                    $mission_role_groups = Helper::getGroupsByPermissionFlag($feedback->user, $station_id, Helper::$PERMISSION_IS_MISSION_ROLE);
                }

                if ($returnPeople) {
                    $person = [
                        "name" => $feedback->user->name . " " . $feedback->user->surname,
                        "mission_roles" => [],
                        "performed" => $performed->format('H:i:s'),
                        "is_counted_detail" => $count_person_detail,
                        "is_counted_summary" => $count_person_summary
                    ];
                    if (is_array($mission_role_groups) && count($mission_role_groups) > 0) {
                        foreach ($mission_role_groups as $mrg) {
                            $person["mission_roles"][] = [
                                "code" => $mrg["short_name"],
                                "marker_color" => $mrg["marker_color"]
                            ];
                        }
                    }
                }

                switch ($feedback->feedback_status) {
                    case 1: {
                        if ($count_person_detail) {
                            $af_i_cnt++;
                        }
                        if ($person !== null) {
                            $af_i_people[] = $person;
                        }
                        if ($returnGroups && $count_person_summary) {
                            if (is_array($mission_role_groups) && count($mission_role_groups) > 0) {
                                if (!array_key_exists($mission_role_groups[0]["code"], $groups)) {
                                    $groups[$mission_role_groups[0]["code"]] = [
                                        "prio" => $mission_role_groups[0]["prio"],
                                        "label" => $mission_role_groups[0]["name"],
                                        "count" => 0,
                                        "marker_color" => $mission_role_groups[0]["marker_color"],
                                        "short_name" => $mission_role_groups[0]["short_name"]
                                    ];
                                }
                                $groups[$mission_role_groups[0]["code"]]['count']++;
                            } else {
                                $groups[$nogroup_code]['count']++;
                            }
                        }
                        break;
                    }
                    case 2: {
                        if ($count_person_detail) {
                            $af_d_cnt++;
                        }
                        if ($person !== null) {
                            $af_d_people[] = $person;
                        }
                        break;
                    }
                    case 3: {
                        if ($count_person_detail) {
                            $af_l_cnt++;
                        }
                        if ($person !== null) {
                            $af_l_people[] = $person;
                        }
                        break;
                    }
                    case 4: {
                        if ($count_person_detail) {
                            $af_n_cnt++;
                        }
                        if ($person !== null) {
                            $af_n_people[] = $person;
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }

        $alertFeedbackTypes = [];
        if ($this->station->alert_feedback_type > 0) {
            $alertFeedbackTypes[] =  [
                "type" => "alert-feedback-i",
                "count" => $af_i_cnt,
                "people" => $af_i_people,
            ];
            $alertFeedbackTypes[] = [
                "type" => "alert-feedback-n",
                "count" => $af_n_cnt,
                "people" => $af_n_people,
            ];
        }
        if ($this->station->alert_feedback_type == 2 || $this->station->alert_feedback_type == 4) {
            $alertFeedbackTypes[] = [
                "type" => "alert-feedback-l",
                "count" => $af_l_cnt,
                "people" => $af_l_people,
            ];
        }
        if ($this->station->alert_feedback_type == 3 || $this->station->alert_feedback_type == 4) {
            $alertFeedbackTypes[] = [
                "type" => "alert-feedback-d",
                "count" => $af_d_cnt,
                "people" => $af_d_people,
            ];
        }


        $arr = [
            "alert_feedback_types" => $alertFeedbackTypes,
            "alert_feedback_groups" => [],
            "alert_overall_people" => $alerted_user_cnt
        ];


        uasort($groups, function ($item1, $item2) {
            return $item2['prio'] <=> $item1['prio'];
        });

        foreach ($groups as $k => $v) {
            if ($v['count'] > 0) {
                $arr['alert_feedback_groups'][] = [
                    "prio" => $v['prio'],
                    "code" => $k != $nogroup_code ? $v['short_name'] : '',
                    "label" => $v['label'],
                    "count" => $v['count'],
                    "marker_color" => $v['marker_color']
                ];
            }
        }
        return $arr;
    }

    public function asInformationReducedArray() {
        $arr = [
            'id' => $this->id,
            'Einsatzstichwort' => "",
            'Einsatzbeginn' => "",
            'Klartext' => "",
            'Information' => "",
            'Ort' => "",
            'created_at' => $this->created_at,
            'Wehr' => "",
            'test' => $this->getTestAttribute(),
            'station' => []
        ];

        if (is_array($this->depesche)) {
            if (array_key_exists("Einsatzstichwort", $this->depesche)) {
                $arr['Einsatzstichwort'] = $this->depesche['Einsatzstichwort'];
            }
            if (array_key_exists("Einsatzbeginn", $this->depesche)) {
                $arr['Einsatzbeginn'] = $this->depesche['Einsatzbeginn'];
            }
            if (array_key_exists("Klartext", $this->depesche)) {
                $arr['Klartext'] = $this->depesche['Klartext'];
            }
            if (array_key_exists("Information", $this->depesche)) {
                $arr['Information'] = $this->depesche['Information'];
            }
            if (array_key_exists("Ort", $this->depesche)) {
                $arr['Ort'] = $this->depesche['Ort'];
            }
            if (array_key_exists("Einsatzstichwort", $this->depesche)) {
                $arr['Einsatzstichwort'] = $this->depesche['Einsatzstichwort'];
            }
        }

        if ($this->station !== null && $this->station->name !== null) {
            $arr['Wehr'] = $this->station->name;
            $arr['station'] = [
                "id" => $this->station->id,
                "name" => $this->station->name,
                "long" => $this->station->long,
                "lat" => $this->station->lat,
                "leitstelle_id" => $this->station->leitstelle_id,
                "parent_station_id" => $this->station->parent_station_id,
                "station_type" => $this->station->station_type,
                "alert_feedback_type" => $this->station->alert_feedback_type,
                "alert_details_protected" => $this->station->alert_details_protected,
            ];
        }

        return $arr;
    }

    private function extractItemProperty($array, $propertyName) {
        // exclude property from array items
        $resArray = array_map(function($element) use ($propertyName) {
            return array_key_exists($propertyName, $element) && !empty($element[$propertyName]) ? $element[$propertyName] : null;
        }, $array);

        // remove null values from resulting array and return
        return array_filter($resArray, function($value) {
            return !is_null($value);
        });
    }

    function shortenString($inputString, $maxLength) {
        if ($maxLength < 3) {
            $maxLength = 3;
        }
        if (strlen($inputString) > $maxLength) {
            $outputString = substr($inputString, 0, $maxLength - 3) . "...";
        } else {
            $outputString = $inputString;
        }

        return $outputString;
    }

    public function userCanViewAlert($user) {
        if ($user === null) {
            return false;
        }

        if (!$user->allow_app_usage) {
            return false;
        }

        $station = $this->getAlertedStation($user);

        if ($station === null) {
            return false;
        }

        if ($station->alert_details_protected) {
            return Helper::userCanViewAlertDetails($user,$station->id);
        } else {
            return true;
        }
    }

    /**
     * Detect if user station needs to be used or station from alertgroup
     * @param $user
     * @return false|Station
     */
    public function getAlertedStation($user) {
        $station = $user->station;
        if ($station === null) {
            return false;
        }
        $alerted_station_id = 0;
        if (isset($this->pivot)) {
            $alerted_station_id = intval($this->pivot->station_id);
        } else {
            $userAlert = $user->alerts()->find($this->id);
            if ($userAlert !== null) {
                $alerted_station_id = intval($userAlert->pivot->station_id);
            }
        }

        if ($alerted_station_id > 0 && $alerted_station_id != $station->station_id) {
            $alertedStation = $user->linked_stations()->find($alerted_station_id);
            if ($alertedStation !== null) {
                $station = $alertedStation;
            }
        }
        return $station;
    }

    public static function generateFiremonEinsatznummer() {
        // Get the current time using Carbon
        $now = Carbon::now();

        // Format the date to DDMMYY
        $formattedDate = $now->format('dmy');

        // Calculate the seconds since midnight
        $secondsSinceMidnight = $now->diffInSeconds($now->copy()->startOfDay());

        // Construct the custom string with padded seconds
        $einsatznummer = "FM " . $formattedDate . " " . sprintf('%05d', $secondsSinceMidnight);
        return $einsatznummer;
    }

}
