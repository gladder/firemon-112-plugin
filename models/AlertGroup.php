<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class AlertGroup extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_alertgroup';

    static $CRIT_EQUALS = "equals";
    static $CRIT_CONTAINS = "contains";
    static $CRIT_STARTS_WITH = "starts_with";
    static $CRIT_ENDS_WITH = "ends_with";
    static $CRIT_REGEX = "regex";
    static $CRIT_CATCHALL = "catchall";
    static $CRIT_CURRENTTRAINING = "currenttraining";

    static $MESSAGE_TYPE_ALARM = "a";
    static $MESSAGE_TYPE_ALARM_STICHWORT = "a-s";
    static $MESSAGE_TYPE_ALARM_STICHWORT_LAGE = "a-s-l";
    static $MESSAGE_TYPE_ALARM_STICHWORT_LAGE_ORT = "a-s-l-o";
    static $MESSAGE_TYPE_CUSTOM = "c";

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label' => 'required',
        'criteria' => 'required',
        'message_custom' => 'required_if:message_type,c|regex:/^[\s\d\w\{\}\ü\ö\ä\Ü\Ö\Ä\ß\!\?\.\-\#\%\&]*$/',
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'filter',
        'criteria',
        'trigger_child_stations',
        'message_custom',
        'pivot',
    ];

    public $belongsToMany = [
        'users' => [
            'RainLab\User\Models\User',
            'table' => 'pm_firemon112_alertgroup2user',
            'key' => 'alertgroup_id',
            'otherKey' => 'user_id'
        ]
    ];

    public $morphToMany = [
        'groups' => ['RainLab\User\Models\UserGroup', 'table' => 'pm_firemon112_group2groupable', 'name' => 'groupable']
    ];

    public function beforeDelete() {
        $this->users()->sync([]); // cleanup user-mappings
        $this->groups()->detach(); // detach groups
    }

    public static function getCriteriaOptions() {
        return array(
            self::$CRIT_EQUALS => "stimmt genau überein",
            self::$CRIT_CONTAINS => "enthält",
            self::$CRIT_STARTS_WITH => "beginnt mit",
            self::$CRIT_ENDS_WITH => "endet mit",
            self::$CRIT_REGEX => "entspricht RegEX",
            self::$CRIT_CATCHALL => "Catch-All (ignoriert Suchtext, löst immer aus)",
            self::$CRIT_CURRENTTRAINING => "Aktuelle Übungsteilnehmer mit Zusage"
        );
    }

    public static function getMessageTypeOptions() {
        return array(
            self::$MESSAGE_TYPE_ALARM => "nur Alarm",
            self::$MESSAGE_TYPE_ALARM_STICHWORT => "Alarm und Stichwort",
            self::$MESSAGE_TYPE_ALARM_STICHWORT_LAGE => "Alarm, Stichwort und Lage",
            self::$MESSAGE_TYPE_ALARM_STICHWORT_LAGE_ORT => "Alarm, Stichwort, Lage und Ort",
            self::$MESSAGE_TYPE_CUSTOM => "Eigenes Textmuster",
        );
    }

    public static function getMessageTemplateOptions() {
        return array(
            self::$MESSAGE_TYPE_ALARM => "Alarm für {WEHR}! {ALARMZEITPUNKT}",
            self::$MESSAGE_TYPE_ALARM_STICHWORT => "Alarm für {WEHR}! {STICHWORT}. {ALARMZEITPUNKT}",
            self::$MESSAGE_TYPE_ALARM_STICHWORT_LAGE => "Alarm für {WEHR}! {STICHWORT} - {LAGE}. {ALARMZEITPUNKT}",
            self::$MESSAGE_TYPE_ALARM_STICHWORT_LAGE_ORT => "Alarm für {WEHR} in {ORT}! {STICHWORT} - {LAGE}. {ALARMZEITPUNKT}",
        );
    }

    public function getMessage($depesche) {
        $messages = self::getMessageTemplateOptions();

        $msg = $messages[self::$MESSAGE_TYPE_ALARM];
        if (array_key_exists($this->message_type, $messages)) {
            $msg = $messages[$this->message_type];
        }
        if ($this->message_type == self::$MESSAGE_TYPE_CUSTOM && strlen(trim($this->message_custom)) > 0) {
            $msg = trim($this->message_custom);
        }
        return str_replace([
            "{WEHR}",
            "{ORT}",
            "{STICHWORT}",
            "{LAGE}",
            "{ALARMZEITPUNKT}"
        ],[
            $this->station->name,
            $depesche->Ort,
            $depesche->Einsatzstichwort . ' ('.$depesche->Klartext.')',
            $depesche->Information,
            $depesche->Einsatzbeginn
        ],$msg);
    }

    public function scopeTriggerChildStations($query) {
        return $query->where('trigger_child_stations', 1);
    }

    public function getMatchesAttribute($value) {
        $res = false;
        switch ($this->criteria) {
            case self::$CRIT_EQUALS:
            case self::$CRIT_CURRENTTRAINING: {
                $res = strcasecmp($this->filter, $value) === 0;
                break;
            }
            case self::$CRIT_CONTAINS: {
                if (strlen($this->filter) > 0) {
                    $res = strpos($value, $this->filter) !== false;
                }
                break;
            }
            case self::$CRIT_STARTS_WITH: {
                $length = strlen( $this->filter );
                $res = substr( $value, 0, $length ) === $this->filter;
                break;
            }
            case self::$CRIT_ENDS_WITH: {
                $length = strlen( $this->filter );
                if( !$length ) {
                    return true;
                }
                $res = substr( $value, -$length ) === $this->filter;
                break;
            }
            case self::$CRIT_REGEX: {
                $res = @preg_match('/'.$this->filter.'/', $value) == 1;
                break;
            }
            case self::$CRIT_CATCHALL: {
                $res = true; // will always trigger!
                break;
            }
            default: {
                $res = false;
            }
        }
        return $res;
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function getRelatedUsersAttribute() {
        if ($this->getIsCurrentTrainingAttribute()) {
            $users =  collect([]);
            foreach ($this->station->events()->upcomingSoonOrActive()->get() as $event) {
                if ($event->is_training) {
                    $users = $event->subscriptions->filter(function ($v) {
                        return $v->available;
                    })->map(function ($v) {
                        return $v->user;
                    });
                }
            }
            return $users;
        } else {
            $directUsers = $this->users;
            $groupedUsers = $this->groups->flatMap(
                function ($group) {
                    return $group->users;
                }
            )->unique('id');
            return $directUsers->concat($groupedUsers)->unique('id');
        }
    }

    public function getIsDynamicGroupAttribute() {
        return $this->getIsCurrentTrainingAttribute();
    }

    public function getIsCurrentTrainingAttribute() {
        return $this->criteria == self::$CRIT_CURRENTTRAINING;
    }

}
