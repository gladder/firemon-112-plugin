<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\Models\CustomSettings;
use RainLab\User\Models\User;
use Mail;

/**
 * Model
 */
class Invite extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $STATE_NEW = "new";
    public static $STATE_DECLINED = "declined";
    public static $STATE_ACCEPTED = "accepted";

    public static $STATE_NEW_LABEL = "Neu";
    public static $STATE_DECLINED_LABEL = "Abgewiesen";
    public static $STATE_ACCEPTED_LABEL = "Akzeptiert";

    public static $TYPE_USER_TO_STATION = "u2s";
    public static $TYPE_STATION_TO_CHILDSTATION = "s2cs";
    public static $TYPE_STATION_TO_PARENTSTATION = "s2ps";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_invite';

    public $rules = [
        'user_id_sender' => 'required|integer',
        'user_id_recipient' => 'required|integer',
        'type' => 'required|alpha_num',
        'status' => 'required|alpha_num',
        'payload' => 'array',
    ];

    protected $jsonable = ['payload'];

    protected $fillable = [
        'user_id_sender',
        'user_id_recipient',
        'type',
        'status',
        'payload'
    ];

    public $belongsTo = [
        'sender' =>  [
            'RainLab\User\Models\User',
            'key' => 'user_id_sender',
            'otherKey' => 'id',
        ],
        'recipient' =>  [
            'RainLab\User\Models\User',
            'key' => 'user_id_recipient',
            'otherKey' => 'id',
        ]
    ];

    public static function getStatusOptions() {
        return array(
            self::$STATE_NEW => self::$STATE_NEW_LABEL,
            self::$STATE_DECLINED => self::$STATE_DECLINED_LABEL,
            self::$STATE_ACCEPTED => self::$STATE_ACCEPTED_LABEL
        );
    }

    public static function getTypeOptions() {
        return array(
            self::$TYPE_USER_TO_STATION => "Zweitmitglied einladen",
            self::$TYPE_STATION_TO_CHILDSTATION => "Untergeordnete Wehr",
            self::$TYPE_STATION_TO_PARENTSTATION => "Übergeördnete Wehr"
        );
    }

    public function getTypeReadableAttribute() {
        $arr = self::getTypeOptions();
        if (array_key_exists($this->type, $arr)) {
            return $arr[$this->type];
        }
        return "n/a";
    }

    public function getStateReadableAttribute() {
        $arr = self::getStatusOptions();
        if (array_key_exists($this->status, $arr)) {
            return $arr[$this->status];
        }
        return "n/a";
    }

    public function afterCreate()
    {
        Mail::sendTo($this->recipient->email, 'pm.firemon112::mail.station-invite-created', [
            'station_name' => $this->payload['station_name'],
            'sender' => $this->sender->name.' '.$this->sender->surname,
            'recipient' => $this->recipient->name.' '.$this->recipient->surname,
            'response_url' => url('account-index')
        ]);
    }

    public function scopeBySender($query, $user_id) {
        return $query->where('user_id_sender', $user_id);
    }

    public function scopeByRecipient($query, $user_id)
    {
        return $query->where('user_id_recipient', $user_id);
    }

    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeByState($query, $state)
    {
        return $query->where('status', $state);
    }

    public function isNew()
    {
        return $this->status == self::$STATE_NEW;
    }

    public function isAccepted()
    {
        return $this->status == self::$STATE_ACCEPTED;
    }

    public function isDeclined()
    {
        return $this->status == self::$STATE_DECLINED;
    }

    public static function exists($data) {
        try {
            foreach (Invite::bySender($data['user_id_sender'])->byRecipient($data['user_id_recipient'])->byType($data['type'])->byState($data['status'])->get() as $invite) {
                if ($invite->payload != null && $invite->payload['station_id'] == $data['payload']['station_id']) {
                    return true;
                }
            }
        } catch (\Exception $e) {
            \Log::info('InviteExists Check failed: '.$e->getMessage());
        }
        return false;
    }

    public function accept() {
        $this->status = self::$STATE_ACCEPTED;
        if ($this->type == self::$TYPE_USER_TO_STATION && is_array($this->payload) && array_key_exists('station_id', $this->payload) && $this->payload['station_id'] > 0) {
            $station = Station::find($this->payload['station_id']);
            $user = User::find($this->user_id_recipient);
            if ($station != null && $user != null) {
                $station->linked_users()->attach($user);
                CustomSettings::UpdateUserRemindersForStation($user, $station); // init reminders for this station
            }
        }
        $this->save();
        Mail::sendTo($this->sender->email, 'pm.firemon112::mail.station-invite-responded', [
            'station_name' => $this->payload['station_name'],
            'sender' => $this->sender->name.' '.$this->sender->surname,
            'recipient' => $this->recipient->name.' '.$this->recipient->surname,
            'response' => self::$STATE_ACCEPTED_LABEL
        ]);
    }

    public function decline() {
        $this->status = self::$STATE_DECLINED;
        $this->save();
        Mail::sendTo($this->sender->email, 'pm.firemon112::mail.station-invite-responded', [
            'station_name' => $this->payload['station_name'],
            'sender' => $this->sender->name.' '.$this->sender->surname,
            'recipient' => $this->recipient->name.' '.$this->recipient->surname,
            'response' => self::$STATE_DECLINED_LABEL
        ]);
    }
}
