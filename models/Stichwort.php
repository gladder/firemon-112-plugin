<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Stichwort extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_stichwort';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public static function getDropdownList()
    {
        // Baue die Liste der Stichworte
        return self::all()->mapWithKeys(function ($stichwort) {
            return [$stichwort->id => $stichwort->stichwort . " - " . $stichwort->klartext];
        })->toArray();
    }
}
