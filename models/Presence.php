<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\Models\CustomSettings;
use Carbon\Carbon;
use Cms\Classes\Theme;

/**
 * Model
 */
class Presence extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \pm\Firemon112\Traits\DateTimeExtension;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_presence';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'station_id' => 'required',
        'visible_from' => 'required|date',
        'visible_to' => 'required|date|after:visible_from',
        'payload' => 'required',
    ];

    public $fillable = [
        'payload',
        'station_id',
        'visible_from',
        'visible_to',
        'category',
    ];

    protected $jsonable = ['payload'];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function scopeActive($query)
    {
        $query->whereRaw('( visible_from <= CURRENT_TIMESTAMP() AND visible_to >= CURRENT_TIMESTAMP() )')->orderBy('visible_from','DESC');
    }

    public function scopeActiveDayshift($query)
    {
        $query->whereRaw('( visible_from <= DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY) AND visible_to >= DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY) )')->orderBy('visible_from','DESC');
    }

    public function scopeByYear($query, $year)
    {
        $query->whereRaw('YEAR(visible_from) = ' . intval($year))->orderBy('visible_from','DESC');
    }

    public function scopeByCategory($query, $category)
    {
        $query->where('category',$category)->orderBy('visible_from','DESC');
    }

    public function getIsVisibleAttribute() {
        return $this->isNowBetween($this->visible_from, $this->visible_to);
    }

    public static function getCategoryOptions() {
        $cats = [];
        $user = Helper::GetFeUser();
        $station = Helper::GetFeStation();
        if ($user === null || $station === null || !Helper::userHasPermission($user, $station->id, Helper::$PERMISSION_IS_ROLE_PRESENCE)) {
            return []; // dont throw exception - it will break precence monitor because this gets called onInit with no user and station in session (needs to be fixed)
        }

        $cat_settings = $station->settings->where(CustomSettings::$SETTINGS_KEY, CustomSettings::$KEY_PRESENCE_CATEGORIES)->first();
        if ($cat_settings != null) {
            foreach ($cat_settings->getPayload() as $key => $category) {
                $cats[$key] = $category;
            }
        }

        return $cats;
    }

    public function getCategoryReadableAttribute() {
        $all = self::getCategoryOptions();
        if (array_key_exists($this->category, $all)) {
            return $all[$this->category];
        }
        return "";
    }

    public function getVisibleFromFormattedAttribute() {
        return $this->getFormattedDateTime($this->visible_from);
    }

    public function getVisibleToFormattedAttribute() {
        return $this->getFormattedDateTime($this->visible_to);
    }

    public function beforeSave() {
        $payload = json_decode($this->payload, true);
        if (is_array($payload)) {
            if (array_key_exists('cols', $payload)) {
                if (is_array($payload['cols'])) {
                    foreach ($payload['cols'] as &$col) {
                        if (is_array($col)) {
                            foreach ($col as &$group) {
                                if (array_key_exists('items', $group)) {
                                    if (is_array($group['items'])) {
                                        foreach ($group['items'] as &$item) {
                                            $person_parts = explode(',', $item['name']);
                                            if (count($person_parts) == 2) {
                                                $name = $person_parts[1];
                                                $surname = $person_parts[0];

                                                $user = $this->findAvatarUserByName($this->station->users, $name, $surname);
                                                if ($user === null) {
                                                    // try to find in linked users
                                                    $user = $this->findAvatarUserByName($this->station->linked_users, $name, $surname);
                                                }
                                                if ($user !== null && $user->avatar !== null) {
                                                    $item['avatar'] = $user->avatar->getThumb(100, null, ['mode' => 'auto', 'quality' => '80']);
                                                } else {
                                                    unset($item['avatar']);
                                                }
                                            }
                                        }
                                        unset ($item);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->payload = json_encode($payload);
    }

    private function findAvatarUserByName($users, $name, $surname) {
        $user = null;
        if ($users !== null && $users->count() > 0) {
            $user = $users->filter(function($user) use ($name, $surname) {
                return (strcasecmp(trim($user->name), trim($name)) == 0 && strcasecmp(trim($user->surname), trim($surname)) == 0)
                    || (strcasecmp(trim($user->name), trim($surname)) == 0 && strcasecmp(trim($user->surname), trim($name)) == 0);
            })->sortByDesc('avatar')->first();
        }
        return $user;
    }

    public function asHtml() {
        $html = '';
        $payload = json_decode($this->payload, true);
        if (is_array($payload)) {
            $html .= '<div class="shift-schedule-head">';
            if (array_key_exists('label', $payload)) {
                $html .= $payload['label'];
            }
            if (array_key_exists('date', $payload)) {
                $html .= " ".$payload['date'];
            }
            $html .= '</div>';
            $updated = new Carbon($this->updated_at,'Europe/Berlin');
            $html .= '<div class="shift-schedule-subhead">letzte Aktualsierung am '.$updated->format('d.m.Y').' um '.$updated->format('H:i').' Uhr</div>';
            $html .= '<div class="shift-schedule-grid">';
            if (array_key_exists('cols', $payload)) {
                if (is_array($payload['cols'])) {
                    foreach ($payload['cols'] as $col) {
                        $html .= '<div class="shift-schedule-col">';
                            if (is_array($col)) {
                                foreach ($col as $group) {
                                    $html .= '<div class="shift-group">';
                                    if (array_key_exists('headline', $group)) {
                                        $html .= '<h1>' . $group['headline'] . '</h1>';
                                    }
                                    if (array_key_exists('items', $group)) {
                                        if (is_array($group['items'])) {
                                            $theme = Theme::getActiveTheme();
                                            $avatar_placeholder = asset('/themes/' . $theme->getDirName() . '/assets/icons/avatar.png');
                                            foreach ($group['items'] as $item) {
                                                $html .= '<div class="'.$item['color'].'">';
                                                $html .= '<div class="avatar">';
                                                if (array_key_exists('avatar', $item) && strlen($item['avatar'])>0) {
                                                    $html .= '<img src="'.$item['avatar'].'" />';
                                                } else {
                                                    $html .= '<img src="'.$avatar_placeholder.'" />';
                                                }
                                                $html .= '</div>';
                                                $html .= '<div class="person">';
                                                $html .= '<p>';
                                                $html .= $item['name'].'<br />';
                                                $html .= '<b>'.$item['label'].'</b>';
                                                $html .= '</p>';
                                                $html .= '</div>';
                                                $html .= '</div>';
                                            }
                                        }
                                    }
                                    $html .= '</div>';
                                }
                            }
                        $html .= '</div>';
                    }
                }
            }
            $html .= '</div>';

            if ( (array_key_exists('comment', $payload) && strlen($payload['comment']) > 0) || (array_key_exists('note', $payload) && strlen($payload['note']) > 0) ) {
                $html .= '<div class="shift-schedule-footer">';

                $html .= '<div class="shift-schedule-footercol">';
                if (strlen($payload['comment']) > 0) {
                    $html .= nl2br($payload['comment']);
                }
                $html .= '</div>';

                $html .= '<div class="shift-schedule-footercol">';
                if (strlen($payload['note']) > 0) {
                    $html .= nl2br($payload['note']);
                }
                $html .= '</div>';

                $html .= '</div>';
            }
        }
        return $html;
    }
}
