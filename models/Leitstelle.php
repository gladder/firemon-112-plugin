<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Leitstelle extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $algo_NK_KRLS_NORD_1 = "NK-KRLS-NORD-1";
    public static $algo_WK_KRLS_WEST_1 = "WK-KRLS-WEST-1";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_leitstelle';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'                  => 'required|between:1,32',
        'email_contact'         => 'required|email',
        'email_alarm'           => 'email',
        'phone'                 => 'required|between:1,32',
        'mail_algo'             => 'required'
    ];

    public $hasMany = [
        'stations' =>  'pm\Firemon112\Models\Station',
        'interested_stations' =>  'pm\Firemon112\Models\InterestedStation',
    ];

    public static function getLeitstellenOptions() {
        return Leitstelle::all()->lists('name', 'id');
    }

    public static function getMailAlgoOptions() {
        return [
            self::$algo_NK_KRLS_NORD_1 => 'E-Mail Format NK-KRLS-NORD-1',
            self::$algo_WK_KRLS_WEST_1 => 'E-Mail Format WK-KRLS-WEST-1'
        ];
    }
}
