<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\Models\Contact;
use pm\Firemon112\Traits\ContactableExtension;
use pm\Firemon112\Traits\FileUploadExtension;
use pm\Firemon112\Traits\SortOrderExtension;

/**
 * Model
 */
class Einsatzobjekt extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use FileUploadExtension, SortOrderExtension, ContactableExtension;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_einsatzobjekt';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'einsatzobjekt' => 'required',
        'strasse' => 'required',
        'ort' => 'required',
    ];

    protected $jsonable = ['coords'];

    public $attachMany = [
        'files' => ['System\Models\File', 'delete' => true]
    ];

    public $morphMany = [
        'contacts' => [\pm\Firemon112\Models\Contact::class, 'name' => 'contactable', 'delete' => true],
        'iconmarkers' => [\pm\Firemon112\Models\IconMarker::class, 'name' => 'iconmarkerable', 'delete' => true],
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function pointInPolygon($long, $lat) {
        $coords = json_decode($this->coords, true);
        $points_polygon = [];
        foreach ($coords as $point) {
            $points_polygon[] = [
                'x' => $point['lng'],
                'y' => $point['lat']
            ];
        }
        return $this->isPointInsidePolygon($points_polygon, $long, $lat);
    }

    private function isPointInsidePolygon($points_polygon, $longitude_x, $latitude_y) {
        // The last point's coordinates must be the same as the first one's to close the polygon.
        if (($points_polygon[0]['x'] != $points_polygon[count($points_polygon)-1]['x']) || ($points_polygon[0]['y'] != $points_polygon[count($points_polygon)-1]['y'])) {
            $points_polygon[] = $points_polygon[0];
        }

        $inside = false;
        for ($i = 0, $j = count($points_polygon) - 1; $i < count($points_polygon); $j = $i++) {
            if (
                (($points_polygon[$i]['y'] > $latitude_y) != ($points_polygon[$j]['y'] > $latitude_y)) &&
                ($longitude_x < ($points_polygon[$j]['x'] - $points_polygon[$i]['x']) * ($latitude_y - $points_polygon[$i]['y']) / ($points_polygon[$j]['y'] - $points_polygon[$i]['y']) + $points_polygon[$i]['x'])
            ) {
                $inside = !$inside;
            }
        }

        return $inside;
    }

    public function asInformationReducedArray($isTrustedCall = false) {
        $item = [];
        try {
            $item['id'] = $this->id;
            $item['label'] = $this->einsatzobjekt;
            $item['long'] = (float)$this->long;
            $item['lat'] = (float)$this->lat;
            $item['strasse'] = $this->strasse;
            $item['ort'] = $this->ort;
            $item['ortsteil'] = $this->ortsteil;
            $item['key_no'] = $this->key_no;
            $item['map_no'] = $this->map_no;
            $item['station_id'] = $this->station_id;
            $item['coords'] = [];
            if ($this->coords !== null) {
                $item['coords'] = json_decode($this->coords);
            }
            $item['documentation'] = [
                'files' => [],
                'images' => [],
                'contacts' => []
            ];
            $item['piktos'] = [];

            if ($this->iconmarkers->count() > 0) {
                foreach ($this->iconmarkers as $icon) {
                    if ($icon->icon != null && $icon->icon->icon_small != null) {
                        $item['piktos'][] = array(
                            'id' => $icon->id,
                            'icon' => $icon->icon->icon_small->path,
                            'long' => (float)$icon->long,
                            'lat' => (float)$icon->lat,
                            'label' => $icon->icon->label,
                        );
                    }
                }
            }

            if ($this->documentation_public || $isTrustedCall) {
                if ($this->files->count() > 0) {
                    foreach ($this->files as $file) {
                        if ($file->isImage()) {
                            $item['documentation']['images'][] = array(
                                'thumb' => $file->getThumb(100, 100, ['mode' => 'crop']),
                                'label' => strlen($file->title) > 0 ? $file->title : $file->file_name,
                                'uri' => $file->path
                            );
                        } else {
                            $item['documentation']['files'][] = array(
                                'label' => strlen($file->title) > 0 ? $file->title : $file->file_name,
                                'uri' => $file->path
                            );
                        }
                    }
                }
                if ($this->contacts->count() > 0) {
                    $contactTypes = Contact::getTypeOptions();
                    foreach ($this->contacts as $contact) {
                        $item['documentation']['contacts'][] = array(
                            'email' => $contact->email,
                            'label' => $contact->label,
                            'id' => $contact->id,
                            'mobile' => $contact->mobile,
                            'phone' => $contact->phone,
                            'street_no' => $contact->street_no,
                            'zip_city' => $contact->zip_city,
                            'type' => (array_key_exists($contact->type, $contactTypes) ? $contactTypes[$contact->type] : '')
                        );
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::info('Error getting Einsatzobject Informationreduced Array '.$e->getMessage());
        }
        return $item;
    }
}
