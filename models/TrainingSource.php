<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class TrainingSource extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_training_source';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label'                  => 'required|between:1,128',
        'label_short'            => 'required|between:1,32',
    ];

    public $hasMany = [
        'scrape_sources' =>  'pm\Firemon112\Models\TrainingScrapeSource',
        'trainings' =>  'pm\Firemon112\Models\Training',
    ];
}
