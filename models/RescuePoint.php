<?php namespace pm\Firemon112\Models;

use Model;
use \Cms\Classes\Theme;
use pm\Firemon112\Traits\ContactableExtension;
use pm\Firemon112\Traits\FileUploadExtension;
use pm\Firemon112\Traits\SortOrderExtension;

/**
 * Model
 */
class RescuePoint extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use FileUploadExtension, SortOrderExtension, ContactableExtension;

    protected $appends = [
        'img_small',
        'img_big',
        'type_readable'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $TYPE_KEY_RETTUNGSPUNKT = "rettungspunkt";
    public static $TYPE_KEY_ANFAHRPUNKT = "anfahrpunkt";
    public static $TYPE_KEY_RETTUNGSTREFFPUNKT = "rettungstreffpunkt";
    public static $TYPE_KEY_NOTFALLINFOPUNKT = "notfallinfopunkt";

    public static $TYPE_IMG_RETTUNGSPUNKT_SMALL = "/assets/icons/marker/rettungspunkt-small.png";
    public static $TYPE_IMG_RETTUNGSPUNKT_BIG = "/assets/icons/marker/rettungspunkt-big.png";
    public static $TYPE_IMG_ANFAHRPUNKT_SMALL = "/assets/icons/marker/anfahrpunkt-small.png";
    public static $TYPE_IMG_ANFAHRPUNKT_BIG = "/assets/icons/marker/anfahrpunkt-big.png";
    public static $TYPE_IMG_RETTUNGSTREFFPUNKT_SMALL = "/assets/icons/marker/rettungstreffpunkt-small.png";
    public static $TYPE_IMG_RETTUNGSTREFFPUNKT_BIG = "/assets/icons/marker/rettungstreffpunkt-big.png";
    public static $TYPE_IMG_NOTFALLINFOPUNKT_SMALL = "/assets/icons/marker/notfallinfopunkt-small.png";
    public static $TYPE_IMG_NOTFALLINFOPUNKT_BIG = "/assets/icons/marker/notfallinfopunkt-big.png";

    public static $TYPE_IMG_UNKNOWN_SMALL = "/assets/icons/marker/unknown-small.png";
    public static $TYPE_IMG_UNKNOWN_BIG = "/assets/icons/marker/unknown-big.png";

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_rescuepoint';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
    ];

    public $attachMany = [
        'files' => ['System\Models\File', 'delete' => true]
    ];

    public $morphMany = [
        'contacts' => [\pm\Firemon112\Models\Contact::class, 'name' => 'contactable', 'delete' => true],
    ];

    public function scopeStation($query, $station_id)
    {
        return $query->where('station_id', $station_id);
    }

    public static function getTypeOptions() {
        return array(
            self::$TYPE_KEY_RETTUNGSPUNKT => 'Rettungspunkt',
            self::$TYPE_KEY_NOTFALLINFOPUNKT => 'Notfall Infopunkt',
            self::$TYPE_KEY_ANFAHRPUNKT => 'Anfahrpunkt für Rettungsfahrzeuge',
            self::$TYPE_KEY_RETTUNGSTREFFPUNKT => 'Rettungstreffpunkt'
        );
    }

    public function getImgSmallAttribute() {
        switch ($this->type) {
            case self::$TYPE_KEY_RETTUNGSTREFFPUNKT:
                $img = self::$TYPE_IMG_RETTUNGSTREFFPUNKT_SMALL;
                break;
            case self::$TYPE_KEY_ANFAHRPUNKT:
                $img = self::$TYPE_IMG_ANFAHRPUNKT_SMALL;
                break;
            case self::$TYPE_KEY_NOTFALLINFOPUNKT:
                $img = self::$TYPE_IMG_NOTFALLINFOPUNKT_SMALL;
                break;
            case self::$TYPE_KEY_RETTUNGSPUNKT:
                $img = self::$TYPE_IMG_RETTUNGSPUNKT_SMALL;
                break;
            default:
                $img = self::$TYPE_IMG_UNKNOWN_SMALL;
                break;
        }
        return asset('themes/'.Theme::getActiveThemeCode().$img);
    }

    public function getImgBigAttribute() {
        switch ($this->type) {
            case self::$TYPE_KEY_RETTUNGSTREFFPUNKT:
                $img = self::$TYPE_IMG_RETTUNGSTREFFPUNKT_BIG;
                break;
            case self::$TYPE_KEY_ANFAHRPUNKT:
                $img = self::$TYPE_IMG_ANFAHRPUNKT_BIG;
                break;
            case self::$TYPE_KEY_NOTFALLINFOPUNKT:
                $img = self::$TYPE_IMG_NOTFALLINFOPUNKT_BIG;
                break;
            case self::$TYPE_KEY_RETTUNGSPUNKT:
                $img = self::$TYPE_IMG_RETTUNGSPUNKT_BIG;
                break;
            default:
                $img = self::$TYPE_IMG_UNKNOWN_BIG;
                break;
        }
        return asset('themes/'.Theme::getActiveThemeCode().$img);
    }

    public function getTypeReadableAttribute() {
        $types = self::getTypeOptions();
        if (array_key_exists($this->type, $types)) {
            return $types[$this->type];
        }
        return "unknown";
    }

    public function getCategoryReadableAttribute() {
        $all = self::getCategoryOptions();
        if (array_key_exists($this->category, $all)) {
            return $all[$this->category];
        }
        return "";
    }
}
