<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Absence extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \pm\Firemon112\Traits\DateTimeExtension;

    public static $ABSENC_TYPE_SICK = "sick";
    public static $ABSENC_TYPE_HOLIDAY = "holiday";
    public static $ABSENC_TYPE_TRAINING = "training";
    public static $ABSENC_TYPE_WORK = "work";
    public static $ABSENC_TYPE_OTHER = "other";

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_user_absence';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'absence_type' => 'required|string',
        'user_id' => 'required|integer',
        'valid_from' => 'required|date',
        'valid_to' => 'required|date|after:valid_from',
        'auto_cancel_events' => 'boolean'
    ];

    public $fillable = [
        'absence_type',
        'user_id',
        'valid_from',
        'valid_to',
        'auto_cancel_events',
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User',
    ];

    public static function getAbsenceTypeOptions() {
        return [
            self::$ABSENC_TYPE_HOLIDAY => 'Urlaub',
            self::$ABSENC_TYPE_SICK => 'Krank',
            self::$ABSENC_TYPE_TRAINING => 'Lehrgang',
            self::$ABSENC_TYPE_WORK => 'Arbeit',
            self::$ABSENC_TYPE_OTHER => 'Sonstiges',
        ];
    }

    public function scopeActive($query)
    {
        $query->whereRaw('( valid_from <= CURRENT_TIMESTAMP() AND valid_to >= CURRENT_TIMESTAMP() )')->orderBy('valid_from','DESC');
    }

    public function scopeOverlapsWith($query, $date_from, $date_to)
    {
        return $query->where(function ($query) use ($date_from, $date_to) {
            $query->where('valid_from', '>=', $date_from)
                ->where('valid_from', '<=', $date_to);
        })->orWhere(function ($query) use ($date_from, $date_to) {
            $query->where('valid_to', '>=', $date_from)
                ->where('valid_to', '<=', $date_to);
        })->orWhere(function ($query) use ($date_from, $date_to) {
            $query->where('valid_from', '<=', $date_from)
                ->where('valid_to', '>=', $date_to);
        });
    }

    public function getIsActiveAttribute() {
        return $this->isNowBetween($this->valid_from, $this->valid_to);
    }

    public function getValidFromFormattedAttribute() {
        return $this->getFormattedDateTime($this->valid_from);
    }

    public function getValidToFormattedAttribute() {
        return $this->getFormattedDateTime($this->valid_to);
    }

    public function getAbsenceTypeReadableAttribute() {
        $all = self::getAbsenceTypeOptions();
        if (array_key_exists($this->absence_type, $all)) {
            return $all[$this->absence_type];
        }
        return "";
    }
}
