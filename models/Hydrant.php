<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\Traits\ContactableExtension;
use pm\Firemon112\Traits\FileUploadExtension;
use pm\Firemon112\Traits\SortOrderExtension;

/**
 * Model
 */
class Hydrant extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use FileUploadExtension, SortOrderExtension, ContactableExtension;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_hydrant';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = [
        'hydranttype_id',
        'name',
        'flowrate',
        'long',
        'lat',
        'station_id',
        'waterpipe'
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
        'hydranttype' =>  'pm\Firemon112\Models\Hydranttype',
    ];

    public $attachMany = [
        'files' => ['System\Models\File', 'delete' => true]
    ];

    public $morphMany = [
        'contacts' => [\pm\Firemon112\Models\Contact::class, 'name' => 'contactable', 'delete' => true]
    ];


    public function scopeStation($query, $station_id)
    {
        return $query->where('station_id', $station_id);
    }

    public static function getWaterpipeOptions() {
        return array('ring' => 'Ringleitung', 'tree' => 'Verästelt');
    }

    public function getWaterpipeReadableAttribute() {
        $arr = Hydrant::getWaterpipeOptions();
        if (array_key_exists($this->waterpipe, $arr)) {
            return $arr[$this->waterpipe];
        } else {
            return "";
        }
    }

    public function haversineDistance($anotherHydrant) {
        return self::CalcHaversineGreatCircleDistance($this->lat, $this->long, $anotherHydrant->lat, $anotherHydrant->long);
    }
    public function haversineGreatCircleDistance($latitudeTo, $longitudeTo)
    {
        return self::CalcHaversineGreatCircleDistance($this->lat, $this->long, $latitudeTo, $longitudeTo);
    }

    /**
     * @param $latitudeFrom
     * @param $longitudeFrom
     * @param $latitudeTo
     * @param $longitudeTo
     * @param $earthRadius
     * @return float|int
     * Achtung! Rückgabewert ist in m (nicht km)
     */
    public static function CalcHaversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }
}
