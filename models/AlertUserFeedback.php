<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class AlertUserFeedback extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_alert_user_feedback';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'alert' => 'pm\Firemon112\Models\Alert',
        'user' => 'RainLab\User\Models\User',
    ];

    public static $TYPE_FEEDBACK_NONE = 0;
    public static $TYPE_FEEDBACK_I = 1;
    public static $TYPE_FEEDBACK_D = 2;
    public static $TYPE_FEEDBACK_L = 3;
    public static $TYPE_FEEDBACK_N = 4;

    public static $TYPE_FEEDBACK_NONE_LABEL = "Keine Rückmeldung";
    public static $TYPE_FEEDBACK_I_LABEL = "Sofort";
    public static $TYPE_FEEDBACK_D_LABEL = "Direkt";
    public static $TYPE_FEEDBACK_L_LABEL = "Verspätet";
    public static $TYPE_FEEDBACK_N_LABEL = "Nicht Verfügbar";

    public static function getFeedbackStatusOptions() {
        return [
            self::$TYPE_FEEDBACK_NONE => self::$TYPE_FEEDBACK_NONE_LABEL,
            self::$TYPE_FEEDBACK_I => self::$TYPE_FEEDBACK_I_LABEL,
            self::$TYPE_FEEDBACK_D => self::$TYPE_FEEDBACK_D_LABEL,
            self::$TYPE_FEEDBACK_L => self::$TYPE_FEEDBACK_L_LABEL,
            self::$TYPE_FEEDBACK_N => self::$TYPE_FEEDBACK_N_LABEL
        ];
    }

    public function getFeedbackStatusReadableAttribute() {
        $arr = self::getFeedbackStatusOptions();
        if (array_key_exists($this->feedback_status, $arr)) {
            return $arr[$this->feedback_status];
        }
        return "?";
    }

    public function afterSave() {
        if ($this->alert->station != null) {
            Helper::MqSendMessageToStationToken($this->alert->station->token, Alert::$MQ_FEEDBACK_PERFORMED);
        }
    }

}
