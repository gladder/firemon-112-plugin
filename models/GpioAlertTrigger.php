<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;

/**
 * Model
 */
class GpioAlertTrigger extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public static $GPIO_MONITOR_MODE_RISING = 'rising';
    public static $GPIO_MONITOR_MODE_FALLING = 'falling';
    public static $GPIO_MONITOR_MODE_BOTH = 'both';

    public static $GPIO_MONITOR_MODE_RISING_LABEL = 'Steigende Flanke (rising)';
    public static $GPIO_MONITOR_MODE_FALLING_LABEL = 'Fallende Flanke (falling)';
    public static $GPIO_MONITOR_MODE_BOTH_LABEL = 'Steigende oder fallende Flanke';

    public static $GPIO_0_LABEL  = "GPIO 0 (ID EEPROM - ID_SD)";
    public static $GPIO_1_LABEL  = "GPIO 1 (ID EEPROM - ID_SC)";
    public static $GPIO_2_LABEL  = "GPIO 2 (I2C SDA1)";
    public static $GPIO_3_LABEL  = "GPIO 3 (I2C SCL1)";
    public static $GPIO_4_LABEL  = "GPIO 4 (Default 1-Wire)";
    public static $GPIO_5_LABEL  = "GPIO 5";
    public static $GPIO_6_LABEL  = "GPIO 6";
    public static $GPIO_7_LABEL  = "GPIO 7 (SPI0 CE1)";
    public static $GPIO_8_LABEL  = "GPIO 8 (SPI0 CE0)";
    public static $GPIO_9_LABEL  = "GPIO 9 (SPI0 MISO)";
    public static $GPIO_10_LABEL = "GPIO 10 (SPI0 MOSI)";
    public static $GPIO_11_LABEL = "GPIO 11 (SPI0 SCLK)";
    public static $GPIO_12_LABEL = "GPIO 12 (PWM / I2S)";
    public static $GPIO_13_LABEL = "GPIO 13 (PWM / I2S)";
    public static $GPIO_14_LABEL = "GPIO 14 (UART0 TX)";
    public static $GPIO_15_LABEL = "GPIO 15 (UART0 RX)";
    public static $GPIO_16_LABEL = "GPIO 16";
    public static $GPIO_17_LABEL = "GPIO 17";
    public static $GPIO_18_LABEL = "GPIO 18 (PCM_CLK / PWM)";
    public static $GPIO_19_LABEL = "GPIO 19 (PCM_FS)";
    public static $GPIO_20_LABEL = "GPIO 20 (PCM_DIN)";
    public static $GPIO_21_LABEL = "GPIO 21 (PCM_DOUT)";
    public static $GPIO_22_LABEL = "GPIO 22";
    public static $GPIO_23_LABEL = "GPIO 23";
    public static $GPIO_24_LABEL = "GPIO 24";
    public static $GPIO_25_LABEL = "GPIO 25";
    public static $GPIO_26_LABEL = "GPIO 26";
    public static $GPIO_27_LABEL = "GPIO 27";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_gpio_alert_trigger';


    protected $hidden = [
        'alert_stichwort',
        'alert_klartext',
        'alert_information',
        'alert_einsatzobjekt',
        'alert_strasse',
        'alert_ort',
        'alert_ortsteil',
        'alert_long',
        'alert_lat',
        'alert_mak',
        'station_id',
        'pivot',
        'created_at',
        'updated_at',
        'alert_stichwort_id',
        'limit_trigger_minutes',
        'last_trigger_performed'
    ];

    public $fillable = [
        'label',
        'gpio_number',
        'gpio_monitoring_mode',
        'enabled',
        'alert_information',
        'alert_einsatzobjekt',
        'alert_strasse',
        'alert_ort',
        'alert_long',
        'alert_lat',
        'alert_mak',
        'station_id',
        'alert_stichwort_id',
        'limit_trigger_minutes',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label'                 => 'required|max:64',
        'gpio_number'           => 'required|integer|min:0|max:27',
        'gpio_monitoring_mode'  => 'required|in:rising,falling,both',
        'alert_information'     => 'required|max:255',
        'alert_einsatzobjekt'   => 'required|max:255',
        'alert_strasse'         => 'required|max:255',
        'alert_ort'             => 'required|max:255',
        'alert_long' => 'required|numeric|min:-180|max:180',
        'alert_lat' => 'required|numeric|min:-90|max:90',
        'alert_mak'             => '',      // no limit here, it's a TEXT field
        'enabled'               => 'boolean',
        'station_id'            => 'required|integer',
        'alert_stichwort_id'    => 'required|integer',
        'limit_trigger_minutes' => 'required|integer',
    ];

    public $customMessages = [
        'label.required'                        => 'Bitte geben Sie eine Bezeichnung an.',
        'label.max'                             => 'Die Bezeichnung darf maximal :max Zeichen enthalten.',
        'gpio_number.required'                  => 'Bitte geben Sie eine GPIO-Nummer an.',
        'gpio_number.integer'                   => 'Die GPIO-Nummer muss eine ganze Zahl sein.',
        'gpio_number.min'                       => 'Die GPIO-Nummer darf nicht negativ sein.',
        'gpio_number.max'                       => 'Die GPIO-Nummer darf höchstens :max sein.',
        'gpio_monitoring_mode.required'         => 'Bitte wählen Sie einen Überwachungsmodus (rising, falling, both).',
        'gpio_monitoring_mode.in'               => 'Der Überwachungsmodus muss einer der folgenden Werte sein: rising, falling oder both.',
        'alert_information.required'            => 'Die Lage-Informationen fehlen',
        'alert_information.max'                 => 'Die Lage-Informationen dürfen maximal :max Zeichen enthalten.',
        'alert_einsatzobjekt.required'          => 'Es fehlt die Objektbezeichnung',
        'alert_einsatzobjekt.max'               => 'Das Einsatzobjekt darf maximal :max Zeichen enthalten.',
        'alert_strasse.required'                => 'Es fehlt die Straße und Hausnummer',
        'alert_strasse.max'                     => 'Die Straßenangabe darf maximal :max Zeichen enthalten.',
        'alert_ort.required'                    => 'Es fehlt der Ort',
        'alert_ort.max'                         => 'Der Ort darf maximal :max Zeichen enthalten.',
        'alert_long.required'                   => 'Es wurden keine Einsatz-Koordinaten übergeben!',
        'alert_long.min'                        => 'Die Längengrad-Koordinate muss größer -180° sein.',
        'alert_long.max'                        => 'Die Längengrad-Koordinate muss kleiner 180° sein.',
        'alert_lat.required'                    => 'Es wurden keine Einsatz-Koordinaten übergeben!',
        'alert_lat.min'                         => 'Die Breitengrad-Koordinate muss größer -90° sein.',
        'alert_lat.max'                         => 'Die Breitengrad-Koordinate muss kleiner 90° sein.',
        'enabled.boolean'                       => 'Das Feld „enabled“ muss einen Wahr/Falsch-Wert besitzen.',
        'station_id'                            => 'Es wurde keine Station definiert',
        'alert_stichwort_id'                    => 'Es wurde kein Stichwort definiert',
        'limit_trigger_minutes'                 => 'Es wurde kein RateLimit definiert',
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
        'stichwort' =>  ['pm\Firemon112\Models\Stichwort', 'key' => 'alert_stichwort_id'],
    ];

    public $belongsToMany = [
        'devices' => [
            'pm\Firemon112\Models\Device',
            'table' => 'pm_firemon112_gpio_alert_trigger2device',
        ]
    ];

    public static function getGpioMonitoringModeOptions()
    {
        return [
            self::$GPIO_MONITOR_MODE_RISING => self::$GPIO_MONITOR_MODE_RISING_LABEL,
            self::$GPIO_MONITOR_MODE_FALLING => self::$GPIO_MONITOR_MODE_FALLING_LABEL,
            self::$GPIO_MONITOR_MODE_BOTH => self::$GPIO_MONITOR_MODE_BOTH_LABEL,
        ];
    }

    public function getGpioMonitoringModeLabelAttribute() {
        $arr = self::getGpioMonitoringModeOptions();
        if (array_key_exists($this->gpio_monitoring_mode, $arr)) {
            return $arr[$this->gpio_monitoring_mode];
        }
        return "N/A";
    }

    public static function getGpioNumberOptions() {
        return [
            0 => self::$GPIO_0_LABEL,
            1 => self::$GPIO_1_LABEL,
            2 => self::$GPIO_2_LABEL,
            3 => self::$GPIO_3_LABEL,
            4 => self::$GPIO_4_LABEL,
            5 => self::$GPIO_5_LABEL,
            6 => self::$GPIO_6_LABEL,
            7 => self::$GPIO_7_LABEL,
            8 => self::$GPIO_8_LABEL,
            9 => self::$GPIO_9_LABEL,
            10 => self::$GPIO_10_LABEL,
            11 => self::$GPIO_11_LABEL,
            12 => self::$GPIO_12_LABEL,
            13 => self::$GPIO_13_LABEL,
            14 => self::$GPIO_14_LABEL,
            15 => self::$GPIO_15_LABEL,
            16 => self::$GPIO_16_LABEL,
            17 => self::$GPIO_17_LABEL,
            18 => self::$GPIO_18_LABEL,
            19 => self::$GPIO_19_LABEL,
            20 => self::$GPIO_20_LABEL,
            21 => self::$GPIO_21_LABEL,
            22 => self::$GPIO_22_LABEL,
            23 => self::$GPIO_23_LABEL,
            24 => self::$GPIO_24_LABEL,
            25 => self::$GPIO_25_LABEL,
            26 => self::$GPIO_26_LABEL,
            27 => self::$GPIO_27_LABEL,
        ];
    }

    public function getGpioNumberLabelAttribute() {
        $arr = self::getGpioNumberOptions();
        if (array_key_exists($this->gpio_number, $arr)) {
            return $arr[$this->gpio_number];
        }
        return "N/A";
    }

    public static function getLimitTriggerMinutesOptions() {
        return [
            5 => "5 Minuten",
            15 => "15 Minuten",
            30 => "30 Minuten",
            60 => "60 Minuten",
            120 => "2 Stunden",
            360 => "6 Stunden",
            720 => "12 Stunden",
            1440 => "24 Stunden",
            4320 => "3 Tage",
            10080 => "7 Tage",
        ];
    }

    public function getLimitTriggerMinutesLabelAttribute() {
        $arr = self::getLimitTriggerMinutesOptions();
        if (array_key_exists($this->limit_trigger_minutes, $arr)) {
            return $arr[$this->limit_trigger_minutes];
        }
        return "N/A";
    }

    public function resetLastTriggerPerformed() {
        $this->last_trigger_performed = null;
        $this->save();
    }

    public function isRateLimitReached() {
        if ($this->last_trigger_performed == null) {
            return false;
        }
        $now = Carbon::now('Europe/Berlin');
        $ltp = Carbon::parse($this->last_trigger_performed, 'Europe/Berlin');
        $triggerThreshold = $now->subMinutes($this->limit_trigger_minutes);

        return $triggerThreshold->lessThan($ltp);
    }

    public function triggerAlert() {
        try {
            \Log::info('Trigger GPIO Alert: '.$this->id.' / '.$this->label.' NOW');
            if ($this->isRateLimitReached()) {
                \Log::info('Trigger GPIO Alert: '.$this->id.' / '.$this->label.' DROPPED due to rate limit');
            } else {
                $depesche = new Depesche();

                $now = Carbon::now('Europe/Berlin');
                $depesche->Einsatznummer = Alert::generateFiremonEinsatznummer();
                $depesche->Einsatzbeginn = $now->format("d.m.Y H:i:s");
                $depesche->Einsatzstichwort = $this->stichwort->stichwort;
                $depesche->Klartext = $this->stichwort->klartext;
                $depesche->Information = $this->alert_information;
                $depesche->Einsatzobjekt = $this->alert_einsatzobjekt;
                $depesche->Strasse = $this->alert_strasse;
                $depesche->Ort = $this->alert_ort;
                $depesche->Ortsteil = "";
                $depesche->X = 0.0;
                $depesche->Y = 0.0;
                $depesche->Lat = $this->alert_lat;
                $depesche->Long = $this->alert_long;
                $depesche->MitalarmierteKraefte = explode("\n", $this->alert_mak);

                Alert::HandleCreateAlertForStation($depesche, $this->station);

                $this->last_trigger_performed = $now;
                $this->save();
            }
        } catch (Exception $e) {
            // since its ajax request purge message directly to wipe from session and not wait for another server roundtrip
            \Log::alert("Trigger GPIO Alert failed: " . $e->getMessage());
        }
    }

}
