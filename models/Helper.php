<?php namespace pm\Firemon112\Models;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;
use GuzzleHttp;
use RainLab\User\Models\User as UserModel;
use Auth;
use Mail;
use Validator;
use ValidationException;
use Event;
use Session;
use pm\Firemon112\Models\CustomSettings;

class Helper {

    public static $PERMISSION_IS_MISSION_ROLE = 'is_mission_role';
    public static $PERMISSION_IS_MISSION_ADMIN = 'is_mission_admin';
    public static $PERMISSION_IS_MISSION_OPERATOR = 'is_mission_operator';
    public static $PERMISSION_IS_ROLE_STATION = 'is_role_station';
    public static $PERMISSION_IS_ROLE_PRESENCE = 'is_role_presence';
    public static $PERMISSION_IS_ROLE_ALERT = 'is_role_alert';
    public static $PERMISSION_IS_ROLE_ALERT_READONLY = 'is_role_alert_readonly';
    public static $PERMISSION_IS_ROLE_EVENTS = 'is_role_events';


    public static $USER_NOTIFICATION_ALLOW_EVENTS_EMAIL = 'events_email';
    public static $USER_NOTIFICATION_ALLOW_EVENTS_EMAIL_DEFAULT = true;
    public static $USER_NOTIFICATION_ALLOW_EVENTS_PUSH = 'events_push';
    public static $USER_NOTIFICATION_ALLOW_EVENTS_PUSH_DEFAULT = true;
    public static $USER_NOTIFICATION_ALLOW_ALERT_PUSH = 'alert_push';
    public static $USER_NOTIFICATION_ALLOW_ALERT_PUSH_DEFAULT = true;
    public static $USER_NOTIFICATION_ALLOW_TRAINING_PUSH = 'training_push';
    public static $USER_NOTIFICATION_ALLOW_TRAINING_PUSH_DEFAULT = true;
    public static $USER_NOTIFICATION_ALLOW_NEWS_PUSH = 'news_push';
    public static $USER_NOTIFICATION_ALLOW_NEWS_PUSH_DEFAULT = true;
    public static $USER_NOTIFICATION_EVENTS_REMINDER_POSSIBLE_INTERVALS = [24,72,168];
    public static $USER_NOTIFICATION_EVENTS_REMINDER_POSSIBLE_INTERVALS_LABELS = ['24 Stunden','3 Tage','1 Woche'];
    public static $USER_NOTIFICATION_EVENTS_REMINDER_EMAIL_INTERVALS = 'events_email_intervals';
    public static $USER_NOTIFICATION_EVENTS_REMINDER_EMAIL_INTERVALS_DEFAULT = [24];
    public static $USER_NOTIFICATION_EVENTS_REMINDER_PUSH_INTERVALS = 'events_push_intervals';
    public static $USER_NOTIFICATION_EVENTS_REMINDER_PUSH_INTERVALS_DEFAULT = [24];



    private static function MqConnection() {
        return new AMQPStreamConnection(Settings::get('mq-sys-host'), Settings::get('mq-sys-port'), Settings::get('mq-sys-user'), Settings::get('mq-sys-pass'), "/");
    }


    public static function MqSendMessageToStationToken($stationToken, $messageString) {
        try {
            $exchange = 'amq.topic';
            $connection = self::MqConnection();
            $channel = $connection->channel();
            $message = new AMQPMessage($messageString, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_NON_PERSISTENT));
            $channel->basic_publish($message, $exchange, $stationToken);
            \Log::info('Message sent to broker: ' . $exchange . ' // ' . $stationToken . ': '.$messageString);
            $channel->close();
            $connection->close();
            return true;
        } catch (\Exception $e) {
            \Log::alert('Exception while publishing Message for: ' . $exchange . ' // ' . $stationToken . ': '.$messageString);
            trace_log($e);
            return false;
        }
    }

    public static function MqSendMessageToQueue($queue, $messageString) {
        try {
            $connection = self::MqConnection();
            $channel = $connection->channel();
            $message = new AMQPMessage($messageString, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_NON_PERSISTENT));
            $channel->basic_publish($message, '', $queue);
            \Log::info('Message sent to Queue: ' . $queue . ' // ' . $messageString);
            $channel->close();
            $connection->close();
            return true;
        } catch (\Exception $e) {
            \Log::alert('Exception while publishing Message to Queue: ' . $queue . ' // ' . $messageString);
            trace_log($e);
            return false;
        }
    }

    private static function MqApiEndpoint() {
        $protocol = 'https';
        if (!Settings::get('mq-use-ssl')) {
            $protocol = 'http';
        }
        return $protocol.'://'.Settings::get('mq-api-host').':'.Settings::get('mq-api-port');
    }

    public static function MqQueueForDevice($deviceToken) {
        try {
            $client = new GuzzleHttp\Client(['verify' => false]); //TODO: workaround for ssl issues -> fix certs!

            $res = $client->request('GET', self::MqApiEndpoint().'/api/queues', [
                'auth' => [ Settings::get('mq-sys-user'), Settings::get('mq-sys-pass') ]
            ]);
            if ($res->getStatusCode() == 200) {
                $parsedResponse = json_decode($res->getBody());
                if (is_array($parsedResponse)) {
                    foreach ($parsedResponse as $queue) {
                        if (strpos($queue->name, $deviceToken) !== false) {
                            return $queue->name;
                        }
                    }
                }
            }
            return null;
        } catch (\Exception $e) {
            \Log::alert('Exception while publishing MqQueueForDevice');
            trace_log($e);
            return null;
        }
    }

    public static function SyncMqUser($MqUsername, $MqPassword, $MqVhost) {
        try {
            if (boolval(Settings::get('mq-sync-user')) !== true) {
                \Log::info('SyncMqUser stopped since setting mq-sync-user is disabled');
                return false;
            }
            if (strlen($MqUsername) <= 0 || strlen($MqPassword) <= 0 || strlen($MqVhost) <= 0) {
                return false;
            }

            $client = new GuzzleHttp\Client(['verify' => false]); //TODO: workaround for ssl issues -> fix certs!

            $res = $client->request('PUT', self::MqApiEndpoint().'/api/users/'.urlencode($MqUsername), [
                'auth' => [ Settings::get('mq-sys-user'), Settings::get('mq-sys-pass') ],
                'body' => json_encode([
                    "password" => $MqPassword,
                    "tags" => ""
                ])
            ]);

            if ($res->getStatusCode() >= 200 && $res->getStatusCode() <= 204) {
                $res_perms = $client->request('PUT', self::MqApiEndpoint().'/api/permissions/'.urlencode($MqVhost).'/'.urlencode($MqUsername), [
                    'auth' => [ Settings::get('mq-sys-user'), Settings::get('mq-sys-pass') ],
                    'body' => json_encode([
                        "configure" => ".*",
                        "write" => ".*",
                        "read" => ".*"
                    ])
                ]);
                if ($res_perms->getStatusCode() >= 200 && $res_perms->getStatusCode() <= 204) {
                    return true;
                }
            }
            return false;
        } catch (\Exception $e) {
            \Log::alert('Exception while SyncMqUser');
            trace_log($e);
            return false;
        }
    }

    public static function DeleteMqUser($MqUsername) {
        try {
            if (boolval(Settings::get('mq-sync-user')) !== true) {
                \Log::info('DeleteMqUser stopped since setting mq-sync-user is disabled');
                return false;
            }
            if (strlen($MqUsername) <= 0) {
                return false;
            }

            $client = new GuzzleHttp\Client(['verify' => false]); //TODO: workaround for ssl issues -> fix certs!

            $res = $client->request('DELETE', self::MqApiEndpoint().'/api/users/'.urlencode($MqUsername), [
                'auth' => [ Settings::get('mq-sys-user'), Settings::get('mq-sys-pass') ]
            ]);

            if ($res->getStatusCode() >= 200 && $res->getStatusCode() <= 204) {
                return true;
            }

            return false;
        } catch (\Exception $e) {
            \Log::alert('Exception while DeleteMqUser');
            trace_log($e);
            return false;
        }
    }

    public static function CreateFrontendUserDry($firstName, $lastName, $email, $mobile) {
        $result = ['status' => 'ok', 'hint' => ''];
        $data = [
            'email' => strtolower(trim($email)),
            'name' => $firstName,
            'surname' => $lastName,
            'alert_email' => '',
            'alert_sms' => '',
            'alert_call' => '',
            'alert_call_second' => ''
        ];

        if (strlen($mobile)>0) {
            $data['alert_sms'] = $mobile;
        }

        $rules = (new UserModel)->rules;
        unset($rules['username']);
        unset($rules['password']);
        unset($rules['password_confirmation']);

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            $result['status'] = "fehler";
            $result['hint'] = $validation->messages();
        }

        return $result;
    }

    public static function CreateFrontendUser($firstName, $lastName, $email, $stationId = 0, $sendWelcomeMail = false) {
        $password = str_random(12);
        $data = [
            'email' => strtolower(trim($email)),
            'password' => $password,
            'password_confirmation' => $password,
            'name' => $firstName,
            'surname' => $lastName,
            'alert_email' => '',
            'alert_sms' => '',
            'alert_call' => '',
            'alert_call_second' => ''
        ];

        $rules = (new UserModel)->rules;
        unset($rules['username']);

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        Event::fire('rainlab.user.beforeRegister', [&$data]);

        $user = Auth::register($data, false);

        Event::fire('rainlab.user.register', [$user, $data]);

        $user->station_id = $stationId;
        $user->save();

        CustomSettings::UpdateUserRemindersForStation($user, $user->station); // init reminders for first time with defaults

        if ($sendWelcomeMail) {
            self::SendWelcomeMail($user, false);
        }

        return $user;
    }

    public static function SplitName($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.preg_quote($last_name,'#').'#', '', $name ) );
        return array($first_name, $last_name);
    }

    public static function SendWelcomeMail($user, $resetPassword = false) {
        if ($user === null) {
            return false;
        }
        if ($resetPassword) {
            $password = str_random(12);
            $user->password = $password;
            $user->password_confirmation = $password;
            $user->save();
        }
        $code = implode('!', [$user->id, $user->getActivationCode()]);
        $mailVars = [
            'name'     => $user->name,
            'email'    => $user->email,
            'username' => $user->username,
            'login'    => $user->getLogin(),
            'password' => $user->getOriginalHashValue('password'),
            'link'     => url('account').'?activate=' . $code
        ];

        Mail::sendTo($user, 'pm.firemon112::mail.frontend-user-created', $mailVars);
    }

    public static function SendUserRegisteredMailToAdmins($user) {
        if ($user === null) {
            return false;
        }

        $mailVars = [
            'name'     => "unknown",
            'email'    => $user->email,
            'first_name' => $user->name,
            'last_name'    => $user->surname,
            'link'     => url('kameraden')
        ];

        foreach ($user->station->groups as $group) {
            if ($group->is_role_station) {
                foreach ($group->users as $recipient) {
                    $mailVars['name'] = $recipient->name;
                    Mail::sendTo($recipient, 'pm.firemon112::mail.frontend-user-registered', $mailVars);
                }
            }
        }
    }

    public static function ActivateUser($user) {
        if ($user === null) {
            return false;
        }
        $user->attemptActivation($user->getActivationCode());
    }

    public static function GetFeUser() {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    public static function GetFeStation() {
        $user = self::GetFeUser();
        if ($user === null) {
            return null;
        }
        $stationId = intval(Session::get('currentStationId', 0));
        $station = null;
        if ($stationId <= 0) {
            $stationId = $user->station_id;
        }
        if ($stationId != $user->station_id) {
            $linkedStation = $user->linked_stations()->where('id', $stationId)->first();
            if ($linkedStation !== null) {
                $station = $linkedStation;
            } else {
                Session::put('currentStationId', $user->station->id);
                $station = $user->station;
            }
        } else {
            $station = $user->station;
        }
        return $station;
    }

    public static function GetDefaultDateTimeFormat() {
        return "d.m.Y H:i";
    }

    public static function GetDefaultDateFormat() {
        return "d.m.Y";
    }

    public static function GetDateFormatYearTwoDigit() {
        return "d.m.y";
    }

    public static function GetMonthNames() {
        return [
            0 => "n/a",
            1 => "Januar",
            2 => "Februar",
            3 => "März",
            4 => "April",
            5 => "Mai",
            6 => "Juni",
            7 => "Juli",
            8 => "August",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Dezember"
        ];
    }

    public static function GetWeekdayNames() {
        return [
            0 => "Sonntag",
            1 => "Montag",
            2 => "Dienstag",
            3 => "Mittwoch",
            4 => "Donnerstag",
            5 => "Freitag",
            6 => "Samstag"
        ];
    }

    public static function extractString($regex, $subject, $matchIndex) {
        if (preg_match($regex, $subject, $matches) === 1) {
            if ($matches !== null && count($matches) >= $matchIndex) {
                return trim($matches[$matchIndex]);
            }
        }

        return "";
    }

    public static function AccuWeatherGeopositionSearch($lat, $lon) {
        try {
            $accuWeatherApiKey = Settings::get('accu-weather-api-key');
            if (strlen($accuWeatherApiKey) <= 0) {
                \Log::info('AccuWeatherGeopositionSearch stopped since accu-weather-api-key is empty');
                return [];
            }

            $client = new GuzzleHttp\Client();

            $res = $client->request(
                'GET',
                'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search',
                [
                    'query' => [
                        'apikey' => $accuWeatherApiKey,
                        'q' => $lat.','.$lon,
                        'language' => 'de-DE',
                        'details' => 'false',
                        'toplevel' => 'false'
                    ]
                ]
            );

            if ($res->getStatusCode() != 200) {
                throw new \Exception("LocationKey for " . $lat.",".$lon." not found due to api error");
            }

            $parsedResponse = json_decode($res->getBody());
            if (is_array($parsedResponse) || is_object($parsedResponse)) {
                return (array) $parsedResponse;
            }
            return [];
        } catch (\Exception $e) {
            \Log::alert('Exception while AccuWeatherGeopositionSearch');
            trace_log($e);
            return [];
        }
    }

    public static function AccuWeatherCurrentConditions($locationKey) {
        try {
            $accuWeatherApiKey = Settings::get('accu-weather-api-key');
            if (strlen($accuWeatherApiKey) <= 0) {
                \Log::info('AccuWeatherGeopositionSearch stopped since accu-weather-api-key is empty');
                return [];
            }

            if ($locationKey <= 0) {
                \Log::info('AccuWeatherCurrentConditions stopped since locationKey is 0');
                return [];
            }

            $client = new GuzzleHttp\Client();

            $res = $client->request(
                'GET',
                'http://dataservice.accuweather.com/currentconditions/v1/'.$locationKey,
                [
                    'query' => [
                        'apikey' => $accuWeatherApiKey,
                        'language' => 'de-DE',
                        'details' => 'true'
                    ]
                ]
            );

            if ($res->getStatusCode() != 200) {
                throw new \Exception("Current Conditions Weather for " . $locationKey." not found due to api error");
            }

            $parsedResponse = json_decode($res->getBody());
            if (is_array($parsedResponse) || is_object($parsedResponse)) {
                return (array) $parsedResponse;
            }
            return [];
        } catch (\Exception $e) {
            \Log::alert('Exception while AccuWeatherCurrentConditions');
            trace_log($e);
            return false;
        }
    }

    public static $Base62Chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    public static $TokenPayloadSeperator = '_';

    public static function intToChars($num) {
        $b = strlen(self::$Base62Chars);
        $r = $num  % $b ;
        $res = self::$Base62Chars[$r];
        $q = floor($num/$b);
        while ($q) {
            $r = $q % $b;
            $q =floor($q/$b);
            $res = self::$Base62Chars[$r].$res;
        }
        return $res;
    }

    public static function charsToInt($num) {
        $b = strlen(self::$Base62Chars);
        $limit = strlen($num);
        $res=strpos(self::$Base62Chars,$num[0]);
        for($i=1;$i<$limit;$i++) {
            $res = $b * $res + strpos(self::$Base62Chars,$num[$i]);
        }
        return $res;
    }

    public static function calcFeedbackToken($user_id, $alert_id) {
        return self::intToChars($user_id) . self::$TokenPayloadSeperator . self::intToChars($alert_id);
    }

    public static function parseFeedbackToken($token) {
        if (strpos($token, self::$TokenPayloadSeperator) !== false) {
            $parts = explode(self::$TokenPayloadSeperator, $token);
            if (count($parts) == 2) {
                return [
                    "user_id" => self::charsToInt($parts[0]),
                    "alert_id" => self::charsToInt($parts[1])
                ];
            }
        }
        return null;
    }

    public static function loginUserToken($email, $password, $token) {
        $response = [
            'success' => false,
            'message' => ''
        ];
        $credentials = [
            'email'    => $email,
            'password' => $password
        ];
        try {
            $user = Auth:: findUserByCredentials($credentials);
            if ($user == null) {
                throw new \Exception("User not found");
            }
            if (!$user->is_activated) {
                throw new \Exception("Benutzer nicht aktiviert!");
            }
            if (!$user->allow_app_usage) {
                throw new \Exception("App Benutzung nicht erlaubt");
            }
            $device = Device::state(Device::$STATE_PENDING)->where('token', $token)->first();
            self::enableDeviceForMobileUser($device, $user);
            $response['success'] = true;
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function enableDeviceForMobileUser($device, $user) {
        // device needs to be already existing as pending
        if ($device == null || !$device->getIsStatePendingAttribute()) {
            throw new \Exception("Device not ready to be added!");
        }
        $device->state = Device::$STATE_ACTIVE;
        $device->station_id = $user->station_id;
        $device->type = Device::$TYPE_MOBILE;
        if ($user->station->mqs->count() > 0) {
            $device->mq_id = $user->station->mqs->first()->id;
        }
        $device->watchdog = false;
        $device->user_id = $user->id;
        $device->save();
    }
    public static function enableDeviceForStation($device, $station, $type) {
        // device needs to be already existing as pending
        if ($device == null || !$device->getIsStatePendingAttribute()) {
            throw new \Exception("Device not ready to be added!");
        }
        $device->state = Device::$STATE_ACTIVE;
        $device->station_id = $station->id;
        $device->type = $type;
        if ($station->mqs->count() > 0) {
            $device->mq_id = $station->mqs->first()->id;
        }
        if ($device->type == Device::$TYPE_MONITOR) {
            $device->watchdog = true;
        }
        $device->save();
    }

    public static function getMobilePhoneVariants($mobile_phone) {
        $arr = [];
        $mobile_phone = preg_replace("/[^0-9+]/", "", $mobile_phone );
        $static_part = "";
        if (substr($mobile_phone, 0, 2 ) == "01") {
            $static_part = substr($mobile_phone, 2);
        }
        if (substr($mobile_phone, 0, 3 ) == "491") {
            $static_part = substr($mobile_phone, 3);
        }
        if (substr($mobile_phone, 0, 4 ) == "+491") {
            $static_part = substr($mobile_phone, 4);
        }
        if (substr($mobile_phone, 0, 5 ) == "00491") {
            $static_part = substr($mobile_phone, 5);
        }
        if (strlen($static_part)>0) {
            $arr = [
                "01" . $static_part,
                "491" . $static_part,
                "+491" . $static_part,
                "00491" . $static_part
            ];
        } else {
            $arr = [ $mobile_phone ];
        }

        return $arr;
    }

    public static function findUserByAlertSms($alert_sms) {
        $alert_sms = preg_replace("/[^0-9+]/", "", $alert_sms );

        if (strlen($alert_sms) > 0) {
            return UserModel::whereRaw("alert_sms in ('".implode("','",self::getMobilePhoneVariants($alert_sms))."')")->first();
        }
        return null;
    }

    public static function isJwt($token) {
        $tokenParts = explode('.', $token);
        if (count($tokenParts) !== 3) {
            return false;
        }

        try {
            $header = json_decode(base64_decode($tokenParts[0]));
            $payload = json_decode(base64_decode($tokenParts[1]));
            $signature = $tokenParts[2];
            return is_object($header) && is_object($payload) && is_string($signature);
        } catch (Exception $e) {
            return false;
        }
    }

    public static function generateUniqueNumber($length, $existingNumbers) {
        $min = pow(10, $length-1);
        $max = pow(10, $length)-1;
        $uniqueNumber = rand($min,$max);
        if (in_array($uniqueNumber, $existingNumbers)) {
            // If the generated number already exists, recursively call the function to generate a new number
            return generateUniqueNumber($length, $existingNumbers);
        } else {
            return $uniqueNumber;
        }
    }

    public static function testRegex($pattern) {
        $result = @preg_match($pattern, "");

        if ($result === false) {
            return false;
        }
        return true;
    }

    public static function userHasMissionAdmin($user, $stationId = 0) {
        if ($stationId <= 0) {
            $stationId = $user->station_id;
        }
        return self::userHasPermission($user, $stationId, self::$PERMISSION_IS_MISSION_ADMIN);
    }

    public static function userHasMissionOperator($user, $stationId = 0) {
        if ($stationId <= 0) {
            $stationId = $user->station_id;
        }
        return self::userHasPermission($user, $stationId, self::$PERMISSION_IS_MISSION_OPERATOR);
    }

    public static function userHasMissionRole($user, $stationId = 0) {
        if ($stationId <= 0) {
            $stationId = $user->station_id;
        }
        return self::userHasPermission($user, $stationId, self::$PERMISSION_IS_MISSION_ROLE);
    }

    public static function userCanViewAlertDetails($user, $stationId = 0) {
        return self::userHasMissionOperator($user, $stationId) || self::userHasMissionAdmin($user, $stationId);
    }

    public static function getGroupsByPermissionFlag($user, $stationId, $permissionFlag) {
        if ($user === null) {
            return [];
        }
        if ($stationId <= 0) {
            return [];
        }
        return $user->groups()->where('station_id', $stationId)->where($permissionFlag, true)->orderBy('prio','desc')->get()->toArray();
    }

    public static function userHasPermission($user, $stationId, $permissionFlag) {
        if ($user === null) {
            return false;
        }
        if ($stationId <= 0) {
            return false;
        }
        if (strlen($permissionFlag) <= 0) {
            return false;
        }
        if (!in_array($permissionFlag, [
            self::$PERMISSION_IS_MISSION_ADMIN,
            self::$PERMISSION_IS_MISSION_OPERATOR,
            self::$PERMISSION_IS_MISSION_ROLE,
            self::$PERMISSION_IS_ROLE_ALERT,
            self::$PERMISSION_IS_ROLE_ALERT_READONLY,
            self::$PERMISSION_IS_ROLE_EVENTS,
            self::$PERMISSION_IS_ROLE_PRESENCE,
            self::$PERMISSION_IS_ROLE_STATION
        ])) {
            return false;
        }
        $validGroups = $user->groups()->where('station_id', $stationId)->where($permissionFlag, true)->pluck('id');
        if ($validGroups !== null && count($validGroups) > 0) {
            return true;
        }
        return false;
    }

    public static function CarNetDetectCar($imageData) {
        try {
            // only for debugging! $imageData = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAHgAoADAREAAhEBAxEB/8QAHAAAAgMBAQEBAAAAAAAAAAAAAgMBBAUABgcI/8QAUxAAAgEDAgMGAgYGBwYDBwEJAQIDAAQREiEFMUEGEyJRYXGBkRQyobHB0RUjQlLh8AczU2JygpIWJDRDsvGDosIlRFRjc4ST0jUmRVV0dYWUlf/EABoBAQEBAQEBAQAAAAAAAAAAAAABAgMEBQb/xAAzEQEAAgIBAwMDAwQCAQUBAQAAAQIDERIEITEFEzIiQVEUQmFScZGhIzMVQ1OBsdEkYv/aAAwDAQACEQMRAD8A+oiVfJv9JrTIhKvr/pNAQlXz+yiiEqfvCg4Sp+8KhsXep+8PnQ2nvE/eHzoJ7xAMlhj3oJEiEAhgQeoNVGH2zAfsteYIJUA/bWR8uvTjtG3963jbb0Yj8axLS9GwTi0ZP7UJ+wj865y6VXnYv6Co0XPEkkeh1DKehoAVFQYUAADYCiuyxXGcDNAmcZt5AB+zWWlaMZ4RF6ACtQzLXsba8vVcRQYSTmMYUHbcfKtuWtNm07NRqA11IXP7qbD51qKnPTYht4LZNEMSxj+6K0zykyjPd2aAWcKMsQB5k0CJLxF+oGc+gqbV5rhl4w4lxaLWI83zPgDJ3RabG2kM8+CkTH+9IaGhyW0USD6VdquCPCn8KG1O47Q8E4bkKVdvU5PyFNpyYd9/SC5BW0hIHmfCP5+NNH1PK8V7W8TudLC7MSlgG7vn8+dNGmW11PcnWoklJ/aY8/jTbWgvFcqneSaVUdBzpEtaKsrdLppWkJOlsDf0pMm2iltDEPBGo9hUZ5PUAZ7OQkf2Z+80ZeS1qgBYIR1LDJ+VS1Z12dIlatZhLDJ9HRbaJR45MAFvj+AxXgyY77ZmNmcOn7visAt4EVEXxKSS0isQCWPLqdh5VJjUab4cUcSlS5aSAOrvCNUJPKaI7ge4H5V0x4uHd57y8+cZr3uPcDTRJ9aRR8aHGSX4hbryYt7Cq1FJIfiifsRk+5o1GKSX4nM31VVfhRqMUEtdzvzkPwo1FIgLNKh3dhn1o1xhAnmXlIw+NDjBi310vKd/nROFfwavFr1f+eT70Z9qv4MXjl6vN1PutD2oP/T9yuMojfDFGfagxe0jD61uPgaqez/Jy9pYj9aBh7GjPs2/JqdorQ41CQfCm09qx68dsW/5pX3FNs8LHJxSyflcp8Tihxn8GpdwO3hmQ+zCjPf8Gq6nkwPxqgtVBOaDs0EZ3FVE5oOoOzQTmg4cz70HUHUEUEH6woOqiPKgmggnxCg7NF04OwOzEfGho1bmZeUjfOhqErfzgnxA+4obl9m+k3HnH/pP51nb1iFxP/8AL+X8abBC4uBzMX8/GooxPN5xD40B97Lj68Of59aH/wApEsx5NCfjQ/8AlIe4PWH7aClxjhU3GeHPZtci3LEESR529xncUVn9lLS+s0uonkRkLKwQ5bScYOM8s4G1ZhrlErnaaOZ+zt6GWLT3edtjzFJZ7w+T30ueMW0qqXEloFyN99QNYjTc1mWnbQyvcLczHSVUqqe+OfyrNtNVrMLhfestyYf1gAOw2rTnNgAaFbbFNNROwBGKqQMljgAc6zprej5OEXYsJriSPu0RM4bYn4VeMnKGjwPhdr+i7eV07xmUnxbgHJ6V0rWHO1pb8QAUADArTmbmggsFGSQAOpoiu95EM6cv7DagrNfOz6FOM9FGo1F0Jbe5l8Xd6R+/K1DRU8lhaIXvb8EdQpwPnRNw8jB2psuH8Y4r9Ete9SSVHQgA48AHXA5ihuSbjtzxK7hZ41SCMZ2J1H7MD76aNMWfit7fjeSaYHoNlpo0oXj3FvEXdQh5gczVaWP0bqGZZmb06VkZvF4Ut40VBgalP20hV6w/4GH/AACkgrw/7s9SEU+FHab/AB/gK1Iv53qMvSpn/ZyH/wCm33mg8VcNsMVdS3HgVmZZ1S3j1MFzIwHIAfwFcbxBjr3XY7g2ff3sezNoEQI/ZXNeesTMu2SIeRm4ndvJqD6MHbT0r38YiHl4QqyXEsrFnkJJOTVa0XkmjWnUR1BFBIxkZ5UGjcWwki1LzA2rKs2tDqI6gJELtge9SQydAmjAxtSFJqo6g6gmgignJHU0BiWRfquw9jRNRP2NS+u0+rcSD/NROEfg1eMX6/8AvDH33ptPaqdHx+/BALq3+Wm2faqsrx66zkxKfhj8anJPZ/k1e0Ev7dsPg1a5M+z/ACNe0sGcNC49jmrtPasdH2hs2/fHwptn2rHLxuxb/nY9waqe3b8GJxOzbOLhBv1OKM8bfg5bqBvqzRn2YUDBIp5MD8aMuzRXZ3FB2aDs0EZ3qjifEKDqK6gigjzoPsotpuX0o5/wCue5evUJFtN1um/0im5NQIW0ur/im9tIqLqB/R5f7c/BRQ1Dvo8n/wAQc/4V/KhpItpf/iD/AKV/KhpxtpP7cn/Kv5UNQgwSf259tA/KhqDLS1kFyxE7jKb4VcHf2qwQnjdoZeCXimZz+qY42HLekkvmi6TbWxwCdI6elcXohYXJA6VloWBnajM7WYYpZpMRRs58lFac5207fs+773MmkZzpXc/OtaSJiGvbWVtaqBFEAQPrHc/OtRDNpmS+LDVwm6H/AMo0laKHAnH6DtiSAAGG/wDiNILL4vI0Xw5c+lNoW19I50xgA+SjUabEfRrh/HMViX96Vt/lQ0rXF9wmyB+kXRnYfsrsKG2Vc9tI4RixtVQD9o/nRN2eb4n2r4nd3kEX0krHJq1aeew6H8qGmLPxC7ms/piKi6uRkJkc745nlV0sQptZT3bap7hsnfwnH3U21paMHc2Eqgk+A8z6URrcL/8A2TCcdCPtNGWdx/8Aqf8AKaLC+VYxg6TjFZJYfHvqp7r99IVc4XGZbGEDH1RSQ/iFr3dhI2ScAffQhmcGjaUzhRk6x91CWnFDruViY6ctjPlUkrG3pmTu+BQx6WGEYYbn9Y0gtGngZztWvuseGjwCQd1PCAFDkd4/MsvML6DYk+wrjnbx+QcW8MzID4QgOPcZrWHwWePkXb411jy5kkVVRQdRHUEUE0GpdP3SQnkGyD9lZlYUrqEI2pfqmkEq9aRNBKsVbIqSHXX7HtSBXqq6g6iJoIoCA2oOI2oBoOoGwrlifKgaWNc1CxNaCDzNaQScjQFUEGgDJHWqGI8o+qxHsaHFYS4uxynZf8xqcmeFfwNuJ30LDFyW+2tRZn2q/g+343eMTrkXAHMrTkns1Pg4/O7lWSP05705M+yfJx4wY7yDOf3WpW8J7CF7RW5I1RutamYZ9qx68esm5uw91qp7dvwcvFbJuVwvxozxtH2MW9tm+rMh/wAwoDWaNs6XB386I+mcY7a2/BeKmwntHZsAhw4AINZ1D01mbPSuSYiVJGcYIrO4amJgoCTH9c59cD8qAh3n9s3yH5UHfrc/1zf6V/KgnVKBvMx+A/Kg7MhH9af9IoR3Yd/cC0Ny7uzmBQ7bbkH396cWbWrDT7O3KzRw3SBmWeJyBgDGCNvvqRDVZ20uNyOnCLgpsWQqfYiky1p8wi/4OLbkSPtrjL0Vadnw26ugDHGQv7zbCkVJs24OBQh+8uHMjHcgbCukVcZu1I4o4UCRoqgdAK0zsRNEJku4Y/2tRHQUGffXjzWU6IuxjbOBk8qxLVNsfgwL8MjaS4ijiVnxqbJ5+VIatpal43we3BHfNduOinw/ZWnPlCjddsZQClrEkC+29DvLHuL/AInfxmRpJWQrnVnSMfjV0aVOC2snEuHRTtJoXGMAZJxtzpo2bYWcUnEryKUd4ItOnUc9W/IUWZVO0USw8S4foUAHUMAelCJZFuQeCqvkzD/zGo0txLsKDpyPoso/uH7qEL3CMHg8Xu3/AFGjNmdx7PcA52w1Qhsxt3loB5rQl57tJGI7e3YNkuMkeXiqwsGcMv4LWzjWRiGA5AUkkV/xqC4tXgSN8uMAsQOtIKwyLW/lsmfufrMdzpzSWphEvE7t5NZlZWJzkEL91E4vecOkaTsnaO5JYxtkk5/aajLxU550mO7TW7O6SkxI1FV8Q9+X3Vxyw6VL464bilyP3Tp5+QxW6s2eQceH410ZgkiqBxQRig7FBGKDsUGjxL+og+P4VIQu1ZbiFrd+f7JrMtKjoY3KsNwa0iAKDgN6SLN6uDH/AIaQKtUdQdigkAdaCcL+8flUEHblvQdmqBoOoLFtgRyMegoC1J+8KwIJXzFBW61tBKQKKLUKggkEc6AKuw9YzgH06VnYbG2Gwqhj0JrMhV0W73xjBx0rVRNumtXAG5G1LeQdrbsx1g4ANLW7Cb9slazRVOurKahp2/lQ0kK/kabNCVZv2Q/wzTZp9P8A6To3j7S28qkgPAN/YmrZzxx/yPp9pKJ+GwSggh4lbb2rMOl4+sfSqw7ODQQXAONzn0oOEgJweflQA13bwqO+mSPO3ibGakm+7D404mj4hGmDrts7c9mH5VqrnljstdkXb9FWAOx1Sr9hP4Ut5bxb00u1l7NY8JDJbtMsriNiD9TPI1m/hrvtg8DsbY8PhmaMM5LE6t8HUaxV1tLeU4WujnsD3MUfNsnyFRNkG9kkOmCIsT5DNDRM5KDVeXccK+RbJ+QqHaGbNx7hUBZIUe6deZc4UfChylQue0t5eRtBEoiicFWEQ6HY71LrDz4gCR3SamIV20hjqxlR51mpLMhB+hynWwIB2BxXRXoFijHBFdUGpoVJONycCjJ1qwbgkXrbj/poin2UfHBgv7rkfdQHYtjjl+PNVP2t+dCPCh2oOL3h7f8AzCPsoU8sW3P/ALKYeUj/APVUbW4T4R7UadN/USD+6fuohdlxcWtgtuAHIJOQc8yT0ozxVLy/NyumQAAZwDtn50Igs311KoVWfHQAk/hijUQTJBcTJhozzB6Dr7mm1SLGZubAe5J/KmwY4aBuZD/lAFA0WMIG6lv8RzQGIYk+qij2FRXsLHfs1bjH7Df9RrTnZ4iZSeQJHtUiWm72bgKWUspyDI4UeoG5rjeXSrO4of8A2lecv659s+prpVl5lx4fjXRkhl3qASKCMVRGKDsUHYoL/EN4IfdvwqCijGNw6ncGgv3MQubYXUQyQPGKxDSio2NdIZNto4mWQynBHKsyp3El0vEP7lIFHFaR2KCQKCcUHYoIIoIxQdiggjagsQj/AHSc+330FfFB3xoOoiKDqKmg4AnkM02holZfI01sF37H9kb88VngoJpDKwYjfGK1oWLRoo9XehyD+7jl/OKmtp3aPC5LFLbFyrliTjFZmmzlKOJpZSoDbJg59eVWtTatBZREBmXO1ak21Et4k2Ea/Kom5T3SAfVHyobkuRQByqhC/VkPpWh98SUXKzn6OjJH9Ut19COlcJt/DrNI35PsBrt1YwRxKd1VDWq/2ZyRGvKyIl3GD8zWnMi+Tu7YujMjAjcZPXyoPP8AE717C1aeS7kCj6oMWMms2vxZtMQ8ye3Nw06mMkpjGojlXltltLMTMu4jxaLiGkm4fw5PiXYbVbZnSL1/C/wvtPFaxlXAkkWIiKXHpsD8a1TMza/8N7gHFJ+J21tLPoVhLJH4EwPq5/E16bGOZmGjxlml4c4kJbBUjPuKzbw1EREsPgt1J9DEMURYo7+3P+NSrdtrdxeRwj/fL2OLP7CnUfkK1Ln2Zlz2m4fbaVt7drh2YKGlOBv6UXapc8d4jcrp7/uk6JENIoaZqme4jt53k2nYDzIz60HWtuidobiFhrXuEYBt98mgtWS4tbtQPqyv92akooOdM9yp6n8MUhpjW7f7rMAP2TWh6KBtXZ+L/wDp1+6jKLS4iXg0KvIoPcAYJ35U2MzgHEYLHhjpMTqMzEY8th+FNhZ4utvxCe6RciVQADy29ai6UOIcRl4nPbHTnu5M+EchRrStbmT6HKoRSveNvq35+WKKuQ5Ea5oDzkYqBH0OHOWBb0LHFBIiij2VEX2FFcefL7KAWPpQRn1FBDMAOeaoEv6fOgHXvyH30SXsbA//ALtQf4W/6jRl4W4bnnNGob9vcGy4DFIuMIwKkfvEA/iflXG1XSGRen/fbjP9o33musObCZfD8a0FMu9ABWgjTQQFLMFHM0HFSDg0EYoLt+P1MPx/Cgo4oLfDbkW0+h945NiDWbQpt7YfRJzgZjcakPpSsipGvgb/ABL99WUXuNx6JoB5xA/aaQMzFUdignTVHYoOxQCRQdig7FBBG1A6P/gpvUj76BOMVBxGwNAHWqgs7AeVFRQTQSpYHwkjbpRBLufagMJ6UEomTgigcEAU+1UFbqNA5bjNA8IDtgYqosKoGkdNqyNBlAC4POgUW3oESsMUQhMlG9SB9taH6HQ2gdpAfE4wxwd65dmtym2a1iQrEQozuMEb07G5Gby2VsGUZOwABOa0yk3UBHNz/wCE35UGZxbg3C+MaTcd8uD4tCMNQ8jtXK+KuSdp2/D5h2r4LFwntELfh0ErwyIpRCjMTnngczXG8V8M9/wpWkM15L3eGiKHDIMqR9makUXjp663Xs2kUNlcFHmZcTyHY5H97b22rpEVhrlWHqeCJw6CyZbES6Neckl8nHmPaulLbXsbxSVWsJSobOkZyhHX1rVux3fNuL39/Y3Rtre7kSEyA6Ry8WM0rCTOzYbQSNaSzSO5e5MbZOARhvyFamCI0tX0EMPDZigAMd4oz5DUPzrJsoSrjnRraxA6fobh56iVAf8AViqynvoou1Mju4VTZjcnrqoAh4lbQLeoz7ySsVx1BUCohE+1/KD10/jRph226SJ55FVT0e8MKxA+BQFA1YGPtocUi2k0gPNjboP5H2VNqCOyiiO5Y5OeePupsEYLcHIjXPnjNA+2iW4uorfUIxI4XUeS5OM0G3/slwoW4+icTedZGZmkVVKk+QwfxpyHnL8RWXEZ7NCzCBgNRGM5UH8aBCyA0HFh5CgEtttt7UUvVvQQWxVA6tqCGOwNEDnNBGd6EvZ8P37L23npf/qNGXhLjOTSWpX7MtNYzWbZ1RguM/vD+A+2uctQrESStI5BLZJbatssojb41oLZd6KArQQVohbHQwbyNBo3fDSlu12JAQWzo6gZrFbNSzgpLAAZJ6CujML09vNcRxiKJnwTnSM4rO4XSDwPiYOPoE5I6CMmm4NAfhHEQPFYXI/8Jvyq9p8GpatlDPe8PayuYZElTeNnQiuc7g1LJFvLEJFdGUiVAQR6mtSL/aKPTdQD/wCQPvNIGPprQ7TQdpqo4ig7TQRpoO07UEaaCGGw96Bsf/AyerCgQedQS31KBY51RNB1B1BIFAyJc5oHCMUQIVu8ODgUBsrhD4zy8qBturaVzy07fM0FpVxRDBnI360FgM4Ub7URwBZsUCJthRYLh30r+9IB9taZs+jJ2l4iNg0Yz6V5+c/hrj/KG7W3kLtG88KOACVOxrW5/Bx/kte0F1fyhIbpGceLCMTjFTdiIhzdrrmNmR72FWT6wIGRTdiYgP8AtlOQMcQt9zgbCnK0JqHnu0HHLy74na3CypI0S5WSPYjeuF+7OoKv+JH9IPcwzKski4lIOCfPPrXSkzpewbS34hxG4ZVkjL5A72R8bcufyrnqJk4w9VwG8vLDs/dr9JUSrNHp21A5DA/cK61iIYpGw3faPiFrDmYfSIpsrsoUjGPX1rUzMvRw085xbiiX8wkit5VPhLaiOYPvV5aPamQT9oe6ijXu3UpOJQDj41r5M8JqiTtF9KhliEb/AK2VZPbGPyq6Z4r0d2JN8YrOmkAzmPuhKBGDkD45omkC3UyanlYnGNqNaNEUCfs59zUR6CPhcd40ty0kyMMABYsrt/eyN9+VBEnZ2xgZmWO5jbQSVkkUnO/kMU5K8tBeGVUOMahmge0nrQLdvq0EZ2oH8LfHFbQ+U6f9QrQ+icTQLKpHXP3CsyPmXaDw9pb71KH/AMi0gU0b1rSi1UEFqIDVQQWoIzQQTkUEZoOzvVJez4bv2Yt/RX/6jWWZeUisHuHd4wJYwDqUc8dfiOfriuOTJxWPDR4ZD3DvM5D6kQZHUhvxAFeSc25Zpbuq3BFrYPHpPfXMpZtvqqDsPjua7x3ltgvEVBHlz25V7PECuwGaKE4oFkigVIciirvFZcJCAqnWgcsBvkgVzo1dW4c+riNupGxkH311lzq3OGMpukj7xwWY4UKMNt1PSudle7SLYDQMe1c4aS0Cb+EfKgDuB5UA9wqjAyABgAHAFEJktomicbcjQfO7yzms5BBcRGORVBIJ553B+Vd2VfTtQdpqqjFB2mojitB2mqIZaAHGFHvQEn/AN/jFAjrUEn6tAA5mqJoOFB1ASjPKgfbLlT70DwtEAi5c/H76A5f6p29MUQ23GVXbkoFBZA2oDUb8qB37Iojk+uDQV5zuaQsIsRqubdfOSukM2aU0jK6qLdly4Usw23NeTT0VjstdrbbuuLW7jbvrUH766Vlx13L7BlG7RiE5y8be3nTTpHaCO0FiF7R8QRUclWLMARsDj86bZpXcqdrwW5vMCO3mxtuUJPyAzSN6btGmlcdluIi3jmggcLEPF36advYDP2VziszLjuq/adlbeW3S6ku7b9aoJLSgFvgQMVrhb8Jyqv2/AbRI9LcUtE6YUltv9Yp7X8M8qtG2WOKOayl4nbfRyF0skY1Eg53y23z3rcY1raFe84HwG8iZZOJMHPJ0Cgg+f1q1xanJDCXgNrNgWck13pJSTvZUhcEeQDMCMdautrGSEN2PilDNcCa37pS/6ydSMdd+X21nWickMeQcHh8MPEJSR/cJ/Gnc5bJa7hXAi4g4IO4KEZ++nYWrbiaIPFK822CRuB8wKaVoxXEcy6o3DD0NZ0GZzQe24bJnglwPJlb/AMooLN+wecMP2lFYlXzC1yqxr5DFagXGagF2BxQcTtVDLJ9N5C3lIp+2g+jcQkwkfMkZ2rEj5t2jYHtHcb4LJGf/ACitQKRwK0qSRn6woAYr+99lABZR+0flQCZE9fnVRHeL5H50DQQY86R8zQKMm+NIFBwkPkv+kUhXtuH7dmbYljgoxwAP3jWbMvPQxXMMqy2d4hc84yWBHlzGPtry3mLR9R2WovpV5d6TEsEigNINYCMcgZB9fKvJXFG/pZ4yp8YkU8UkwAukALk/V2z+Ne2ldNsiRY+7bVONuSqp/HFeiBR1AtjOK0HR2RmGRPEo82yPwoLElpw+OyVQ5muiT3jFsKo6Bcc/jWdyrNezYk4Yac8zWtyLE9jFNappul79SEVGUgMN988vIVmOy7U+HxsnFoEdSrLKAQeYOa1PdnTe4MsZ4hDqD69R04Ixy3zXOzb1vEbuW1TUspQAny32/jXOB5yftPxNGOiQaf7yCtRGw2z7TX07aWkQN0ygxSY0PQWM0t9YM7qrPqxgjb2qbSYMEMqQ3DyqoLKT4eXI02RD5mdzk5Pxruie71HIBxQCVoB00HafWg7TQRpPnVRxHrQLmGFHvRXA6eHnHWQfcaCtqqIPSxTVpOnlnG1AC9aoLFB2KDsb0BKKCzag92fegeBRARrqLHJHsfU0ASjEKjLZOM70F2EaV+A+4UDaIYg3FAZoIBAOaCvcHnSDcrHDo4TPC3ekMuW0lfxrpDnaZekn4RxG4s0kh4fPJk7CNGYggjoBXl5Q9UVjm2+03CtVlYPPwy8eUwlA6JgRnpqyPOuldS45N+4zOEcMt+CXVvxSV4IzEpDkSBskg7DHXcVrhKTaGiPonEb+TiEcEMUkgA7yVNTkex2FNQzGST7147e0eeW+dlQctRA+QqsPLfpKCaVjJMhP95ulGhC6tf2ZIAfcUUX0qPBKOraeekg4oIjmLZwd8mg4yNgny50CjIDnbnRZ7mGRrmxkt2YtlWQA+oyPurThbtLwUgZZCDzBrD1QZDbu5DfVHnRpeW6FqmEOMUZ1JUfEJ1uxMvTbHmPWhqXprWdbuASx535jyNZ1o7tqbtE/CLOWAWplMkCvkPjkAPKs62d1GftzeS3SxQ8PQlF21SHfG/QU0upYlk004MjBVAc7CtaNSu6Zm+pGzey5oakxbK/dci1m+EZoal30DiZ5WV0f/Cb8qLqU/onivP8AR13/APhb8qGpZl7w3jc99/vNtxBo8jGqN8Db2qdk4qFpw+6jnPfW8q/40IrXZri02XT0xUNAOaGi2YjpRktmxQLL71RAbfnQWo2zEKqFOfFQQDvWZHrOFWazdn43Z38Wry88VzlplzWCWtgst1PKHl5QJudPTI9efTG3POK5WtEwunXlqsckFvZK2VUGQk7mQ7DPsTivNTe2ooPicC3Ulxefq8qqNpWTxMmy6sY88V64kmrzl7hW8OcEZGa7RLnKlar3l2FJOP41tG21vGuQNQGDsDsfetcRTkjCsQKaVGBjn8N6aCLoD6M22/vWZDnyePWsjfXkSJ39yoJ/OstNTgJjPEYVMZLksVbVsNt9utYsupej7QRM7RoATuxwPYelc2tSyLfs/LxEsEYRhRnL9acl1P4VJeD3FheGNl1lf2k3BrXI4T+HquBaFsmWVgpLk4Y48q5zpONo+y9ePClhcBZEBETYGoeRrVdHG0/Z8fMdz1ST5Gu/KD2sn4kQS4H7Mn205Qe1k/EiUShhlX+Rq7hn2rfhbtrVrgEg6cHG4rPODhaPs9LD2A4hcQpNDdWzxuAVJLDb5U5wnZzf0fcXH/Mtj/nP5U5wdiX7CcaHJIW9pR+NOUGin7E8dXP+6KfaVPzpyg0zrrs3xZcqbQ5TJOGU9PenODTKKkWIGP2/zrW4Z0r6TVDBLIsRhEjCMkMUzsT54qBa9aoKg6gigNRsTRFu2QrCpII1bj1oG5A5mgQsgCty3HWg6UL3SqCCRvzoL0Y50DAKkIsohGPatQBYEHlQAdqCtOdjQP4dtOSf2YWrTL08Xa/jCYlu+L3Mrk+CKMgAn3/n41zjHWGbSocW49cPiTitw9xI26W2rwj1ateGYhkS8RICXdzIXf8AYiTwog8gKbXQl7VSqMBHPoZNvlimzhYF7xia6tEBOjW2SoPSiVppitcyl2bVjJzR20A3Eh5saKs2PEZbUuq4KyDBzQXl7QSRkqYlI98VE1L0/ZxOF8VgM/EuO2fC0zgJIS0jeuNhj40NQ9LFZf0eRf1/appT/cGkf9Jp3ItEfYNxxX+jPhciotxe3gcZLwksAQeRzindmdT9lWPjP9EiP3h4NfSSE5y6Ofs14qaa5z+GjF23/owg/q+z7e5sVb7zTRzn8LkP9JX9HkZxHwp4j6WCj7jTRzldT+lPsOuyiVPQWp/CmjnLbh7Ydk5oFmbidlBqGdE0qq6+65yKml5lv2u7Ggknjlh/lkBpo5kP2u7Bk5fiti/vv+FTRzTF2x7Bxf1XEuHp/hXH4U0c1hO3XY87Jxuz/wBeKqcjP9teyfP9N2Pv3wochDtj2Ublxvh/xuF/Oou4NXtP2Yflxrhp/wDuk/OhuDo+McBm2j4lYv8A4bhD+NOxyhYR7Cb+rmifP7rg07HKEvYWso8USOPVc0a5Kc3Z3g8g/XcMtWz+9Cv5UOTF4j2Q7IOp7+3tLf8AvLN3ePtAptOUPE8c7FdnVR24X2lso5ByinuoyD6ZByPtps5Q+fTxtBM8TFSyEglGDD4EbGtAAaCzE3gqoByc8qCAajT3PBDp7MQtkbM/P/Ea42VU7i74leJeXKhYYRpIJxg8yTnlzxn0FebJxh1rWZQjCKctGF1BWaMhdy3RiSeZJ6eftXGO7p2qC1UXNrc61BlNvIhOc5I8Y3O/OvVSY05W+ru8tdwTTOFiiZ87DSMkmu1JjbMVtk7NDh3YzjAlE08K26427wkn5KCR8a1OSsO8dJkt92lLwiO3JWVrgt5LEAp+JbP2Vmeo09mP0y1vMq7cOtyci0kJ/v3Ax8go++uc9RL119JpHmXDh/7tnbL8XP3tWfeu9FfTcEeYE1lKUKmK1x/9FT+FY9y35dY6LBH2Lbhs7Sd5qTXt4goGKzyt+XaMGKPsP6Fcd2F1jI/aBwacpX2cX9KDw24bdp8+5z+NZ7te3i/pD+iZf7UCqvGn9Kf0O5/5y/z8aH0f0uHB2U7zKPf/AL0T6PwZLwGfwySsQHHhYrz+2jPKn4B+gzn+t+z+NF5V/Dv0H/8AMP8Apocq/h36CPSRv9NVndPwj9BP0kP+ihun4SODXC/VmYewNXcszXFPmpi8P4kn1LyZfZ2FNy5zjwT+01YeOLuvErj/APK9XlLn+n6f70WYIe1EpxDdzS+gBb8K1HNxt0/R/eD14P2r1hpCE3z+tCx/9QFa1dwnF0P2Z83YK9nLM8lrEGYsQs4bBP8AhzWom8Oc4OlnxyVm/o7kHPitoh8mMn/6KvOfu5/pMU+In/SvJ2CePOOL2Lexk/8A01qMsfdn9BM+In/SjL2Ov4iQksEo81YjPzAp71V/8bknxKnN2d4nBubZmH90hvurUZKy439PzV+yrNw+8tolmltpY0b6rshAPxrcPFbHavlWIwa1DMLMAgUhzcSIwHMR8tvfzxUFpSDBCF6IKBb/AFvYZohaAGNthy/CgKbGldhv/Cgsd5pUHzNBYTpUgWu8RN2YKMdTWoZVJb62G3eg+29Ag8Qh5DUfYUCnnEnKN/lQMhmZCc24bIx4iNqCzeRvwS5eJ7qK7ukAUNE2pIx8RuaE8WXcXRuCHkOJDgEgYz6n1oQs38Vq7ItrfK8YXcvsc+2KaZiVWOGND4rlOXTJ/CmmplYvGtn7pYGk0qmG1efp6U0m1TuoORkkP+T+NNNbRotht+tPxFNG0hYAfCknxYflTRsTLGAXKn03onJucF7QXPZuwcxRW7PO+SJraN25dCynAonK32Wz/SNxhjmMWUajdsWMO/8A5KLET+VHh3EpDf3HF7uO1kkmJz30CFBk9FIwPhRi0z+SpuJwpJLNbyIhbOEUbDPQA8qzpitbKi347sa5Vc88YO3pTTU1Q1yjqcx4PnUWIMtXjtFN8wDSRnESnq/mfQc/lWnSJHYtY6Xn4g8ksrMSFGfmTRmYn9qbie077XbRoIwANLoM+tZIif3GgW8y6kgVATsDU2kyuxfQhYPK9vESgOcqM1rbnuzKtoxMHzsFUn3NId5Ns41ddTDIHIHzrUEBkCm+RQAB1GKG1zuoTt3S/KhtBt4CP6pflQ2AW8SnKAofNTQ20eG3RciObdo9sdPcCkRtzt2a1xFBNbkOm2NyBvjnz6cunnWbRop3ePvrwWt1JCOHWsbIcZ8T5HQ7nGDz5Vjj/LvtRPEJWbdYgM/swoPwrXH+TbW4Zeh2CyBSvXCKPjyrnarpSWnpZmwIdsnqo/Guf1PRqp9uEjkVpIVDAg4ZQR8abn8szELD3ondT3EWVyAVt0A5+1Nz+SIgoy6lMndKJMknTFjAzzGOVNycYXLXiH0YhU1MqjJVw2FH+Gs7k4wluOGRmhyAD+2sZGo5z0HLlWOKxoiSd+7Dq0hdcZ/Vnw+laipOj7ZGRRMx06/2CmGPwNdYxy52yRHiG7w/g/FLyFTaWkNrCRnvLhAdXrggmukY4ZjqLx4lqW3ZW6Dg3nEIXXqsVogJ+JB+6te3X8L+ry/1SvN2f4f+4f8AQn/6ae3X8H63P/VJbdnbA/sn/RH/APpqe3X8NR1/Uf1Ev2asTyHzRfwAp7dfw6R6j1EfuJfsvaNyKD/wz+dT2qtR6p1H5JfsnAeTJ/pYf+qs+1V0j1bqPyQ/ZT93uv8AWw/OnsVdI9XzffRLdk5OjJ8JD+IrPsVdI9Zyf0wU/Za4XkHPtIv8Kn6eHSPWZ+9Vd+zV6vJXHxU/caz+nl2j1mn3qS/BL1eccrf+Ex+6p+nluvq2D71kQt+JRW/0eQAwA5CTIw0+xxkU9m7X/kOkt52rG1k1YWeH2DN+VY9mztHqHSz9xiyvCuzQkes6L95FPbs1HWdLP3QLC/c4jiV88tEqt9xrPt2dI6jp/wCow8I4yBn9HXJHmIiae3Zr3un/AKl2w7PXk7YuZVgPWNVMkg91Gw+JFbjHM+XkydXSvw7/ANp7NmHs6I/6vhctyf3rm6RB/pQ5+2ukY4h4rdVNp724/wBoXY+DcawPo9pwuy9QoZv9WCavCfy5TmwfutaywvZjilx/x/HHx+5GpYfbj7q1wn8uc9Vir8MYz2Z4NZDN3eTf+NcBAfurXt1cp6+8+NGw2HZ8EdxYfST6RySj5nIrXGrjPV5Z/dJsq8Ngj1foa2THSVIVJ9hknNXUOM9Rkn7/AOzoFhkYiHhdjgAEEJnIPI7Idvj0pqGfeyfn/Z5sC/8A/C7P/wDAp+/FNQnu3/qkuXhSSAh+E2bZ8oEH26qahqM+WPFpZt9wOytrWSaXh3cRYzIY31rjPMocggc6VrDU9Re0fW8V2r7L28Eq3kPDbR7bSA0lvHgZPIlRy+6s32YdWns+e8ftra3nQ28XdBo8ld8ZzjIzVrZMleKvbjEKe1dHnju6QN4yCANPUVGilLbjz2+r60Eu7HTqAON/q4p3RdR4P0eWltS8oYFWUkDHXO9Z7tcqqEl/K7YRAg8lz99XTO4l0dtcXRy76VPUnNNs7iFheEa8CNmcnzGAKcmea2OCJGmTKC/tTkc1SSExMBkHcgjO4q7dK6D4+n31nbp2Z4Ikk/WORnmcZrcuUQEleWKQTCQfSqDBwMmghpc8hj41NpoboywRy61YPnYc13602aK1GqujEd+SplumBmhpa4fZvfS+NgETzOM1JlJjX2WrzhjswCSM4XlhSazsif4JThE5gt5lw0U07ReE+LUoBO3x29jVWZ7B41MTOLZbfuFhGCmN8+tHPHHdmYPlR1cBQWIYZHkjQYGvcZPTz+yiHzN9JmVYh+pj8Meds+vuaCHglj+sjL8KjSYU1Pk8hzoL/e5UKEz7b1ly4q0szFO5Q5VjkjzrTXFqPZ/RLFW21aSWPrSGIttUsyBDz8vurUOkdioD3l6zfuigu5oJ1VAGrNBKP3Uyy9Bs3tVJ7vQ2soli0k70cZ7PP9pOEtGgvImLIDodf3Oo+FYeje3mjzrRrR1vK0bjBxjlUmCJelsriK5iDMJHkAxgMdvtrjMPRSViYguVVXBXoWOfvqab24TKraIxJsfXamjZouB3WdThsEYPlTSbEoG+mWTQw8RwMD7KaNm2cHfvlJO8dDjTtypEJuIbvDOC3d1L3dvEZHJyzclX1/n+FdIo43vMvZcM7MW/D8TTAT3H7xHhX2FdOzh3ahVyeVOx3AUcdKu07gKt5U2dwEHyomyWY+VDYNRqgg5AxijLtXpUa2jNDaCRRNgbFGtgODTRyCcA00cgsAwwwyPWml5yrSWdq27W0J9TGDTRzlVm4bYv9a3X4Er91Goyfypvwi1Q5gMkJ8423+3NZ4ukZrfkorxG2ObfiVwN84kbVn57fZUmkS6x1Vo8r3Cu0V6l2ttxCMEMQqyoMbnln389vaucxMPZTJXJHd7VLjurYynxHko8zW3myzpC8MurzD3VxJpb/lo5RR8Bu3xNaeXe1q24NaWx1Rxxo3Uxxhc/Hc/bRNrotbf9qMP/AI/F99DaLm3jltJIO7Uo6kFOQPpQecsriawnkicgSW58Wr9pDuflsfQHFB6nUCMg0QtmyaADuMHcHmDVNsVbSGxn+gzDNhckrbsd+5Y80/wnp8qG3zH+krsLc8PJ4hZx67ZVw6KPqDJOR6b/AAqRDvM83h400xID5CtTDjMcSZGLl0jUsxGwUZNSFiTLGxvL3MiII4U3aRzhRvn+cVmbM2kcktlZ7Z+kyjYZ+qKd5Y7yT3k9+pZzhAcADYU7QdoXbThTS5McRfG+w5VNrMtazi4ZbZN1Ie9H7Eo0j7edNOczZTvryzZmmtoyrdHXZT8OtVqKWnyzHu5pSS8h+G1HaKVjyWiySE92u3meVNLNogGXDFTzFNEWiWdhuoxW2RKgJyXA96AtC/2i/b+VEQ2OWtT86C/wvh9nd3J+m3y29vGAXYAljvjAGOdXR2erV+xtvEsFnD375GDMskmo+232YppnlL1tpwfha26vLwzhVsSM4ltUZvlvj4mpo5T+UXdrwh7R4G4hFAG2LwwIGA8gSdhjb0ppnlH5UhZ9mLe3jit+IzJ3K4QC50g753086aPchS4rxDhtrbC14TPNcXlwCqySXzERn97H3U0coYUdndcLu0X6HYzIqrKc3DEE488g5IOaaWZgciQ8TkNzNwzhkbHYLJdTZA+DU0zF4cnCbR20rwzhTnyW5nJ/6qab5Sf/ALOppyeBcPX1NxcAfa1NHKWJx5IbNks4bG2gnYbtBLI/hPTxE8/SoclaNbaK3NvJIEkznfofegs2lwsqaGI1Lsd85rJ3WhFasnig38xtQ7ltbRYPdGSMkEZB6HnQ7q6cKnEnexsjBd99qNcoX7q4B4Y6vGVYr1IIqw4zSdvOSzSW0wKbZQDfrWod9TpMN2YBkjUW51EXUv4HkCBumcnYUDknRwxVgQpwcUVIbIBFB6PgVrwibhtw94ytNjYEjI58h8t6xee7pWI0o8PlKIu+RyGeoH8K6RPZ5bRG2jdQC8spYgM95GVI5+x9wd6loWk6eIm4RerIwEOdJwSCMfwrMS9GuRRsnH15Ik/zg/dmnI1/K5ZOLdgRMWI/dU/jis2aia/lsoy/18YkYFdt+vrvWHQzdsSCMhufP+NGh928mcnl00VDUy0eHcKmkQd7iOLGwxufyH871pmZir3HBuyzyosk6fRoDuFx43+HQe+9biHC1nqoooLSEQ28aog6Dr7+da05uLE86CCaAC3pUEE0C3YVVL8J5gUZcNKnIUZ9RQDpXJ250HaE/dq7TUAMaeVNmoAVT1ps0W0QJ2NNmgiEZPiFXacQvbsN8g02cSmicdKJ3LkV+ood1cqSTmqmymQ9RQ2UVG+eVGokmKNZbmBMDLSA5/w+L8q52ezp4e0t1EtzbQndVGth7fxNSEz27tvVWpcfsnVREh6CdVFef7Rr9Dnt+JqPCrCOX/CTsT6ZJHuR5UGhwuYNEbct/VY0+qH6v4j/AC0RePpQAdqqEXFvHeW728q5Rxg+Y8iPUc6Bdp/vsEvDOIYa4h2LEf1inkw9+R+IotZ4vkvbzsLJwe4ku7QAWpGSoBOk+nofso7a5PDwRm21M8bTI27RLqGfc43HpUs427p4hxK5uoBGkbwxL9WJQQq1mKsxXTLSByctsK6NTLUtSsdqSvIHGSu2azLnMPQWPFuGWlmim8XPXwkHPyqac+MsnjN4nFLuNIsNDHvr/eJpp2rtSmjkY4xpVeWamnWJgv6NI2/h0j151qIZteGhw7hvEOKyR2tnCc48THYAeZrTl2h7vhHYfh/DlWS7UXlx1LjwD2HX40OUy+dcZ4OOGyzRd5G6o5EbI2pZU6MPnWYb2xK1DWnUHbUBK3hIHWm0aHDriKzY3IlcXag90oQFQcYySeu+2BzxTYW/GL+T693M3u5q7lnhVwW5mgMpWd8nZgpKnz3rO5a4VRaQXl9dJa2kUk0znCooyTWk4vX2X9HVwFEnFOL29ox5xxr3rD0O4HyJofS0puzXCU7iK4vrm8gTw6o9KOu3rqGNgKjO4/C7bdm+y0Q1Lw67uD0MsjH7FAFDc/horDZ28Wnh3C3tj5rC7DHXap2N2BxXjsnDYFaz4PF37+GFJbU5Lf3dtz+VOxuzyqcC7Q3l3PxW4RXnuzrcvZ6wPLGVIAx5VdL3OHA+0GfqRf8A/OH/AOimjlI/0N2hX+zX/wDx4H/pqag5T+RDh3aMDAnQf/ZKPwpqDlP5T+je0f8A8Sg/+2UVNQcpEOHdowf+Ki//ANZDTUHKS5+EcfngkheaNkkUggWyDI9xyrWoTcs2LsneXMEcgiBVlDDER/OmoNycOxlz+1bZ/wDBb86djlYX+xs2f+DY/wDgtTscrGL2PuFGFsZsHnphbenY3YY7J3K8rO4A8u6anY3Z3+ydxq1fQp//AMTU7LuT/wDZq+UALZzgruMQtTszMSdbWrG5W2c9xJnBVxgr1rGTJFClJsqdpuCvcFEjRJLhOTgnJGOVZpki7VqTV4WRGViGyCOY8q6sw6LQkis66lzuMnejpDcuI7q2VL2ThwsrS4bSixucDAHmSeRB355rnMOtbLluiTlRCkratgBk5+2ucw7PUcF7OzXEwAXvZCOW2lB6nr93vViGb2iHveF8AteHYmlxNcc9bDZT6fnXSIeebTLReXNVkotmgHNBBNAOfOooWO1ApmyaqBzQSASM0HaTQCWxzogC9AJBxmgAmgjNAatkFaACaBbHnQKbHlVNQU6qaM6hUuV7uFmzzOBWtpFe5fCEEvEWc8okC/Fjk/LA+dcbS+rgr2et4US880x2wAg9OpFah4slu7U1nzrTE+BBvWoDBBoDG9BV4lbRXthNaynwyoVJ8vX4c/hQed4HeutvC8u0kDm3nHlvj7Gx9tE09OJc0NuLZqg026UFXiMEvgvrZf8AeLbcKP8AmJ+0v5etRZ7nTwWvHOGgMFdJFypIzz6H8RRqtuL4P237JzdnL55YYybVzjdj4D5H08qjtasX7vHSF2OUXR/hJrTj2ns5fpABHiJ8yTVTsNBcDOlcZ570OxgW5MegxRkk5yVBPtQM+jXELDvbVFJHUFTQNX1iZf8ADO345qbONvwdGEZsCSZSfMq3/pFNnH+HpeBdoZOExC3WBZov2nEehs+pzvV5QxbFL0w7Qxz4+j2t1KCNyItgffOK12ZikvBcauHs+GNwziPDh3mTJFOpwY2+qRn9oED8aw3MvJRRSTzCOJdTHpRqsLP6Ivv7L/zCs8oa079FXQ5qo+NOUGjIeEzSGQmWGPu42fMjY1YHIep6Vdo6K2jitPpEzFVJwNI3amxUjCJJ3jrqRTsv73pVTT0KdsbsoqScKs3t1GBGveJgehVwaGlwdorOxtLjiHBDJb3k6dzJFMod4ASCSr4wynGNxkevOrtmYYb9qOOSE6uK3e/QTsB99NEQWeN8UlOH4lcnP70zH8akxoisSGXiN4rlTePJjqJGIpE7JxxBRvpz9aRz/mNVnhAGuXZwxZjjlk8qNagwXG2NbfOmmdJ78H9o/OmjSO8BPM0NOEi+tDSfpGORb50NLFlxP6JOJTEsw5FJRqH/AHoaay9rUhbTb8H4c0PMrPbBiT7jBHworHF/IASsjJuThScChxNj4vOmPGzD1JonFaXjsu2dWPfJocT047H+1rHwozxk9OM27f8AMI9xQ4y2+zvam24Vd980EVyCRudnT/CenPyqWjktZ4tbj3bS24vbdxFZDf8A5kzBiv8AhwNqzFNNTfbyzkTOAScPtscEHoa1NYkrMw1uGpKhWJBI7cxnLMfOkViC0TZS7U9nW+gvxWK0mhKkd9mMhTk8+WxqpFdPEttRpr8Pil4pGlhapcSspDNrk/VJ0zjpzrLpSJfSuynY8rAMEiL9udh9bzCjy/k55Vlq15q99b28FjAIoECr18yfMnrWnFDPk70CmbNBGaCNVQCW9aASfWgW7dKAAaqOG5xQN5Co0gmgQ7ZJqsgzvQS7gjagWTQCTQSr6TmgguDQLZxQKZ1NVAFlzzNBR4jIP1cedvrH+fYVfs3FeUGcEGmyMzc5mMh+PL/ygV548vqT9GF63hMbLYIxHikJc/H+GK7PmNBVJ6UQQjNAQAWghn2wKIUzZoPMSRra9orm2cfqb+LWMHqPCwHw3/zVVbvDp2mtlMhzIuVf/EDg/aKMtBBUUerFASHfJNFU7dhw/iTW3KC5zJF5K37S/jQBx7g8PGLB4mjDvpwAf2h5fl/3pLpS/Hs+OXXZ1bO7kja3gWJDtJK+nb1B6ikF6fdSuV4EuEebvHHS2QDH+Y7VXLiTJxW0gYJb8Pi/xyMZD8hgUa4g/SnELkmO374MeSwIE/6RmqupQvZ/i1w5eSEqWOSZXAI9871mWtwv2vZeBcNe3uT1SBc/aavFOctSKw4NbqFisAxH7crsx+zArXFmbycncxnMNvGp81UZ+dXTPIwyzHc5ppl4e+4Jxu+Ci5vDMFJxrYmuPJ04xCvD2Z4lbvrjlRWxjIFOTXYxuBcYPO6z9lOUGim7O8XP/Oz/AJjTlBos9m+LjcnP+anI4tb9CLddnbHiILvFaz6LyOMZdFzucfzzrTLE4rHZvxZk4Sk/0RiBb9+AHbPU42qbNC/RoQ6XuGEn72PCDTbWlG5ikhkkikXRIp0uKsMq6RPI4RRljWoGnEOD2YP0kzXsvVYmCRj4nc/CqzO5NROB3/6uPvbOUnw94wK+2aMzzhnX1hPYTd3MBuMgg7EVFrbavmjU9nb+dUdQGoHM5qBokQdKAhLF1UfKiDVoSM93QETETsgFQRGIyoyooJZ7ZTjRk+hqgO9h/sz86AtUZGeQqC3w3htzxa5FvZQNKx5kcgPM0HoYey/D4D3dz2l4bDN1T6ShAPuDt8au4NDveyXE7S3F1byx3NsdlmikWRD/AJlJFNwzphi8mt2IcEMvl0pJD6D2Thm+j2nGYeIxC5Kt+rlQ6QDlcHz2rMukM7i9tHCuh+ILLgMHQykgAjBAOPX7K8sYNW3t746us042r3eP4J2fueNT5Ud3bqfHMRt7DzNeuZ4x5eKtNW5W8Pr3Znsfa2lshaHu7cbhD9aU/vMf5+ArBa71xKxoFQBQBgADAArTnshnJ3oFMxNAOaCCaCNW9QCT60EE1Qktk0EE0Bp50B6h55qASwoEMN6qBzQCTQCTQCWoBLUAlqoWzUCWfeoB1FmA86DI4hMZ5ZBGd5GESHyycA0t4enBT6m3AuIY4YhjVhEHl0A+Wa408vZ1U8avaxoscaxqfCihR7Cur5hocLQC8486qFGXNAJkxRA680GF2nRoUtOIgH/dphrP9xvCft0/Kiwu8PkEfEJIyfDKglX1I8Lf+j50JbAOetAYAoCBoqrxOE3FmdGRLEe8iI5hhy+fL40D+HXyXtnFcKRhxkjyPWhHh5jtz2dh4haPdqu2P1pA3Xyf8/TejthycZfL14FaW07RXzlHHRmZQR0Oynas8od/02WfjDVteE8MVdVunD3I/tJ2J+RH4VrlDjbBlr8oPduIwxHurSPuh/8ADOpHy2rTlNbQpLd9+CdZOkkEEYKnqCOlac5F3221ED3wA1MQABkmgxL/ALZRWrGOyjEzD9tvq/xptriz17dcXVwcQFf3THt99NnF75k9M152wFR0X7KASozy+yoBO3SgB842X51BkNHxjh9+95wl0iMv9bGT4WPnitRYZpF7d9rLB+NtHEsziMOuNKg7fYTW/Ka0udq+CzcD4rLbPCZI5mDwTMSAV8seYO1Z8Nb28rxRz9OOrdlVFPwAFaqikRlsDKg8/atSLtpw64uFDRIqIeTOcZqTMGjLzhN3axCWeENEf+YhyB7+XxpEwaTbXgaI2V2e8hYYRzzStOc1Z8sQid0bcg7EcqLAMUV2KDt6Dt/OgnegnU3nQErsOtByyMBzoB3Jzmgnc9aB1rbfSpxHrCKBqd25Io5k0khdu+MTT236PsC1tw9dioOGl9XPX25D7ay1DNEUQ2Lmqumlwfi/EuAXP0jhl0UztJE26SjyZeRFDT1XELSx7S8GHG+GRCCUNoubcHPcyY2H+FsbH0x0o5TXTDs+J3kFgtkjkETD4Vl0r3aPD+CXXHbxri6Z0skbCgHeTHPHpnmflmq6ceL6r2f7ORW0EUk0KpEgHdQAYA9SP59fU52s9E8mBzowrM+TzoFs1AGaDs0AEmgBpCOlECJd9xig5mJG1AvNBGaCQ2KCS5oBLUAk0Ak0Ak0AE0AE70Ak0AM1Ap3oEs1ULkl7qF5OoG3udqDMtl72/iXpGC59zsPsyfhXO76fRU3bb0/C1EnFIQfqxAufhy+2lHDrL7yaen74DrWnk2W1wScLQ24MebVUQ0lAOomgNaCvxa1N9wm5th9aSNlX0bGx+eKLDF4Xeiax4bfDzVG9mGn/AKtJ+FCXqY3BG3KgbrFALOScjp50CUkkKyQySDvAMo+OY6H3HX+NBicKvvoHGZ7CTPdz5ki/xA+IfjQmJbcl80MBN13axnOC+2V9sZ+yjVYl5G87Kpxjuv0YkpCsR39woCInPSuc6h5cyPjWLRyfQwZ5wRqZ1/DStv6NrARr9Nv7idxv4SFX5HNZjE3b1C+90roriXYe3toC9sSsIz3mljqA8/I459KsV4uduonL8nzDiV1HwS9uLKUFHifBAH1vX412izxZKTv6WPcdoZGbEMYUebbmrtz0q8T41JeoLeAskZ+v6+ntTbWmd3KIuXIqKW0cTjwNhvI9aD6yWZv2jXIdqbPPFAJRicljUAlSOZNADA55tUUs6h+yxoMrjVn+kLMxmNg6HUjAcjSJ0mlFu1naqPhy8PmSSZIvqO8OtlxyIOPtrpE1k0yeDX99wjjC8SNh9JkXV4biHWpJ5kg8+tXZoFy8nGO0DSy2625upsmJE0qoJ5AdBTZo3js0kcyWkOUUqGIXbboPbFZgFazXFhcxMn661KhZ4mYaZF5HYnyrUijxS3S0vp4I2JjVsxt10ncfYRSqEFGni75dyNmHUVqWSu7cc1Pyo1p2k+RoadpPlQ07FDTqDsUE1BAG1BO/lVHUFh27mxEasNdw2WAO4UcgfLJycegrKrvBOGLfTlpg3cRY1KpwXJ5Lnp6mszbTTYn7QQ2Fs68KtIWVCUZlUBQcbEDHiHqeeKmpkMMHDO0/DXnsbX6DxWFdRiAAS4A54xgZ9gPjzGtzUZXZ7in6M4iRIzLa3iGC5A8jyb3VsN8Ku04zZ6TgPA5ri6ku+I22kg4SP99s7tjyOPj7VnbpWsVfUOD8DFvGkt0igqPBCBgL6n19OlVxtZsO/OtMwru9QKOpuVB2iiOOAKASaADQA2/OgU4I5UAasHag4nO4oIBoOoIJoIzQR0oBJxQAWoFs2KBZagHXQAzYoEs+aACapKrfyfq44gcFjqP3D8aNVBwhQzvcEbOxI/wrsB9h+dee3eX3MNfbwPR8DQkTTnfU2hT6Dn/PpXbxD4uS3NsKGY0czPDH6mgEux9KqIB8zQGuTyopgIXrQQ8mVIWqPIWOpbXiVkuzQzyCMfu76k/CjT11lcLPaRSp9V1DD2IzUZMluI4YmllkVFUbs5wBQYX+18dzKI+H2NxeODg6RpUDzyfypsncrIk4pM8cl3JbcOXJ0Kv62Q+YHQ/AHpTbVayeODT3BV4BJA4bULq42k354Uefl4fao6xWseWhBwu2t5Q0yd/MTtLL4sn0HQ/zmjU218WkGx1yarKc+dBm9pb0WHZy9n1BWMRRM9Sdh99ZmdQ6YKcssPi/bqxe64bw3jQCrNMndSlmCg6fqnfqRWcc7l36inG0vBOjJkmWMnyVs/dXZ4RWsTSSKqKWdzgADmTQet4XwVGljtooYpLlpkjmmniDqgKliVB2wAOZG/MYqsTOmZx7gskFvDxArGguQ0kaImgmMNgMVAAGcdKy6Vjk96UY7p8j1rkiNJJ23PUUEmM+VFQ0bN0waAe6PUiggxHzFAtosDORUCzEDtjegU0AO9Qef4zGthxmw4gylolZQ4Hoc4+IzXSo3e3fZURQ2fHuDMZrNowsxQasId1bHUYOD7U0j56yz3lwIIdT6m8Kjlv1rWw3i8itfuEOQirHnz0qB+FTRtXtoLliJoG7vB2bXpwa1s0137T9p48g8evGwOl0xz9tQ0X/ALY9pM78avP/AMxoaR/tn2j68Zuj7vmiad/tn2h//msx98H8KGhDtl2g/wD5jq/xRRn7xRdCHa/j5/8AeIT/APaw/wD6aGhDtb2gPKSE/wD2UJ/9FTsvcX+1faIjcwnHnYQ//op2O6R2p7Rc+7tyP/7bAf8A0U7Gla/47xa+tTHdQwKmoHXHYxRNn/Eqg07GmWqPKxKox0jLEb49asK9j2Z4dNednL1raNnlQyHwjr3e341mRg8JIggljmYaJCDowCTjPx60mWq12bbTT2/HLe+kaOBEcEqG5LyIxz5ZqRd2/T2l6LgvZuG94i94E+kLLKzW8GnbBOQWB6Y6fE7c2+RbHGN9Y4RwqCwRZZmElzzzzCn09fWta08trTK5NxCzikMc15BG4/ZeVVPyJqsFNxGyPK9tz7TL+dBAurZjtPE3s4NEML0AlxQAWoBzQQTQATQCd6BbJ1BoB0sOVBOkjpRQnI6UA5oiM0EFqBbNQLLUC2egAtQAWoFu/rQKLUEDLMFG5JwKEeGVfzl5pWj3ORHH6k7Cku+DHys07VVt7Lw8goRfh1+wV56+X1+rtwrp6/h9sLeyiiK+ILv7nc/aa7PheJ2tYPIbCiET3VvbAGSREzsNRAzWhX/SFu52nTHvRBrd239sh/zCgL6XGdlkQ+zCqglctvmgapHlk1GnlZT9G7TX6Yx30ccuPYaf/TRpq9nriSXg2iIKTA7xksdlCkgZ+GKmyK7cOFRX0qy3Dz8VcfV04WFfY7L9pNNukY23bcLkVArPHbRD/lWy7/FiPuA96N6iF2CK0sye6iGtub51MfdjuaGpkxpGbrgelBzKrRFScZ6jmPWgUlxlPFgMDhverC+IVOK8ctuEWJurgk74RBzc+QrNraaxYLZJfLe0XbCfjLEXUwjhjbMdtGN8+p8/U/AV5bZOT7fT9JXFDzHH+MXPEuHfR5SFgiAeGNeS79fhWqWZzYKxhtLyBOTXrh+eltdn4p3u9dvYz3rIP6uFGY4OxO24wDz9q0zL6Hw60l4erR/QnmjtwkVyspKyHVhQo2BOVHT9ljTbMwX2oiuJeG35Np3Q7wNJcXiLFJJgYCxITlUUADAyTWdOlZXtK43FcgDR/tKAHFBAYk7jBHMUVxP84oB9MHFQAxPIZNAo6s/Vx8aAWVzuDvQIfWv7X2VNqy+Iwm7tngkXIO4ONwfOmxU4L2y4v2YjNi6i4tCdopAcD/Cen3V13Es6lV4x2t+mh/ollDZ6/rMm7H47U1EGpeaIeQ7KT8KvZUiG4IwFbHtTsONtOeatTcGkG3lHNTTcGg9w/wC6abNO7p/I02aR3beRoad3beRoaTocdDQ1LgJB50NSIPOvJ3Hxp2WYmDY5L1ziNpWJ6DJrPZqtL28NG14dxm9fuoxIxYfVG5x7CsTesPZToc1vnOv7vfdi7LivZcXDvNaxLcKA0dyeo5HAOcjJ2OOdZ912/Q1j77/sspwXs6k89xLLG0kzFn7tMKM9BzwPjXObxP3eimC2LtWjEvuy/AX4gGs3nCaNRYHUqtnyIyRjfnSMsV+7pbouXe0aegt79+GQiHhCRiMINTTSESSHqSQhHP2FbjNV4rdBmn7lt2k43KkiPAsCYIMsdwkjL6hVGon4V0jJV5bdFmr9nm34THcu0pa7kLHLObaUkn1OmtOM0tXyqy2XDIJNE973LeUsbqftFaZ2E2XCfDniUK55asjNQ2vDtvecBWKys+IrdRgZBfDqnTGcE49BRnS3D2646CcX/CrrPIJIIz/5wKqcatzhPbPiL2gPEbBJZAx1NbEsNPmAmvJ58yKM8V237d8FnIV5WhbOCHwd/YEn7KJps23FLO9jEltcpIpONjg59jvQ0fr9aIjV6ig7VQECKCdjQCaASoPlQCUHlQKdPWgUyHzoFMG8qBTZHMGgWzUAM2KBLGgEmgB5e4geXO4GF9z/ACTViFr3hkxKZLqNOiDW3udh9m/wrjkl9b0/H3b9soa7t4WwVUh3H20x+HLrsnKdPYmdQNtzWo8vnz4KaVm5nArUMvmXG+0kt1xKZoJSI1cohH7oOB+fxrpDTMPGLrrcv86Gnfpi5HK4b50NCXj10P8A3g/Ghpatu11xZSLJJLlQdx50NPp9hfRXtjBcqSzSxK+iMaiuRnB6D4kVy23GK0+QXHZ4cSu1unBtmC6CVbUzLnOD0GN+RPOm3aKVr5aFpwjh9imhYUOTq8XiyfPfr61F2uNcL0yTWjQe8d+uKIJcL6mg4uBuTyoalj3XGMGRu/VUjY4weeDjpz+J+FHOZnbzXHP6Q7fh93PDZwmaUDSS2yqw57fZ8K4zeIfX6foJyxyu8Hxbj99xRme6umZuSqp2ArjN5l9bFgpTtDF3LYxk1zdZkfEiqQOqtnTCgJB6kZP34rtR4M8/8NnnQd69kPzT0fZeGzkvkNzNeaxIixW9ovjuGJ2XVnCjzO9ac7Pe9ooILLicEzXcUFsyjEVuuok5IJG/i3UjJO/lisWju1E9lC74RG/Y+44rccLW1WR3dZorhsMRkf1bcgSByOK1aOxSe7awd+tcWnAY5UAshI1r9ZeWfuqDo3Eq8sN1B6UBacgjFBBQDYqc0A6QceAUEGJT+zj4UAmNMHb15UUpoIznw8qBTWMR3Kj5VFKbhdu3/LUj2oI/REBJ/VrUEHg0f9moNAB4Mh/YWilHgcLZyqj4UANwCAHDKvuRTcrsD9n7cfsKfLam5Nl/oC3zgxCpuR36BtuXcg03I79A2reFYMsOg3NZ3LpXHe32d/s7aKR3qomemdR+Sg4+OKc5eqvR3t9jrfgfAIJVa8WeVP3YtKk/PUPuqTleynQ2r2rH+Vj6Nwu6vFt7HhqWyqpI1ys2rfmd9z8AK5zPJ76YbYI3af8ACzfQT8Nt49EqiNn0skaaVG3PapLeK1L/AGUpmYoWB1EDNc5eilYifCpDdGUjIAyKPRbHo3WRuDijnx2N7mRgFzgDlg1O5XHG+w+Ez3bXMklskJlj2xJvj1GRXSu3PqMeLX1j4jHxAy/SruB8jbvEI+8b1Zmzn09OnpGq6VTxK4K6PpsxT+znxIvyakXtDpbosNp3NYVJrK3vyO94daSsNgYh3Lf+UgGtRmtDz39L6afOy14Bw6VgpgaHcDMj5C/6cGukdR+Xhv6TT9lpVLrgbdyqWqQMFznvFVCfLfn9tajPV559IyfaVS24DdSyhXhjgQnDOspH2b5+VdIy1ee/pnUV8QuS9mbA408VZM/2sY/MVrnV5p6fNTzSRWvZ24tZhNZ8QtpCpB+syf8ASa1txmv5iW1HxztFY4xGJVHRLjWD8H8XyNHPjDVs+3EurRxDhtzCcbOkTMGPlgZx86HGXoDxu2jXU8qge+aM8VaTtjwuL/mM/thf+oihxVZu3lhGdoiV82niH/qovFTP9IkIBzBag563hx9iUOKD/SRaAtmKAgfVxOxJ9/1e320TjKrc/wBKMCOoi4fIyHmzEj5cqHGQN/SlZiPUthI75+qX07e+DQ4y1LXt3wa6to5Wd4nbOqLZmT5GjPGVxe03BpNWL1QBvqZGVSPQkYPwoak+LiVhcKDDfW8gblplU5+2hqTWAPQGgU0a+VAl4gu+Tj2oFmMldQORQZd1P37rGD4FP/c/KjURygMcn0aykvWG7sSF8+ij+fOvPfy/SdLTWFc4ZeNLNDO7AFxk+xzj8K6xD4We25evW5CwqRzKg1qIeae0KHE+IG3sJ5gd0Q6f8XT7a3EJEvkVzJ3AwTy2rWnSIZzX7Z51nbWi2vXP7Rps0BrqQ8mNNppAnkkYajkA021EP0twXu7fgHDkKjUtrECPI6BXJ2iJWnuWOw2FGtQSWLnnvVEo2QDy9KCpxPjvDuDQ95e3Cx5+qnNm9hWZtFXTHgvln6YeM4j/AEnSuWXh9qsajk8viY/DkPtrjOaH1Mfpk6+uWHfduuI3SCOZi6H6yHADe+AKxbLMvTX0/HWsakvhN7aXtndw8Snii7gd9E7kAk75UDmSdsYrVLzLnmwcbxqHlzqZifM1yfQjwgqairMEC41cyRWojs897fSo8WAhtnAbeRhtj3/hXbDHd8zrcn/Ewga9T4T0fY7iX0LijRm4NstzGYjcLGGeLP7S56+2+9ac7Pp8nYOW+vTeXE0A4fHCiQsXGlY0IAGc8yqk9RlqL5ed/pF7S8OeFeCcG8PD4nMkhBJEjnnjPIfZk0dK9m5kk8x7jrXBEhepNBGMZ3z0qKF4yD3q8+RA60DVkV1yNgfMcqCdicjJ+FBy/VJIzRXbH0oB0gjOKgExjTsCT6UA916HNBHdb7ZNBwiKsQARRU92w5cudBBifOxFRQGJ88s0AmJh+yaLoABZsLGznlhRmjpXFazjaXJOwhizz7xskf5QPxrPJ6sXS9vqWEtrCJAbm6M7EbKiEY/n1rM2einT9/pqr3DiaExRRrCgJAAPP1I5fZXK1nvx4uPyZeJEla1kY4kXK5bAGOfSsw91Yr+0pIPEQwYMvrtWXXlozE0M8cynSyf3eY8q1DM8bHXl7cXFs0bGJg+NgTkHzq8nPHirWVVCcDVv54rL0TAEtYsllcRHpkE0XnKAHUkFwcciBzobiUc+dSYX9qbC4+h8UVz9SUaWrVZTLj5Y3rnC3Fs0bfVdcV1l8X43eXCKsrRSKDpOMEVxl9aLbqGWyi0lowUbG2DUbrkmVWKeQal1emfMUduMSYsq6Srx59QcEUZ4yUzBeuB60a4yIzERhlPoamyKlyPG4xJEjj1ANaiWbYa28itfoluxZLS3JPV4Vb7xW4y2h5Mnp+G/7YXBe2xRozY2oDgglYwvOukZ7PHb0fD57sqbslY/RjIeIuGG5GVfPtyrr7unzf8Axs3y6jf+FVOzlhyN8xP96Mr92ae/WfLV/R88W7a/yGfstqQG0u7XVn9qVgftUVv3aPNb03qa+ayV/stxNFDG7swvn34rXOHlt0uWvmshbgl1GDq4jZ7cv18e/wBtXblOOY8hazvhKI7RY7zbcxRI4HuRkVrbPGBfo/jK/W4Sp9oE/KonFYj4TfyqDNwwKpH7oB+QINDiVLweRV1NwxgQcYRZM/iKJ3VNJtSSovbYnn4yPwFDut2PHb2ykDJxa4Zf3XP5hqGm9b9uZUA7/RMo8wAfmP8A9NGdNa37ZcKuMLI7QSHkHGx+P54oTU264jJMDGg0p1wedCKqWGZSF2ZyI1PqeZqTOoenDTlkqPjZxBb2MW2d8f8AlX7yfhXnrG5fbz39vHKeFQXlzxGRIYGMaMFTOwIXHLPtXoiX52YmXsrXhhseHrHLL3cUKnLSNnC+9a2zwl47tNxpZEZEJWBN9+beprUEREPCJa8T47dMljaT3LDfRDGWIHwrNrO0UmVn/YftTz/QV/8A/gb8qzya9ufy4dh+1BOP0HfZ9YWFOR7U/k1P6P8AtU//APB51/xYX7zU5EYv5avCv6LuPy3cZvY4rSEMCzNKrHHoFJ3pyajF/L7IjBEVEGFUAD2FYdYjQJrkQgZDMTnCj8zt1rUM2nSuvEHkk0rLECFLBF3DADJGo89h0ArWnL3NzqsMDtN2zSwia24ZLG9yzlS/MRYAyfU5P31xvfi+t0vR+73tD5xdXk11cNPcztNK31mc5rx2taz79MdafFXaQHGNvOkdnT+4GJbGM1WJMSHO5rTnNje5+qFGSTgAdaupcZvGty+p9k+yVjZcMWO+gD3M65kfOCueg9q9FKRp8bqupvadUeJ7Q8HbgnGbi0KjRnVEQMAqf5+6udq6ezDl92jy/E7K5vIz3MDsE8WynlWqW049ZTnjYJs7hT/VN8q9O4fCmEpFcRsGWKQEeSmm4ZmG5Hx3i5te4MLv/eKmtcmYrxUnsL27OqQHJ86zyanu+riRRgZ3rmrhj33oD0535VBIHpkDnQKdWQ94o6bgDnRTVZWXI6jyoDIBUHlUEHA5nNBIxgb9cbUHAgnnQRp3zn4miuyQdhQcSRzX7aCDnA8/LrQSqO76ERmbGyqpYn4Co3GOZ8LK8G4s6lxYSAepXJ+GafU7Vx0j52Zt9b8RsSTccMlVQdnlPh2HyrlbcPo4MXTz4srxzXEieNhgFcovh2J3OORxWY3L3TStfAGeeJ2BQFOjLvWZdKVpKuksjXZVo5GikXSpHNG89ulIduMQTLHLE5y7jfnWXTHMTHg6CzTiFjIFlf6ZBuoJGD5Ee9a4uc5ZpPhRDs0ffAlXU4cDzrD0RESh55CByO3nijUUhCSM7MojYlRk6d9qLqEq8ecFt/IjlQ1JgWNgRk49DVYmbIddtsHHShElSKhTOhlboQamnSCZIu8j3bcdaadIsu2/Gby2UI6rKo2yDg1Yu81+mpaexM96lxdGUIULYyD50mXaMU0jsYJgRzrLnNe7Pk8FwR50eqPAs0SQS7oaNQlQPooOBnNEL/YFGkCijBNGRZzVR23lQcQtB2w5NQRgBtQC56HG9XbNq1t5hYS8uk2E0mByGc/fWovZ57dH09vNDBxS5U+JtXowH4AVqMtnmt6T00/Yf6WkzvEv21qM9nnt6Lgn76Pgubq4H6uwmkHnGCfwrcZp/Dy39HxR4yQupaXci+K1mT0ZDXSMk/h47+mxHjJAP0A8zN/7OjbVzZ1QZ+Zq83G3QzH7oKn7CQTDLWUAJ/s5Av3HFa5ucdLePwpH+j26jz9DuGiz+w7K6n3FTkn6W7Ysey95GgtgIUKICxUYUk55AeoNOTP6efu1bXsvoljeWZm0A4Crjc8zmszO3qxRGFrxdmuGGRppoTLI37TuTp2wMDlSI0Zcs3UrWx4H2b/WzXscc+SC77yMQd8KN8e1S1tM06a2SfpqwO0XaaPiU721s7i3jAOHGNeR9bHPHTFapass9RgyYo+qr59dTPxG9FtblmizhVG+T6V0tLxxD7p2V4Lbdm+CxWUKAzsA9w45s+N9/IchXPbtENoyYHibHoKGiHuByXYedGoglpvKjegFurHahoue7htoWmldY41GS7nAFO0EUnlp5HiHa5Lq9W2jiEce4R5DhmJG23QZxWfciHrydBfjyY/GpuNWN8092Yz3PJPFjlj086XvKdH0dLzuzy00skzmRzqYnc15ZmX6OlYrGqg05rGmhrETWtMzYwIq1YcZtJittsK05zttdmLPvOJJfzqDBbt4QeTP0+XOrR5eot21D0XaztqOA8PCQODfTDI/+WPP3P8APSvRD5c1rTvZ864fxe849xVbeVme5uG0o7MWJJrNqS6YeurEd6vrMfC+G9kOzxF6A90yeMk50DOce5PT8Aa1SkPLm6i2SfoeGeGCRy4iChiSAByqvJNkraQncY+VGRfQUNBwskU8vnQbYmy2ck58+lAQkLHGo+1AxHB21EVFESFbnnFAYZSPrZIoFMNAZhgrjJHWglHQgMCfPAqBmRkdPXlQEAC2emOtB37OVHrzoICtkkjCgcs86HaEaQoLHl69KNRWbLtrwe8vAO6t2CH9uTwj8/so6RjiPlLVt+y0KENeXTyED6kfhX861xdItEfGGvbwWdoumCBUx1xk1WZtaw2uGPLYUIqU4WQFXUMGGDqGcipqGo7fcj6FaYx9FhA/+mKah0jJaPEkycJ4dKMNaRfAY+6pxhqM+SPuqS9mOGSfVWSM/wB16z7cO0dXkj7qE/ZSUZ+j3xx0Dis+3L0V66PvDLuOD8Ts3Lm2WXAxrjG5Hw/GszWXqr1GK3mXn7mMxXhZh3Syjxa8kA/CuMw+nitFoUWWRThXjYZ/Z3rOnauoPhne2njuADlfrDzXqK1SdMXryrwaPG4EubaLiMADaMawP2lrVo5PLgtwtwIuLGJrBL2xdgv7aE5xU4usZbVnV1DvpI8a125ZFZemaV1uDdQ6Haq56A2GrLWgPuc8s0aCVwKNAzjkSKihZWY6tWcUaSdqCGXI55FBAGmNhn4VAG2kUadyOx286oIE0E1UTkUEZoOyaB1vZXV4dNvbyS/4FJ+dWKyxfLSnys2LXsfxOfxTd3AvXU2o/IfnXSMcvDfr8UfHutfojs9w/wD47ivfuOaRt+C5P2irwpDj+p6nJ2x10k9oOB2R/wBw4UZGHJ5MKfmdRpzpB+k6nJ/2X0RN20vX2jtraMdMhmI+3H2VPc/DpHptPvaVRu1PFW5TRL7Qp+IrPuWdo9PwffYT2m4v/wDEr/8AhT8qc7NfocH4/wBmQcf49cPpgn1nmcQpsP8ATWudnO3S9NXzH+3qOEW3aC5ZZZ7tVi5/1MeD8dP3Z9xXWvJ8fqLdNH/XV6OGGG3LsW1ySHLNjHsAOgro+bubCMydBVWInStfztHw65aNisgicqV5g4OKN4ojl3fJTIy6mY5dG1Zbc4PP7cfOvJbs/VVrEa1CrfOLv65Y45NyI9Nq5xk01l6WmSPrh7L+jbs1bRQS8VlgGS+mBnGSMfWI+O2fQ16q2tZ+Y6vBiwzqj3pnVBiMV08PDFJ8yU8pO5OfuqtxBTTDqfhRrSrd8StrGLvbqeOBOhc7n2HX4VJmIbrivknVXl+KdvY4spw+HU39rN+C/n8q5zliH0sXp0z3u8hf8ZveIzd7cXDyN0LHl7DkPhXC15l9LHhx4/EKIZtWckHnnNZiZdbd41ENrifaS54pw6KxeJVCHLysxd5D7nkPSus328ePpq452ydK7Ln3rnMberYhpHLeqzMu1A0c5lGvfyokjUHIxvRmXs43i4NwWHvQMJGJGB6k74+4V2rD5155X2+acZkuuJ3U17ISxJyRjYD0r0Vh8fqL8smlLh11JZcQt7qLAkikDLkZGc+Valxo9re8cvO0V4jyBu6jIOnUTk9ST1Jx+A2FZhq6w1yQRhc1HNIuwPrJ06GgJbsHkfntRBLdDXucg9RQa6sWIGRmooyzbk/LzoOJwCCOdQEcA6g2epoODDOTucZ5c6KIynmFODQLcgeJUw3Wgcr9OWOlAZJ1bnOD0op9vbz3LCOJGZue3WjXGzatezbDDXM3d/3V3NNN6irUt7CxtCGigUuP233Na01uZWGkJ5mhosyY5GjUQEvRrSNVDTtVQ0HXQ0jVRrSdVDTs0HaqCne8KseIrpuraOQ/vY8Q+NZmsO1M2TF8ZYF32IgKf7pMVK/VD/dmuc44fQx+pW1q8Mi74ZxDh6aJoA8efLIrnxmr2Y8+O/hTgnSJDCuY1OQUbcb/AHU5Q7TTfeC+Hu3DzNDOha1k5MviArMN5IjJO6+VOZE1siOHQ8iPKsy7Umdat5JhYjMZO4qOspYEZozAGk0jfcGjWnB1YUNIK460No074o1sJBBors7YoOcac4cMD5CoFnkKy07maokUEhSxAAJJ5CrESTMR5lp2vZ7il1gpatGp/al8I+3eukUmXlydXip5lqL2Ut7RRJxTiUcQ/dUhc+xb8q17UR5eSeuvftjrtP07svw3/h7VryQftMuR82/AVrdIZ9rq8vn6SbntpeMui0t4bZRy21kfPb7KzOX8N09OpHzljXfE76+/4m6llH7rN4flyrnN7S91MGKnxhVrLs740E0k8+Vi3sLm5AaOPwk41scLnyz1PoN61Wtpcb5sePy9Fw3sbLJh7kYXzkyo+C/WPx011jG+Xm9R+1HqLPhVhYoo0CZl3GpQFB8wo2+JyfWu0VfJyZsmWe6493nrVceBRlZqNa0JWGfOqlhMzGNljfQ5BAYDODRK9pfPOIdmeKWV2Wnh1282U79TlQTyLdRvjnXnvSX3cfW4Jp37SLhXYriFzcKeIRG1gB8Q1Au3oAOXuftrnXFK5/UaUx6p3l9CijjtreOCNRHFGoVEXkAK9UV0/P2vNwPcquy7+1aZissziXHLXhq/7zIQx3EaDLn4fnUm8VejH09sk9o28lxLtvdTZjskFsv731nPx5CuM5Zl9TH6fWPnO3m57ya5kaSWRndubMxLH41ym23vrStfBGCay3MpC+ZozsQIHIVpnack0QO5J6UZSobzoghnlRlyx7jOSc7ADc02sR+TO5nYlURiVBYqgyQAMknFa1Zm18cdpM4vx+54lCq3BAAOcKMDA5Cu+Pu+T1nHFGoYa3Yjn1MoIOxXpivRPZ8HzO1P6NjiYhj5M40+x3FS0t43veGRQ2dqIseInLHHOue2rr2mMrjSMH0quYe5h5BBQA9rEQdseooFG0jG4bfzAoNdVJwGxj0oDCADBJz03xmgLQp2wMb86iiEYUA9Ty2oCK7eIDag7IBwFz8KCCdYOygtQ7QK1sLi6cRwxl/h9X40dK1mz0dnwCKMBruTW37inarxb1ENaPu4E0QRqi/3RWmtTLi5PWoukasUNALk1F07NGtBNFcKCaCMUHYFBNBNB1BFB1BDKrjSyhgehFJiFiZjwy77s3w+9BPd923mv5VzmkPVj6vLTw85edlL+1Je0bvVHRefy/71zmkvpY+ux2+TFnt5I3K3Nmyv1ZRpNc5iXurkrb42U2tkZtcUw2G4cYqO/OYKmDREBo3BIyMqdxWXSsxYh2LDGnHlSZbmF+C2hvLcOF7txsSOprUQ89r2rKmyyRsUOCQcEVnTvExIDIQfEKba1CSxHMEe4oaRnNDSGPhqbWO6Y4ZZ5BHDG8jHkqKSaaZtatfLXtOynE7nBkRbdT/aHf5Ct1xWny8d+uxU8ND9CcC4ZvxG/wC8cc4wcf8AlXJ+2unCtfLz/qupy/8AVVB7TcNsAV4Xw0Z/fYBfzJ+dJyVjwR0ebJ/22Zt12n4rdZAuO4X92Eaft5/bXOclp8PVTocNGU7tIxd2LMeZJyTWNvXWta+EZptqe7qI6gkAsQAMk8hSIk3EeWja8DvbiQI0ZjJ30FSXx/hG49zgetdIpLx5esxUeksOyUcQV7gqp5+LDv8AL6o/81dYpD5eXr727VbsENrZnVBF+sxjvXOp8e55ew2rpEQ+fa17z9QnuSTzqs8Qa2PM7UadqFRBDfnsKMmpltkHxqsyehVNl8TedHOUXKd/byRSsAsiFSPQjFCFOC/D2cLqMu8as3uQCaOkVZnEO0dhZAie6DuP+VF4j8fL41mbxV6MfS5Lz2eU4n2xu7nMdov0aPzU5Y/Hp8K4zlmfD6mPoa1j62DNczT5aWVmJ55bJNc+W3sitakYos90j2oiRmjKcCqm0j2ozsXSoyEczQF9nrReO04OdKqc+tD6a+WxZ8FMEAvuKXJsbZx4SRmSUeSLzPucCu1Mc/ufL6n1CKxxxF8Q40VtGsuFQfQbSQaWOcyzD++3l/dGB710m0V7VePp8d8k87svgVjFxbi+Lg5tYAGZc4Mh5Ko9z9ma7Y69ng6zL7tmj204KLaIyG2igmiCkrD9XBGR0Fajy4ct10xuzVi17xVbgrlbeLJ9+Q/n0rFmqdoepksNX1GIPtzrMM+ZL7u6jOBkgHrvVEC4lwfDuB7UR30qTqoxQGZ1dTlih88UG14tH1TjPWg7WSc86A8nTkZJHmKip3yQTgY5gczQSurPX2zQFhtsbUB2c1lLxFLWefxscBACST+FTb3Y+lvMe5p62Jo4YxHEoVR0WtbZmszPLQu8Ymi6SH86GnF/WiRDtVGtILUTTtVGtO1UNO1UNJzQdmg7NB2aDs0HZoJzUEZoOzQTmg7NCe5csMU66ZY1dfJhmjVbWr4ZN32V4XdZKxGFj1Q/nWeES9ePrs2N5njvZefh6wPBOZombQQRjTn0zXG1Jh9Tp+urf/sY83A+IQa2a2cop3cA6fnyrE45h7a9Vit2iSYz9GBAMkRPPIBBP2ffSNw6dreJBLqlcuHRifXH31JhqNVJlikUAlG98VnTpFolo24a5QIkTyyY3VE1Gunl5b2is78H2vZO+nIM2i3B8zlvkP4U9vbnfrqVj8rx4TwHhP8Ax9z30g/YJP8A0rv8zV40j5OE9R1ObtSC5e1kFshi4ZYLGvQuAo/0r+dPciPi1Xob375ZZF3xziV7lZbpwh/YTwr8hz+Nc5yWl7MfS4qeIUKz5eqNR4hFQTQdVHYzyGaDUsezXFeIKHjtmjjP/Ml8Ix57863FLWePL1mLHPef8Na17JW+fHcvdsDgi3GEHu52+WT6V0jFp4cvqUz8I/y27Xg9paDZUjP7sGQfi58R+GmukVh8++fJfzK6jpDH3cEaxpz0oMCtdnHW/MhaX1zRdALE/WOKGkah0FBIyxyaMiBAOBkmiGKMnxb/AN0UZWQh05chF8hVc5IuOJwWsZIZVA5sxwBRquO1vDz132w4fHIytM8uP7MZB+NZnJEPbTor28w8jd9oL25iMCTNHBqOlF22zsCRzrz3yTL6eHp6UjvDKLE8zXJ60UEVplIoynB86qSIbUZlIoggKMuzVXUy5UJ35A9TRrUR5aHDuEXXECzwqEhj3kuJW0pGPVjsPvrVacnk6jraYY+lbPEOGcIOjhka8QvBzu5k/VIf7iHmfVvlXorStHxb58vUT2Z0s1xeXDXF3O88znLM5ya55Mm3bF01a13Zm8SusKQDgk6R+NMdduvUZfbxaq9l2Askh4SbySNXaSXUAw6DYfj869j85Jva5JLy3u525Mhz7nejUMfsUoHBpGCZYzEE/AVys1D0JKYOMA+1ZhqXFQR0xVTRLhRnIHxoaVJUhYH8KIrSQnO2SD6YoPQY0sc6jjzoJEmkaSpwetBChQc+Z6tiipY6lzgkDyoJ7wZ0hCp/wn50RxlIU4G56Y2NG6+XlYZL7h3FJbwJqeKUk5398jnjfnXm71l+qw3xZMXGHr+FdsrK9wkv6iXqHOx9jXauSJeTL0F615Vb8V9HJKI1bUdOokdB/P3Vvbw2paPKyJRim3PSddF04NmhpIehp2rNDSc0HZoOzRdO1UNO1UNO1UNJ1UNO1UNO1UNJzQ07NDTs0NOzQ07VQ07NB2fWg7VV7H9lW5sLK6z9ItYnJ5kqM/PnWZrDtTJkr4sxbrs3wEk4jdD+7G9c+FXtp1nUx5ku37O8NR8xWZf1lcn7OVOFWrdZlnzLUFn3MBCRg6RlYkwoJ8vIVvw8s5Im2peG4zxnicszws5towSO7j8PwJ5mvPe1n3Om6fFFeUsP1ri+k6g6gnnQWLewu7ttMFvJIfRTWorLlfNSnys2bTsXxKfDTGOBf7xyfsrpGOXhv6lip8Wvb9ibCAF7y6aQDc48I+ddIxRDx39Sy2+MNCG24dCirw/h0MgU5E0q+EHzBOSfhtXSIiHjtly3+UmyYl3upTcH9wjTGP8AL1+OaOektOzAAbAcgOQoaBrzy3qNaQW82+AoaQZMbDaggEmgnUq+poiQzOR09BzoytRWzYy3hHlVc7WTLdQWqnGMijMVtZ5Ti/bOKNmjt/1zjbIOFH51i14h9LD0M+bvI3vFLziUuZpGbJ2Qch7CvPa8y+nSlMcdhycF4lHw9uISWki2ykAysMDJOBWInbH6jHE8YlQX6vOtTDtXt3iU4o0nFBwFVjadNGdp00BYxRlGcUXW0qGfly8zyo1rS1ZWFxfTrDaQvPIegHL8q1Ws2+Lhl6mmGO67IvCeByOl+f0jfLyt4X/VL/jcbn2FeiuOI+T4efrb5J1RRvuK33FtIupRHAn9XbRDRGnso+/nSbxHhnH0dr/VclAoGANvKuMzNnurWtY1CJpViQknBxtViuyZ7bYEsrXV1hd98KK9VK6fF6rPynT7ULKLg8EPDoyCbaJYyQANTBRq+3NdHhr4ZnHl08Bu+9YJMgbVHjOMLvv70ar5YnY2PTwIEj68jH7h+FcbS1VvmJSMYz8cVz26FtDjk2PStIARZJDNn0oJMMQ6VIQDxKTsDWoD88iWHy51WRAgZA6ncUEBQMnYjGMeVFcmoEgjHlQSC2CSeYxjGaDkGSNuXPPlQUJuEi+bVbj6PcRfVkXr55qWryd8PUXxfHx+GTfWGiXRfxfRZidpkHgevPbHp9zpPUIt2r5/COHdoL3hqC2tu7Cq5Y6k+v79azGSYfUt0+PNHd6Ww7aWsoC3cbW7fvDxL+YrtGSJfPy+nWr8HoLe/guYxJDKkiH9pGyK6beC2K1fKwJR50256GJQaqaFr9aGk66Gna6Gka6Gna6GnaxRdO10NJ17UTTu8xQ07XQ0nXQ0kPQ0nVQ0nVQ0jVQ0gtQiAyTpGMu4A96NRRUfiQJ0woXPn0rPJ09r+oGi5uN5H0r5CjW61+J0drGm+Mn1qszkmTwMDajnO5TVIY3Guz0HFEZ49MVxj6+Nm9/zrnau3s6bqrY5eI4hwK84a2J026MPqn2Nee2OYffw9XiyR9MlxcF4jPEZY7OVkAzkKTn286zGOZanqcVJ+qWpZ9jL+Z4WuGSGJxqffLKPLHnXSMUvJk9RxxH0vT2vZrhNmo/Ud6w/akOc/DlXaKQ+Xk63Nf4tHXDbRbBIo132wAK08ve3lWe/kl/4dPD/AGkmy/Acz9g9aNe3CuxQsHmY3DjkX+qPZeQ+0+tTbcQhpnc8zTbWkA0RBkHnmggyn29qGghi3KhpOpUGWNE0jvWYgKKGliG1Y7udI+2jna8R4HNe2XDYy80qIPNjRmKXyeHnOJ9uoVBSzRpT0Y7L+ZrE5Ih7sfQTPzeTv+M33EWPfTHR+4uwrjOSZfSx4KY/ChWYh1nyZFJJDPHJG2llYEEdN6zMOWbw9Dx7ji39sYGuZLhtsZPhXHlXkw48kT3eTHiis9nnE+rXtnt5e6szpOwo0nIoykEedElI96My7ViqvFADPy5efSjXEyOLW4RFMshOAqjP/emplzvmrSPqbP6JteFxLP2gujASMpaRYMzj2/ZHvXopimXxeo9Q1OqKd72lurq3Nnw2FeG2J2KRnxyD+83M/YK6TaKPHXBk6ieV2VHCqDbmeZrjNps+lixVxxqBtsNvhWXSZQ8ohQux/jWnOZZF5evKxGee3sK7Uq+f1PURWOMD4BGZeO2Kg79+h+RzXbw+TO97fYHlR1keRS8pBKvnl5n3qpLK484PZ7iExJJWLfPPxEAf9VGoZ/Z8rb8FtkZtJKlvLmSfxrjZatcSHAZXz8azDUhLsDvnHnQdqxkgA0Cu/dWOqIkA9DRNjWVJDsxU+RFDZjx8+QKnn51tHJgY1DG3tQGgAO2++eVRRodWNgCOtBynB28+eeVBDHRHIeWF2+O1Bb4bHH3ImBy2kk1U32Fd3cb8NuIXhSUsCEBHJjsCPjWXTF528lLcdkzxGfh8009tPFIUNywwjMOeMZAGemB71farbw9lfUOoid8tfw6bs3cBRNw64hvYW+qVYKx9t8E+ikmuNsFo8PqYPWa2+nJXX8svVecMuiP19pMOasCp+NcZi1X1q5sGeNRO2zZdsL23wLlFuE/eHhb7NvsrVcv5csnQUt3idPQ2Panh13gd93Ln9mUaft5fbXWt6y+fk6PLT7NhZwwBDAg7gium3jmsxPeBd760NO700NJ76hxSJaHF3eihpJcedDSO86A0NJ10NCD0TSddDSdfrRNJ1jzoaTrHnTaaJmvoYR4m38hTbpXFaym1/PcHEKaR51nbrGKK/JMdm0h1TuWPlTRN4r8V2KFEGFArTja0m0Zjbs0R2aCNVF0gvQ0EsCMHcUa0gvQ0W8yopZ2CqNySdhTbUQqPfvL/AMOvh/tX2X4DmfsHrWdukUV2ZdYeRjNINwz8l9hyH31G4qFpmc7mjXEOaJp3eAbChpBkPU0NB7wUNOLAbscUTTu8ZtkGAaGjY7VmOXOn350Ym+vAbrinD+FITJKuv90btQjFkyeHm+JdtLibMdlGIl/fbc/l99cpyRHh78XQVr3s81cXU91IZJ5XkY9WOa5zaZe+tK1j6SajbsUZTyojsijKM7VWZQPqD2o1UVRt1GU+9E0lQz7KKNaEYwGCg62Jxhd9/wAauplmctaR9TYj4E0Vut5xm5Th1qR4Q48b/wCFRua7UxTL5PUeo1r2qTL2lS1U2/Z21NqpGGu5cNM3t0X4b+tdvop4fM/5uonux+7LyGWZ2lkY5ZnOST51ztkm3h7sXSVx+Rk1zenbiRijOyprlIee56AVuIcr3isKV40pj76bEakZRTzb2Hl612rR83L1MMonLE+tdnz5ttsdlyy8ftXC6tJJx/lNHOX1OFwGWRJCJlcaExkH1P2fOpBKv25xZ9kIrcf1l5cKB5lVGT9uitNQz4I1S3jj2/VqEHpjavOsHrKQMAUakzv8AA7mroM153wMe29Z0BYLJsMbdaojuzg4++gdzzgeE74Jya0iVB3PSgdGAvNhk/aKipKBSQMEZ6fxoIVgN1wT1zQKu3It2wMcvvoEWU7wysyklNOGHpVIjcNu5t4F4dFchhkzRk7+TA/hWWqTp8PvZTLeSvuSzknPvXdyHZcUveGyFrS6lt2OzBWOG9xyPxo09PZf0gzmFbfitnDdwjqFG3+Ugr/pCn1qTFZWLTWd0nTTgbstxne0umsJz+wTt/pY/c59q4zgrL6WH1LPi7TOw3HZjiUOWgRb1Of+7kl/9BAb7MetcLYLVfZwesYrdr9lC24he8PkKwXEkLKd0PIH1B2rnu1X0v8AgzxuO7ds+2VxHgXduso/fjOk/kfsrpGXXl5b+n1t8Jbdp2k4Xd4AuREx/Zl8P28vtrpF62eG/R5qfZpBgyhlYEHlg86082k6yKGnd4aLpwfzNDSRJiiaF3tDSRJRNJ7yhoXeCiaV5+IRQ9ct5CjpXFayqbm6uzhAUU1Nu3GtDobFRhpDqNNOdsi6iqowABSHLYwa1DOhBhRnQtVDTs0XQS9DQS5qLoJajWgNIFBJOAOtCIU3vzJtbKHH9oxwg9vP4fOjpFFZ2UsHlczOOWr6q+w6ff61l0iANMznnRrQc7bmg4uByoIL55moaQX8qGka/M0QY1Hl4Qep50ZTpjU5J+J3NXvLO5lTu+0VhYAhX7yT91Nz8TWZtFXanTXu87fdqr+7JWJu4Q8gnP51ynJMvdTpKV+TPELyDXNLu24XmTWe8vRExX4rVt2d4ldnMVpJp83GkfbTjLFupxV8yf8A7H8XOf8AdwMebgZ+2te3Lj+twT92XdWF1ZymOeB43AzhhjbzrGpq9Fb0v8Fem4lqYmPKKrKKMub6pPpRJSuygUaqkCo3pIBOyjPrRrSfCp3Oo+Q5U1MszetGpacCuJ7f6XeyJYWQ5zTnSD6Acya7UxTL5fUeoVq5+0NhwvMXALTvpuRvblcn/KvIfH5V6IrWr5Fs2bqJ1DGnkuL+4a6vrh7iZubO2a52yPTh6Ksd8iARyAArk+hEVr4FkdKM2nbi3meVXTG4JDy3MndWyaiBlmJwFHmT0Hqa6Vo8eXPFSnuLaxJ7vTdXHWRhlFPoDz9zt6da7xV8u+a1mVcTyXErSSuXdjuWOSa6Q88lDnUIek7Dxxy9okSQgAwyke4QnH2UZs+kcJiabiMShSTnw5G3v8OdIRm9v7pbztPZ8NjP6uyTLf4jufs0j4Ul0qqRyhQAWAB6iuGlWVkG2GyPMU0DGPMEDrQECNW23xoDDAA6TignLHbmaByghTpJyDjB2qok88bDfoKBhJzkMpPlVEHnyODuTpoIzzGTuPKo1HgN6FFo7LGzhBq2PQVlrHj5STaywXFuO5xz8WTuPQ1otjtUy+bTbQjvDjv0BXoQdqpV8huAe/cEFsH411cifQHI8jQSN22yp8qK9RwTsNxnjiy/RBbs8S6mh79O8A9Uzmgow8Q4xwmdreOWVe6YgxONSgj0PKpCx3ju2oe3TXGIuMWMV2g2y66yvtk6h8GFJrWWseS1J+iy9COzHFWxaX0nD5m/YJ7xPbDaWHzauE4Kz4fUw+q9RT59/wC45+ynFI1L26RXqfvW75P+k4Y/AVxnBaPD6mH1nDb59v7s9LniHC5jGGntpF5xsCpHuprnu9X0YydP1HiYlq2na+9jwLhI5x5/VP5fZWoyzLlk6DH5iZhtW3anh8+BIWgbykG3zFdYvV479Fkq1IriGddUUqsPNSCK1t5Zpao8Hzoy7eg7URRYBJcrEMscUaiu1KS8muDpi2HpU27xStTIbNR4pTk00za6/GVVcDatPPMGq/rRnQw9E04ttQ0hZN+dF0ZqozpBei6QWoaDqFFiFWa9UEpCO9YbHBwq+5/k0dIopTShzmdu9P7uMIPh1+NZdIqW8zyHngUdIqEeu9EcZAKADJmoukF6GnaqIjVnl86AZLuG2XXI6r/eY0mYThN/iy7ztRFGCtupkbzOwrnN4einST+5hXfFry8yJJSFP7C7CsTkmXsphrRTALHasd5dm3wrgT3GJZzoTp5n8q6VrMvLl6jjH0vVWFhaWIAt4EDdXbdjXaKxD5d8lrz9Tatnckam1DyG1ah5bxDQ7pWQPk4PnWocdyTfcHs+L2ZguI8jmrDZlPmD0qWrtrHmtjs+TcT4e9ldSLoIQSPHnG2pTgj+fOvLaun6TBk92rPZcGo2ig5x4D7UZtAlGeVZl2pCcqvPc07yTeIaVnwW8vojcSFLO0X600x0KPnzrtTHMvl9R19MaX4zwjhB7vhFt+kLof8AvVwuEU/3V6/HFeiKVp8nxcnVZuo7QyLy4veKz/SOI3LzP0DHZR5AcgPas2y/0u2Hod97hVQgwBiuO9vp1pWvhJzzppqZR6+dNMTO0M2BzrUQ42vxdJAEUPeOYlIyIl/rG+H7I9T8Aa7Uo+Zm6qJ7VULniB7vuIlEcX9mvI+pPU121p8+bTbyzZGZm3NVkFBI50IbHZq6Wy43b3MmSiONeP3eR+zNGbPttveWnBuFNxWaJT4CISQQZc8gMncHbJG2PerVKvmsE01/xC54hO5Z5XOWPUk5P21zu6L8WoYIGTn2NYDO7KYIJB8xQWFZ+Wcg0Hd6QQHXSx5adxQEl0rDbBI570DlkBA05GRyFBZEp0lVUKOWc86onWhJION9sqd/hQGBtjUo57EcqioJKEg7EHkc7UHGXmyjfoBtn0o1SvOW3w5o7zga6UVfpEXjB33I60e72+MQ+XvcXPC+IyQS6oZo2K5B/nIrhFtPqzipmr9Xlp/pz6RAkciaXEqMGXdThgfhXWttvnX6K9JmbeHjeP2MnD+N3lrKhUwzMu3oSK9D5H3ZvPpq++iwvcGh7/ikKDLgEtpxk7DPKpI2+B8Xl4f28ivbEm3WGTDruQVC4fIJ64O3rWoaeuPH5Oz/APSLPwvjNvaX9jdzYeQ2qasPuHBAz1++pXwxplzdnOy9x20uOGXF9d8O7ycrCktsNAydgSWyPlSte7WmXxv+j7jfD+OTcNtrSW8CYKvDGWyp61NptmSz8e7NXP0aSa5tXH/KfOP9JptrbYs/6Q7kRLb8Ts0u4hyBwQP8rhl+WKTG2otNZ7f6XV4t2Q4ps0cthKeqsVGfUNqHyK1xnFWX0MPqGfH2i3+Rt2d75dfDeJ29yh5LIe6Y/HJT/wA1cZwT9n1MfrOv+yP8KdxY8U4ViSe2uLdekoB0n2cbH4Guc0vV9HH1nS5o8/5aPDO0csaaLmdic7MwyMVa5Ey9LSY+iG5BxpJFDZVh5qa7cnhnp5r5gc3FcLiMZJqbSuJnTXJZtUjE/Gs7eitGlYyR90MVpxyRMLoKkbMK088TKc71DSQ5HWhoYl9cVU0PvfWjOkhx0oaT3tDTu8oTBU15FDhSSznki7k/z50arRSmuXk/rW0r/ZofvPX4fbU26xjV2nLDSg0qOQG1NukVBnqTmo07VRlBkptdBzmmzSNQUb1AJlAocdqNzxm2txu3eN+6tSbRDrXp7T5ZNx2huZGPdBUXoMZrnOSXor09Y8s2a4muG1SyM59TXPbtWta+C6aanukDJxUPu2OC2Blk71gNC9SOZrpWrhnycYeoiwuANgPKu0PmzC2jBRnNac5hm3vbC0sMpbL9JlHkcIPj1+FZm0R4dI6Wb/KXmuIduuJzkh+IdwvRIPDj4jf7aRznwT+kwfKWV/tLOzZPEbgnzMrU4ZEjrOk++jhxOW5j0tOZULFjnxbnmfeuNotHl78OTFePo0KVQyaxyNZentII4nkJ0LkDmTsB7npQ7QhjDEG1frTjodh+dVztePuuWfCLy8i7+TRa2yjxTTHQo+ddK47WeLP19MUCfi/BuEHRw63/AEpdj/nzKREp/urzPxxXeMcU+T4mTrM2ftDLvr3iPGJhJxC5Zwv1U5Kg8go2FJyR+10w9Fa/e4URUGFXFcbWfUx4sePwOsw6WnfhB2oxMhY1pzmXIrykiMDw7sxOFUeZPStxSZeTJ1MUCb2O1B+jsHk/t2Gw/wAIP3nf2rtFIh8vJnnJLJuLtpGbDFiTksTkmujhtVLHOaM7QTmqIoJFBYtZO7lDZxQb44lNNFHaREb7BVrMzpG1aAQwxoMsFXpXOZ20tJI2vKxtvvzHKgbrcnwJjPLJxQMbWccgPPnmqJEQbGTzPLJoJ7gAjKjJ68jUBBNIAHvQaJyfCWGQMbCgIlWwTt6bYoJRl3xg5HPTRUYAON8e2aBV5n6NI0QwVXIbrkb0jRG4ncM3h3H5rNmmt11WxOZIOsfmV81+7r51mdvqdPmpkjjZocU4bw/tXbfSbV1E4GNXL4GszXb2Vm2DtaN1eIvLC/4Pc9zcowXOA38ax8Xpi3KPpncNviEfDO1rpLNKtrxAqAzE6RIwAGQeh2GQduuRXprbb8/1PS3xzt5XjHZjiHCJD30DPGN+8Qb48yPL1G3rXTw8k7WewV7Bw7tnw+4uxG1uZDHIXGQFcFcn03rVY7LXw9bxHs9a2X9K1/FfIILG5jkuIiBhTtrKjHnpYVmsd2Y8sFrRuLduY7OxuHvWEOAzAgBtGWA2zgEn5Uar4D2ynl4p2os2urc280saLJIDtIASocf5QPiKMx5F214isl1wzidndTJdNboZm1tlZVJGQSfIA7edWFh6Hj00fbT+jCDtHcKG4jw2T6PdOuAZBtgn5r8zSSXy3IzsSBUVGph0yPSgdDczWrB4ZZISeqkiml5NzhvbXjXD2ylx3g65OCfcj8azprk27XtfwPiUipxjhMcZY4M0IEZHrlMZ+INZmlbPRi6jJj7476/hb7Q8GXgd7btZ3TSW11GJYZDs2M4IOOeDXlvTg/Q9D1luo7ZIZ91xVkykX1vPyrE3e6McOi4gsigStgnnikS1NYacHEkwNL1vbjOOVyPiLbeIGtbcZxwsJxI+dXbn7SwnEAcZptn2jVvY25mqz7cmC5Q8mFGeMu7/AMjQ4uNyRuTQ4qr8SeXaE6U6yef+H8/vqbbjGV34UEJnfmTuT7mm3SKhyWNFTRlBcCoALE0EFwKbWe5Et9FCMyyBPQ1nbVcdpZlzx5BkQoWPm3KszeHojB/UyrjiNzcE65Dj90bCuc3l2ila/FWyTzqNuoOoOoL3DbI3c2+yL9Y/hVrG2MluMPUQxrFGqIMKBgV6Kxp8+1uUjluorWIySuFUedN6TjMzqHk+N9pXmUxhjHD0QHdvemrWTLlxdPG7eXlri/lmJGdK+QrtTFWr4fUdfly9lQ712fP7z5TmkaPHaDIJ3gcMhx6edZtWLO+HNfDO4ej4bcC7Tu8gaxgAnk1eG9Jh+r6bqq5K85aVrwq9vY2MsgtbOJjrklbSqnrt50rSZcs/WY8U8xS8f4LwVTDwm0HELkjDXNwPD8B/29zXpjHFfk+Jk6zLmnVWFcT33EmU3k7FF+pFnwoPIDkBSckV+Lph6K+TvcSRpGvhFcJtNn1ceDHj8CrPeHaZ34SCOtVmZcTijnMgeUKPU7CtxWbPPfNFY+oThIB/vRIb+wQ+P/Mf2fbc+g512jHEPl5eqmfipXV8zqEIVUU5WJNlHr6n1O9dHjmVB3eQ78vKjOw90TVEd0R0oOMRomgmJh0oacI3P7JpsOgs7iZsKh9zTY9LwvhKwgM/ic8yTischsxrgYBAPIA9Ky0cni/axnljz96IYMgnUQccsdfjQM2b9WASTjAzneqCXGMBSD50BDJBYjntjNAeGYkL4tthjGKC5HGgbWdh1z+19lQMV9xgBVJ6b4oJMZbdWU+LbfBqgsZIQkAty8Q++gBkjeM5JLMeWNsVB5eBTbsYzkd2xGVO4I6iinRtc2E63drhWJ5fsSenofT5VNRD6HT9ZMRwyeHoLfiPDe0VsbW6jCTDYxvsQfSszES9UVmk88fh5ftB2ek4Mvfo4e3LYGTvnyrnxmPi706mLdtdlfhvaW5tYhCWW5twf6ibcD2PNT6jf1rpXJMfJzy9Fiz98c91mbhXZ7tCQ1tL+jb076JDgMfRhsfj/qrvW9Z+L5GTpcuLzAb6+7adnpLWWeY3SWmDBKyhjpU5xnmy7dCR6103+XniY+8Gv2v4Reds4O0wkmsJdAWaFI8nJXSxDbg7EkZA6VE4mcf4VDZdv+E3QupuJ8MmELrKd8QjYjIAGAATRpS/pN72G9trN+GPZgNLNqcL+s1tgYx0CqvxzWYllg21zc2/ZOeHvmSCafOjJwzYAB8uh+VJlpi789m9RVQO3QkGgkjy8VBwx5GgbbpJNOkcZBZmAUHzpDVfMvedqL4zcYW0V8w2ECQR/axP21481n6D0iv0y89NLpVmNcK13L62bLNKTpZ4HwSTiiNd3Nz3EJfu0bqz88AeVe7jWIflb9Tlm/azYfsTxZN7O7gnHlq0n7d6x7dXSnqPUU+6rPwvtHw3efh82B1UZFZ9t7Ker/8AuQQvGJ7dgtxE8Z8mGKzws9dfUenv8trcXHYW5vg+tZ1Z6q5cN/haF2Picb8pB86bdOCyt4D1+2tbZ4Gi6I5N86m2eJbXZuJe4LeBd335+Q/P4U2zxOLM3tVNwJdOMZyR0ozuTNQAptkLOBzOKbCmnAo3Fdqc/FLeHIeTJ8l3NZm2nSMNpZlzxyV8rAvdjz5msTd2jDEM2SWSVi0jliepOa5zO3aNQGiuoOojqDqAkUswAGSTiivVWFsttbqmPEedd6w8GS+z7i6jtITLIcAfbWplzrTbxnGeMyTyEk5P7CdFpSs2nux1PVVwV1TywGDTOWckk9a9UVisPzGTJbJbc+UmzY/V51WCHjaM4ZcUA1UdQaHC5Sk2jPPce9cMtYnu+p6dk1fTeteEXPE5Zvpl68VlbqJWZmJVARnOPj+HMipjtuNQvWY9Zdqs9pb2s5jgcyJnwuyaWI57jJxXO+3t6SuPXaodjXHu+j/Z3xrbnMuJ2oztxYAb1Yq5XvFYQoeVO81LHCDgyv8AVPoOrH0HxxXaMb5uXq4jsU16IM/RdSNyMzfXPt+6Pbf1NdYh8+2S1mfJMTsvzrWnMKpq96iGpCccs0BrEOoPtUDFhGMac77UDhaLqA6df+1Tanx8ODdM7cgKbFqLhStlsKBz3rOzS7DaRRbKM5HMimzR6KdQC7Ajqcb70RajVcZQYOOlAaBhhCwZWBJ9PzoHgZXcMygjJx70HB01ZxjHQVQUWliTkZHPV1oDJXUVJUb9OVBxcZyHGpvnQXnLaQvhYDlvy+GaCQqg9DqPMnGn50EiRQug5G+7AZxQHIVjJBweTbHagVsTnOMemDUGRxC3aGYTKpZZDhyOefagqyORpRM6WGD5MKzMzUiNx3UL26SzljW5VijjwSr9ePHr1HofsrVYi7thz5Mc/SuDj8U1utjxhWurR/FHMBpcdAff3+2sWrL6uHNjy96xq3+mfe9mnKNdcLm+lwDfMf10/wAS1zjcPTbjbtM6n/THE80PhkXUvnirH/8AlfcyV7XjcNbhvaO7tYzFFOJYG+tBMNaH3BrcZLV+TjfpOn6jvinjIJ7Ds/xNiyiThU7dR+shJ+JyvzPtW65Ky+bm6HLi8qU3Bu0PDYWa1llntV37y0lLIB5kDdfiBXbk8M7jysX3bGTjHBrPhnGrZrg2WVhuEfS+nbwtkHPIb7GnkZPEuK/TkighhW3tofqRA538yev89SSXgZvr9ooJGSeYaghsZ32PpQSCcfWB96DV7ORJJx60MkZKxyd42OoUasfZWbSutRpp3M5ubu4uCMd7Kzewzj8K8OSe79b0FOGHbOu22CDmxreOrz9fk4Rp9G4XwtojwXhyrnuk7yTH77bn7a9Evzn32+hW9lG6YESkAbZHrirpldHC7YIxEYXnupI+6mhVuOD2dypWaOOQH+0jDffTSMO9/o34PejvFsrdM5/qy0Z5+m1NNRZ5+7/ors0Y6Lu4s/UsJF+wZqcXWvUZK+JU2/ou4zn/ANn8Xtp/7rko3y3NZ9t6q+pdRXxJE/YrtjYQPI9okyoCSUcHOPcis+29Meq2+9Xn47ri0Cf/ALMZ9RLFvP8Anl8Ke26R6nX71N/SfGFGBws/aacP5P8AyVP6f9piuuPP4xw1cDzJBPzNOH8s/wDko/p/2mfi/Ercj6Tatb6tgWXIz6GsWrMPT03V4cs6Ik4pcvuJxv0C8q48ph9itMetwqvdzuTqlc/Gs8plv6Sc5qdx1B1B1B1B1EdQTzpKx3lq8FtBLP3zjwR8vU1qtXPNbUN2WRY1LsdKqMk+ld47Q8MRu2nkeNcXad858I2RfxrNK8pTqs9enq8/gyMWfJJ517OL8xfJa0mKvM+fQGjmarFDkEj1IoHB45E0zRgrzyPtoKl1w4xoZoCZIhz8x/CqyomrAsWZ0zKfWuWTvD29JPG8PRW919HkJZWkRlAKayoPlnG+PbB9RXni2n28+D3awRLK09w0rY1NucDA+AqTO3StK1BWdNzLqunObIGXfRGpd+eB0HmTyA9TXStNvHk6itQvJDH9bTcSeQ/q1/Fj8h713rXi+Vl6i11Wa4eV9Uj62xgeQHkB0HpWp7vPBBUu3iNAaQnOAM5oHC3IbdfQ4rOw+OPW2yjY9KbFkWwxkr7elZ2pgtVZQCmx3wB602LC2oABXAOOWKC1FFpxqUjGxJ96CxFG2NQwyrz2O1RDCMnOFJHPTQGqZGs5JHI4qiWXSxcgYx7mg5D3hLqeuds0QcczHCBeWzFtqCGZ92ydvQ7VQ76wzg6sbb0EuoJA18qCN9iyj5UGwC3hOFU4336YqBbK7YBYnzI6UDGI8QycA+FuefSg4xnQT3mCBk55+lAsDJ2O/wAd/SiolRZUYMpK4xj8qDBuVlsJ9LrrRmyGLc/5/nzqa3DpNdsrtIUnsoZl5o5UjG4yOvyrNI1LnPZjW/F2I+jXifSIVGFOcOg9D+B2r0TEJymFy1muLN/pPC7pnVRkhdnUeq+XtkVzmkPdi6y9OzSXi3C+MDRxS27mc/8AvMAwSf7w5GuVqTD6WHNS0fTbX8Sq3nZedIjc2brdw/2kG5X3XmKzEzC2xUtPft/dkiS4gOGXWPtqarbw3GTqMX23C1acVe3kV7eeSCQHI0nGDWom1PDM/pc/mNS024nbXpP6U4bb3hPOVRok98jmffNajNDz5PTJmP8AilXbs1we/f8A9ncV+iO3KC9U7ezqN/8ASPeu0WiXy8nTZcU/XCnedieN2cZk+jLNEP8AmROHQe7DYfE1rbgxJrWe3I7+F488iRsabCt+h+Bqiem6/Kg2OzoaOa4ulZgIIWJ8iSMAfOsWnUOmKOWSDx4YwPIV4d7l+xxxxxQbwOz/AEn2ktoMZVW1N7Df8K9OONQ/P+oZOVn0+0mNlxtJJkJ1Rt4jtggitRO5fNnw9RBx6zWMkuUxnmpre2dAk7UWp1RxBnPmdhTZpRPG7mZvCgXfbrU2ugScZ4mF0K+FHLTTZpRur28lGZJHPxq7YBZT3QfSXkx0OTt5/fWdtLt7xaeDgV4zTyGRozGgLHm3hH31eQpw2UcNnGspA7tdP1RyA9xWWoUmnha4EaxJnpkjPyrLWlv9Eo4Ejwq+2SCDkUNqN/wqxvYZIBCY3xjB6mmk3q23zxkeGSW2cAPCxU+u/wDGvPeH6roc/OukVzfRdQdQdQdQdQdRU0DoYJJmCouSTipEbWZ4w9HDCttAkSchzPnXaI08EzyljcZ4nlWhDYjT6x/ePlSfqlq9q4KTe3l5GadppC5PPkPKvZFdQ/K581st+dvAAa08ww9AxZMdaBiSDO/PzFBoWlz3b51AIRhgeTDyPlUCOK8PjeP6ZaR6F/5sf7p8x6bj51oZ1muZ09655PD19LH1NtsZryP0O/pAp6+ZpJyQzqvuTgDHOtVrt575q1dIFh/4lijdIU+v8TyX7T6V3rj0+Vm6ubfFXmuS66MLFFnIiTkT5nqT6munh4dzb5KrSM5wNhUHLGc0FhITsCMZqbDo4cZJOPWmxYCqCB6ZzprKnAxKcqGbONwKCyJIlZVOTt0ztv1qB0ZhkBJK4zigs6BkFRkYAGQTU2GgbOpIJPIY502O1SNp04B9TVQzu5cthhnYjYGgYqAg5Y7+RoDFuFdlGSOhI39+taAtG528RyPEc+tQGhAAVNWx3I5igLu9G65zz58t6IAI+wZ8+fnQMUmNlCtqA8tiaAhH4iAQwxn0FBeSUFBhX5fKgLdiX0jn/O9BOt3bBbYeQopoQlCC6gDmDsRQKLLgqMEE4zjegAagwA2HL2oBuLWG4VrdgzZHhOncGpPdqtuLyPF7OaOFoGAYc0b0pFtO3t+48q6GO6wwxkV1ju881mH1K07CcF4zwGwvbGafh1y8CkyoTIjNjckcwc55H4Unsz3ec412T43wgNLe2P0y2X/32z8W3mw6f5gPekTElbTH8MqzuLm3cS8OuixHRDhh7r+WRWbUiXux9Zen8tAccsOIeHi1kO85G4gGl/iORrjbG+jh6nHb4zr+/gqbs+l2C/DZ47pOelTpkH+U/hXP6qvTauO3yjX9vDIks7m0kKjUpHNWFa5xPyY9jLj745QLqRPDMmR6bipwifisdTevbLC/Y8YmtnV7O9khYctLHb8vhVi16+UnF0uf4zpsr2na4UpxPh9tehvrPp0ufcggn4k10jNH3eTJ6TPmk7Jk4d2S4ofC83DpW6MAVz/5cfJq61tFvD52TpMtPMKl12CulUycPvra7j5+GQA/I4Y/6a1O4eeImPMK0VtLwzhbW06hJ7mQal2yFXff41xyWe7o6crkytpQmvLHeX6XLPDG3/6O4lW+uL+QZC4QfE5P3D517X5TLblL6DxeZL+GO4ijVZIZQCy82BH8BWXCOzP+hSzMMhmqptrcP7O3LkMsBUee9DbUg4ZBHIomYK3XNXSNS3tLMwHvdAPLHqBvWtDL4jb8NBJSZRzGw9MisjMJu0mTuomkh1YJKEbYA+zegocYXFzY2f8AbT62/wAKgn7yKy1C7HaS3y+EkYHTrn4rRqGXPwO4tJxNGxJT9nI+7WaxETDpNqnT8fuYLcRzRjK8iyhQPsrNrxDVcfJhWnFZ7riMgdQoI8Okn8zWqyWrqGH2rtRbcaSdRhbpMn3rN4e/07JqzFrzP0jqKmiuoOoaSBRqIEqE1F4yP9WnqaNdjI79oEAiwjZJ1edWJlytxn7rP6ZZrQxhi9wxxkCukTLz2nHX7vMcSmcytCyshQ4ZWGDmvThpqH571DqpyTpTSOSVtEaM7eSjJrtE6l8ud6Xk7P8AGnUMnCL5geot3P4U3CK9zYX1kf8AerOeD/6sZX7xTcCvkiqDWTFA+ObHI1BoQXeCoY4jIxnyB5igTBbiK5LAeADIz09K45JfT6LUHu4AOTj1rjEPpWyRCQrGISuwhiPJ3G7f4RzP3etdYxzL5+XrP6Sjdd3kWwMedjKx/WN8f2fYfM12isVfNvktefqVS6rsvOjIFVpDvvQOigLb4+ypMosLakfWOOtZ20sxWeQNW+KmxaitYwclcjPnk02BZLdwdTjYgdPLyoDhtUtAGU94D68qB9tJBLIe7zqOxUjljypqRdjTB8IA2wB51nUh6jS+Gz5D0oGAagWKgY3xQTCmW1YwT6/ZVDNIzjwgfHegNoiCpbfI2yNsUE6Fxy5Y+FBIVe7wCPUVUdpwAMgAbUAsuRksxzt7URKoXUsFYqOZIJxQCRgg86DgGCkqxDDn60GioXKnYr1B2z8qKIbOAykb52OMCgWV9VUZ2ANAUbRyrqXOSMN6+lAzUqqQFXz9aBR0qdTfVHPfFAKzxySARsrDVyBzQiOypxa3+kWbxxpiaPdGAyCfL2rLphvxl894oweWOTTokUlXXqDW6OvUPrv9E1xFxLgS2UzHMEroMdAfEPtLV1l5Z8PXXHBuJ8PheWCYXTKWIKoVbTjIGxyfLrz5Vjiy8rxXsvwDjpLX9kLG8L6DNbMI215xg7BWOfQGkDyPGP6PuOWYZ7TRxqBf3RouFH+HmfhqrWze/DyIHc3BRJHt5kO8Uw0Mp9+XzxWZpWz1YupyY/DSXj93GBDxGBbpMbCZcnHo3P7a5Tj0+ji66n7u39v/AMN0cG4gP1MzWch/YlGpfmN/mK4zXT6NcsXj6e/9v/xTuuz88ad6IxJH0liOpfmKRa0eWbdPiuzzDcwnwOceRq8q28uXs58f/XZwvJE2ljPwp7cfaWv1d69stVmG9QDEUzRk9AxH2U/5K+JXfSZvImYMxkZy7nbUxyaxabW8vTiw4cXeqtcyMy6EGpm6CulI08vVZLW7Vew7B3nCLW3MPFbqSzPeFt4ydWR5745eVejtL4d8OWveYfRLaDg8+X4ZxBZs4OkOrHI5bbGnGYeed/eG4+nUFi028oXxKcH7Ac1dMlyfpDumIudeP3MH8abFGG01qxkds788ffUEvas5P/F7nOYmDfZmgz7mzvUT9XfyEfuyh0+/aptUWXHeJW8yW84Ei6t123J9RWtir2ijWDtekaf8i01ED95jj7lFZlYeh4EYkshK22skgkUgldZ4bh9J0sp82H51YZTccA4ZeQd28Cefh8P3YrNsdbOlclqvEcR4HDwuZ2iYEB9IPwzjmfMVmK8W5vNoea7YR97wmCfHihkxn0NLd3Xp7zWzyh57V4366viHUbdU2uk4ptrSdupqLGoc0ir/ABrXFm2SICHaRgqgnJ8qunGc1p8Q0bbs5xK63ZRCn70nh+zn9la04zk18pXW4Jw2wQNeTmZvIbD86sxx+RWeU6iFeOYzXS23DIo7UtnxjY49TzxSO/xXNNcFd2hYg7PcKtJDNfE31w5ye8JCZ9FG59yfhXsiNPyOa85bbakV08Kd3aQGGP8AdjxCp+Cjf4025biY0yD2k4f9KkguBHHKjlG1g8wcc8VoakU8M0OqJv1Z6xPt9lZGdfcF4fxRXKRxmQc2jAR19wNj8R8asDx/FODz8NYsSZIs4DgYwfIjofs8ia0KANVFm3kB8D/VbnQXI5G/qyy684GTgY6E1iaxLviy2oIzQxHwAXEo/wCY6+Bf8Knn7n5UisQZctrq80zySGSV2kc82JyarkAapOuKCVi686kizHHgZUHArMizFAfrYzgbkGoqxHAeZ2RuW+T/AAoLiQKCoLZJ3wOv5VAxEkDlGQAZ2350Emxhc+JASetTYrT8KmY6Y5O7TG2RzrWxbsuHtbKNX6wjYb8t/XlRF/SFfSTy2Izn5HrWRIUkDDFhn9k1VFpXTlwSD060DRpKDUSrcsGiJVQpDA7+Z/OgIMQoO2MEZPXHlQQJDtsOVA1WUoDty6H1qiV0E8/YE8qAmU5zkYxigXjw7HYnlQRo3y3PyxRHOAdyxxzORzNBaCnUVVcnpgVVdnDAtjfnj86A3U/XA1EYySKiIQklmDAY3OQAd/Kg4sNWw88dOdALHUNzqQ7EUC8RDKoAvLBG2/wqq5lVSVkOGxsFGT99B5/tHwBLgfSYV/WAZbn4v41InTtW3Nf/AKKbx+H8duLRjs6K4H95T+RNdInbneun3sHIyOVaclXiHC7LikBhvbdJVIxk7Eex5igxLjsvPbiSTh10zvoARJ2JUEDHIYHQbjBBHPes6GNxjhFlxTFvx/hccz6GIkcaWAUgErIPF1B3zsag8dxH+i9lBbgXFQqNv9Gvx4T7OBg/ECmyJfPuIRPw6/msOI2zW1xA5WQIcgH26/A1rW3SL6HaXl1buHsbzUfJWKt7YPP4ZrE4oe7F12Ssd+/9+7RXtCrto4pw+OY9WA0P8x+VcZxS92Lr8c/Lt/fucIeB8QGbe7e2c/sTDI+YrlxmHurkrfz3IuOzVxpLxxrOn78Lav405Xhi/TYb+ezLk4a6EqSykdDtWoyuVvT51qJRDby2zFgobPmd6s3rZcPTZenncRtaS+ZBh1dR5EZFY4T9nq/U18ZKnxXcTHKhVPmh0n7Nvsq8r1T2uly+GtZ9peK2ZBt+JzrjksuJF+38q3Gf8vPf0qk/FvWn9JHF4tIuLe0u1H7Sko32/lXSMtZfPv6Vlj4tu3/pM4c64vuH3EOeeVDj7a3usvHfpMtPsuR9pOynEBgXSQudsljHj5/lWtPPNLx9l+3WG4X/AHPiksi+SyLIv2HP2VNMrFtwvFystxIrBMlQFxqODgbgfz1poea4qZ7jtNxGeZAJAIo8Kc48OfOucy1C/wAGm7yQAxYROikjFIlqXqn4jGkSKmN9vE3Ktw56WUZLW1aaRt8ZJO1aNPC8Tmm4hcHQjFAxOw5nz+6uW22B2ltZF7O3ayLjYMvuDyppuvmHhF+op9N68ln7DBMzVIrLvExEuLgczU0k3iPKAzOdKKWJ5YFa05zeZ+LQt+BcQuMFoxCp6yHH8fsq6c5t/VP+Gjb9nrGDDXU7TMOi7D58/urfHbnNv6Y/ys/pbhvDfBaQRiTyjXUx+NWI0xf6e+W2lK945dMuqeSOzjP75y59lFdopazw367Dj7U+phSXsd1cfqpppQB4jKAN/T0rnkxxV26LrMma/GyI+MPZTNFbqO+kGDId9I8gK3ip228fqXUbye3D1/BLV5OHRSyZZ3BJZtyd67afHa6WgA3ppl53jnYGbid097w1gXfeSLGTnzA57+mauzbI4V2W47YcTRgyR6HBZd21+hXG/wAabNvYXPAjCxvL+do7oriOKMAMPfoo9Ovl1ojPuLdLyJ4ZUXvNOCDycfz8qy0+f8V4e3Drwx79226E88eR9RXRFRTg0FuKYqQ+Mn6u/kag5wQ2M0aSke/n7msyLEMQVj6b8udZkPjiBO6++9FW44FYgb6eWCeVA9Yj4SAMDY+tZ2LKQH6qoRnn58qbFiKHvEC6yrEnB3Kn8qA8qpClkxkHOOZoGCIsST4hzHPbH3bUEmMEkBAo6YJxQHGgDEjkdqCVwckDbPI0BMrDk2xNESAWYg5X3GaCSvhOMNjJHTIoomiCoCxPLYcz7UExq7jSsZJAzjGDQQh04y5XPXFARj5ruRz5kYqoN9AAGpVJ5nmTQCXLyK2eVBzyFm/rC22DzoCBjWMltRHIH1oiAweXYsSTg+fvQWiysSA4APU7ZqjkDIrAYA54POg5JJd1yQh2O3OgkNoTAVQ5Od/x8qAC2jcjrsRuCaKJjldZYbnfb19aAXGEDgeEkjx5AJqBQkJXSmdOeWftoJ1srqy77DDauVJXfFXsbEW/aGz4pCdDJLpk8nU7N8cHNIdK33D7Zw2TveHwt1C6fltXWHnWqo6gCWGKeJopo1kRhhlcZB+FBh8Q4XbWEUf0VO7jJI0ZyB7Vi0D4V/Stadx2tE4G11bo/wARlf8A0irUeLHKtNLCXk6KF161HJXGoD58qBi8RYKQYId+unepOpWtrVntOmjacZjjYFHmtW80bUvyO/21znHEvdj6/NTtvbag45LcKFmW14inkdn+R3rnOKXtx+o4/wB0TX/6Gf0Jctpbv7CU/suMrXGcUPpY+r5fCYn/AO0PwF3XVbTQ3Sf3GwfkaxwtD1xmxz86qE/BnU/rISh/vDTTdoZtg6fJ4Vn4fPEfCzL6HenuR93P9HMf9dtAxdIdwH+ym6Sa6nH5+oSXksXNJE9jV478H6iY+VJMF7E/1xG3+JcH5jBrUcqsf/zZPlEGI8OdQEkR845Nh8/zqxltDlfoentH4et7Ncah4Q7ScV4pxIHBEUThjEwxjlncg+wrtGXfl8zL6daJ/wCPu0p+3PCUZ2trZpHY+J9IQv6nNT3KsR6V1BCf0gNqwLVwmdh3wY46Dc+VPcq1/wCKzHp28EjjvUEC+cm/3AmnKGJ9Nz/g1O13D2zm5gJZgM4I+8Ypyhmegzx9jeOdobO34NNLbcQt2nOFjWB1Y5J32B5YzvS3ZMPSWvb6onT53d8Vlu3YzzvMWGMFvwrjytL7lem6XHX6IjajH3jKEWI56ZrE6dqc58R/lo23AuIXWCY+6U9W2/jSN/ZbWivzn/DVt+zNnANd3OXPkNh861xmXP3In4x/kyW/4bw3Kw93GBtld2PxrUahrv8Aun/KjNxueRNcMWhP7WdtK/bzrUUtZ48nXYMfx7/2ZFzxmD/n3El037kXgT58zXaMWnzcnqWW3x7f2Z83HLggpbKlsh6Rjc/HnXSKxD51slreWezs7FnYsTzJOTWu8swvWq9zbl9JLNyAG5rzXnlOn2+l/wCDp5y/ls8A7M3N9clpBhz4nY8o18zXaPD4uS3OZn8vfxRR28SxRDCIAq+wqsGZoOD45GgsDid6q6RdTAf4zQUZhryx3JqDK4hLFaRd/I+gIdjigwu0trHfcL+lw4Ph7xT8N/sz8hVHihWg2M7EelBaCl8EDmByrLSyluNJYHJ8hWNqYSI1BOST6Gmgy3kVicRszNvgEbb0F5QxICDxE50k5wPU1kPRXVSGA15/Zyf+1Bc2KLmPA5DGd6AkhUIEU6c+dQMSEKzagvmTgk7UDdBVwDsQNvb3qo5Yy2ZCyEeo3/iaCSGJBRtRxjAwAfOioK6EXfORlsjl/DeglQVbUxICjONqB2QUBB2OcnmDQAI8EKuARvudqDn70x5wccycZoIRiHChmBJxvtnzojizgHIKbZBG9AMsrKAWQjz51QiRznUcg7YyOvSggTE4AAYaTzyOfWogknOSMeHzPI+VaEo7KASoABBP5UDO8UnUVO4/eoNPSNfUkHbfJoIBOD4jg88UEiQsME6VxyAoC1KRkKW1b6iNx59aBThHyJQSo5aTgE9Kihzr2yTk8/TyxQThCE0sANzvjeiEABFBALH0GKCW+qASAcdeu330VxMjIzAeFSeY+7pQfQOznaeCa3SOKRWc7tC/hfVjfHn99dIlmXo4OM2E90LTvxHckZEMnhZuuwPP1xy61dov1R1BmdoJVg4U0zqWVZEB08xqYLn4as1LeB8b/petV7vhd6RsGkiY++CPuaswPmDkFiRjHpWlRQDVBZoNTg1ml04DDm2PsrNplXql4M/dAR3D6f3Xw6/I1x5y1Cu3CbqE6o4sH963kKH/AEnI+2m6y9FOrzY/E/5cvE+JWfha4yP3LmMr9oyPtpw34e6nqO/nH+FiPjKMM3PC8r1eA5H2Vicc/d66dbinxaY/ualxwK7OO97tvJhiuc44e6me8/G0T/ZL8Ds5hqgulwfOse27R1Nv3VVJuzMxBKiOQehpxtHgnL09vlVRm4DPCf6p1/w05W+8J+n6e/xkmaK7IAadzjH1hT3PzB+jtX43J0XijAdflTlQ9rqo+8BIvPJMU5UZ4dV+Idouz+4PhV5Y/wAHDq58TBiWV5KcayfRVqcq/aD2M8/O8LsHZ24lwZW0j++34VeVpZnDijzPJoQ8DsbcZmk146DYU478tRMR8I0eeJ8L4eMRKgP90bn41qKxHhi8z/6k6Vp+P3TrqiiESfvynSPtrpFbz4eO/V9Nj++2Ld8ZRs99eSTH9yEYH+o/lXSMW/k8OT1O8/GOLPk4u4P+7Qxw/wB7Gt/mfwrpFK18Pm5M98k/VO1GW4lnfXLIzt5sc1pyLoOpHc1ExsyGMyShR151m1tO/T0nLOn0HsvwOzmsTd3dwYyzaURY9TaR15gbn7q40r3ezrMn/ovSKYYYfo9rH3UIOcZyWPmx6mu0vmQ7NVFO44zw+0uktZ7uNJn5ITv8fL40FvXnrQcWoALbUHmZrgcYHEEA8MMjRL8Bz+eaAOFDv+BNC+4jcrv5c/xNB4V0McjIeakg1oSn1vKpA0bMEoD5be1Zs0vxwLjWgPPxEbAjasKF7e7EuF8Q9unSgm2Ets7Lp0HOlg43Pzqi7DN3rZZVUrzyMDy++sjTSORQoZTGSpI9V6H22qCY1IwGy2ACCTsetBYTDHJ3UjbB360BDSCMoQCccuVAenL5HPpnp8aCQulXzyU7Y8/nQTHHGUyVYN0KsAAfUY3oAGVcAnAG2xzigERE5yD55B6VUMMbSnSiFjjSAvP+NATRnYBsqOhz4aKlVDdBsMc+ZoOP1QuAVIxqI69aIWuAzEjG3Sga8avp0A75Jzvj3oFOqyOCWDrjlz+ygERIm+tiCx3A0saqOkjjBChQwY8870CWiUShgjaQcHGxFADDU5OfQHz9qDY1fq9ShdueFqiTOWGnAAGR9UA0C1Ug7hjvzO9QFhyBoGuPVjI5sfQc6AGOGIbbbO4ooGkUjGsDJ+s43FAMeCA2dR5YxQTJgqrgNucDbbagUdRi8J1kE6sDYD8aBQlLeMHBP7wG/r5UHTM8USqCwJ8Z6L6e1NnhqWPa66s5o7e6Vb+BD4TJ/WJ0BBrUSeXuOC9rouIyqLW7SVdJ12040zK2RyPJts/ZW9pp6e3voLk6VbTJ1RtmFNslcbtWvuB31rGP1ktu6p6Ng6T88VR8r/pIiXiXYX6XGMiN4rhceTeH/wBdYR8ZAxW2nUEHnUEig2ezkoS60t++G+8fiKzeFfRbdEaMYYE159NGmAHaiFSWwIwVBHqKDLu+CWrgyJGYpD+1GdJ+yrFtKyn4VeshIkE6+UqBvt510i2zlpRkjmtm3t2XfnBKR9hzU40eqnW56eJkyLi00O4u5o8f20Zx8xT24+z2U9Tv++IXoe0V2cfrLeYeQkA+/FScdvw719QwW+VdLR4xFIv6+wf/ACpqH2VznH/D0V6zB+2+i2v+DMPFEyn/AAMKz7f8PTXra/8AuQUbrhWfCh/835Vn2/4dP1uP75ILN/bIcx2bH/wz+NWMf8Oc+oYP6kPxuVF0iJIh/ekVa6Rjt+Hlv6lgUpuPNya8jUeUSlj8zW4xPJf1Wf21/wAs+fjEbkk9/P8A/UfA+Q/OukYqvHf1DPbx/pWbi9xjEKxwDzjXf5netaiHite1vKpJNJMxaR2dvNjmtblmJ0CoO6UHUHUDI7eSU+FT71JvWrti6e+WdQ1eHWBaZIV3kkYDNeW9+U9n2sPTx01d2fRLeNIIUijGFQAD4V3iHwst7WsdqrW2JjsnVVHnJOx30ri8t/PfxmKSQucbuo/d0889PL1oPQGaNEZiwRE5ljjSPWmkV7TidnfhjaXCyhDg6elNB5ag8/2esnRuJF0IU3rqCRscf96C3aWbW1m6kY174+FEfO73H6Quccu9b7zWmi1+tURtcMjYRjVGArbhjXOzTUSIqQw8LDbbasqsLEpTRqKg7gHnQNjjRh4gI/MdfuqbCjaIzDYhgdWQAKbFyNBEE0KSMFcaenSgYsShSPEu25oHxLhQMls8jyPzoJ7tSuQGznGdW2KCUyRnGPw+NA3I0CRxpTl86CQEOMAD1zQCI8NzOUGTnrQEybFlQAtyB39aqIXGrI0EAcuh96CcMwLIuMDn5UUWouxJJ1DlgbE0AKHD6MchucGiOIOHVguCc4A/nFBEQxjJ2PnVHIjJs2+NiRQS7Su4OvVgALiiK4jbIAYKN/rDNAUqtLoeR1cjYjl7UC5VJxpJXfAC8h7UFtmV2DZbPXNUQSuAzHfOMY6UHM6kAAAMMnJGfag5Y2kYRlwDj6zELQQoJU46fW35j1qBbYkTWR55ZcbUEqxC68eA7nGV36UUsOGJ3xtnfrQLkZoWMbssbjmCcH5fCgryknYSMQRtk7tQJbTJJgEsTsRjI5fz8qgSpiDqJWUFRjUuE3/HArQDvwh1q7YTkQcex3OabmF7t/hPb/iNiyQX6pewjAxI2JFHTDc/nmtRMSdn0Ds520seJM0dtfmd2bP0a5IWVPRTyYZ++teE4sbiXD3ueyF9w54XjYQyxpG/1gEJ7v7AprG2NPgwmI8LKGHrXRRARSfVfQfJvzoFuNLEZBx5VPJ4nutxRJIgLL8q42tar6uLDjyV7mJC0LaonPzwR8aRl/KX6H+iWlacavLXAL5A/eH4j8a1utnjv02SvmG3adq8riWM4HVTkCnCHBq2/HrS52W4UHybY1maSLLytNEVjdW9jWONjZNozW7SpceFGGQx2Ga0bYfFGR5GEeSAmB7mqKcUOBJEcku2MY5jnRp5aTwyMB0NdYZ25ZZF+q7D2NaNmLfXS/VuJB/mNDafp92f/eJP9RobKa4lf60rn3ahsBJNDbs0HUHVBIorqJPYSoznCqSfQUarW1lhLBzu7BfQbmuU5Yh7cfQXt8uyzHbRR/s6j5muM5Zl9LH0WKnnucMZrlL3V4xPZr9n3gHEcyOA4U92D1PX7Pvrtjq+d6jm1Gnq1fau8PgiaRURmdgqqMknkBWhn2faCw4hctbWspZxncqQD7U0NNRlfDlgP3VzWdjP4tw9+J2z2iTrAZVGWO42IODj41pEcL4HbcDtzHHcCeaT67LnSoHIep/hQXS1EWbQwlXQyLEZBh9Skq4xjfG4OOtBXvo1s+8BmWVIlLFxndcZ6+lUfJXYu7OebHJo0hfrUlHo7IpbQpHJ3gJAzkcjXOWmmsKMQxOFDZ0gcsVhVhQQ2SobAwMbHFAwIDgoNzjGRsd9/wAKBhi1Mwwu3kaBgJZSAN9OPDQEo0LpJyeeqgZkMgDeHHQdPyoJVteAoACjBJ96A0wMKx8LbknfHmao4aU1LnC+YG5FAYCov1wpIIzmg6OPUwUeLfbzNAWkHkdjsduvSg7uXcAcgObY5UEKAXwG5fDIohgEccWtSuS264z096ANQJAyRjGBjnvQTpIzqCk8j1yaCHRCFKHPnnz/ACqhfUtuABgkedB2BobBByeR50RzIZCuVGQOWwoJ7vwhVOf7uMmggoCis2dhsT0oO1a0LHYjGxHOqF7tuoK4GSc5oCwhcAvgY54oO73UAfENIwTjnv70AyM2MAgMcbg7j8zUAMPEFDHJ205yfhQLfCkxnKsTjUQMA8qKUrhpNLE55+9AJjIUkAjGx2+t6GgS+pEKliFUkgZ2A9B8BQL/AFju40O7jLYTfA+H870FVgCzNpC6mIAUmgCUoYyUIJ1dAD8/L296oryLL3Su7Agr+8f5zQVncq+U5g5575rS1nXxek4V/SLxfhqCC7K8QtwMKs5OtfZ+fzzRqZrPyeEngYyM6rgMSdPlSLNThifirFSvMYrTjNZr5RV7JJ0dw8fqPWsWpEvRj6i1FuK5SQ4+q3rXG1Jh9LF1VchurzrGpl6uWnYUnPI+YpFphm1KX+Qw0gGNWoeTDP8AGukZJeW3RUn4mw3s8B8LuPZsj5Gt+5WXlt0V6/GWlB2nuYhpYq48m8P31r6ZeW2K9flC1Hx2wkIM1sYt86tO32ZrPtuaLm7sZPHBKrMF/eBOazxaePvAFu5QuCAxxiutUIrUjhURNB2KK7ag6iOxQEkbOcIpY+gzSZiGq0tb4rCWUp+thR61znLEPZj6LJb5HpaQpucufWuU5Zl78fQ4q/I4EKMKAB5VzmZl661pX4u1eVNNc3bnrTRtIGN6nmWtanYOHO0vFCyH6uCPga9Va6h+c6rL7mXT6DG+UB8xWnnRdW8d7ayWsrsiSqVLJzHrQU7Ds9w/hL97FcNcS4wCFKhfXJ3J/neqi1f8WsuHRLJe3DLnZVVdTH2GaA7e8gvYFnt8923LVz+NAzVQRq3oLNrEZJAOnM0GT2svBbcDuHBw1y3dR+3X7M1R82oH2MBubuOID6zb+3WpMq9ELqSOQIY+8I2AGdsVzmGmrGNaCQtgncAgbe+KyHaPFrIK9VwaA0y2TqKnfGT6Z/KgYoXTjxKQdztvQHpQDQgxjfV50E4CAALnGOuaBiaWG2NyARjf3Hyqg2jjV86mztkg5JoA7sKQDnAO2qgaQzDTITsM7j7qDgybq4JAOwBxQGGw2BuT5+KgApkE+W4z/GglyXkbTgLuQmScD0oJjJyNs5HIHFVE92i7qMoeeRg79aCFjRSNWpVIyD5/CoJZhqJwVVuXpQSquSWJyzbE8ifzqiNbYA16iD7ac9aCQYwHDZIYbDG+elELcbDUAMfAYoAwV3RiBgn2qgHC5Gogkb+QFBwc6Szk+EbAmgDGSXKlQDthqDiCgzqBHIb5IqBfforBl8QUg6gOVAbHUSNAz5k4+fnQJGUYuZCHxzxjHKgmSTAwQGIPxJPnQcjxr4z9cN9U7rjrv70FcskiFmfYjdQQN88qKVOGkUlQSyjGxJwPKgUoURMxLBl2QafrY233qhE4ATTkEeenTnfegrythcYXAB+FAmaQOCveDTnc74oKzsH1gLnVg6jnIqiu66GwRy6da0EMTp35+dQi0x8Sy2R41BHrTi7Rmn9wTFG/1SVPkam5jy37eK/jsBreRd8ZHmK1F4lzt096+O5W6nO4q+XHvE7X4ZO8jBPMc68966fZwZIyV1JU7SRtlW2NarES8+e96T9KFuyPrLn1FanFDNOtmPkclyjftYPrXOaTD1U6uljAwI86zqXo5xZICg5GQf7pxVi0wxbFiv8AKAlCf2gf8Sg1qMkvPboqT8ZV5LRycgqfbat+7Dhbobx4ko20o/YPwrXOJee3S5Y+wDG45oflWtw5zjvHmEaW8j8qbhnjP4EIpDyRvlU5VbjDknxBi2kx6Y96z7lYdo6PNP2OjsVz+sc+yisTmeinp8/eT0ggj3EYY+bb1znJaXsp0eKvmDDJtjYDyGwrG9vRqtfAdeeVNLz/AAjJ9qrO5dtRncOzVWJEDWXSJa/AuFHjF+LbBKkHPv0/n0q08uPVZfbxaW+EdmWsL24MsqzYcqjqNioP1vjXqh+bmdxttg42HKii1UE6qCjecD4dxa5jnvrieMxDAWNAwYZz1IxQXFSCBBFbRmOJdlUnJx6nzoIeVUXLHrgADJJ8gPOg69W5s7E3ZVFC48L8yT026/lRF/gtxJecO2hKTzvoA/u9T+HzrQ8J214ol9xf6NbvqtrMGNCDszftH57fCqrzZoNLg5ETvKVBbThSeQ86xZaryT3BlLlyA2+AdvSsw1L0MRbuo5EVQQNQ+/41gOWIB2zlcDbkQd+fpQEIlCZycauflzoGKrFgoxpGcg5FAQ1JqCsNsYAO/wA6oaqAZSRMEHcE0EryIU42w2Bk0DEwyszsAwwMbbjHSgiWYnJUMRuCeWr5UEuqYXCkZwaAcjOcYUc/Sg4NpyACTnmeeKCVkZ10sjjrzz70BBCoOkMV5kjeglB3khwQoC+LJGfLb1qoh3OsRgnAHnsaDkVmAXIG/wBWgJ2IJTC4OBqYnwkUEOXcqWc7LpHoPKgkMxVhGxCsuDjqM9RQdlSQmgIVBAI3yaIB5UXZ107/AAx5UAmTHhCjOMY01QBQOmR4iTswNAkoNZYkthicgnHyoCWRnySxYk5B8/WghAdt1Bbffr6ZqAW0IQhCkZxsScHljPzNAPjGVGGGcZyR+VAt1BOzDbmTvjzoBbR3QIAB5gY5npvRSnyVXDAjOMHpQInVpFCiXQOun+edULigkiZ27zWTzboR8aBbljrVn2zuwPP4fE0CXYnSpLNoBPPlQLCBvEdOnGRq+fOgSw1p3mAASSMDagQTp5nb0wDj3pCKzAaSWz6VqAmQdN8fdVCyNRxyHmelAsgryO2adk7uWR1I0mnGHama9R98jbSID6iscZj4vR+ppbtkrsSLGpLRud+lZnc/J2pGKneltiYB1wazHZ0tEZY7EPbSJ4iNjyNdott8/LgvWexRBHMfOtvPPdIJByrEexo1W1q+BrcSDqGrE44d69Xevk1br95CK5zjn7PVXrYnyYtzG37Q+NY4S9Feqxz9zFcHkazqXorkrP3EGqadOcJzTRuEah500coRqHnTTPKHaieQqpyl2/nih3dsPWh2cWocoQTV0xyRqHWmmeTgaS1EjB2zWXWJ7PTdkriKW2uY0AEsUgZj1ZSMD5EH513pXT4vV5ecvWXMMMFrGluWcSqGeRhjP90eg+010nu8cRqGW4KmoA10E6qDtVAS5YgDcnkKI2LThMNmDfXkikoPCxHhjz0UdWP843J0M65D8cug7K0VlCcKo5k+Xqx+yrKTIe1PF07NcK+hQELxK6j0hV/92i/M9Pn7oaiHy1jQQkbSSBVBJNBrfR/CEjQ6U3ypySQPzrm0fbxOgXwltRwfDuN9qD0dnE0UEUZOpdjjG588VzVZKqBqBJOT0wfWgcqaR4cFQdzjBNBOkFjhd/LNAxV3J09BgDegN4mjJyDlOdALYU6VYHA3xQEGDBRpUMeZPP8AjQCxwxZvqjmBVE5dsBd2OcHPWgkGREZ9Axq3OnwnrzoiW1IjtsfPblQCScA42O2R51RILaQSoJON+ZoGt3bfW+sAdhtQKChCqkZ5g7560BKQo1agjA5BHOoDQAa2Ygr5k8q0FmJRiXRpVttR5etRBPh3JjjVScaUByDt55oAV2DHUoGc4HQVQLlW23C5zgY3oIYYG3uDQEFVm0gajz35Y60FQsgOlQPEMHI5URzosbHUSSQN9xigl1yuAxAI5gYJ9KKABc6dITTyUrQcoc40ooCkgk7ZqAWZy2UUgMMMMgfz0oFEg5ydh1AorlAZgXAUHGM7beZPlQC47ptQcfWyPDz2++iEOyAMMga+eobk7cxQVmj1HIzhTzG/pnNVSRHqVUxoAP7u2PWgBkJbSpD7beefaoFsiDUVYkAdRigTJAGTwjLA5PtWtiu8eokAZOenLFNhLRqQcNpweoqoXpzgqaBbjpjNULCZBJ2/GgWykYPSqI3HKgMSMvMg1Jrt1rmtB0d4yDGdjzB5VznG9Ves15H3tvL9aMA+a7VPrh2m+DJ5gJtYn/q5RnyYYqxkmPLnPR0t8JLeymUZ0kgdRvW4vDz36TLXzBWh06H2BrW4eeaTXzCGB6j5ijGkDGdjj2ajW9LMEpPhbJ8ia5Wq+j02abRo7Nc3s5Rt2RU0u4dkCmmtwnVTRyRqppnk7XV0k3AX9a1pxnKEyeVXTHuO1E00chrvWLO9JDPMANKnetUq4dTn4xpY4HxNuFcTjutJeL6syD9pDz+PUeoru+U+p2M9vLBHG8oa1uF1wTjkM/zv5GgRfcPlWeS1V1SUKGB+tseRx1FTSbeVvOzPaQSmSHiiyjoNRT7MYps2ucOteK28BHFNBY/UdSPiCB/O9BoJA7Y2oL9jw+eWZRDGXkz4RjO9QWZLO7u5I/0lqVwMLbRjDewHQep3960F8c43Z9krcKyxTcU0/qbVTlLcH9pvX7T9tWIKw+UX97PfXUt1cytLNK2p3bmTSZamVIneokvScFsPo0QuHx30n1QR9UDp71zs1DX8Dj9YmGwRgIDkfAZNZU/uLWNo9CRa3AYFceH8uVA0MVLooYMThSx8OfKgsxsFIc5R1GTldvSgYHChhpGT0O3yFQCXIz3gAwMAg5HrQFktuyk7eefhmgPcrsFGdgMgfzzoOcL3fiVTk8hnb36VRKDQc5yinbPWg5fEwOoA4+s3KgjAOCoyTtgkADy3JoDeaVIzGpIQn6mrK/Zt8aqAVzGVZSFYEEb5oCDSairKuW5nAz5+VAZQhiTkr6jJoBK92Mch15ZoIVyEJwMKQdQ5igEhsga1IJBJA2oO7xyrAY07nOBk0EoNycYDfVU77UQbAbYxywCOvwzQC2zElvCcHcbjz2qidwoU4yRkgc6CVXELDcb5yDQD3eqVQAuo74z9lBUyoCaH8bbksBsaCQMxurMi6SW5fXO22aBaKQ+5UenSg5nxIG5ZOxTagCSTw6i2Bnfb76CZQXi2ZiEI8A6DnQBGuqMsy6nJxz6VNgRg6lxktnCimxWnQvjVMWAAAz022oK7KTGXLatWPCB/PpQNIZXKIgZATuoIIAHrRSHg3CsxGwzvyP8AJoAWMK6sRqQDJXHMZoFMNAZlJG2Mk0CWjlAKoScctudUA4OxGR6k0FdlGDhjknHLbHrVQllGCwXAGOVaC2AO4670Cim2P4UA6NPMZoFsvLAoI0bmqBK4oIxg1dHhIdl61nTUZLQYlw4PMis+2706q1TRfMRhyGH94ZrPtvR+s35GJoG+tCu/UHFTV4a9zp7eawgrZn9lwfcH8Kbue30tvyHu4VbKZz603ZPbw1+CdVNNcvy7VU0e4jV600e4gzKOZqxVic9YAbgdK1xcZ6kpp2Na05e9aUd5nnWtM+47vMVJqsZBidR0JrPF2jNWENcsRhRirFGL9TMgB3rWnnmZsYhxVR6Ps52lfg2ba5iNzw6RsvEDhoz+8h6H05Gg99ZLDxa2E/CZ14lAg/qx4ZofdeY+0U1KCJdSVF3LCw5rcR6sfHBP2Cm4Az263KqknELfSDnaNhv8FqaNNm3i4HHpWGO8vpMDwomkH8fspo0sXnEpeF26y3C2nBrNtjqkAmYeh5npywaRBp4Xiv8ASAlpC9r2fjKyMMS8QmGZGPXSOnud/QVrTWngbi4kmkeSR2d3JLMxySfMmoKjP0FGWxwTho71bmcA9Y0Ycz6/lWLWaq9CsYZWRQGy2NW+sAdOeP8AtXNqRRqIlUMXyGyDnY0F1WWWQyZDdANwSBsPxoOWXvUYOpJbBG/PH/agspHG0JYkDB0kE/ztQKZs6WGQcnGfuoC55JyjZwPP+cUEpG+p1AJPMgk+HFAesNhSvNs75x8KCZJFjkADLgjmNwd+R9aBhCnSvNSPrN13oJQiI6V0spGPb0oIkUs+H1EE/W86CVlYyaRGJMbYzt86DiquuTpAY8tRJA+VAyRUBYwF2TbGpcEbb53PWqgVJdBGA2c5ODz+HzoIYqW0vnRnmN96BiKNzpUkDABHp5CgVpI8YwT7bbUHPo3TDYx9tAIUgIpc7dKIgPqbxBscgNqA1lAGrUSBtv0qgA4TJOCdW29BDuQRgjHkaBjNlcll9id6CiGSIaiQW8uWfSggTF5HaRR0GkDYUEKT3LbAgc9uVQduYxhjgDJXb7qBbK2FcBmAGodd/QD2qmxuJCmpf1hzuQMj+JobBCzlFAJyTkgrkHf5GoClUx5kO2QVcZG34CgpEGPToQEMcgZzkUUQhA8IPd5GNWMnpQAy6wkYJ2JGeYPryoOktwulJCjsTnWGBwMUCGiAAY+Ig42BwP5G/SgXpBmGrwId8nxb0CgPCV0k431DnVCpELDlpYbk++KCo1uVOtnLDqKqBZSCQE+dABUaMjY+VFB3a4yG8XUYoFEAg5z860FsnUCgHS31dO5oiO7y2w+BoBKEbA59qoErigHFERigkA8qK4Z86Dst0NNLyl2Wx9amjlLtTedDlKNzRnujB8qCcUEYoO5UHYoIIoI5VRINAatQNV8UFm3unglWaGV4ZU3WSNirD2Ior01p/SH2jgjEcl+l4i8lu4Vk+0jP20F8f0k8VAz+j+Dg/vC03++rqv4Oyrc/0i9o7pe7XiP0ZD+zbRLH9oGftpv+Ds85d3stxK01xM88rc3kYsx+JqbXak8vPNNM7ILM7aVGc9BSUlrcM4WRNHLOmvByYwM49xXObNVeijtyE5k6sFgRjbP8Kw0fEpACj9YoOoaTuBn7KAimpgVYFsnO2OtA9NRXBUAtvuDnPX0oIZnbwk7k5Lnc/Cga6Kp55PQ885HP0oGTS65VOlMf3Vx09PhQRGQpPjOnGxABIHlj5daDhcTyRmVdTMoABJ+rjpQEAurUzCVRhSudJB/D+FBFuWVgzIHdRyOd+ftQSbkr4dZIOQdOwAPQ0DI9k1a8g77DNUNBxhlfTgbkelBCQjDaWUAnn0+dB2nwA9MkA7UBBgFyGOGGAuRyogQoSMgMcHmoPOg6MpqOtjnqcc6BhYhQI21HG+BzFBBJkG7ZI2C53NEEZFaPu2VURSceHc/GgVIuh9irjzNAtfCTqAXGwBoG6LcRqANB5E5B1euOlAoFDHpBDA55gfDptQczEFQQpIU41HPP8aBRVhhW07DJBPOgqMuSrOAAv7TfcaqIZ5EUtG2lmIBHXH4YoB1yJuzbtuRgDTUUbKUGw3I5ig6RikRjV1YhcjBO1ULV5e7YMds5wds/Cge4ZGJKruMoDkDFQS6ygkNgMc+Ek9aBJiaJiH1KeoO+aAWYI+cAqDnPU0AOCvNMEHYjegWQZBHnxsTgeLagUyESsGGPEck74oAz4Xj1kBtgQ22M75FAnTiMsFJ3x1ooHiaRV2+t0I+4ZoEmLKlGQEnlzGKoU8ICtqyGB2BH3/ZQBhcqzKQAcHB570gJmQBjpAA6AHJrUBbW7qmTt13GKADuBsOVABQHc6gemKogxljnO+POgExnGSOXrRAkY3Iz8KAO7yc0EaD15UEaDzFUcF86CdHixyoIK6W3G1BH1j9XHtQdp+ygnQegoO0Zz50HCM+YHlmgggYOF+dAJG/OiI0nHI0EEY9KoHHlQSDQEGoDD0E6/WgZ3+2+dqAO996ATKTyoGQ2stw6jBAJ54rM2G/YcHEaAqo1kblj0658hWJs03bWyj7wGQSKQAWZRnBxz6VkTpPiGoKugqevvvRTo7SBCZI2XVgYI2JPxG9A1Y+8ZQy4YjbPn/OaDgBrwo5g4C8vegMlliDGNggbc6frc+tQdIgKpJojwCVIL5J9xQDoIVZDliRyB6fnVBEsUAYbDqBgjfNAOe7DJqLheTADl6HY+VA2RypVSFffOcfW9/soIJWNlGCU+q2Rgg+uDQdPEYmkilBBUfvZ+RBoDR/AWwD1LFhjeiGhtSA68q3U5oIOFwVk0j62w00UEupSo8LciWQ0BOuGXS2fUfjVRytoYPkOGGN9jnFAeJSoYBcAZO24oiBoYjLEDbfH4UBxuWbKldRz0GN9qDi4CeJAFxjPmaCBgREKQy6sKM5IFAoESKQvQ9aDnUv4nYleQ2wBVFZopUA0Nr88/wAKB8LsUOpTgEZ33HtRISwUgEat+WomhKq1rAr6VBkkUZI2I+ed6BQcLGQ0SLgEbDck+tAyS2jdo5e9bUw2Zwdx8M1FLj3ZQyhtgRnl9hoDKbkZzgHYDlVR0QVQ/gztnOelAXeKWLt4nIACchnHpUUKSvkHVnOx60CZCGcgsSw2BFBKKFly2MEjOTz/AJxQQ4JOCAgAwufKgiSEBT9Z0VioYDA5UCpYhsrRnfYdB/GikvCI5Gw5yT7/AG+VALKQw0uQwxv9woFPEoYg6tjjly+NAl0hIXVGSzH63kfTy+2qAaLdcAtqGwIOQfKgCRdgjLpbBGQPWgU8JQnVjbYEGgSyEnKtkeu5+NUC2JAvgUYGNhQLkA5hwNhj39KqF6PQnzIFBGOnXzNBGgatR+r5UAhNzsdueao7A3Vvq4oICh9l2IFAHdjT1LA0EY9MZ5ZoDKE4AHT50ELHnmwBz5UE6Sn1OZ5nrQQYzgnoNjmg4RE7nmennQSYwCMk1Fd3YOyqfeg76OCdOM58udNokW+F1ZII2psDJZn3PnV2FfR9JAORn0pscbR2+qpNXaaD9FkyBjny9abNJ+hzgZ01OUAxw+6Lae7pygOTg10/MqOXWnJdLcPACGHesc+XIGs8jS/Dwa3TU7RAeLl5em9TatC3s0E+UiIw2BvjArOxqRxrE3hKscAeIYzv5b0Ux4u7PeFT+sGcgD7BQAoidsE6QQTgDmMDagJowUAR20jxAEnHwoJERyH8LAfst1HX2ogooWkUd0jnRufFvjcbUDUlHdFSNsAaSclf52opbEjKIo8y5XIJ60ARRkAHXkbELyx6UQzSSc6wWGxKbYAA6DnyoOKog3fUXzkA+nU0VwVmXbUunkDuPgKCUdVOruw5DBhgc6ApDIoBOlM5IxyOOozz9hQAmlwcDHueflRDDIQpVGwNzuc4I6UEuqSQFpAMgDGnYiqFrG6+DDaNWo5PpQPEqEBnjVNOeQ5ny57URAJZnYSHGxIdjy5UBmebumQs2knUy6/rHzx50UEsmrUwJDdPLHlQcsymJRpLMD1IwPhvRCypZSRkjoMUBnRHnu2zjfC7g+o2oOkZigwOXPJ3oOE8ZAyu/pt/3oGaBIC0YO2wBxmgUygPofw4PTaqILlUKa2K52B5UFRHJU5whHnzPxogTH4gQmtsZCq1ATRLqjcPsnPBzqPX4UUIXPgBO231tl+NB2qNFLBxkDbG+/pUEIxlbQg3YYyWx086BfeNFq1hSANOB8qoknvgAhO4xk0Bo4wT3afI+GoDlxIVfUoKrjG3LB9qBDEK2TnWcAY3FALDUmSwDjbYYJoBaMogJYDJ29d+VFLaR+8LZA07ZJwD86ACBKiyZIx58zt5UAYDqNJAyNRBGM8tqBcqEnYbjfLZPwzQQYxJCCXUaeg2yPP1oBlibIdEBXONJ3xQLkhDyeEjDbgLnHzNABjG7EYGMb74oFsgU8hpHJtO1AiSEDcMTjyG1DQRFrBVm6ZXz2rRorUjv3eQZGOkDYb0AltJ7rChkODvvQc0RbBxgedNiUt2kcoq745cqbAiErtz8/amzQSMYAHuTTZoLICc/LFVEBCo+pt5nlQMWAyOqouok7DzqKkQqPEc48xQEIgFzyzuKAmjbSDp1agd875oOESFNjv5Y6U2aMS21A6hhVBINZ2aSbYqqtsVNXYaltlxkh13yOtNg5YIw2mMBgBgsDsfUVAcduqEHSrkjO42FAv6KzamUYPmRtignu44gDMhUk5BClgP+9BcghtzmNsd5j6hHOimfRwgYliQuBQPyoYeDw+h39s1BMkbLIRMmhhjGoEEDbfFA8xq/dx7mHYHUcYPXFQPhVAq6Q2MDlz9d/hQOMKNKC4DgAZKk5aiJjbOYySSpxkDJHyoOHhjPdknYY57+f4VVG8rsscUsjGMDUEbOFoJEB7suv8AVltjjbbpQLMOnT4dKY2I3Ht/CgNlclCqA5AAG4BGcZz8KggMCNKjGnGeWwrQhWHf6Sm/IAZx5cuvOohyPrRohnSGOkBds/ZjPnigXoCkkKAMeY3ooAhRGkeRg5P7W/qMH+edBMcia86dOsb5O38fnQGO5VQsiODqwG5AD22++gjQUlZe7TDLgb8t+g6HaqiHAU5JXK7FSMbnof40E6XaNpirvGjY1FchfIZ+VBwTwFjhxyXu8eEnfBx6Z86A44lLFdBbJ21dB86DjG8cmIRgE48OOfp50Ebh1Lp+1vk8z06bfOgsFSvjwMnOAVB++gCIKFMbBNTbE4O3rRAiIYzggczk7fCgYI1XxDxKR4hnO21ALSbBWORjAzjl5UAGNGXSTk9FG+3OqhOpSCAArKc5zQALiSRcMhRxscHIYdDjpQ7Al7z6yk5B2G1DsXpMLg5DEb5549a1IDxNJpC7Mcg7jNZkPldkhMXe6ivNskg+Q/nzNByRllbJ0A8lJO5oOIVAq4Mh5lBvtUBSpGsBkWJ0ZpM5HLT1A257+daFVlJJVRsAPrHlnz9ayoziJVHTHix50EvKmQkTNpzgE4+6qIZwHIMaN5NjHX0PKgiRR3QCac53OwJ/GoBdpEcKDoGMnzxQAqknIG/LI23/ABoqAqyIoUqQAVIIzj2oIniEYADLv15+/Og4xLpWNMHWcDBHr060ZJljGomNcLsABsdWKNAa1ZGCshG+cDmKCGhGSFXA6jUCfjQKFue7Y5wcgD1oIceIsmAT0xigS+GUD6uwBI/n86BbrIcMh1b7E4z6/bQZ11NokKeMHVhsEb/GtQKZINswEe5OWckfACtIiGJ3kMasdzg4zQbDQKojUnfHhNc1GsGh1JBZSNwBg0ANC2jUxIQ7eEkVQIt9CeIkrjYnk1BJRSwCKoI57ZpsB9GWQklhnVjC+XnypsAkYRvEp9uVA3ukYg5B6HAx0+2gloTpUnUaKYU14J2x5DfnQHHFrJCRsQDnHOoGGMOxYuSSS2dO/wCVA1EBVxqZiQdsbZ26Z/nFQTHEqo7hwSpwox9b4Gg4Qtqzv4cYGaoIC5VMCQyDciPpnyxj0oCWMvGhMOGJ+rjBPWgZ3TOG2Bzk6R0AO4GTkVkMhhQ4wIwX2UNjI/jVDTAml21IdLYAOc+/lQEqsrYfDIp5A75/k0ERAs2rW3h+G4/kUFuNnWRZiobw7h99WKAtA0q6KVUkkALtnPL/AL0DUyC2SoxsAT+YoIUsXGeROf5+VARVYlyygnqM5APnkUE6NQ2U5/acnpkUAJgyBMjTyydxg9fPzoGBXeMRa2ZS2cHlsP53oOB04Jy3mcbigJGTQwTGW3Ldc9MeVAorj/mZDjLZOc77/wA+dVAxQnU2JkPX1yPKgcmAj/qYyWUjxg4yeu3WoAWJ1JXIkby55yM8/OgGVcx69MYGwxkA++CaKlTFpGVAUnPw5Yx+VAxUifP60DA8LHpVQGSjnvYyxIwCD4SOW1BwZgC2DvvuOdASqELSALHJqyPFhRnzBGMGglfBMoyVJAYaSG39KIcrZkDEFcbs2OvtQGXzCRozk7vk59vKgkuO6x3eNj4gN8+X84oAZo2OrTlgMA7b0BIpK5xjOenKgBgWJWNl3Jxlef2VQhtYC51YzgZbYe3UUHCQk6nOAP7vPHLNQLkDRgl8aScYzsSP550CtZ30gq3M77H22qo4SNpXRvp+J9j+dAkhlIO2Phk/ChsxZxNJ+s1M2nYDbGT5+VDSBKqKcMVKHljpyORQc8miJWDDUdthigGNpkLSIxAfZiDgY8s0Vy65NPh8IG/pQMCEoxLxhcZ3PPFQJlZAoRSFOdl5ZoDRRFJyySM5G9BDRLkhH2A+ryHLpQdHG7q7YbSp8e2y+WflQS6KRhVV9gC+k5oATARgpYueeRtQLACEFF6k+fKgJlWbDCIR5OQWOfs9xQDLuThsMN8hcZPwoAY6UOlQwYajsKiuW2yrMrEtjkw58s4oFPAoVVRQ2NyQST91VEIAdIlfuwq7YUnrRolY1ZS5YBtWQRuPnRATvaWySo5bvlAbTpOMH7jQZT30k2oQIqqOpO+OVXQdb2IVUlnjR2YnGrfpvkU2HmyjcGIjTEMsVB28iQPjQA8IeYjEYXTkHmfSoCWMgjXt5GioZCVDRrls4HTNByK2kMTpYHfHOgLuDjZwDncHOo+1Az6N3cIkcEEHnjn8/wAqiI7pFCsultvEQCQPfOK0BjtkY5G+3lvUQLRxofAuNRwwPlRoQ0lwHxpzjbGcelQHoR4fCp7wb6tQA5nf3oGRRv3WSSPFuSedO4JojGqIgZTIpBYnY+madxPcxiOMiYmT9oDcJQEVIGhiVGMnfIO/OgcpWRVQk63K4XYKee/p0+dBOnXGToYA8scid8/GghokOFZGZSCAG+PL4/fQCIWjuCGP6vUNQYkn1PKgc6QgHSS2DhSd+vvRIc7yNjCpg7MRsfLB+VGoWIo1mTQ4Yn9nQRg+efhRBrAQDoIV1IIB5sNxz9PKgOUsqnvsiUHIXz+NBMZYIrYy2SDg5/nlQLuEnmQxCcrGckAjO+3yoOsY+IBnkY6lVVBVjknO1BalQvMoyx1DSQBgigIZOckA42ODlsDHLzoIyiSgqdKnrp/DnREvIX+pDkPuw329RvQGkSKgAdjvuCMfI/woogBsyEqCCDkdPL5UE6IxI3hAVtzvvnHPlRAvG8bB+8GRsuxO3Pb50HGNdDZBL4yABn5+lUSkIdF0kbDJ2wRighgk2owAq5OAmkb/ABoFrGyurFQB5bDNBKmdVIjkGw8Q1YPPrQEGZl1SeMAbb7fCgmN4UgdTG/fsRo6ADzNDZTQqdjpRm/e6e9DaVSEDALYxq0sRjV6bHP2UQxQNIy7KT0/hQSCxOhkLLjO3vkedAZXWhlDaWXAVM9KAJGGqR2YL5qMYqgTIozhtufvtQQHDsCWBBGMetQD3TsjEGPbfBO/yoBKlVbVgYHmMVUKkfVGMqdYOARyIoAkHcBSZFPhyNDZPx/KgENLD41JBJBJU7CghkfBcDWTywOfrQ2KRCqAxsdx4hnnQ2BYiikgnBGSc0ChDISHUjVqztQWZGjYKgj0Er4mxuxznPpUVBkZhq2Hhxyxke1BwJK+EDAHLP2UNCyo2wAcb+eDQ0E6UuFPhkVgNQUkZ9PSgJ1SRCE8KnOx/hQJAYIyAbOfEP4UE908R0yeuV1AHl0oO75Y8aQEUjTgnb3NFEx8JwMsdgOXX7fjRlxjJCt9UkgZDA9PyoqJ1diHDAkk5zsPfyFZC1xHHIrRAHBAOQT6VRG5OhgoIGVFAM0bQzEiMxsuVGfrfZRQiLZjqC5GN9tRz69c0ANGizHRGETOMMxPL+TQJubQSBDHrjckl2G2rHKgrx8Kjin1llx9ZQRj7RV2iz3LZUYO4xjG388qbaAVQsTg++D8d6iFANnSQ2gnbA2Y0EiNBJhtWwIHkPhiioOoa2VhqO2MYBFAsJ3isVYgqu4bANUNjLJ3UmpWYHPiAP2UDWzINXM6sYJ3HXaoiUtyyeJSY84JAz7b0ESLsNMr5BwRnBx+VFLaPvdSqFTAoKkkEiBiN8cxjOKosZLQKoCjLBsltj6UBFVwPHgZ5YJx6/wA+VQ7LNrC0ilyxCoeZ2UHG3PlQ7F28DxN42P6xTqCHBI6gk0BNbgiNWXcb4LbMen4UHI7qR4vF5bk/z+VBYCNLC2mVkjDAhDyJz0FAWY03kI0jfrlT6YoBaJX0NGHw2xJG+eo50ERJISUbGSB4tIwT60D1RCralCybeEbD12oH92piBGdtgynn7+dBPiR9Q7wMu3hoHCDW8cmAQww2Ry/neiJlh7ssvNdXJd+frQSIRnAX6+ygHIPvRURKkUWszEsSS518xjljPoenWgnZs6XKxqPrNuT5fhQGG3TKBio3XVufbb0NAKxCRzqbQF5F1wxI6dcURwQlcmQ4GxyRQGSi6D4QMaSeYO/PHxooljV0Kc8HKnPzxQEIpJEaMZeMb40gYP5VULaJSBpcb7MQv1fzoCaFY5PGV042YD5GgTqQYIOMnB04zj2zQQAusAKzAjYk+u1BZkZVjUox8SnIZyR5/OgU6zRsV0oMjGG25+3KgYIlhdu8V12IAWQYB5jbByKBKklFzIcrtg8qBniYanBGPMY5+R/jQQxjAV8LJsRk7+nPn5URH6wjDli6D6pBzj3oIOoxJl2GDqGTuPyoGFlMReR5e8x4SOVAuMhz4+XM5GCf5FVEi4VSDGI9O4Ibl/POggJmLvE3TPIfxoquSwDiNlPLbqKCNcmDpcpjc75B2qIW8hiOfrejAj8feg7vo9ZLrzXHj5j7KohpEkxpkKs48WRtjp70ClkceFAwPvuT60Do272NnclWO6sQTn054A9aAGkYlVKgDyUD76ihQIpIxz3+NAaku7NIFC/tGNdx+dVHBVGRkkf2hGcUaGZVaJVEZ33JxyqImTEk+XYkkY1/s0HKigBmKug2AxRAMC2VJxnmp2z9lAwd33ng8I07huZ8hsKKCWEiVhkA8xjOPtoEv3hbCMFQHOSuSaosKFV9QIcKfCcYGayOEUoljTwnO5Ibz8zyFULkw851gKAea7A0B6RjAAO23nzqBMmAQTkEjYb86CGimkZYhhiTjC7n50UBj1thEKLnqc+9BJQBkAUBTtn8ag5maIlQe9wfrYxtQCYBo7zA1rvjV9lUWS1iIpVkSRiFBhMbgaT67e1BSSLU4L4OORx99NhaorMSjZPTPT8qbDHVe6L61JzjYYH87VBU7vLnwk5549a0HdwpfUVygXLDO+aiiLAxxk6MptgbYHrQH9GjkQOMAFsNgcjQcVIiPd6jgeMZ+6g63XMutYlBC5IcZBAoiFg1u7MuWxtpAUZoAEA0EYJ6YO/Xc/Kiq7II28JOgNjJPL+cUDcozAgj/LjHxoGxBD4GIYvvgthQemaCw1x4OrMRszY28xj51NDljUJ3mx07bNk00Oh0RSArqRlOAT0x91ULYnHiZg2ck5xkUHCKOeEk4G5zkjfH5H76B8IXukyoCHkc7A+w9KCyrSaGEZj07ZwNzy6ehoju8Zl0vIXI3VTvjzGTQdG5jJ7qVwD9YkY6EY+2o0cyjQGQksUOsMcY/OtMgaVZJVBUBkUnTnGTnPtUVZEhlbxq5ODkpgdNtvgKBBIBGT4V5AefWiOZA8gOtgGGxB/Cglom28RDA5xnBaqHDDsdavG/x0/96Dv93IOGfWSAVO4xjc52+WKgiOFiMpEShOFJHUDJHrQE6BCQkquoAClkAJHw2296BLOWJlDuxVQNJ5AZ/jQMZjKSrPgkb5PTOcc6oc6kxLo7xoI8blR4WPPf4H5UCURWJEYfVkbDB2+yg4CEMSB4tshd/jnpQQ4iKrnWDjJxtQEZghVWCPGo04AP/egLUDGFhTG2ANOSfj5UAjBALEqCD41XB9wP40EmCTVkysVJyDyP3+lEMDSpEH0QkxkKSd22z08qCq0cZfwx+JRncc/iKAwpEYYEs3VcdPeghlZADLtq2O+9AC+BGyM5xpJHL39KAdIjGSeX1vCQBk8xQCrKJT3ZMmduQOKDkKTSaHk8AGck9KqE7opwzAjkScgigIhGVjJuxGy8gT8qBJUMGUO4wuwbxZ+IoA8QiCsofkQQcmqaPuDHGkaRoQwHjOoEH5cvnWTRR7sxAvl9sAk9ao7CKVKleWccvh60BSMQGRtI2yAN9NQCjHuyoCtk7kjcUDQdK5RsODgD086KjQ2wU4J5nPOgarMDkrnHLJ++giKMl8FgMnOBj5UEyNFGcJIxkPPyB9DQASuNZZi4PTr6UHFPExYsTzyp50ElGIYyDTpO+DuaBi4MXdujkJnCk7b7igAqocjCDwgnSSBny/CgYwAy+pSBsE07sKmwmRE1q0algQRvTYhZVCghemCc8qCQQ7HY5JwWGSSKAXj0KQjZAPQc6BTx/RpC0yNqbGBq6+WKCVb6xOSr7eIcqKPQqqCwDNjGnG/2c6ADiXSutNYOBGNiBUChAISUI0A8t9R59fKglXkERZfPfHPFUcWkWMBdyx36belAowaUG5x036/yaKm3VY1Osry355/CgiO4VpHkdyxBxrJyc/GiIKP9JJVz3WN1KYJ+/HOgdEHGtolkOj6zEbDpvg7UUQtXChidWDjnpyD70AzYDAojJgY088j1ojmWJWyzavCdI3yPIUCzFjGhiCPkaCCqyLtgBt9RXfPr1oFxQ/R5C8id4nUHY86eVMj/AFsmYkYjfCkZOOm9PAMq8Tl3BGk/VJ3HpnypsTaxBnzPqONyqnB36g4psTHJ+pNuFDqx1Bini+fPHOgYIQJVAkB2yRzUb7ev8igJ03OgjWCcBT9tDQY4xKiptk9cct6G1tQVdRhCukHc4B38+lBJmUg6Qzg7tkDA9vTegmNo+6kDaj5DGQaqBcOEAIKgHScfWxuPn6UEtLqDFd8P1ABb+RUU+AssbsnhKjmOnLn5UDEjLiJmVXVhkeIbA59efPbnRA3SY8IkEvIZAOw+VAsfVCq2UAxgDJxQHG7jVq3I2UMep5UEvEcagpMm+cDbp1FADP8ArI1xuq81Hz/nagMSNGweFdlffUo3G/TkaHYSsJ5MRooD7jHT2zQ7IGJC0jRBhjLHJBHLNUDIAQAEKKeS43x8PSgKV3AMzPnfB0k5P8+9AN0FRI5ImBJyCCdRAx6fH5UC41SSRe9dIlbPPJI/hQcq4QhmDFdyeeB59SaAk0RofGDk9DkUDMr3AxIrKHJxqwSOu3lQLUxggO8hXBwVO4H84qDlTLlgwU4JyTs385rQbrVwGVPCo38QG/mKiOVmi1asqNWx23HXl91BWOmIl0kzg4OxOPn+FPI52ZnaR5NRxzz9YfH8KeAoyAEKzFQQBgdRQQZJF1QtJpjbfG+/lkUERyJ3JhRYgX2L9c+x2ocS1ZZI3LDWQTkkDByfTcUPBXfuuzhCoGAKqCxLIniOBHjGSfCMmgIEjAiLuVO4br68vvobJIkw2VGRv4Rv8KG3Dcg41ADHlQEj6cZ0BhnGN/nQSg0jUwy3M6snNAaKWkJwAW57YoGAHURsF3xiggB235j5n5UBhAudhgeeahsHdgs2rbAyMnAxQ26OFm2VdCAZJOxNVTCkQjOHKkc80Ekgf1T7seY54qDlBKkE4ZCdh1oOeeS4bMhBwMNyXb0wMUAxpBHGFcuZNsHUAuM+XWghAW8JBPMArQHgMO6B0qx3Ztgdv4mg5RAoy0n1FBVRnFAPdp3pYgnmfFQBGj4DsrKpbouMjrg0Nocu5BJ1a8bA70NhQiePI0ppODvgn3qK4xBW1ks2Rggj7aCGSDvMkuyggMRtk0EEBmLhnO+SW32oJURgHUQxOcNn7xQRjCCXGw3Az096AYz3g1Oo3JOEzgUAmJQr6FLuzBtuVBPcaVBeMlOZ5c6aNmOkWlSFYrpDE55NgZ3wNqaNllSwBHI/W8zQSrBkGMl16E7AUBF25KVUttg/woOCMYn0kkZzsMD+NBxh0qAcrnfGMigPWiq8w8H7ihedAmCRGbIZJWX66+Xv8KL5dMGSZJ4sc/DoGnHlv5dd6HhxkZu8abBY7OfPPXyq6ASa1AAyXU7Kcb/b71nYfEgjh1SpJESfCSukedXRuDZIXkEM0QckkDOnAJ/GmjcIx3EKg7jpyzvz96Ce43BUKNuWM/Z50BxgBQms+ZDY64xv+FBwkGknL7NgZGAT70DsN3QwGUMTq8XMUChliW2UZ3BO9A+JwAoCMzjlqGeRojhuCCGTbmi77Hr1oJUSqp8YdWbGnVjUB51R0wczs776zkHcDB60AquDyB9jyqA8GUMdLcwp8vnQMEksUoaEtGzLjXnOfP41TasVBJA1up6acZA8t/KhsxXVSI5IsaTgk51e29EMIWZEBYMTspXw4FAJVVcIf1gH7BzkfKihRQkgbDBSc4YZxv8ACgB5448ALpR/CNTdR1/70EqyMFMbE4J22+dARikidu8hAxndhuKIWIhG2SNW24bIA9NsGgkyRWskhaDG+EVznHpjrQGoSVW7tyHj+rqyTnmR/wB6CQAVAOQw2LAYzt5CqBXIjZ9SaQMDO/2UBhlYaiMahyGSM0QtpJI1UKW0htlzjPx96AZWZwRrfQN9JaopRXFxhQjAdMasfKgc+XjTJSTIBK6t8csEfLnvVQmTu42LrHlTgjPIYoFOEZHKDXk6htQRHGUHeNCNlx6r7eVAEjMyaiD5DI3x06UASSghNMUaKAQSowWHrQFb3fcShgNLqdnomk3dzBoR4wyvk6g2CB7Ua0//2Q==";
            $carNetApiKey = Settings::get('car_net_ai_token');
            if (strlen($carNetApiKey) <= 0) {
                throw new \Exception('CarNetDetection stopped api-key is empty');
            }

            $client = new GuzzleHttp\Client();
            $imageParts = explode( ',', $imageData );
            $binaryImage = base64_decode($imageParts[1]);
            $res = $client->request(
                'POST',
                'https://api.carnet.ai/v2/mmg/detect?box_offset=0&box_min_width=180&box_min_height=180&box_min_ratio=1&box_max_ratio=3.15&box_select=center&features=mmg&region=EU',
                [
                    'headers' => [
                        'api-key' => $carNetApiKey,
                    ],
                    'body' => $binaryImage
                ]
            );

            if ($res->getStatusCode() != 200) {
                throw new \Exception("CarNetDetection failed due to api error");
            }

            $parsedResponse = json_decode($res->getBody());
            if (is_array($parsedResponse) || is_object($parsedResponse)) {
                return (array) $parsedResponse;
            }
            return [];
        } catch (\Exception $e) {
            \Log::alert('Exception while CarNetDetectCar');
            trace_log($e);
            return [];
        }
    }

    public static function GetUserNotificationSettings($user) {
        $arr = [
            self::$USER_NOTIFICATION_ALLOW_ALERT_PUSH => self::$USER_NOTIFICATION_ALLOW_ALERT_PUSH_DEFAULT,
            self::$USER_NOTIFICATION_ALLOW_EVENTS_EMAIL => self::$USER_NOTIFICATION_ALLOW_EVENTS_EMAIL_DEFAULT,
            self::$USER_NOTIFICATION_ALLOW_EVENTS_PUSH => self::$USER_NOTIFICATION_ALLOW_EVENTS_PUSH_DEFAULT,
            self::$USER_NOTIFICATION_ALLOW_NEWS_PUSH => self::$USER_NOTIFICATION_ALLOW_NEWS_PUSH_DEFAULT,
            self::$USER_NOTIFICATION_ALLOW_TRAINING_PUSH => self::$USER_NOTIFICATION_ALLOW_TRAINING_PUSH_DEFAULT,
            self::$USER_NOTIFICATION_EVENTS_REMINDER_EMAIL_INTERVALS => self::$USER_NOTIFICATION_EVENTS_REMINDER_EMAIL_INTERVALS_DEFAULT,
            self::$USER_NOTIFICATION_EVENTS_REMINDER_PUSH_INTERVALS => self::$USER_NOTIFICATION_EVENTS_REMINDER_PUSH_INTERVALS_DEFAULT,
        ];

        if ($user === null) {
            return $arr;
        }

        $setting = $user->settings->where(CustomSettings::$SETTINGS_KEY, CustomSettings::$USER_NOTIFICATIONS_SETTINGS)->first();
        if ($setting != null) {
            foreach ($setting->getPayload() as $key => $setting) {
                if (array_key_exists($key, $arr)) {
                    $arr[$key] = $setting;
                }
            }
        }

        return $arr;
    }

    public static function SaveUserNotificationSettings($user, $params) {
        if ($user === null) {
            return;
        }

        $fill = [
            'settings_key' => CustomSettings::$USER_NOTIFICATIONS_SETTINGS,
            'settings_payload' => $params
        ];

        $setting = $user->settings->where(CustomSettings::$SETTINGS_KEY, CustomSettings::$USER_NOTIFICATIONS_SETTINGS)->first();

        if ($setting == null) {
            $setting = CustomSettings::create($fill);
            $user->settings()->add($setting);
            $user->save();
        } else {
            $setting->fill($fill);
            $setting->save();
        }

    }

    public static function IsUserNotificationSetting($user, $flag) {
        if ($user === null) {
            return false;
        }
        $settings = self::GetUserNotificationSettings($user);
        if (array_key_exists($flag, $settings)) {
            return boolval($settings[$flag]);
        }
        return false;
    }

    public static function DeleteUser($user) {
        try {
            $user->email = 'anonym-'.time().'-'. $user->email;
            $user->name = 'Anonym';
            $user->surname = 'Ous';
            $user->alert_email = '';
            $user->alert_sms = '';
            $user->alert_call = '';
            $user->groups()->sync([]);
            $user->linked_stations()->sync([]);
            $user->alertgroups()->sync([]);
            $user->alerts()->sync([]);
            $user->save();
            $user->subscriptions()->forceDelete();
            $user->alert_feedbacks()->forceDelete();
            $user->devices()->forceDelete();
            $user->invites_sender()->forceDelete();
            $user->invites_recipient()->forceDelete();
            $user->otp()->forceDelete();
            $user->absences()->forceDelete();
            $user->reminders()->forceDelete();
            $user->pushtokens()->forceDelete();
            $user->settings()->forceDelete();
            if ($user->avatar !== null) {
                $user->avatar->delete();
            }
            $user->forceDelete();
            return true;
        } catch (\Exception $e) {
            \Log::alert('Exception while DeleteUser');
            trace_log($e);
            return false;
        }
    }
}