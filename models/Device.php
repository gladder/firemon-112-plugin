<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;
/**
 * Model
 */
class Device extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $STATE_PENDING = "pending";
    public static $STATE_ACTIVE = "active";
    public static $STATE_DISABLED = "disabled";

    public static $TYPE_MONITOR = "monitor";
    public static $TYPE_PAGER = "pager";
    public static $TYPE_MOBILE = "mobile";

    public static $MQ_RPC_COMMAND_REBOOT = "REBOOT";
    public static $MQ_RPC_COMMAND_SHUTDOWN = "SHUTDOWN";
    public static $MQ_RPC_COMMAND_REFRESH = "REFRESH";
    public static $MQ_RPC_COMMAND_DISPLAY_ON = "DISPLAY_ON";
    public static $MQ_RPC_COMMAND_DISPLAY_OFF = "DISPLAY_OFF";

    public static $WD_ALLOWED_RESOLUTIONS = [
        "1024x768",
        "1280x720",
        "1280x800",
        "1280x1024",
        "1360x768",
        "1366x768",
        "1440x900",
        "1600x900",
        "1600x1200",
        "1600x1050",
        "1920x1200",
        "1920x1080",
        "3840x2160"
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_device';

    protected $appends = [
        'last_heartbeat_minutes_readable',
        'background',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'wallpaper' => ' nullable|image'
    ];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
        'mq' =>  'pm\Firemon112\Models\Mq',
        'user' =>  'RainLab\User\Models\User',
        'wachendisplay' =>  'pm\Firemon112\Models\Wachendisplay',
    ];

    public $hasMany = [
        'deviceerrors' => 'pm\Firemon112\Models\Device',
    ];

    public $belongsToMany = [
        'gpio_alert_triggers' => [
            'pm\Firemon112\Models\GpioAlertTrigger',
            'table' => 'pm_firemon112_gpio_alert_trigger2device',
        ],
    ];

    public $attachOne = [
        'wallpaper' => 'System\Models\File'
    ];

    protected $with = ['wallpaper', 'station'];
    protected $hidden = ['wallpaper'];

    public function getBackgroundAttribute() {
        if ($this->wallpaper !== null) {
            return $this->wallpaper->path;
        }
        if ($this->station !== null && $this->station->default_wallpaper !== null) {
            return $this->station->default_wallpaper->path;
        }
        return null;
    }

    public function getStateOptions() {
        return Device::getStateOptionArray();
    }

    public static function getStateOptionArray() {
        return array(
            self::$STATE_PENDING => "Pending",
            self::$STATE_ACTIVE => "Active",
            self::$STATE_DISABLED => "Disabled",
        );
    }

    public function getIsStateActiveAttribute() {
        return $this->state == self::$STATE_ACTIVE;
    }

    public function getIsStateDisabledAttribute() {
        return $this->state == self::$STATE_DISABLED;
    }

    public function getIsStatePendingAttribute() {
        return $this->state == self::$STATE_PENDING;
    }

    public function getIsTypeMonitorAttribute() {
        return $this->type == self::$TYPE_MONITOR;
    }

    public function getTypeOptions() {
        return Device::getTypeOptionArray();
    }

    public static function getTypeOptionArray() {
        return array(
            self::$TYPE_MONITOR => "Monitor",
            self::$TYPE_PAGER => "Pager",
            self::$TYPE_MOBILE => "Handy / Tablet",
        );
    }

    public function getRecentAlertAttribute() {
        if ($this->user != null) {
            return $this->user->alerts()->active()->first();
        }
        return Alert::byStation($this->station_id)->active()->first();
    }

    public function getRecentAlertTimeoutMinutesAttribute() {
        if ($this->user != null) {
            return $this->user->alerts()->activeMinutes($this->alert_timeout)->first();
        }
        return Alert::byStation($this->station_id)->activeMinutes($this->alert_timeout)->first();
    }

    public function getRecentAlertTimeoutMinutesAllAttribute() {
        if ($this->user != null) {
            return $this->user->alerts()->activeMinutes($this->alert_timeout)->orderBy('id', 'desc')->get();
        }
        return Alert::byStation($this->station_id)->activeMinutes($this->alert_timeout)->orderBy('id', 'desc')->get();
    }

    public function scopeState($query, $state) {
        return $query->where('state', $state);
    }

    public function scopeStateActive($query) {
        return $this->scopeState($query, self::$STATE_ACTIVE);
    }

    public function scopeStateDisabled($query) {
        return $this->scopeState($query, self::$STATE_DISABLED);
    }

    public function scopeStatePending($query) {
        return $this->scopeState($query, self::$STATE_PENDING);
    }

    public function scopeType($query, $type) {
        return $query->where('type', $type);
    }

    public function getLastHeartbeatMinutesAttribute() {
        if ($this->last_heartbeat == null) {
            return 60*24*365*10; // 10 years offline! ;-)
        }
        $crbn = new Carbon($this->last_heartbeat, 'Europe/Berlin');
        return $crbn->diffInMinutes(Carbon::now('Europe/Berlin'));
    }

    public function getLastHeartbeatMinutesReadableAttribute() {
        if ($this->last_heartbeat == null) {
            return "nie";
        }
        $crbn = new Carbon($this->last_heartbeat, 'Europe/Berlin');
        return $crbn->diffForHumans(Carbon::now('Europe/Berlin'));
    }

    public function getLastUpdateMinutesAttribute() {
        $crbn = new Carbon($this->updated_at,'Europe/Berlin');
        return $crbn->diffInMinutes(Carbon::now('Europe/Berlin'));
    }

    function getIsOnlineStateOk() {
        return !$this->getIsTypeMonitorAttribute() || ($this->getIsTypeMonitorAttribute() && $this->getLastHeartbeatMinutesAttribute() <= 16);
    }

    function getIsOnlineStateWarning() {
        return $this->getIsTypeMonitorAttribute() && $this->getLastHeartbeatMinutesAttribute() > 16 && $this->getLastHeartbeatMinutesAttribute() <= 30;
    }

    function getIsOnlineStateError() {
        return $this->getIsTypeMonitorAttribute() && $this->getLastHeartbeatMinutesAttribute() > 30;
    }

    function getIsRpcCapableAttribute() {
        return $this->getIsTypeMonitorAttribute() && $this->watchdog;
    }

    function getIsDisplayResolutionOk() {
        if (!$this->watchdog) {
            return true; // if no watchdog res un unknown and by definition OK
        }

        if (strpos($this->running_version, "@") !== FALSE) {
            $parts = explode("@", $this->running_version);
            return is_array($parts) && count($parts) >= 2 && in_array($parts[1],self::$WD_ALLOWED_RESOLUTIONS);
        } else {
            return true; // if no @ existing resolution not (yet) detected and by definition OK
        }
    }

    private function rpc($command) {
        $queue = Helper::MqQueueForDevice($this->token);
        if ($queue !== null) {
            return Helper::MqSendMessageToQueue($queue, $command);
        }
        return false;
    }

    function reboot() {
        return $this->rpc(self::$MQ_RPC_COMMAND_REBOOT);
    }

    function shutdown() {
        return $this->rpc(self::$MQ_RPC_COMMAND_SHUTDOWN);
    }

    function refresh() {
        return $this->rpc(self::$MQ_RPC_COMMAND_REFRESH);
    }

    function displayOn() {
        return $this->rpc(self::$MQ_RPC_COMMAND_DISPLAY_ON);
    }

    function displayOff() {
        return $this->rpc(self::$MQ_RPC_COMMAND_DISPLAY_OFF);
    }

    function enable() {
        $this->state = self::$STATE_ACTIVE;
        $this->save();
    }

    function disable() {
        $this->state = self::$STATE_DISABLED;
        $this->save();
    }

    function getShouldAutoRestartAttribute() {
        return $this->getIsTypeMonitorAttribute() && $this->auto_reboot;
    }

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }
}
