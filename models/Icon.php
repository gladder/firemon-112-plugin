<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Icon extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $TYPE_RETTUNG = "Rettungszeichen";
    public static $TYPE_RETTUNG_FLAG = "rettung";
    public static $TYPE_WARNUNG = "Warnzeichen";
    public static $TYPE_WARNUNG_FLAG = "warnung";
    public static $TYPE_VERBOT = "Verbotszeichen";
    public static $TYPE_VERBOT_FLAG = "verbot";
    public static $TYPE_BRANDSCHUTZ = "Brandschutzzeichen";
    public static $TYPE_BRANDSCHUTZ_FLAG = "brandschutz";
    public static $TYPE_GEBOT = "Gebotszeichen";
    public static $TYPE_GEBOT_FLAG = "gebot";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_icon';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label' => 'required',
        'group' => 'required',
    ];

    public $attachOne = [
        'icon_small' => 'System\Models\File',
        'icon_big' => 'System\Models\File'
    ];

    public static function getGroupOptions() {
        return [
            self::$TYPE_BRANDSCHUTZ_FLAG => self::$TYPE_BRANDSCHUTZ,
            self::$TYPE_RETTUNG_FLAG => self::$TYPE_RETTUNG,
            self::$TYPE_WARNUNG_FLAG => self::$TYPE_WARNUNG,
            self::$TYPE_VERBOT_FLAG => self::$TYPE_VERBOT,
            self::$TYPE_GEBOT_FLAG => self::$TYPE_GEBOT,
        ];
    }

    public $hasMany = [
        'iconmarkers' => 'pm\Firemon112\Models\IconMarker'
    ];
}
