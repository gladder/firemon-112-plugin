<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class IconMarker extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_iconmarker';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'long' => 'required|numeric',
        'lat' => 'required|numeric',
    ];

    public $morphTo = [
        'iconmarkerable' => []
    ];

    public $belongsTo = [
        'icon' =>  'pm\Firemon112\Models\Icon',
    ];
}
