<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Reminder extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $timestamps = false;

    public static $NOTIFICATION_TYPE_PUSH = "push";
    public static $NOTIFICATION_TYPE_EMAIL = "email";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_reminders';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'user_id' => 'required',
        'reminder_trigger_at' => 'required',
        'notification_type' => 'required'
    ];

    public $fillable = [
        'user_id',
        'reminder_trigger_at',
        'notification_type'
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User',
    ];

    public $morphTo = [
        'remindable' => [],
    ];

    public function scopeUnhandled($query)
    {
        $query->where('reminder_handled_at',null);
    }

    public function scopeUser($query, $user)
    {
        $query->where('user_id', $user->id);
    }

}