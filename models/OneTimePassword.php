<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;
/**
 * Model
 */
class OneTimePassword extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_otp';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'identifier' => 'required',
        'reason' => 'required',
        'otp' => 'required|size:64',
        'user_id' => 'required|integer',
        'expired_at' => 'date'
    ];

    public $fillable = [
        'identifier',
        'reason',
        'otp',
        'user_id',
        'expired_at'
    ];

    public static $REASON_AUTH = "auth";
    public static $REASON_AUTH_LABEL = "Authentifizierung";

    public static function getReasonOptions() {
        return [
            self::$REASON_AUTH => self::$REASON_AUTH_LABEL,
        ];
    }

    public $belongsTo = [
        'user' =>  'RainLab\User\Models\User',
    ];

    public function getIsExpiredAttribute() {
        $exp = new Carbon($this->expired_at);
        return $exp->isBefore(Carbon::now('Europe/Berlin'));
    }

    public function getIsUsedAttribute() {
        return $this->used_at != null;
    }

    public function scopePending($query) {
        $query->whereRaw('( expired_at > CURRENT_TIMESTAMP() AND used_at IS NULL )')->orderBy('id','DESC');
    }

    public static function getSmsPassword() {
        return rand(100000,999999);
    }

    public static function getPasswordHash($password) {
        return hash('sha256',$password);
    }

    public function isPasswordMatching($password) {
        return self::getPasswordHash($password) == $this->otp;
    }


}
