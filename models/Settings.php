<?php namespace pm\Firemon112\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'firemon112_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}