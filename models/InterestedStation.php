<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\Models\Settings;
use Mail;

/**
 * Model
 */
class InterestedStation extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_interested_station';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'station_name' => 'required',
        'contact_person' => 'required',
        'email' => 'required|email',
        'long' => 'required|numeric',
        'lat' => 'required|numeric',
        'leitstelle_id' => 'required|integer',
        'station_type' => 'required',
        'status' => 'required',
    ];

    public $fillable = [
        'station_name',
        'contact_person',
        'email',
        'long',
        'lat',
        'leitstelle_id',
        'station_type',
        'status',
        'comment'
    ];

    public $belongsTo = [
        'leitstelle' =>  'pm\Firemon112\Models\Leitstelle',
    ];

    protected $slugs = ['token' => 'station_name'];

    public static $STATUS_NEW = "new";
    public static $STATUS_TESTING = "testing";
    public static $STATUS_ONBOARDED = "onboarded";
    public static $STATUS_CANCELED = "canceled";

    public static $STATUS_NEW_LABEL = "Neu!";
    public static $STATUS_TESTING_LABEL = "Testphase";
    public static $STATUS_ONBOARDED_LABEL = "Produktiv";
    public static $STATUS_CANCELED_LABEL = "Stillgelegt";

    public static function getStatusOptions() {
        return [
            self::$STATUS_NEW => self::$STATUS_NEW_LABEL,
            self::$STATUS_TESTING => self::$STATUS_TESTING_LABEL,
            self::$STATUS_ONBOARDED => self::$STATUS_ONBOARDED_LABEL,
            self::$STATUS_CANCELED => self::$STATUS_CANCELED_LABEL
        ];
    }

    public static function getStationTypeOptions() {
        return Station::getStationTypeOptions();
    }

    public function getIsNewAttribute() {
        return $this->status == self::$STATUS_NEW;
    }

    public function getStationTypeReadableAttribute() {
        $arr = self::getStationTypeOptions();
        if (array_key_exists($this->station_type, $arr)) {
            return $arr[$this->station_type];
        }
        return "n/a";
    }

    public function afterCreate()
    {
        // Send Mail to Firemon 112 Account Manager
        $firemon_accountant = Settings::get('firemon_account_manager_email');
        if (strlen($firemon_accountant)>0) {
            Mail::sendTo($firemon_accountant, 'pm.firemon112::mail.testaccount-note', ['obj' => $this]);
        }
        // Send Mail to interested Person
        Mail::sendTo($this->email, 'pm.firemon112::mail.testaccount-confirmation', ['obj' => $this]);

    }

    // TODO: PROD
    // Send Mail with info about prod and smsio etc

    // TODO: CANCELED
    // Sent Mail with confirmation that account is suspended
}
