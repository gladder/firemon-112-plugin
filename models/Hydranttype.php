<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class Hydranttype extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_hydranttype';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public $hasMany = [
        'hydrants' => 'pm\Firemon112\Models\Hydrant'
    ];

    public static function getHydranttypeOptions() {
        return Hydranttype::all()->lists('name', 'id');
    }
}
