<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;
use pm\Firemon112\Traits\DateTimeExtension;
use pm\Firemon112\Traits\FileUploadExtension;
use pm\Firemon112\Traits\SortOrderExtension;

/**
 * Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use FileUploadExtension, SortOrderExtension, DateTimeExtension;

    protected $dates = ['deleted_at','date_from','date_to'];

    public $belongsTo = [
        'station' =>  'pm\Firemon112\Models\Station',
        'user_group' =>  'RainLab\User\Models\UserGroup',
    ];

    public $hasMany = [
        'subscriptions' => 'pm\Firemon112\Models\EventSubscription',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_event';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'category' => 'required',
        'station_id' => 'required',
        'date_from' => 'required|date',
        'date_to' => 'required|date|after:date_from',
        'label' => 'required',
        'description' => 'required',
    ];

    public $fillable = [
        'label',
        'location',
        'description',
        'date_from',
        'date_to',
        'category',
        'subscription_required',
        'email_reminder',  // TODO: delete this property (and user_group_id too!) because this decision now is up to the users
        'duty',
        'public',
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public $attachMany = [
        'files' => ['System\Models\File', 'delete' => true]
    ];


    public $morphMany = [
        'reminders' => [\pm\Firemon112\Models\Reminder::class, 'name' => 'remindable'],
    ];


    public $morphToMany = [
        'groups' => ['RainLab\User\Models\UserGroup', 'table' => 'pm_firemon112_group2groupable', 'name' => 'groupable']
    ];

    public static $CATEGORY_TYPE_TRAINING = 'uebung';
    public static $CATEGORY_TYPE_EDUCATION = 'lehrgang';
    public static $CATEGORY_TYPE_MEETING = 'sitzung';
    public static $CATEGORY_TYPE_GATHERING = 'versammlung';
    public static $CATEGORY_TYPE_EVENT = 'veranstaltung';
    public static $CATEGORY_TYPE_MISC = 'sonstiges';

    public static function getCategoryOptions() {
        return array(
            self::$CATEGORY_TYPE_TRAINING => 'Übung',
            self::$CATEGORY_TYPE_EDUCATION => 'Lehrgang',
            self::$CATEGORY_TYPE_MEETING => 'Sitzung',
            self::$CATEGORY_TYPE_GATHERING => 'Versammlung',
            self::$CATEGORY_TYPE_EVENT => 'Veranstaltung',
            self::$CATEGORY_TYPE_MISC => 'Sonstiges'
        );
    }

    public function getCategoryReadableAttribute() {
        $all = self::getCategoryOptions();
        if (array_key_exists($this->category, $all)) {
            return $all[$this->category];
        }
        return "";
    }

    public function getDateFromFormattedAttribute() {
        return $this->getFormattedDateTime($this->date_from);
    }

    public function getDateToFormattedAttribute() {
        return $this->getFormattedDateTime($this->date_to);
    }

    public function getIsRunningAttribute() {
        $now = Carbon::now('Europe/Berlin');
        return $now->between($this->date_from, $this->date_to);
    }

    public function getIsTodayAttribute() {
        $now = Carbon::now('Europe/Berlin');
        return $now->isSameDay($this->date_from) || $now->isSameDay($this->date_to) || $this->getIsRunningAttribute();
    }

    public function getIsUpcomingAttribute() {
        $now = Carbon::now('Europe/Berlin');
        return $now->isBefore($this->date_from) || ( $now->isAfter($this->date_from) && $now->isBefore($this->date_to) );
    }

    // NOTE: in whereRaw it's max important to take outter ( )
    public function scopeUpcoming($query)
    {
        return $query->whereRaw('( date_from > CURRENT_TIMESTAMP() OR ( date_from < CURRENT_TIMESTAMP() AND date_to > CURRENT_TIMESTAMP() ) )')->orderBy('date_from','ASC');
    }

    public function scopeToday($query)
    {
        return $query->whereRaw('( (date_from <= CURRENT_TIMESTAMP() AND date_to >= CURRENT_TIMESTAMP()) OR DATE(date_from) = CURDATE() OR DATE(date_to) = CURDATE() )')->orderBy('date_from','DESC');
    }

    public function scopeRemindable($query)
    {
        return $query->whereRaw('( date_from BETWEEN CURRENT_TIMESTAMP() AND DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR) AND email_reminder = 1 AND email_reminder_done = 0 )')->orderBy('date_from','DESC');
    }

    public function scopeUpcomingSoonOrActive($query)
    {
        return $query->whereRaw('( DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 30 MINUTE) > date_from  AND CURRENT_TIMESTAMP() < date_to )')->orderBy('date_from','ASC');
    }

    public function scopeByStation($query, $station_id)
    {
        return $query->where($this->table . '.station_id', $station_id);
    }

    public function scopeByYear($query, $year)
    {
        return $query->whereRaw('( year(date_from) = '.intval($year).' )');
    }

    public function scopeById($query, $id)
    {
        return $query->whereRaw('( id = '.intval($id).' )');
    }

    public function scopeByMonth($query, $month)
    {
        return $query->whereRaw('( month(date_from) = '.intval($month).' )');
    }

    public function scopeByCategory($query, $category)
    {
        return $query->where('category', $category);
    }

    public function beforeDelete() {
        $this->reminders()->delete();
        $this->groups()->detach();
        foreach ($this->subscriptions as $sub) {
            $sub->forceDelete();
        }
    }

    public function asInformationReducedArray() {
        return [
            'id' => $this->id,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'category' => $this->getCategoryReadableAttribute(),
            'description' => $this->description,
            'subscription_required' => $this->subscription_required,
            'duty' => $this->duty,
            'label' => $this->label,
            'location' => $this->location,
            'public' => $this->public,
            "group" => $this->groups_readable
        ];
    }

    public function getGroupsReadableAttribute() {
        return $this->groups()->exists() ? implode(", ",$this->groups->sortByDesc('prio')->pluck('name')->toArray()) : "Alle";
    }

    public function getRelatedUserAttribute() {
        if ($this->groups()->exists()) {
            return $this->groups->flatMap(function ($group) {
                return $group->users;
            })->unique('id'); // note - this is from user model
        } else {
            return $this->station->all_users ?? collect([]);
        }
    }

    public function afterSave() {
        foreach ($this->related_user as $user) {
            $this->updateReminders($user);
        }
    }

    public function updateReminders($user) {
        $this->reminders()->user($user)->unhandled()->delete();
        $userSettings = Helper::GetUserNotificationSettings($user);
        if ($userSettings[Helper::$USER_NOTIFICATION_ALLOW_EVENTS_PUSH] === true) {
            if (is_array($userSettings[Helper::$USER_NOTIFICATION_EVENTS_REMINDER_PUSH_INTERVALS])) {
                foreach ($userSettings[Helper::$USER_NOTIFICATION_EVENTS_REMINDER_PUSH_INTERVALS] as $interval_hours) {
                    $this->createReminderIfNeeded($user->id, $interval_hours, Reminder::$NOTIFICATION_TYPE_PUSH);
                }
            }
        }
        if ($userSettings[Helper::$USER_NOTIFICATION_ALLOW_EVENTS_EMAIL] === true) {
            if (is_array($userSettings[Helper::$USER_NOTIFICATION_EVENTS_REMINDER_EMAIL_INTERVALS])) {
                foreach ($userSettings[Helper::$USER_NOTIFICATION_EVENTS_REMINDER_EMAIL_INTERVALS] as $interval_hours) {
                    $this->createReminderIfNeeded($user->id, $interval_hours, Reminder::$NOTIFICATION_TYPE_EMAIL);
                }
            }
        }
    }

    private function createReminderIfNeeded($user_id, $interval_hours, $reminder_type) {
        $reminderTime = $this->date_from->copy()->subHours($interval_hours);
        if ($reminderTime->isFuture()) {
            $this->reminders()->create([
                'user_id' => $user_id,
                'reminder_trigger_at' => $reminderTime,
                'notification_type' => $reminder_type,
            ]);
        }
    }

    public function getIsTrainingAttribute() {
        return $this->category == self::$CATEGORY_TYPE_TRAINING;
    }
}
