<?php namespace pm\Firemon112\Models;

use Model;
use pm\Firemon112\Models\Helper;
/**
 * Model
 */
class Station extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_station';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'token' => 'required|unique:pm_firemon112_station',
        'email' => 'required|email|unique:pm_firemon112_station',
        'long' => 'required|numeric',
        'lat' => 'required|numeric',
        'leitstelle_id' => 'required|integer',
        'station_type' => 'required',
        'sms77io_key' => 'sometimes|size:64',
        'default_wallpaper' => ' nullable|image'
    ];

    protected $hidden = [
        'diverafree_accesskey',
        'sms77io_key',
        'monitoring_emails',
        'forward_depesche_emails',
        'registration_pin',
        'default_wallpaper',
        'created_at',
        'updated_at',
        'deleted_at',
        'diverafree_enabled',
        'leitstelle_id',
        'parent_station_id',
        'station_type',
        'token',
        'email'
    ];

    public $morphMany = [
        'settings' => [\pm\Firemon112\Models\CustomSettings::class, 'name' => 'ownerable', 'delete' => true],
    ];

    public $hasMany = [
        'mqs' => 'pm\Firemon112\Models\Mq',
        'devices' => 'pm\Firemon112\Models\Device',
        'alerts' => 'pm\Firemon112\Models\Alert',
        'hydrants' => 'pm\Firemon112\Models\Hydrant',
        'users' => 'RainLab\User\Models\User',
        'einsatzobjekte' => 'pm\Firemon112\Models\Einsatzobjekt',
        'assemblyareas' => 'pm\Firemon112\Models\AssemblyArea',
        'alertgroups' => 'pm\Firemon112\Models\AlertGroup',
        'rescuepoints' => 'pm\Firemon112\Models\RescuePoint',
        'events' => 'pm\Firemon112\Models\Event',
        'child_stations' => [ 'pm\Firemon112\Models\Station', 'key' => 'parent_station_id', 'otherKey' => 'id'],
        'wachendisplays' => 'pm\Firemon112\Models\Wachendisplay',
        'news' => 'pm\Firemon112\Models\News',
        'presences' => 'pm\Firemon112\Models\Presence',
        'groups' => 'RainLab\User\Models\UserGroup',
        'gpio_alert_triggers' => 'pm\Firemon112\Models\GpioAlertTrigger',
    ];


    public $belongsTo = [
        'leitstelle' =>  'pm\Firemon112\Models\Leitstelle',
        'parent_station' => [
            'pm\Firemon112\Models\Station',
            'key' => 'parent_station_id',
            'otherKey' => 'id',
            'scope' => 'onlySuperOrgs'
        ]
    ];

    public $belongsToMany = [
        'linked_users' => ['RainLab\User\Models\User', 'table' => 'pm_firemon112_station2user', 'key' => 'station_id', 'otherKey' => 'user_id']
    ];

    public $attachOne = [
        'default_wallpaper' => 'System\Models\File'
    ];

    public static $TYPE_ORTSWEHR = "ort";
    public static $TYPE_GEMEINDEWEHR = "gemeinde";
    public static $TYPE_AMT = "amt";
    public static $TYPE_KREIS = "kreis";
    public static $TYPE_LAND = "land";
    public static $TYPE_STANDORT = "standort";
    public static $TYPE_KATS = "kats";

    public static $TYPE_ORTSWEHR_LABEL = "Ortswehr";
    public static $TYPE_GEMEINDEWEHR_LABEL = "Gemeindewehr";
    public static $TYPE_AMT_LABEL = "Amtswehrführung / ReFüSt";
    public static $TYPE_KREIS_LABEL = "Kreisfeuerwehrverband";
    public static $TYPE_LAND_LABEL = "Land";
    public static $TYPE_STANDORT_LABEL = "Standort";
    public static $TYPE_KATS_LABEL = "Katastrophenschutz";

    public static $TYPE_FEEDBACK_NONE = 0;
    public static $TYPE_FEEDBACK_IN = 1;
    public static $TYPE_FEEDBACK_ILN = 2;
    public static $TYPE_FEEDBACK_IDN = 3;
    public static $TYPE_FEEDBACK_IDLN = 4;

    public static $TYPE_FEEDBACK_NONE_LABEL = "Nicht verwenden";
    public static $TYPE_FEEDBACK_IN_LABEL = "Modus: Sofort / Nicht Verfügbar";
    public static $TYPE_FEEDBACK_ILN_LABEL = "Modus: Sofort / Verspätet / Nicht Verfügbar";
    public static $TYPE_FEEDBACK_IDN_LABEL = "Modus: Sofort / Direkt / Nicht Verfügbar";
    public static $TYPE_FEEDBACK_IDLN_LABEL = "Modus: Sofort / Direkt / Verspätet / Nicht Verfügbar";

    public static function getStationTypeOptions() {
        return [
            self::$TYPE_ORTSWEHR => self::$TYPE_ORTSWEHR_LABEL,
            self::$TYPE_STANDORT => self::$TYPE_STANDORT_LABEL,
            self::$TYPE_GEMEINDEWEHR => self::$TYPE_GEMEINDEWEHR_LABEL,
            self::$TYPE_AMT => self::$TYPE_AMT_LABEL,
            self::$TYPE_KREIS => self::$TYPE_KREIS_LABEL,
            self::$TYPE_KATS => self::$TYPE_KATS_LABEL,
            self::$TYPE_LAND => self::$TYPE_LAND_LABEL
        ];
    }

    public function getStationTypeLabelAttribute() {
        $arr = self::getStationTypeOptions();
        if (array_key_exists($this->station_type, $arr)) {
            return $arr[$this->station_type];
        }
        return "n/a";
    }

    public static function getAlertFeedbackTypeOptions() {
        return [
            self::$TYPE_FEEDBACK_NONE => self::$TYPE_FEEDBACK_NONE_LABEL,
            self::$TYPE_FEEDBACK_IN => self::$TYPE_FEEDBACK_IN_LABEL,
            self::$TYPE_FEEDBACK_ILN => self::$TYPE_FEEDBACK_ILN_LABEL,
            self::$TYPE_FEEDBACK_IDN => self::$TYPE_FEEDBACK_IDN_LABEL,
            self::$TYPE_FEEDBACK_IDLN => self::$TYPE_FEEDBACK_IDLN_LABEL
        ];
    }

    public function GetIsOrtswehrAttribute() {
        return $this->station_type == self::$TYPE_ORTSWEHR;
    }

    public function GetIsGemeindewehrAttribute() {
        return $this->station_type == self::$TYPE_GEMEINDEWEHR;
    }

    public function GetIsAmtswehrAttribute() {
        return $this->station_type == self::$TYPE_AMT;
    }

    public function GetIsKreisAttribute() {
        return $this->station_type == self::$TYPE_KREIS;
    }

    public function GetIsLandAttribute() {
        return $this->station_type == self::$TYPE_LAND;
    }

    public function GetIsStandortAttribute() {
        return $this->station_type == self::$TYPE_STANDORT;
    }
    public function GetIsKatsAttribute() {
        return $this->station_type == self::$TYPE_KATS;
    }

    public function getAlertsSortedAttribute(){
        return $this->alerts->sortByDesc('created_at');
    }

    public function getAllUsersAttribute()
    {
        return $this->users->concat($this->linked_users);
    }

    public function getAllUserIds()
    {
        return array_merge($this->users()->pluck('id')->toArray(), $this->linked_users->pluck('id')->toArray());
    }

    public function scopeOnlySuperOrgs($query, $station) {
        switch ($station->station_type) {
            case self::$TYPE_ORTSWEHR:
                $allowedSuperOrgType = self::$TYPE_GEMEINDEWEHR;
                break;
            case self::$TYPE_GEMEINDEWEHR:
                $allowedSuperOrgType = self::$TYPE_AMT;
                break;
            case self::$TYPE_AMT:
                $allowedSuperOrgType = self::$TYPE_KREIS;
                break;
            case self::$TYPE_KREIS:
            case self::$TYPE_KATS:
                $allowedSuperOrgType = self::$TYPE_LAND;
                break;
            case self::$TYPE_LAND:
                $allowedSuperOrgType = "n/a"; // will provide no results since that string is not a valid station type
                break;
            case self::$TYPE_STANDORT:
            default:
                $allowedSuperOrgType = null;
                break;
        }
        if ($allowedSuperOrgType !== null) {
            return $query->where('station_type', $allowedSuperOrgType);
        }
        return $query;
    }

    public function resetRegistrationPin() {
        $allPins = Station::pluck('registration_pin')->toArray();
        $newPin = Helper::generateUniqueNumber(6, $allPins);
        $this->registration_pin = $newPin;
        $this->save();
    }

    public function getAdminUsersAttribute() {
        $adminGroups = $this->groups()->where('is_role_station', true)->get();

        $adminGroupUsers = collect();
        foreach ($adminGroups as $group) {
            $adminGroupUsers = $adminGroupUsers->merge($group->users);
        }

        return $adminGroupUsers;
    }


}
