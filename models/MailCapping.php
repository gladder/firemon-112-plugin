<?php namespace pm\Firemon112\Models;

use Model;
use Carbon\Carbon;

/**
 * Model
 */
class MailCapping extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_mail_capping';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = [
        'email_address',
        'email_type',
        'email_reason',
        'last_sendout'
    ];

    public static $MAILTYPE_MONITOR_OFFLINE = "monitor-offline";
    public static $MAILTYPE_MONITOR_RESOLUTION = "monitor-resolution";

    private function getHourlyFrequence() {
        $freq = 1;
        switch ($this->email_type) {
            case self::$MAILTYPE_MONITOR_OFFLINE:
                $freq = 8;
                break;
            case self::$MAILTYPE_MONITOR_RESOLUTION:
                $freq = 12;
                break;
            default:
                break;
        }
        return $freq;
    }

    public static function ShouldSendMail($email_address, $email_type, $email_reason) {
        $cap = MailCapping::firstOrNew([
            'email_address' => $email_address,
            'email_type' => $email_type,
            'email_reason' => $email_reason
        ]);
        if ($cap->last_sendout === null) {
            return true;
        } else {
            $now = Carbon::now('Europe/Berlin');
            return $now->diffInHours($cap->last_sendout, true) > $cap->getHourlyFrequence();
        }
    }

    public static function DidSendMail($email_address, $email_type, $email_reason) {
        $cap = MailCapping::firstOrNew([
            'email_address' => $email_address,
            'email_type' => $email_type,
            'email_reason' => $email_reason
        ]);
        $cap->last_sendout = Carbon::now('Europe/Berlin');
        $cap->save();
    }
}
