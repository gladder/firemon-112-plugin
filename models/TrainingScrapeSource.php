<?php namespace pm\Firemon112\Models;

use Model;

/**
 * Model
 */
class TrainingScrapeSource extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public static $algo_FOX112 = "FOX112";
    public static $algo_LFSSH = "LFSSH";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pm_firemon112_training_scrape_source';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'label'                  => 'required|between:1,64',
        'url'                    => 'required|between:1,512',
        'scrape_algo'            => 'required'
    ];

    public $belongsTo = [
        'training_source' =>  'pm\Firemon112\Models\TrainingSource'
    ];

    public static function getScrapeAlgoOptions() {
        return [
            self::$algo_FOX112 => 'Fox 112 Lehrgangsseite',
            self::$algo_LFSSH => 'Freie Plätze Seite LFS-SH'
        ];
    }

    public function scopeActive($query) {
        return $query->where('active', 1);
    }
}
