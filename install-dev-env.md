1. Pull firemon112 plugin and theme and keep both of them in a seperate folder (needed for injecting sources into docker container)
2. Build Container by running docker-compose.yml
3. exec shell into webserver (/bin/bash)
4. change to webroot `cd /var/www/html`
5. delete *composer.lock* `rm composer.lock`since it's from the repos and blocks us from updating
6. execute `COMPOSER_MEMORY_LIMIT=-1 composer install --no-dev`
7. execute `php artisan october:up`
8. execute `php artisan package:discover`
9. if your database was not seeded automatically execute `php artisan db:seed --class="\Backend\Database\Seeds\SeedSetupAdmin"` and log into backend http(s)://localhost/backend by using username admin and password admin
10. execute `php artisan jwt:secret` to get JWT up and running
11. enable firemon112 theme under http(s)://localhost/backend/cms/themes
12. setup http(s)://localhost/backend/system/settings/update/pm/firemon112/settings
  > * Message Queue Credentials (see docker-compose: host = localhost, user = rabbit, pass = rabbit, sys port 5672, api port 15672, api host rabbitmq-firemon)
  > * Google Maps API Key
  > * Accu Weather Key
  > * AWS Polly Pool Id
  > * Firemon Admin E-Mail
  > * SMS77.io Sender Key and Number
  > * Expo Access Token for Push Notifications
  > * CarNET Token for Cat AI usage
13. Add at least one Testleistelle http(s)://localhost/backend/pm/firemon112/leitstellen/create
14. Create new Station and first Admin User fot that Station by using http://localhost/onboarding (Mails will not be sent but you can find them in System Logs - to get Activate URL, initial Password etc)
15. additional information that is needed might be added here in future

Attention
==
To get JWT Running we need a workaround that Authentication Header will reach Laravel Parts. Add this to .htaccess

RewriteEngine On
RewriteCond %{HTTP:Authorization} .
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

Also to get resized images working in frontend you need to allow (whitelist) resize folder in .htaccess

RewriteCond %{REQUEST_FILENAME} !/storage/app/resized/.*
