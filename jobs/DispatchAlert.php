<?php namespace pm\Firemon112\Jobs;

use pm\Firemon112\Models\AlertDispatchJob;
use Illuminate\Contracts\Queue\Job;
use Illuminate\Console\Command;
use Validator;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;
use Carbon\Carbon;

class DispatchAlert {

    public function fire($job, $data)
    {
        $id = intval($data['alert_dispatch_job_id']);
        $d = AlertDispatchJob::find(intval($data['alert_dispatch_job_id']));
        if ($d !== null) {
            $d->handle();
            \Log::info('Successfully dispatched AlertDispatchJob ' . $id);
            \Log::info(print_r($d->result, true));
        } else {
            \Log::alert('Failed dispatching AlertDispatchJob ' . $id);
        }
        $job->delete();
    }

    public function failed($data)
    {
        // Called when the job is failing...
        \Log::alert('Failed dispatching AlertDispatchJob ' . intval($data['alert_dispatch_job_id']));
    }


}