<?php namespace pm\Firemon112\Jobs;

use pm\Firemon112\Models\Alert;
use Illuminate\Contracts\Queue\Job;
use Illuminate\Console\Command;
use Validator;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;
use Carbon\Carbon;
use Artisan;

class  QueryWeatherForAlert {

    public function fire($job, $data)
    {
        $alert_id = intval($data['alert_id']);
        $alert = Alert::find($alert_id);
        if ($alert !== null) {
            $exitCode = Artisan::call('firemon:query:weather', [
                "--alertid" => $alert_id
            ]);
            if ($exitCode != 0) {
                \Log::alert('Failed dispatching QueryWeather COMMAND for alert ' . $alert_id);
                //throw new \Exception(Artisan::output());
            } else {
                \Log::info('Successfully dispatched QueryWeather for alert ' . $alert_id);
            }
        } else {
            \Log::alert('Failed dispatching QueryWeather for alert ' . $alert_id);
        }
        $job->delete();
    }

    public function failed($data)
    {
        // Called when the job is failing...
        \Log::alert('Failed dispatching QueryWeather for alert ' . intval($data['alert_id']));
    }


}