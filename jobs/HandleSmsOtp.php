<?php namespace pm\Firemon112\Jobs;

use pm\Firemon112\Models\OneTimePassword;
use pm\Firemon112\Models\Settings;
use Sms77\Api\Client;

class HandleSmsOtp {

    public function fire($job, $data)
    {
        $id = intval($data['otp_id']);
        $otp_readable = intval($data['otp_readable']);
        $d = OneTimePassword::find($id);
        if ($d !== null) {
            if ($d->getIsExpiredAttribute()) {
                \Log::info('Not handled SMS OTP (Expired) ' . $id);
            }
            if ($d->getIsUsedAttribute()) {
                \Log::info('Not handled SMS OTP (USED) ' . $id);
            }
            if (!$d->getIsExpiredAttribute() && !$d->getIsUsedAttribute()) {
                $key = Settings::get('sms77_io_key'); // improvement coult be to add sms token from station
                $client = new Client($key);
                $extra = [
                    'debug' => false,
                    'label' => 'otp',
                    'from' => Settings::get('sms77_io_sender')
                ];

                $client->sms($d->identifier, "Firemon 112 Einmalpasswort: " . $otp_readable . " - es ist 15 Minuten gültig.", $extra);
                \Log::info('Handled SMS OTP' . $id);
            }
        } else {
            \Log::alert('Failed dispatching AlertDispatchJob ' . $id);
        }
        $job->delete();
    }

    public function failed($data)
    {
        // Called when the job is failing...
        \Log::alert('Failed handling SMS OTP ' . intval($data['otp_id']));
    }


}