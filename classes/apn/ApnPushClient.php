<?php namespace pm\Firemon112\Classes\Apn;

use Pushok\AuthProvider;
use Pushok\Client;
use Pushok\Notification;
use Pushok\Payload\Alert;
use Pushok\Payload\Sound;

use pm\Firemon112\Models\Settings;


class ApnPushClient
{
    private $authProvider;
    private $production = true;

    public function __construct() {
        $this->authProvider = AuthProvider\Token::create([
            'key_id' => Settings::get('apn_key_id'),
            'team_id' => Settings::get('apn_team_id'),
            'app_bundle_id' => Settings::get('apn_app_bundle_id'),
            'private_key_content' => Settings::get('apn_p12_key'),
        ]);
    }

    public function sendNotifications($deviceTokens, $title, $body, $topic, $data = [], $critical = false) {
        $alert = Alert::create()->setTitle($title)->setBody($body);
        $payload = FiremonPayload::create()->setAlert($alert);
        $payload->setCategory($topic);

        if ($critical) {
            $sound = Sound::create();
            $sound->setCritical(1);
            $sound->setName('sirene.wav');
            $sound->setVolume(0.9);
            $payload->setSound($sound);
        }

        $payload->setCustomValue('data', $data);
        $notifications = [];
        foreach ($deviceTokens as $deviceToken) {
            $notification = new Notification($payload,$deviceToken);
            if ($critical) {
                $notification->setHighPriority();
            }
            $notifications[] = $notification;
        }

        $client = new Client($this->authProvider, $this->production);
        $client->addNotifications($notifications);
        $responses = $client->push();
        $result = [];
        foreach ($responses as $response) {
            if ($response->getStatusCode() !== 200) {
                $result[] = [
                    'statusCode' => $response->getStatusCode(),
                    'result' => $response->getReasonPhrase(),
                    'token' => $response->getDeviceToken()
                ];
            } else {
                $result[] = [
                    'statusCode' => $response->getStatusCode(),
                    'result' => "OK"
                ];
            }
        }
        return $result;
    }
}