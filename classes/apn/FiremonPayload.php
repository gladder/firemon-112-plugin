<?php namespace pm\Firemon112\Classes\Apn;

use Pushok\Payload;

class FiremonPayload extends Payload {

    /**
     * Payload custom values.
     *
     * @var array
     */
    private $data = ['screen' => 'Alert'];
    protected  function __construct()
    {
    }

    /**
     * Convert Payload to JSON.
     *
     * @return string
     */
    public function toJson(): string
    {
        $str = json_encode($this, JSON_UNESCAPED_UNICODE);
        throw new \Exception($str);
        $this->checkPayloadSize($str);

        return $str;
    }

}

