<?php namespace pm\Firemon112\Classes\Expo;


class ExpoErrorManager
{
    /**
     * Parses an error response from expo
     * @param $response
     * @return \Exception
     */
    public function parseErrorResponse($response)
    {
        $textBody = (string) $response->getBody();
        $result = null;

        $result = json_decode($textBody, true);
        if (is_null($result) || json_last_error() !== JSON_ERROR_NONE) {
            return $this->getTextResponseError($textBody, $response->getStatusCode());
        }

        if (! $this->responseHasErrors($result)) {
            return $this->getTextResponseError($textBody, $response->getStatusCode());
        }

        return $this->getErrorFromResult(
            $result,
            $response->getStatusCode()
        );
    }

    /**
     * Constructs an exception from the response text
     * @param string $errorText
     * @param int $statusCode
     * @return \Exception
     */
    public function getTextResponseError(string $errorText, int $statusCode)
    {
        return new \Exception(sprintf(
            "Expo responded with an error with status code: %s: %s",
            $statusCode,
            $errorText
        ), $statusCode);
    }

    /**
     * Returns an exception for the first API error from the expo response
     * @param array $response
     * @param int $statusCode
     * @return \Exception
     */
    public function getErrorFromResult(array $response, int $statusCode)
    {
        if (! $this->responseHasErrors($response)) {
            return new \Exception(
                'Expected at least one error from Expo. Found none',
                $statusCode
            );
        }

        $error = $response['errors'][0];
        $message = $error['message'];
        $code = $error['code'];

        if (is_string($code)) {
            $message = "{$code}: {$message}";
            $code = $statusCode;
        }

        return new \Exception($message, $code);
    }

    /**
     * Determine if the json decoded response has errors present
     * @param array $response
     * @return bool
     */
    public function responseHasErrors(array $response)
    {
        return array_key_exists('errors', $response) &&
            is_array($response['errors']) &&
            count($response['errors']) > 0;
    }
}