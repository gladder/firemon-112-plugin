<?php namespace pm\Firemon112\Classes\Expo;

class ExpoUtils
{
    /**
     * Check if a value is a valid Expo push token
     *
     * @param mixed $value
     * @return bool
     */
    public static function isExpoPushToken($value)
    {
        if (! is_string($value) || strlen($value) < 15) {
            return false;
        }

        return (self::strStartsWith($value, 'ExponentPushToken[') ||
                self::strStartsWith($value, 'ExpoPushToken[')) &&
            self::strEndsWith($value, ']');
    }

    /**
     * Determine if an array is an asociative array
     *
     * The check determines if the array has sequential numeric
     * keys. If it does not, it is considered an associative array.
     * @param array $arr
     * @return bool
     */
    public static function isAssoc(array $arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * Check if a string starts with another
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function strStartsWith(string $haystack, string $needle)
    {
        return (string)$needle !== '' &&
            strncmp($haystack, $needle, strlen($needle)) === 0;
    }

    /**
     * Check if a string ends with another
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function strEndsWith(string $haystack, string $needle)
    {
        return $needle !== '' &&
            substr($haystack, -strlen($needle)) === (string) $needle;
    }

    /**
     * Wrap data in array if data is not an array
     *
     * @param mixed $data
     *
     * @return array
     */
    public static function arrayWrap($data)
    {
        return is_array($data) ? $data : [$data];
    }

    /**
     * Validates and filters tokens for later use
     *
     * @throws \Exception
     *
     * @param string[]|string $tokens
     *
     * @return string[]
     */
    public static function validateTokens($tokens): array
    {
        if (! is_array($tokens) && ! is_string($tokens)) {
            throw new \Exception(sprintf(
                'Tokens must be a string or non empty array, %s given.',
                gettype($tokens)
            ));
        }

        $tokens = array_filter(self::arrayWrap($tokens), function ($token) {
            return self::isExpoPushToken($token);
        });

        if (count($tokens) === 0) {
            throw new \Exception('No valid expo tokens provided.');
        }

        return $tokens;
    }
}