<?php namespace pm\Firemon112\Classes\Expo;
use GuzzleHttp\Client;

class ExpoClient {
    public const EXPO_BASE_URL = 'https://exp.host/--/api/v2';

    /**
     * The Expo access token
     *
     * @var string
     */
    private $accessToken = null;

    /** @var Client */
    private $client;

    /** @var ExpoErrorManager */
    private $errors;

    public function __construct(array $options = [])
    {
        $this->client = new Client($options);

        $this->errors = new ExpoErrorManager();
    }

    /**
     * Sends push notification messages to the Expo api
     * @param array $messages
     * @return ExpoResponse
     */
    public function sendPushNotifications(array $messages)
    {
        $actualMessageCount = $this->getActualMessageCount($messages);
        [$compressed, $body] = $this->compressBody($messages);
        $headers = $this->getDefaultHeaders();

        if ($compressed) {
            $headers['Content-Encoding'] = 'gzip';
        }

        $response = $this->client->post(self::EXPO_BASE_URL . '/push/send', [
            'http_errors' => false,
            'headers' => $headers,
            'body' => $body,
        ]);

        $statusCode = $response->getStatusCode();
        $textBody = (string) $response->getBody();

        if ($statusCode !== 200) {
            throw $this->errors->parseErrorResponse($response);
        }

        $result = json_decode($textBody, true);

        if (is_null($result) || json_last_error() !== JSON_ERROR_NONE) {
            throw $this->errors->getTextResponseError($textBody, $statusCode);
        }

        if (array_key_exists('errors', $result)) {
            throw $this->errors->getErrorFromResult($result, $statusCode);
        }

        if (! is_array($result['data']) || count($result['data']) !== $actualMessageCount) {
            throw new \Exception(sprintf(
                'Expected Expo to respond with %s %s but received %s',
                $actualMessageCount,
                $actualMessageCount === 1 ? 'ticket' : 'tickets',
                count($result['data'])
            ));
        }

        return new ExpoResponse($response);
    }

    /**
     * Retrieves push notification receipts from the Expo api
     *
     * @param array $ticketIds
     * @return ExpoResponse
     */
    public function getPushNotificationReceipts(array $ticketIds): ExpoResponse
    {
        [$compressed, $body] = $this->compressBody([
            'ids' => $ticketIds,
        ]);

        $headers = $this->getDefaultHeaders();

        if ($compressed) {
            $headers['Content-Encoding'] = 'gzip';
        }

        $response = $this->client->post(self::EXPO_BASE_URL . '/push/getReceipts', [
            'http_errors' => false,
            'headers' => $headers,
            'body' => $body,
        ]);

        if ($response->getStatusCode() !== 200) {
            throw new \Exception(sprintf(
                'Request failed with status code %s',
                $response->getStatusCode()
            ));
        }

        $result = json_decode($response->getBody(), true);

        if (! array_key_exists('data', $result) || ! is_array($result['data'])) {
            throw new \Exception(
                'Expected Expo to respond with a map from receipt IDs to receipts but received data of another type'
            );
        }

        return new ExpoResponse($response);
    }

    /**
     * Set the Expo access token
     * @param string $accessToken
     * @return void
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Get the clients request headers
     * @return array
     */
    private function getDefaultHeaders()
    {
        $headers = [
            'Host' => 'exp.host',
            'Accept' => 'application/json',
            'Accept-Encoding' => 'gzip, deflate',
            'Content-Type' => 'application/json',
        ];

        if ($this->accessToken) {
            $headers['Authorization'] = "Bearer {$this->accessToken}";
        }

        return $headers;
    }

    /**
     * Compresses a string if > 1kib in size
     * @param array $value
     * @return array
     */
    private function compressBody(array $value)
    {
        $value = json_encode($value);
        $compressed = false;

        if ((strlen($value) / 1024) > 1) {
            $value = gzencode($value, 6) ?? $value;
            $compressed = true;
        }

        return [$compressed, $value];
    }

    /**
     * Gets the actual message count
     * @param array $messages
     * @return int
     */
    private function getActualMessageCount(array $messages)
    {
        $count = 0;

        foreach ($messages as $message) {
            $recipients = ExpoUtils::arrayWrap($message['to']);
            $count += count($recipients);
        }

        return $count;
    }
}