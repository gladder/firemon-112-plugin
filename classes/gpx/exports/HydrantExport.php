<?php namespace pm\Firemon112\Classes\Gpx\Exports;

use pm\Firemon112\Models\Station;
use phpGPX\Models\GpxFile;
use phpGPX\Models\Link;
use phpGPX\Models\Metadata;
use phpGPX\Models\Point;

class HydrantExport
{
    public static $FiremonIdPrependStr = 'Firemon112::Hydrant#';
    public static $FiremonLeitungsartPrependStr = 'Firemon112::Hydrant::Leitungsart#';
    public static $FiremonNenndurchmesserPrependStr = 'Firemon112::Hydrant::Nenndurchmesser#';
    public static $FiremonHydrantTypePrependStr = 'Firemon112::Hydrant::Type#';

    public static function download($station)
    {
        $link 							= new Link();
        $link->href 					= "https://www.firemon112.de";
        $link->text 					= 'Firemon 112';
        $gpx_file 						= new GpxFile();
        $gpx_file->metadata 			= new Metadata();
        $gpx_file->metadata->time 		= new \DateTime();
        $gpx_file->metadata->description = "Firemon 112 Hydranten-Export für ".$station->name;
        $gpx_file->metadata->links[] 	= $link;
        foreach ($station->hydrants as $hydrant) {
            // Creating trackpoint
            $point 						= new Point(Point::WAYPOINT);
            $point->latitude 			= $hydrant->lat;
            $point->longitude 			= $hydrant->long;
            $point->elevation 			= 0;
            $point->time 				= new \DateTime();
            $point->name 				= $hydrant->name;
            $point->source 				= self::$FiremonIdPrependStr.$hydrant->id;
            $point->symbol 				= self::$FiremonHydrantTypePrependStr.$hydrant->hydranttype_id;
            $point->type 				= self::$FiremonLeitungsartPrependStr.$hydrant->waterpipe;
            $point->description 		= self::$FiremonNenndurchmesserPrependStr.$hydrant->flowrate;
            $gpx_file->waypoints[] 			= $point;
        }
        return $gpx_file->toXML()->saveXML();
    }
}
