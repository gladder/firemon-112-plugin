<?php namespace pm\Firemon112\Classes\Excel\Exports;

use pm\Firemon112\Models\Hydranttype;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class HydrantExportTypeLegend implements FromCollection, WithHeadings, WithMapping, WithTitle, ShouldAutoSize, WithStyles
{
    public function headings(): array
    {
        return [
            'Hydrant Typ ID',
            'Bezeichnung'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Hydrant Typ IDs';
    }

    public function map($hydrant_type): array
    {
        return [
            $hydrant_type->id,
            $hydrant_type->name,
        ];
    }

    public function collection()
    {
        return Hydranttype::all();
    }
}
