<?php namespace pm\Firemon112\Classes\Excel\Exports;

use pm\Firemon112\Models\Station;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class HydrantExport implements WithMultipleSheets
{
    protected $station;

    public function __construct($station)
    {
        $this->station = $station;
    }

    public function sheets(): array
    {
        return [
            new HydrantExportDataSheet($this->station),
            new HydrantExportTypeLegend($this->station),
            new HydrantExportPipeLegend(),
        ];

    }
}
