<?php namespace pm\Firemon112\Classes\Excel\Exports;

use pm\Firemon112\Models\Hydrant;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class HydrantExportPipeLegend implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize, WithStyles
{
    public function headings(): array
    {
        return [
            'Leitungsart Key',
            'Bezeichnung'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return 'Leitungsart Keys';
    }

    public function collection()
    {
        $coll = [];
        foreach (Hydrant::getWaterpipeOptions() as $k => $v) {
            $coll[] = [
                'key' => $k,
                'value' => $v
            ];
        }

        return collect($coll);
    }
}
