<?php namespace pm\Firemon112\Classes\Excel\Exports;

use pm\Firemon112\Models\Station;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class HydrantExportDataSheet implements FromCollection, WithHeadings, WithMapping, WithTitle, ShouldAutoSize, WithStyles
{
    public static $SheetTitle = 'Hydrant Export-Daten';

    public function headings(): array
    {
        return [
            'Firemon ID',
            'Hydrant Typ ID',
            'Bezeichnung / Nummer',
            'Leitungsart Key',
            'Nenndurchmesser',
            'Long',
            'Lat'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function title(): string
    {
        return self::$SheetTitle;
    }

    public function __construct($station)
    {
        $this->station = $station;
    }

    public function map($hydrant): array
    {
        return [
            $hydrant->id,
            $hydrant->hydranttype_id,
            $hydrant->name,
            $hydrant->waterpipe,
            $hydrant->flowrate,
            $hydrant->long,
            $hydrant->lat
        ];
    }

    public function collection()
    {
        return $this->station->hydrants;
    }
}
