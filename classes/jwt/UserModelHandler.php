<?php namespace pm\Firemon112\Classes\JWT;

use Illuminate\Contracts\Auth\Authenticatable;
use pm\Firemon112\Classes\JWT\UserPluginResolverContract;
use pm\Firemon112\Classes\JWT\UserSubjectBehavior;

class UserModelHandler
{
    /**
     * Add listeners
     * @param \October\Rain\Events\Dispatcher $event
     */
    public function subscribe($event)
    {
        $model = app(UserPluginResolverContract::class)->getModel();
        $model::extend(static function (Authenticatable $userModel) {
            $userModel->implement[] = UserSubjectBehavior::class;
        });
    }
}