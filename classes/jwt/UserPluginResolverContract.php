<?php
declare(strict_types=1);

namespace pm\Firemon112\Classes\JWT;

use October\Rain\Auth\Manager as AuthManager;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Model;

/**
 *
 */
interface UserPluginResolverContract
{
    /**
     * @return string
     */
    public function getModel(): string;

    public function getResolver(): PluginContract;

    /**
     * @param Model $model
     * @return JWTSubject
     */
    public function resolveModel(Model $model): JWTSubject;

    /**
     * @return AuthManager
     */
    public function getProvider(): AuthManager;

    /**
     * @return array
     */
    public function getSupportPlugins(): array;
}