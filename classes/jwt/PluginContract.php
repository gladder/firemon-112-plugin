<?php
declare(strict_types=1);

namespace pm\Firemon112\Classes\JWT;

use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Model;

/**
 *
 */
interface PluginContract
{
    /**
     * @param Model $model
     * @return JWTSubject
     */
    public function resolve(Model $model): JWTSubject;

    public function initActivation($model): string;

    public function activateByCode($code);
}