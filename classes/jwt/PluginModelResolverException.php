<?php
declare(strict_types=1);

namespace pm\Firemon112\Classes\JWT;

use October\Rain\Exception\SystemException;

/**
 *
 */
class PluginModelResolverException extends SystemException
{
    /**
     * @var string
     */
    protected $message = 'Model not support in resolver';
}