<?php namespace pm\Firemon112\Classes\JWT;

use Illuminate\Contracts\Auth\Authenticatable;
use October\Rain\Auth\Models\User;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use PHPOpenSourceSaver\JWTAuth\JWTGuard as JWTGuardBase;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenBlacklistedException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\UserNotDefinedException;

/**
 * Class JWTGuard
 * @package pm\Firemon112\Classes\JWT
 */
class JWTGuard extends JWTGuardBase
{
    /**
     * @param  Authenticatable  $user
     * @return string
     */
    public function login($user): string
    {

        $this->validateMethodParam($user);

        $userPluginResolver = app(UserPluginResolverContract::class);
        $token = $this->jwt->claims([
            'pwh'=>$user->jwt_pwh
        ])->fromSubject($userPluginResolver->resolveModel($user));

        $this->setToken($token)->setUser($user);

        return $token;
    }

    public function user() {
        $user = parent::user();
        if ($user !== null) {
            if ($this->getPayload()->hasKey('pwh')) {
                if ($this->getPayload()->get('pwh') == $user->jwt_pwh) {
                    return $user;
                }
            } else {
                return $user;
            }
        }
        return null;
    }

    /**
     * @return bool
     */
    public function hasToken(): bool
    {
        return $this->jwt->parser()->setRequest($this->getRequest())->hasToken();
    }

    /**
     * @param User $user
     */
    private function validateMethodParam(Authenticatable $user): void
    {
        if (!$user->isClassExtendedWith(UserSubjectBehavior::class)) {
            throw new \InvalidArgumentException(
                sprintf('user param must extend %s', UserSubjectBehavior::class)
            );
        }
    }

}