<?php
declare(strict_types=1);
namespace pm\Firemon112\Classes\JWT;

use October\Rain\Database\ModelBehavior;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

/**
 * Class UserSubjectBehavior
 * @package pm\Firemon112\Classes\JWT
 */
class UserSubjectBehavior extends ModelBehavior implements JWTSubject
{
    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->model->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}