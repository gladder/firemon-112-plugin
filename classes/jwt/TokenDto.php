<?php
declare(strict_types=1);

namespace pm\Firemon112\Classes\JWT;

use Carbon\Carbon;
use October\Rain\Auth\Models\User;
use pm\Firemon112\Models\Helper;

/**
 *
 */
class TokenDto
{
    /**
     * @var string
     */
    public $token;

    /**
     * @var Carbon
     */
    public $expires;

    /**
     * @var User
     */
    public $user;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        foreach($data as $name => $value) {
            $this->{$name} = $value;
        }
    }

    public function toArray(): array
    {
        $user = $this->user->toArray();
        $user['mission_admin'] = Helper::userHasMissionAdmin($this->user); // Keep this for legacy (mission-perms now part of alert)
        $user['mission_operator'] = Helper::userHasMissionOperator($this->user); // Keep this for legacy (mission-perms now part of alert)
        $station = $this->user->station->toArray();
        $station['user_roles'] = [
            Helper::$PERMISSION_IS_ROLE_ALERT => Helper::userHasPermission($this->user, $this->user->station_id, Helper::$PERMISSION_IS_ROLE_ALERT),
            Helper::$PERMISSION_IS_ROLE_EVENTS => Helper::userHasPermission($this->user, $this->user->station_id, Helper::$PERMISSION_IS_ROLE_EVENTS),
            Helper::$PERMISSION_IS_ROLE_STATION => Helper::userHasPermission($this->user, $this->user->station_id, Helper::$PERMISSION_IS_ROLE_STATION),
            Helper::$PERMISSION_IS_ROLE_PRESENCE => Helper::userHasPermission($this->user, $this->user->station_id, Helper::$PERMISSION_IS_ROLE_PRESENCE),
            Helper::$PERMISSION_IS_MISSION_OPERATOR => Helper::userHasMissionOperator($this->user, $this->user->station_id),
            Helper::$PERMISSION_IS_ROLE_ALERT_READONLY => Helper::userHasPermission($this->user, $this->user->station_id, Helper::$PERMISSION_IS_ROLE_ALERT_READONLY),
        ];
        $linked_stations = [];
        foreach ($this->user->linked_stations as $linked_station) {
            $s = $linked_station->makeHidden('pivot');
            $s['user_roles'] = [
                Helper::$PERMISSION_IS_ROLE_ALERT => Helper::userHasPermission($this->user, $linked_station->id, Helper::$PERMISSION_IS_ROLE_ALERT),
                Helper::$PERMISSION_IS_ROLE_EVENTS => Helper::userHasPermission($this->user, $linked_station->id, Helper::$PERMISSION_IS_ROLE_EVENTS),
                Helper::$PERMISSION_IS_ROLE_STATION => Helper::userHasPermission($this->user, $linked_station->id, Helper::$PERMISSION_IS_ROLE_STATION),
                Helper::$PERMISSION_IS_ROLE_PRESENCE => Helper::userHasPermission($this->user, $linked_station->id, Helper::$PERMISSION_IS_ROLE_PRESENCE),
                Helper::$PERMISSION_IS_MISSION_OPERATOR => Helper::userHasMissionOperator($this->user, $this->user->station_id),
                Helper::$PERMISSION_IS_ROLE_ALERT_READONLY => Helper::userHasPermission($this->user, $linked_station->id, Helper::$PERMISSION_IS_ROLE_ALERT_READONLY),
            ];
            $linked_stations[] = $s;
        }
        return [
            'token' => $this->token,
            'expires' => $this->expires,
            'user' => $user,
            'station' => $station,
            'linked_stations' => $linked_stations
        ];
    }
}