<?php namespace pm\Firemon112\Classes;


use pm\Firemon112\Classes\Expo\ExpoClient;
use pm\Firemon112\Classes\Expo\ExpoMessage;
use pm\Firemon112\Classes\Expo\ExpoUtils;
use pm\Firemon112\Classes\Expo\ExpoResponse;

class ExpoPush
{
    /**
     * @var ExpoClient
     */
    private $client;

    /**
     * Messages to send
     *
     * @var ExpoMessage[]
     */
    private $messages = [];

    /**
     * Default tokens to send the message to (if they don't have their own respective recipients)
     *
     * @var array
     */
    private $recipients = null;

    public function __construct(array $clientOptions = [])
    {
        $this->client = new ExpoClient($clientOptions);
    }

    /**
     * Check if a value is a valid Expo push token
     *
     * @param mixed $value
     */
    public function isExpoPushToken($value): bool
    {
        return ExpoUtils::isExpoPushToken($value);
    }

    /**
     * Get default recipients
     *
     * @return array|null
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Get messages
     *
     * @return array|null
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Sets the messages to send
     *
     * @param ExpoMessage[]|ExpoMessage|array $message
     */
    public function send($message): self
    {
        $messages = ExpoUtils::arrayWrap($message);

        if (ExpoUtils::isAssoc($messages)) {
            throw new \Exception(
                'You can only send an ExpoMessage instance or an array of messages'
            );
        }

        foreach ($messages as $index => $message) {
            if (! $message instanceof ExpoMessage) {
                $messages[$index] = new ExpoMessage($message);
            }
        }

        $this->messages = $messages;

        return $this;
    }

    /**
     * Sets the default recipients
     *
     * @param string|array $recipients
     * @throws InvalidTokensException
     * @throws \Exception
     */
    public function to($recipients = null): self
    {
        $this->recipients = ExpoUtils::validateTokens($recipients);

        return $this;
    }

    /**
     * Send the messages to the expo server
     *
     * @throws \Exception
     */
    public function push(): ExpoResponse
    {
        if (empty($this->messages)) {
            throw new \Exception('You must have at least one message to push');
        }

        $messages = [];

        /**
         * When a response ticket has DeviceNotRegistered it has no indication which push token produced this error.
         * However it is known the order of messages and response tickets are the same.
         * So the only way to keep track of invalid tokens is by their indices.
         * For this to work we need to flatten messages' recipients.
         */
        foreach ($this->messages as $message) {
            $message = $message->toArray();
            $tokens = $message['to'] ?? $this->recipients;

            if (empty($tokens)) {
                throw new \Exception('A message must have at least one recipient to send');
            }

            foreach (ExpoUtils::arrayWrap($tokens) as $token) {
                $messages[] = array_merge($message, ['to' => $token]);
            }
        }

        $this->reset();

        $response = $this->client->sendPushNotifications($messages);

        $notRegisteredTokens = [];

        foreach ($response->getData() as $index => $ticket) {
            if (($ticket['details']['error'] ?? '') === 'DeviceNotRegistered') {
                $notRegisteredTokens[] = $messages[$index]['to'];
            }
        }

        if (!empty($notRegisteredTokens) && is_array($notRegisteredTokens)) {
            foreach ($notRegisteredTokens as $token) {
                \Log::alert("Expo Token " . $token . " is not registered (anymore) and needs to be deleted!");
            }
        }

        return $response;
    }

    /**
     * Fetches the push notification receipts from the expo server
     *
     * @param array $ticketIds
     * @return ExpoResponse
     */
    public function getReceipts(array $ticketIds): ExpoResponse
    {
        $ticketIds = array_filter($ticketIds, function ($id) {
            return is_string($id);
        });

        return $this->client->getPushNotificationReceipts($ticketIds);
    }

    /**
     * Set the Expo access token
     * @param string $accessToken
     * @return ExpoPush
     */
    public function setAccessToken(string $accessToken): self
    {
        $this->client->setAccessToken($accessToken);

        return $this;
    }

    /**
     * Resets the instance data
     */
    public function reset(): void
    {
        $this->messages = [];
        $this->recipients = null;
    }
}