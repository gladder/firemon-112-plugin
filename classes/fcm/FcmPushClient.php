<?php namespace pm\Firemon112\Classes\Fcm;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

use Google\Auth\ApplicationDefaultCredentials;
use Google\Auth\Credentials\ServiceAccountCredentials;
use Google\Auth\Middleware\AuthTokenMiddleware;
use Google\Auth\OAuth2;

use pm\Firemon112\Models\Settings;

class FcmPushClient
{
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://fcm.googleapis.com/v1/projects/' . Settings::get('fcm_project_id') . '/messages:send',
            'headers' => [
                'Authorization' => 'Bearer ' . $this->get_oauth_token(),
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    public function sendNotification($message)
    {
        try {
            $response = $this->client->post('', [
                'body' => json_encode([
                    'message' => $message,
                ]),
            ]);
            return json_decode((string)$response->getBody(), true);
        } catch (\Exception $e) {
            return "Exception " . $e->getMessage() . '\n' . print_r($message,true);
        }
    }

    private function get_oauth_token() {
        $serviceAccountInfo = json_decode(Settings::get('fcm_service_account_json'), true);

        $scopes = ['https://www.googleapis.com/auth/cloud-platform'];
        $creds = ServiceAccountCredentials::makeCredentials($scopes, $serviceAccountInfo);

        // Create a Guzzle middleware with the credentials
        $middleware = new AuthTokenMiddleware($creds);
        $stack = HandlerStack::create();
        $stack->push($middleware);

        // Request a token
        $token = $creds->fetchAuthToken();
        return $token['access_token'];
    }
}
